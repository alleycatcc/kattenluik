#include <stdbool.h>

#ifndef _CONFIG_PROJECT_H
#define _CONFIG_PROJECT_H

#define ADC_MUXPOS ADC_MUXPOS_AIN18_gc // --- PF2
/* The trigger code gets sent as a wave. It begins with 1 and 0. Both of
 * these trigger an interrupt, and once we've seen 0 we begin the read_code
 * sequence. The value for `CODE_LENGTH` does not include the initial 1-0.
 */
#define CODE_LENGTH 4
#define CODE_WINDOW_MS 90
#define DELAY_CHUNK_MS 50
#define SPEAK true
// --- in production we turn off everything below level=warn for extra speed.
// note that level=trace will probably break `read_code` because we're too
// busy printing to read the bits as they arrive.
// --- production
#define SPEAK_LEVEL ACA_SPEAK_WARN
// #define SPEAK_LEVEL ACA_SPEAK_INFO
#define DEBUG_ADC false
// --- power consumption = active ~ 1 mA / standby ~ negligible
#define F_CPU 4000000
#define MANUAL_BUTTON_ENABLED true
#define PIN_BUTTON_POS 2
#define PIN_DURATION_MODE_CONFIG_1_PORT PORTA
#define PIN_DURATION_MODE_CONFIG_1_POS 4
#define PIN_DURATION_MODE_CONFIG_0_PORT PORTA
#define PIN_DURATION_MODE_CONFIG_0_POS 3
// --- the port that both the manual button input and the pi input are on --
// note that these must currently be on the same port because the interrupt
// handler is too difficult to write generically otherwise.
#define PIN_INPUT_PORT PORTA
#define PIN_INPUT_ISR PORTA_PORT_vect
#define PIN_RELAY_PORT PORTA
#define PIN_RELAY_POS 7
#define PIN_PI_POS 1
// --- when mode is PROG
#define RELAY_DURATION_PROG_MS 6000
// --- let transient currents associated with power-up / reboot settle
#define STARTUP_DELAY_MS 500
#define USART_BAUD_RATE 9600
#define USART_USART USART1

#endif
