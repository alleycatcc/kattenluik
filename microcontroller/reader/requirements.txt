appdirs==1.4.4
Cython==0.29.34
hidapi==0.13.1
intelhex==2.3.0
pyedbglib==2.22.0.7
pymcuprog==3.14.2.9
pyserial==3.5
PyYAML==6.0
