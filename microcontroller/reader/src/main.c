#include <stdarg.h>
#include <stdbool.h>

#include <stdio.h>

#include "config-project.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <util/delay.h>

#include <alleycat-avr/bits.h>
#include <alleycat-avr.h>

#define dbg(...) do { \
  aca_debug (app.printer, ##__VA_ARGS__); \
} while (0)

#define inf(...) do { \
  aca_info (app.printer, ##__VA_ARGS__); \
} while (0)

#define err(...) do { \
  aca_error (app.printer, ##__VA_ARGS__); \
} while (0)

#define war(...) do { \
  aca_warn (app.printer, ##__VA_ARGS__); \
} while (0)

#define trc(...) do { \
  aca_trace (app.printer, ##__VA_ARGS__); \
} while (0)

#define tca_per(ms, prescale) ((uint16_t) (ms / 1000.0 / (float) prescale * F_CPU))

volatile bool code_window_open = false;
volatile bool manual;

enum duration_mode {
  DURATION_MODE_PROG,
  DURATION_MODE_INFINITE,
  DURATION_MODE_VAR,
};

struct input_t {
  aca_pinin_t manual;
  aca_pinin_t pi;
  aca_pinin_t duration_mode_config_1;
  aca_pinin_t duration_mode_config_0;
};

struct output_t {
  aca_pinout_t relay;
};

// --- needs to be global, so that the ISR routines can access it;
// other functions should pass it around explicitly and not access the
// global instance.

struct app {
  aca_printer_t printer;
  aca_adc_t adc;
  struct input_t inputs;
  struct output_t outputs;
} app;

static void reset_timer () {
  TCA0.SINGLE.CTRLESET = TCA_SINGLE_CMD_RESTART_gc;
}

static volatile void triggered_pi () {
  code_window_open = true;
  reset_timer ();
}

static volatile void triggered_manual () {
  manual = true;
}

static void pin_intr (aca_pinin_t pin, uint8_t cfg) {
  register8_t *ctrl = aca_pinin_ctl (pin);
  write (ctrl, cfg, PORT_ISC_gm);
}

static void cli_input (aca_pinin_t pin) {
  pin_intr (pin, PORT_ISC_INTDISABLE_gc);
}

static void cli_pi () {
  cli_input (app.inputs.pi);
}

static void cli_manual () {
  cli_input (app.inputs.manual);
}

static void cli_inputs () {
  cli_pi ();
  cli_manual ();
}

static void sei_input (aca_pinin_t pin) {
  // --- although we're only interested in the falling edge, we use
  // bothedges (and check the direction in the ISR handler) so we make sure
  // to properly wake up from sleep mode.
  pin_intr (pin, PORT_ISC_BOTHEDGES_gc);
}

static void sei_pi () {
  sei_input (app.inputs.pi);
}

static void sei_manual () {
  sei_input (app.inputs.manual);
}

static void sei_inputs () {
  sei_pi ();
  sei_manual ();
}

static void timer_overflowed () {
  code_window_open = false;
  sei_pi ();
}

static void init_timer () {
  TCA0.SINGLE.PER = tca_per (CODE_WINDOW_MS, 1024);
  TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV1024_gc;
  TCA0.SINGLE.CTRLA |= TCA_SINGLE_ENABLE_bm;
  TCA0.SINGLE.INTCTRL |= TCA_SINGLE_OVF_bm;
}

static uint16_t adc_read (aca_adc_t adc) {
  return aca_adc_read_single (adc);
}

static bool get_duration_mode (struct input_t *inputs, enum duration_mode *mode) {
  bool cfg1 = !aca_pin_read (inputs->duration_mode_config_1);
  bool cfg0 = !aca_pin_read (inputs->duration_mode_config_0);
  if (cfg1 && cfg0) *mode = DURATION_MODE_INFINITE;
  else if (cfg1) *mode = DURATION_MODE_PROG;
  else if (cfg0) *mode = DURATION_MODE_VAR;
  else {
    err ("get_duration_mode (): invalid jumper configuration");
    return false;
  }
  return true;
}

static uint16_t duration_var (uint16_t adc_val) {
  // --- 1023 = .1s, 775 = .5s, 258 = 1s, 0 = 5s
  // write ax+b=y, determine a and b using x1, y1, x2, y2
  float adcf = (float) adc_val;
  float a;
  uint16_t b;
  if (adc_val > 1023) {
    err ("duration_var (): invalid input: %d", adc_val);
    return 0;
  }
  if (adc_val <= 258) {
    a = (1000.0 - 5000) / 258;
    b = 5000 - 0*a;
  }
  else if (adc_val <= 775) {
    a = (500.0 - 1000) / (775.0 - 258);
    b = 1000 - 258*a;
  }
  else if (adc_val <= 1023) {
    a = (100.0 - 500) / (1023.0 - 775);
    b = 500 - 775*a;
  }
  return b + adcf*a;
}

static bool get_relay_duration (struct app *the_app, uint16_t *duration, bool *infinite) {
  *infinite = false;
  *duration = 0;
  enum duration_mode mode;
  bool ok = get_duration_mode (&the_app->inputs, &mode);
  if (!ok) return false;
  if (mode == DURATION_MODE_PROG) {
    *duration = RELAY_DURATION_PROG_MS;
    return true;
  }
  if (mode == DURATION_MODE_INFINITE) {
    *infinite = true;
    return true;
  }
  if (mode == DURATION_MODE_VAR) {
    uint16_t adc_val = adc_read (the_app->adc);
    uint16_t ms = duration_var (adc_val);
    inf ("duration variable ms: %d", ms);
    *duration = ms;
    return true;
  }

  err ("get_relay_duration (): invalid mode: %d", mode);
  return false;
}

static void do_delay (uint16_t ms) {
  uint16_t num_chunks = (float) ms / DELAY_CHUNK_MS;
  for (uint16_t i = 0; i < num_chunks; i++)
    _delay_ms (DELAY_CHUNK_MS);
}

volatile static void relay_close_g () {
  aca_pin_off (app.outputs.relay);
}

static void relay (aca_pinout_t pin_relay, uint16_t duration_ms, bool infinite) {
  if (infinite) {
    inf ("toggling relay (infinite mode)");
    aca_pin_toggle (pin_relay);
    return;
  }
  inf ("doing relay with duration: %d", duration_ms);
  aca_pin_on (pin_relay);
  do_delay (duration_ms);
  aca_pin_off (pin_relay);
}

static bool cmp (bool eq, bool x, bool y) {
  if (eq) return x == y;
  return x != y;
}

/* The code begins with 1-0 and then alternates. CODE_LENGTH does not
 * include the 1-0. We enter the read_code sequence after we've seen the
 * falling edge, meaning the input is still on 0 when we start this
 * function, so the first value we want to read is 0.
 */

static bool read_code (aca_pinin_t pin_pi) {
  if (!code_window_open) return false;
  // --- we already got the 0
  if (CODE_LENGTH == 0) return true;
  // --- what to expect as the next value (1 or 0).
  uint8_t wait_for_a = 0;
  for (int i = 0; i < CODE_LENGTH; i++) {
    // --- infinite loop to wait for the next bit; if the timer overflows in
    // the meantime then quit.
    while (cmp (!wait_for_a, false, aca_pin_read (pin_pi))) {
      if (!code_window_open) {
        inf ("window closed, code not received in time");
        return false;
      }
    }
    wait_for_a = (uint8_t) !wait_for_a;
  }
  return true;
}

static void init_outputs (struct output_t *outputs) {
  outputs->relay = aca_mk_pinout (&PIN_RELAY_PORT, PIN_RELAY_POS, 0);
}

static void init_inputs (struct input_t *inputs) {
  inputs->pi = aca_mk_pinin (&PIN_INPUT_PORT, PIN_PI_POS, true);
  inputs->manual = aca_mk_pinin (&PIN_INPUT_PORT, PIN_BUTTON_POS, true);
  inputs->duration_mode_config_1 = aca_mk_pinin (&PIN_DURATION_MODE_CONFIG_1_PORT, PIN_DURATION_MODE_CONFIG_1_POS, true);
  inputs->duration_mode_config_0 = aca_mk_pinin (&PIN_DURATION_MODE_CONFIG_0_PORT, PIN_DURATION_MODE_CONFIG_0_POS, true);
  sei_pi ();
  sei_manual ();
}

static aca_adc_t init_adc () {
  return aca_adc_init (ACA_ADC_CONFIG_SINGLE_CONV, ACA_ADC_SELECT_ADC0, ADC_MUXPOS, 0, true, true);
}

static void init_bod () {
  aca_bod_vlm_set (25);
  aca_bod_vlm_sei (true, true);
  char buf[255];
  bool ok = aca_bod_show_config (255, buf);
  if (ok) inf (buf);
}

static void adc_debug (aca_adc_t adc) {
  uint16_t val = adc_read (adc);
  dbg ("ADC val: %d", val);
}

static void init_printer (aca_printer_t *printer) {
  if (!SPEAK) {
    *printer = aca_mk_printer_null ();
    return;
  }
  aca_speak_set_level (SPEAK_LEVEL);
  aca_usart_t usart = aca_usart_init_default (ACA_USART_CONFIG_USART1, USART_BAUD_RATE);
  *printer = aca_mk_printer_usart (usart);
}

int main (void) {
  aca_clock_main_internal_set_freq (F_CPU);
  _delay_ms (STARTUP_DELAY_MS);
  cli ();
  app.adc = init_adc ();
  init_printer (&app.printer);
  init_bod ();
  init_inputs (&app.inputs);
  init_outputs (&app.outputs);
  init_timer ();
  sei ();

  set_sleep_mode (SLEEP_MODE_STANDBY);
  inf ("ready"); _delay_ms (10);

  while (true) {
    if (DEBUG_ADC) adc_debug (app.adc);

    // --- if the relay is open (due to us being in infinite mode), don't
    // sleep, or else we'll miss BOD / VLVL interrupts.
    if (aca_pin_is_on (app.outputs.relay))
      trc ("relay is open, not sleeping");
    else {
      sleep_mode ();
      // --- from docs, do this upon waking up.
      ATOMIC_BLOCK (ATOMIC_FORCEON) {
        _delay_ms (10);
      }
      trc ("woke up due to %s", manual ? "manual button" : "activity on pi wire");
    }
    // --- button was pushed (manual = true): open the relay.
    if (manual) manual = false;
    // --- try to read the code, open the relay if successful.
    else if (read_code (app.inputs.pi))
      inf ("successfully received code of length %d", CODE_LENGTH);
    // --- otherwise, reloop
    else continue;

    cli ();
    uint16_t duration;
    bool infinite;
    bool ok = get_relay_duration (&app, &duration, &infinite);
    if (!ok) inf ("not opening relay");
    else relay (app.outputs.relay, duration, infinite);
    sei_pi ();
    sei ();
  }
  return 1;
}

ISR (PORTA_PORT_vect) {
  trc ("activity on A");
  // --- transition on Pi input (1-0 or 0-1)
  if (is_set (&PIN_INPUT_PORT.INTFLAGS, PIN_PI_POS)) {
    // --- falling edge (input = 0)
    if (aca_pin_is_off (app.inputs.pi)) {
      cli_pi ();
      trc ("interrupt on port A due to pi line falling");
      triggered_pi ();
    }
  }
  // --- button pushed
  if (MANUAL_BUTTON_ENABLED && is_set (&PIN_INPUT_PORT.INTFLAGS, PIN_BUTTON_POS)) {
    if (aca_pin_is_off (app.inputs.manual))
      triggered_manual ();
  }
  PORTA.INTFLAGS = 0xff;
}

ISR (TCA0_OVF_vect) {
  timer_overflowed ();
  TCA0.SINGLE.INTFLAGS |= TCA_SINGLE_OVF_bm;
}

ISR (BOD_VLM_vect) {
  // --- respond to VLVL interrupt.
  // - if the voltage drops below the BOD threshold, we don't have control
  // any more -- the device will reset and the pins will go to high-Z, so
  // the relay will probably close.
  // - if the voltage is between BOD and VLVL, try hard to close the relay,
  // and disable inputs -- we assume something is going wrong with the power
  // supply.
  // - if the voltage rises above VLVL again, we can hope the power supply
  // is restoring itself. If it hovers above VLVL if may not be enough to
  // open the relay on open events, but we don't abort the program.
  if (BOD_STATUS == BOD_VLMS_BELOW_gc) {
    relay_close_g ();
    cli_inputs ();
    relay_close_g ();
    war ("voltage level dropped below VLVL");
  }
  else if (BOD_STATUS == BOD_VLMS_ABOVE_gc) {
    inf ("voltage level above VLVL again");
    sei_inputs ();
  }
  // --- datasheet doesn't say we have to do this but it's how most of the
  // other peripherals work.
  BOD.INTFLAGS = 1 << 0;
}

ISR (BADISR_vect) {
  war ("uncaught interrupt");
}
