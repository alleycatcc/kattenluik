#!/usr/bin/env bash

set -eu
set -o pipefail

DEFAULT_SPEED=4

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0"

# --- la movida es:
#
# - clone https://github.com/avrdudes/avr-libc/
# - get gcc-avr from apt (v5.4 is fine)
# - put device-specs/specs-avr32db48 in /usr/lib/gcc/avr/5.4.0/device-specs/
# (source: https://raw.githubusercontent.com/epccs/AVR-Dx_DFP/master/gcc/dev/avr32db48/device-specs/specs-avr32db48)
# - in avr-libc:
#   - edit shebang in devtools/mlib-gen.py, make it python3
#       ./bootstrap
#       # --- may be necessary:
#       # autoupdate && autoconf && automake --foreign --add-missing --copy
#       ./configure --build=`./config.guess` --host=avr
#       make -j
# - then this script should work

avrlibcdir="$HOME"/src/avrdudes/avr-libc
avrgcc=avr-gcc
pathopts='-B '"$avrlibcdir"/avr/devices/avr32db48' -B '"$avrlibcdir"/avr/lib/avrxmega3' -I '"$avrlibcdir"/include

compile () {
  local speed=$1
  local file=$2
  local fcpu
  if [ "$speed" = 8 ]; then
    fcpu=8000000
  elif [ "$speed" = 4 ]; then
    fcpu=4000000
  elif [ "$speed" = 1 ]; then
    fcpu=1000000
  else
    error "compile (): bad speed"
  fi

  cmd "$avrgcc" $pathopts -Os -mmcu=avr32db48 -Wmain -DF_CPU="$fcpu" main.c util.c -o"$file"
}

to-hex () {
  cmd avr-objcopy -O ihex "$1" "$2"
}

run-pymcuprog () {
  cmd pymcuprog -t uart -d avr32db48 -u /dev/ttyUSB0 "$@"
}

write () {
  local hex=$1; shift
  run-pymcuprog write --erase -f "$hex" "$@"
}

write-fuse () {
  local offset=$1; shift
  local literal=$1; shift
  run-pymcuprog write -o "$offset" -b1 -l "$literal" -m fuses "$@"
}

set-fuses () {
  # --- brown-out: 2.85v, enabled continuously in both wake & sleep modes
  write-fuse 1 "$(((3 << 5) | (1 << 2) | 1))" "$@"
}

speed=$DEFAULT_SPEED
fun compile "$speed" a.out
# fun set-fuses
fun to-hex a.out a.hex
fun write a.hex
