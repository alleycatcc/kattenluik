#ifndef _EENDRAAD_H
#define _EENDRAAD_H

#include <stdbool.h>
#include <stdint.h>

#include <alleycat-avr.h>

typedef struct relay *relay;
typedef struct relay_hw *relay_hw;
typedef struct relay_sw *relay_sw;

void diag_flash_one_by_one (float delay1, float delay2);
void diag_blink_all_leds ();
void diag_all_on_forever ();
// --- sequentially blink relays and probe leds for each bus: meant so you
// can quickly test a single reader in each bus by moving the ethernet cable
// clockwise.
void diag_test_each_bus (aca_loop_t loop);
bool diag_led_toggle (uint8_t n);
bool diag_led_on (uint8_t n);
bool diag_led_off (uint8_t n);

relay_hw relay_hw_mk (aca_pinout_t pin, uint8_t code_length, float pulse_length);
relay_sw relay_sw_mk (aca_pinout_t pin);

void bus_led_operate (uint8_t bus_num, bool led_fail, bool state);

bool bus_relay_operate_auto (aca_loop_t loop, uint8_t n, bool trigger);
bool bus_relay_operate_external (aca_loop_t loop, uint8_t n, bool state);
void eendraad_init (aca_printer_t printer);

#endif
