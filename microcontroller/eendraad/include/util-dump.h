#ifndef _UTIL_DUMP_H
#define _UTIL_DUMP_H

typedef void (*print_t) (const char *s);

void dump_u8s_u8 (print_t print, size_t n, uint8_t *buf);
void dump_u8s_char (print_t print, size_t n, uint8_t *buf);

#endif
