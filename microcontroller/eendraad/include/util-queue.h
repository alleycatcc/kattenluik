#include <stdbool.h>
#include <stdint.h>

typedef struct queue *queue_t;

bool queue_push (queue_t t, void *el);
queue_t queue_mk (uint8_t size);
bool queue_empty (queue_t t);
void *queue_shift (queue_t t);
void queue_destroy (queue_t t);
