#ifndef _CONFIG_V1_3_H
#define _CONFIG_V1_3_H

// --- number of bits in the square wave, not counting the preamble 0-1 / 1-0
#define CODE_LENGTH 5

// --- amber
#define GENERIC_LED_0_PORT PORTB
#define GENERIC_LED_0_POS 4
// --- blue
#define GENERIC_LED_1_PORT PORTB
#define GENERIC_LED_1_POS 3
// --- red
#define GENERIC_LED_2_PORT PORTB
#define GENERIC_LED_2_POS 2
// --- green
#define GENERIC_LED_3_PORT PORTB
#define GENERIC_LED_3_POS 1

#define NUM_BUSES 8
#define NUM_GENERIC_LEDS 4
#define NUM_RELAYS (NUM_BUSES)

// --- half a period of the square wave
#define PULSE_LENGTH_MS 20

// --- the relay, and the led on the eendraad board near the UTP connector
#define BUS_0_RELAY_PORT PORTF
#define BUS_0_RELAY_POS 1
// --- the leds in the probe
#define BUS_0_LED_OK_PORT PORTF
#define BUS_0_LED_OK_POS 5
#define BUS_0_LED_FAIL_PORT PORTF
#define BUS_0_LED_FAIL_POS 4

#define BUS_1_RELAY_PORT PORTA
#define BUS_1_RELAY_POS 0
#define BUS_1_LED_OK_PORT PORTA
#define BUS_1_LED_OK_POS 1
#define BUS_1_LED_FAIL_PORT PORTA
#define BUS_1_LED_FAIL_POS 2

#define BUS_2_RELAY_PORT PORTA
#define BUS_2_RELAY_POS 3
#define BUS_2_LED_OK_PORT PORTA
#define BUS_2_LED_OK_POS 4
#define BUS_2_LED_FAIL_PORT PORTA
#define BUS_2_LED_FAIL_POS 5

#define BUS_3_RELAY_PORT PORTA
#define BUS_3_RELAY_POS 6
#define BUS_3_LED_OK_PORT PORTA
#define BUS_3_LED_OK_POS 7
#define BUS_3_LED_FAIL_PORT PORTB
#define BUS_3_LED_FAIL_POS 0

#define BUS_4_RELAY_PORT PORTD
#define BUS_4_RELAY_POS 7
#define BUS_4_LED_OK_PORT PORTD
#define BUS_4_LED_OK_POS 3
#define BUS_4_LED_FAIL_PORT PORTD
#define BUS_4_LED_FAIL_POS 2

#define BUS_5_RELAY_PORT PORTD
#define BUS_5_RELAY_POS 1
#define BUS_5_LED_OK_PORT PORTD
#define BUS_5_LED_OK_POS 0
#define BUS_5_LED_FAIL_PORT PORTC
#define BUS_5_LED_FAIL_POS 7

#define BUS_6_RELAY_PORT PORTC
#define BUS_6_RELAY_POS 6
#define BUS_6_LED_OK_PORT PORTC
#define BUS_6_LED_OK_POS 5
#define BUS_6_LED_FAIL_PORT PORTC
#define BUS_6_LED_FAIL_POS 4

#define BUS_7_RELAY_PORT PORTB
#define BUS_7_RELAY_POS 5
#define BUS_7_LED_OK_PORT PORTF
#define BUS_7_LED_OK_POS 0
#define BUS_7_LED_FAIL_PORT PORTE
#define BUS_7_LED_FAIL_POS 3



#endif
