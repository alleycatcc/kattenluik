#ifndef _CONFIG_PROJECT_H
#define _CONFIG_PROJECT_H

#include <stdbool.h>

#define CLK_PRESCALE 1
// --- 1MHz seems to work fine at 400k
#define F_CPU 4000000
#define USART_BAUD_RATE 9600
#define SPEAK true
// --- production
#define SPEAK_LEVEL ACA_SPEAK_WARN
// #define SPEAK_LEVEL ACA_SPEAK_INFO
#define TEST_EACH_BUS_FOREVER false
#define TEST_EACH_BUS_ONCE false
#define TEST_ALL_ON_FOREVER false

#endif
