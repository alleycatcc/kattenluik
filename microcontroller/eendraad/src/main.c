#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#include "config-project.h"

#include <util/delay.h>

#include <alleycat-avr.h>

#include "config-eendraad.h"
#include "eendraad.h"
#include "util-dump.h"

#define dbg_dump_u8s_u8(...) do { \
  dump_u8s_u8 (debug, ##__VA_ARGS__); \
} while (0)

#define dbg(...) do { \
  aca_debug (app.printer, ##__VA_ARGS__); \
} while (0)

#define inf(...) do { \
  aca_info (app.printer, ##__VA_ARGS__); \
} while (0)

#define err(...) do { \
  aca_error (app.printer, ##__VA_ARGS__); \
} while (0)

#define war(...) do { \
  aca_warn (app.printer, ##__VA_ARGS__); \
} while (0)

#define trc(...) do { \
  aca_trace (app.printer, ##__VA_ARGS__); \
} while (0)

volatile uint8_t debug_leds_n = 0;
// aca_printer_t printer_g;

struct app {
  aca_printer_t printer;
  aca_twit_t twit;
  volatile aca_loop_t loop;
} app;

void debug (const char *s) {
  aca_debug (app.printer, "%s", s);
}

static void init_printer (aca_printer_t *printer) {
  if (!SPEAK) {
    *printer = aca_mk_printer_null ();
    return;
  }
  aca_speak_set_level (SPEAK_LEVEL);
  aca_usart_t usart = aca_usart_init_default (ACA_USART_CONFIG_USART1, USART_BAUD_RATE);
  *printer = aca_mk_printer_usart (usart);
}

static void init_bod () {
  aca_bod_vlm_set (5);
  aca_bod_vlm_sei (true, true);
  char buf[255];
  bool ok = aca_bod_show_config (255, buf);
  if (ok) inf (buf);
}

uint8_t debug_bytes[100];
uint8_t debug_byte_n = 0;

bool status_update (aca_loop_timeout_t t, void *data) {
  (void) t;
  (void) data;
  dbg_dump_u8s_u8 (debug_byte_n, debug_bytes);
  if (debug_byte_n) debug ("---");
  return true;
}

/**
 * b[7:6] = 0 -> led-grant
 *          1 -> led-deny
 *          2 -> relay, type given by b[5:4]
 * b[5:4] = 0 -> relay auto, trigger direction given by b[0]
 *          1 -> relay external, controlled by b[0]
 * b[3:1] = n -> subtarget id (i.e. bus number (0-7))
 * b[0]   = 0 -> off (for leds); trigger low for relay auto; close lock for relay external
 *          1 -> on (for leds); trigger high for relay auto; open lock for relay external
 */

volatile bool process_byte (uint8_t data) {
  uint8_t which = data >> 6;
  bool relay_type = (data >> 4) & 0x03;
  uint8_t bus_num = (data >> 1) & 0x07;
  if (which == 2) {
    // --- auto
    if (relay_type == false) {
      bool trigger = data & 0x01;
      dbg ("got relay cmd: byte=0x%02x bus-num=%d relay-type=auto trigger=%s", data, bus_num, trigger ? "HIGH" : "LOW");
      if (!bus_relay_operate_auto (app.loop, bus_num, trigger))
        return false;
    }
    // --- external
    else {
      bool state = data & 0x01;
      dbg ("got relay cmd: byte=0x%02x bus-num=%d relay-type=external state=%s", data, bus_num, state ? "ON" : "OFF");
      if (!bus_relay_operate_external (app.loop, bus_num, state))
        return false;
    }
  }
  else {
    bool led_deny = which == 1;
    bool state = data & 0x01;
    dbg ("got led cmd: byte 0x%02x bus num %d led-%s %s", data, bus_num, led_deny ? "fail" : "ok", state ? "ON" : "OFF");
    bus_led_operate (bus_num, led_deny, state);
  }
  return true;
}

// ---

bool on_i2c_any (uint8_t status) {
  return true;
}

void on_i2c_too_fast () {
  diag_led_on (0);
}

void on_i2c_error (uint8_t status) {
  diag_led_on (1);
}

bool on_i2c_addressed () {
  return true;
}

bool on_i2c_receive (uint8_t data) {
  debug_bytes[debug_byte_n] = data;
  debug_byte_n = (debug_byte_n + 1) % 100;
  // --- clear debug bytes
  if (data == 0xff) {
    for (uint8_t i = 0; i < 100; i++)
      debug_bytes [i] = 0x00;
    debug_byte_n = 0;
  }

  return process_byte (data);
}

void on_i2c_request (volatile uint8_t *buf) {
  // --- only used for testing
  *buf = 0x42;
}

void on_i2c_stop () {
}

static void init_twi (aca_twit_t *twit) {
  *twit = aca_twit_init (ACA_TWI_CONFIG_TWI1, 0x0a, true);
  aca_twit_on_any (*twit, on_i2c_any);
  aca_twit_on_receive (*twit, on_i2c_receive);
  aca_twit_on_request (*twit, on_i2c_request);
  aca_twit_on_addressed (*twit, on_i2c_addressed);
  aca_twit_on_stop (*twit, on_i2c_stop);
  aca_twit_on_too_fast (*twit, on_i2c_too_fast);
}

static bool init_loop (volatile aca_loop_t *loop) {
  // --- @dies on fail
  // --- prepare one timeout slot per bus, plus one for the status update.
  char reason[255];
  *loop = aca_loop_mk_loop (NUM_BUSES + 1, ACA_TIMER_TCA0_16, 10, 255, reason);
  if (loop == NULL) {
    err (reason);
    return false;
  }
  aca_loop_timeout_add (*loop, status_update, NULL, 1000, NULL);
  return true;
}

int main (void) {
  cli ();
  aca_clock_main_internal_set_freq (F_CPU);
  aca_clock_main_set_prescale (CLK_PRESCALE);
  if (SPEAK) aca_delay_ms_basic (500);
  init_printer (&app.printer);
  eendraad_init (app.printer);
  init_bod ();
  init_twi (&app.twit);
  if (!init_loop (&app.loop))
    return 1;
  sei ();

  diag_blink_all_leds ();

  if (TEST_EACH_BUS_FOREVER)
    while (true) diag_test_each_bus (app.loop);
  if (TEST_EACH_BUS_ONCE)
    diag_test_each_bus (app.loop);
  if (TEST_ALL_ON_FOREVER)
    diag_all_on_forever ();

  diag_flash_one_by_one (100, 60);
  diag_blink_all_leds ();

  inf ("ready");

  // --- @todo
  // set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  aca_loop_main_loop_enter (app.loop);
  return 1;
}

ISR (BOD_VLM_vect) {
  // --- respond to VLVL interrupt -- we don't really need to do anything at
  // the moment but this is where we can add things that need to be cleaned
  // up or turned off.
  if (BOD_STATUS == BOD_VLMS_BELOW_gc)
    war ("voltage level dropped below VLVL");
  else if (BOD_STATUS == BOD_VLMS_ABOVE_gc)
    inf ("voltage level above VLVL again");
  // --- datasheet doesn't say we have to do this but it's how most of the
  // other peripherals work.
  BOD.INTFLAGS = 1 << 0;
}

ISR (ACA_TIMER_TCA0_16_VECTOR) {
  if (ACA_TIMER_TCA0_16_INTR_CONDITION) {
    aca_loop_tick (app.loop);
    ACA_TIMER_TCA0_16_INTR_CLEAR ();
  }
}

// --- we use noblock to help us catch the too fast condition.
// ISR (TWI1_TWIS_vect, ISR_NOBLOCK) {
ISR (TWI1_TWIS_vect) {
  aca_twit_handle_interrupt (app.twit);
}

ISR (BADISR_vect) {
  war ("uncaught interrupt");
}
