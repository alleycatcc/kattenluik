#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// #include "util-avr.h"
#include "util-queue.h"

#include <alleycat-avr.h>

// --- note, throws away message
#define _die(...) do { \
  exit (1); \
} while (0)

struct queue {
  uint8_t n;
  uint8_t size;
  void **data;
};

bool queue_push (queue_t t, void *el) {
  if (t->n == t->size) return false;
  t->data[t->n] = el;
  t->n += 1;
  return true;
}

queue_t queue_mk (uint8_t size) {
  queue_t t = malloc (sizeof (*t));
  if (t == NULL) _die ("queue_mk (): out of memory?");
  void **data = calloc (size, sizeof (void *));
  if (data == NULL) _die ("queue_mk (): out of memory?");
  *t = (struct queue) {
    .n = 0,
    .size = size,
    .data = data,
  };
  return t;
}

bool queue_empty (queue_t t) {
  return t->n == 0;
}

void *queue_shift (queue_t t) {
  if (t->n == 0) return NULL;
  void *ret = t->data[0];
  void *tmp[t->size];
  // @todo better data structure
  memcpy (tmp, t->data + 1, (t->size - 1) * sizeof (void *));
  memcpy (t->data, tmp, (t->size - 1) * sizeof (void *));
  t->n -= 1;
  return ret;
}

void queue_destroy (queue_t t) {
  free (t->data);
  free (t);
}
