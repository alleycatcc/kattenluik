#include "config-project.h"

#include <util/delay.h>

#include "util-delay.h"

#include <alleycat-avr.h>

void delay_ms (delay_t d) {
  if (d == DELAY_MS_5) _delay_ms (5);
  else if (d == DELAY_MS_10) _delay_ms (10);
  else if (d == DELAY_MS_15) _delay_ms (15);
  else if (d == DELAY_MS_20) _delay_ms (20);
  else if (d == DELAY_MS_25) _delay_ms (25);
  else if (d == DELAY_MS_30) _delay_ms (30);
  else if (d == DELAY_MS_35) _delay_ms (35);
  else if (d == DELAY_MS_40) _delay_ms (40);
  else if (d == DELAY_MS_45) _delay_ms (45);
  else if (d == DELAY_MS_50) _delay_ms (50);
  else if (d == DELAY_MS_55) _delay_ms (55);
  else if (d == DELAY_MS_60) _delay_ms (60);
  else if (d == DELAY_MS_65) _delay_ms (65);
  else if (d == DELAY_MS_70) _delay_ms (70);
  else if (d == DELAY_MS_75) _delay_ms (75);
  else if (d == DELAY_MS_80) _delay_ms (80);
  else if (d == DELAY_MS_85) _delay_ms (85);
  else if (d == DELAY_MS_90) _delay_ms (90);
  else if (d == DELAY_MS_95) _delay_ms (95);
  else if (d == DELAY_MS_100) _delay_ms (100);
  else if (d == DELAY_MS_105) _delay_ms (105);
  else if (d == DELAY_MS_110) _delay_ms (110);
  else if (d == DELAY_MS_115) _delay_ms (115);
  else if (d == DELAY_MS_120) _delay_ms (120);
  else if (d == DELAY_MS_125) _delay_ms (125);
  else if (d == DELAY_MS_130) _delay_ms (130);
  else if (d == DELAY_MS_135) _delay_ms (135);
  else if (d == DELAY_MS_140) _delay_ms (140);
  else if (d == DELAY_MS_145) _delay_ms (145);
  else if (d == DELAY_MS_150) _delay_ms (150);
  else if (d == DELAY_MS_155) _delay_ms (155);
  else if (d == DELAY_MS_160) _delay_ms (160);
  else if (d == DELAY_MS_165) _delay_ms (165);
  else if (d == DELAY_MS_170) _delay_ms (170);
  else if (d == DELAY_MS_175) _delay_ms (175);
  else if (d == DELAY_MS_180) _delay_ms (180);
  else if (d == DELAY_MS_185) _delay_ms (185);
  else if (d == DELAY_MS_190) _delay_ms (190);
  else if (d == DELAY_MS_195) _delay_ms (195);
  else if (d == DELAY_MS_200) _delay_ms (200);
  else if (d == DELAY_MS_300) _delay_ms (300);
  else if (d == DELAY_MS_400) _delay_ms (400);
  else if (d == DELAY_MS_500) _delay_ms (500);
  else if (d == DELAY_MS_1000) _delay_ms (1000);
  else if (d == DELAY_MS_2000) _delay_ms (2000);
  else idie ("invalid delay");
}
