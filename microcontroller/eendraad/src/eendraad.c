#include <string.h>

#include "config-project.h"

#include <util/delay.h>

#include <alleycat-avr.h>

#include "config-eendraad.h"
#include "eendraad.h"

#define DELAY_TEST_EACH_BUS_MS 300
// --- only useful for flashing / diag tests, so it doesn't need to be in the
// main config
#define NUM_LEDS (NUM_GENERIC_LEDS + NUM_BUSES*2)

#define MK_PINOUT(i) (aca_mk_pinout (&GENERIC_LED_##i##_PORT, GENERIC_LED_##i##_POS, 0))

#define ierr(...) do { \
  if (printer_g != NULL) aca_ierror (printer_g, ##__VA_ARGS__); \
} while (0)

#define die(...) do { \
  if (printer_g != NULL) aca_die (printer_g, ##__VA_ARGS__); \
} while (0)

struct timeout_info {
  uint8_t n_initial;
  uint8_t n;
  bool initial_state;
  bool next_state;
  aca_pinout_t pin;
};

struct relay {
  aca_pinout_t pin;
  struct timeout_info *timeout_info;
};

struct bus {
  aca_pinout_t relay_pin;
  aca_pinout_t led_ok_pin;
  aca_pinout_t led_fail_pin;
};

// --- for debugging
static aca_printer_t printer_g = NULL;

static aca_pinout_t diag_leds[NUM_LEDS];
static aca_pinout_t generic_leds[NUM_GENERIC_LEDS];
struct bus buses[NUM_BUSES];

struct relay relays[NUM_RELAYS];

// aca_pinout_t multiplex_addr[3] = {
  // { .port = &PORTD, .pos = 4 },
  // { .port = &PORTD, .pos = 5 },
  // { .port = &PORTD, .pos = 6 },
// };

static void init_generic_leds () {
  // --- must change here if NUM_GENERIC_LEDS changes
  generic_leds [0] = MK_PINOUT (0);
  generic_leds [1] = MK_PINOUT (1);
  generic_leds [2] = MK_PINOUT (2);
  generic_leds [3] = MK_PINOUT (3);
}

static void init_buses () {
  buses[0] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_0_RELAY_PORT, BUS_0_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_0_LED_OK_PORT, BUS_0_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_0_LED_FAIL_PORT, BUS_0_LED_FAIL_POS, false),
  };

  buses[1] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_1_RELAY_PORT, BUS_1_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_1_LED_OK_PORT, BUS_1_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_1_LED_FAIL_PORT, BUS_1_LED_FAIL_POS, false),
  };

  buses[2] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_2_RELAY_PORT, BUS_2_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_2_LED_OK_PORT, BUS_2_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_2_LED_FAIL_PORT, BUS_2_LED_FAIL_POS, false),
  };

  buses[3] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_3_RELAY_PORT, BUS_3_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_3_LED_OK_PORT, BUS_3_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_3_LED_FAIL_PORT, BUS_3_LED_FAIL_POS, false),
  };

  buses[4] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_4_RELAY_PORT, BUS_4_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_4_LED_OK_PORT, BUS_4_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_4_LED_FAIL_PORT, BUS_4_LED_FAIL_POS, false),
  };

  buses[5] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_5_RELAY_PORT, BUS_5_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_5_LED_OK_PORT, BUS_5_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_5_LED_FAIL_PORT, BUS_5_LED_FAIL_POS, false),
  };

  buses[6] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_6_RELAY_PORT, BUS_6_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_6_LED_OK_PORT, BUS_6_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_6_LED_FAIL_PORT, BUS_6_LED_FAIL_POS, false),
  };

  buses[7] = (struct bus) {
    .relay_pin = aca_mk_pinout (&BUS_7_RELAY_PORT, BUS_7_RELAY_POS, false),
    .led_ok_pin = aca_mk_pinout (&BUS_7_LED_OK_PORT, BUS_7_LED_OK_POS, false),
    .led_fail_pin = aca_mk_pinout (&BUS_7_LED_FAIL_PORT, BUS_7_LED_FAIL_POS, false),
  };
}

// --- must come after init_buses
static void init_relays () {
  for (uint8_t i = 0; i < NUM_RELAYS; i++) {
    struct timeout_info *ti = malloc (sizeof (struct timeout_info));
    if (ti == NULL) die ("init_relays (): out of memory?");
    aca_pinout_t pin = buses[i].relay_pin;
    relays[i] = (struct relay) {
      .pin = pin,
      .timeout_info = ti,
    };
  }
}

static void init_diag () {
  memcpy (diag_leds, generic_leds, NUM_GENERIC_LEDS * sizeof (aca_pinout_t));
  aca_pinout_t *ptr = diag_leds + NUM_GENERIC_LEDS;
  for (uint8_t i = 0; i < NUM_BUSES; i++) {
    *ptr++ = buses[i].led_ok_pin;
    *ptr++ = buses[i].led_fail_pin;
  }
}

void diag_flash_one_by_one (float delay1_ms, float delay2_ms) {
  for (uint8_t i = 0; i < NUM_LEDS; i++) {
    aca_pinout_t this = diag_leds[i];
    aca_pin_on (this);
    float delay_ms = i < NUM_GENERIC_LEDS ? delay1_ms : delay2_ms;
    aca_delay_ms_basic (delay_ms);
    if (i != 0) {
      aca_pinout_t prev = diag_leds[i - 1];
      aca_pin_off (prev);
    }
    if (i == NUM_LEDS - 1) {
      aca_delay_ms_basic (delay_ms);
      aca_pin_off (this);
    }
  }
}

void diag_blink_all_leds () {
  for (uint8_t i = 0; i < NUM_BUSES; i++) {
    aca_pin_on (buses[i].led_ok_pin);
    aca_pin_on (buses[i].led_fail_pin);
  }
  for (uint8_t i = 0; i < NUM_GENERIC_LEDS; i++)
    aca_pin_on (generic_leds[i]);
  _delay_ms (300);
  for (uint8_t i = 0; i < NUM_BUSES; i++) {
    aca_pin_off (buses[i].led_ok_pin);
    aca_pin_off (buses[i].led_fail_pin);
  }
  for (uint8_t i = 0; i < NUM_GENERIC_LEDS; i++)
    aca_pin_off (generic_leds[i]);
  _delay_ms (300);
}

void diag_all_on_forever () {
  while (true)
    for (uint8_t i = 0; i < NUM_LEDS; i++)
      aca_pin_on (diag_leds [i]);
}

// --- @todo add preamble?
static bool hw_cb (aca_loop_timeout_t t, void *data) {
  diag_led_toggle (1);
  struct timeout_info *ti = (struct timeout_info *) data;
  if (ti->next_state) aca_pin_on (ti->pin);
  else aca_pin_off (ti->pin);
  ti->next_state = !ti->next_state;
  ti->n -= 1;
  if (ti->n == 0) {
    ti->next_state = ti->initial_state;
    ti->n = ti->n_initial;
    return false;
  }
  return true;
}

static bool relay_operate_auto (aca_loop_t loop, struct relay *relay, bool trigger) {
  struct timeout_info *ti = relay->timeout_info;
  aca_pinout_t pin = relay->pin;
  *ti = (struct timeout_info) {
    .n_initial = CODE_LENGTH + 1,
    .n = CODE_LENGTH + 1,
    .initial_state = !trigger,
    .next_state = !trigger,
    .pin = pin,
  };
  return aca_loop_timeout_add (loop, hw_cb, ti, PULSE_LENGTH_MS, NULL);
}

static void relay_operate_external (aca_loop_t loop, struct relay *relay, bool state) {
  aca_pinout_t pin = relay->pin;
  if (state) aca_pin_on (pin);
  else aca_pin_off (pin);
}

bool bus_relay_operate_auto (aca_loop_t loop, uint8_t bus_num, bool state) {
  // --- extra check, although NUM_BUSES and NUM_RELAYS are currently identical
  if (bus_num >= NUM_BUSES || bus_num >= NUM_RELAYS) {
    ierr ("bus_relay_operate_auto (): invalid bus num");
    return false;
  }
  return relay_operate_auto (loop, &relays[bus_num], state);
}

bool bus_relay_operate_external (aca_loop_t loop, uint8_t bus_num, bool state) {
  // --- extra check, although NUM_BUSES and NUM_RELAYS are currently identical
  if (bus_num >= NUM_BUSES || bus_num >= NUM_RELAYS) {
    ierr ("bus_relay_operate_external (): invalid bus num");
    return false;
  }
  relay_operate_external (loop, &relays[bus_num], state);
  return true;
}

void bus_led_operate (uint8_t bus_num, bool led_fail, bool state) {
  if (bus_num >= NUM_BUSES) {
    ierr ("bus_led_operate (): invalid bus num");
    return;
  }
  struct bus bus = buses[bus_num];
  void (*f) (aca_pinout_t) = state ? aca_pin_on : aca_pin_off;
  aca_pinout_t p = led_fail ? bus.led_fail_pin : bus.led_ok_pin;
  (*f) (p);
}

void eendraad_init (aca_printer_t printer) {
  printer_g = printer;
  init_generic_leds ();
  init_buses ();
  init_relays ();
  init_diag ();
}

// --- operates relays as auto with trigger low
void diag_test_each_bus (aca_loop_t loop) {
  for (uint8_t j = 0; j < NUM_BUSES; j++) {
    uint8_t i = NUM_BUSES - j - 1;
    if (!bus_relay_operate_auto (loop, i, false))
      return;
    aca_pin_on (buses[i].led_ok_pin);
    aca_delay_ms_basic (DELAY_TEST_EACH_BUS_MS);
    aca_pin_on (buses[i].led_fail_pin);
    aca_delay_ms_basic (DELAY_TEST_EACH_BUS_MS);
    aca_pin_off (buses[i].led_ok_pin);
    aca_pin_off (buses[i].led_fail_pin);
  }
}

bool diag_led_toggle (uint8_t n) {
  if (n >= NUM_LEDS) return false;
  aca_pin_toggle (diag_leds [n]);
  return true;
}

bool diag_led_on (uint8_t n) {
  if (n >= NUM_LEDS) return false;
  aca_pin_on (diag_leds [n]);
  return true;
}

bool diag_led_off (uint8_t n) {
  if (n >= NUM_LEDS) return false;
  aca_pin_off (diag_leds [n]);
  return true;
}
