#!/usr/bin/env bash

cmd() {
  echo "* $@"
  "$@"
}

wait-for() {
  local cmd=$1; shift
  while ! pgrep "$cmd"; do
    sleep 1
  done
  cmd "$@"
}

go() {
  local t1
  local t2
  t1=$(date +%s)
  cmd chromium-browser \
    --enable-smooth-scrolling \
    --disable-overscroll-edge-effect \
    --kiosk \
    http://localhost?local
  t2=$(date +%s)
  if [ $(( t2 - t1 < 5 )) = 1 ]; then
    echo >&2 "Process died too quickly, restarting in 5 secs"
    sleep 5
  else
    echo >&2 "Uh-oh, we lost the browser, restarting"
  fi
  go
}

# --- easiest place for this
xrandr --output HDMI-1 --rotate left >& /tmp/debug
wait-for kattenluik go
