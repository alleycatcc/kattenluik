- button 'read key': unblock admin site, read key.
  - if it's a new key:
    - secret is generated, keyId and secret are stored in DB.
    - response body { keyId, isNewKey: true, isNewSecret: true, }
    - first line of list in FE: keyId, new indicator, line gets highlighted blue.
  - if it's an existing key with no secret:
    - secret is generated and stored in DB.
    - response body { keyId, isNewKey: false, isNewSecret: true, }
    - FE shows info toast.
    - line gets highlighted blue, scrolled into view.
  - if it's an existing key with a secret:
    - response body { keyId, isNewKey: false, isNewSecret: false, }
    - line gets highlighted blue, scrolled into view.
  - response body { keyId, isNewKey: true, isNewSecret: false, } is an error.

button 'check key': unblock admin site, read key, send 'tryAuthenticate: true' to processAttemptAdmin

in key row: (note, doesn't need to be coupled to an assignee)
 button 'regenerate secret':
  delete secret from db
  show message 'now read key'

don't check isactive in admin.
