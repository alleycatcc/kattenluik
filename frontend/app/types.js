import {
  pipe, compose, composeRight,
  noop, T, F, recurry,
} from 'stick-js/es'

import daggy from 'daggy'

import { cata, } from 'alleycat-js/es/bilby'

export const requestIsPending = cata ({
  RequestInit: F,
  RequestLoading: T,
  RequestError: F,
  RequestResults: F,
})

export const foldWhenRequestResults = recurry (2) (
  (f) => cata ({
    RequestInit: noop,
    RequestLoading: noop,
    RequestError: noop,
    RequestResults: (results) => f (results),
  }),
)

export const foldWhenNotRequestResults = recurry (2) (
  (f) => cata ({
    RequestInit: () => f (),
    RequestLoading: (_) => f (),
    RequestError: (_) => f (),
    RequestResults: noop,
  }),
)

export const foldIfRequestResults = recurry (3) (
  (yes) => (no) => cata ({
    RequestInit: () => no (),
    RequestLoading: (_) => no (),
    RequestError: (_) => no (),
    RequestResults: (results) => yes (results),
  }),
)

const Status = daggy.taggedSum ('Status', {
  StatusActive: [],
  StatusInactive: [],
  StatusMissing: [],
})

const { StatusActive, StatusInactive, StatusMissing, } = Status
export { StatusActive, StatusInactive, StatusMissing, }

// --- react-responsive-select determines equality using JSON.strinigfy, so we need to do this in
// order to be able to use this type as the value of a react-responsive-select component.
Status.prototype.toJSON = function () {
  return '{rep:' + this.toString () + '}'
}

// --- these will be used in the update/insert queries and must correspond with what the database
// expects in the `status` column.

export const statusTag = cata ({
  StatusActive: () => 'active',
  StatusInactive: () => 'inactive',
  StatusMissing: () => 'missing',
})

// --- this is the text that will be shown on the sliders / select menus in the interface
export const statusText = statusTag

export const statusFold = (f, g, h) => cata ({
  StatusActive: () => f (),
  StatusInactive: () => g (),
  StatusMissing: () => h (),
})

export const statusIsActive = statusFold (T, F, F)
