export const messages = {
  'app.containers.Example': {
    fruit: 'Kijk: {num} {num, plural, one {appel} other {appels}}',
    mood: 'Ik heb hoofdpijn',
  },
}
