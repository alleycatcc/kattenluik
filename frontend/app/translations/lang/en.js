export const messages = {
  'app.containers.Example': {
    fruit: 'Look: {num} {num, plural, one {apple} other {apples}}',
    mood: 'I\'m fine',
  },
}
