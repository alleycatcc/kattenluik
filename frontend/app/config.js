import {
  pipe, compose, composeRight,
  join,
} from 'stick-js/es'

import { fontFace, } from 'alleycat-js/es/font'

import { envIsDev, envIsTst, envIsNotPrd, } from './env'

// const debugRenders = envIsNotPrd
const debugRenders = false
const debugReducers = false
const debugSelectors = false

import fontLora from './fonts/lora.woff2'
import fontWalterTurncoat from './fonts/WalterTurncoat-Regular.ttf'
import imgBorderScruffy from './images/box-scruffy.png'
import imgLineScruffyHorizontal from './images/line-scruffy-horizontal.png'
import imgBackground from './images/house.jpg'
import imgSlider from './images/slider.png'
import imgMonkey from './images/monkey.png'
import imgGorilla from './images/gorilla.png'
import iconEdit from './images/icons/edit.svg'
import iconKey from './images/icons/sleutel.svg'
import iconLock from './images/icons/lock.svg'
import iconMore from './images/icons/more-grey.png'
import iconTrashBinRed from './images/icons/trash-red.svg'
import iconInfo from './images/icons/info.svg'
import iconClock from './images/icons/clock.svg'

import iconCBlocked from './images/icons/c-blocked.svg'
import iconCCheck from './images/icons/c-check.svg'
import iconCClear from './images/icons/c-clear.svg'
import iconCQuestion from './images/icons/c-question.svg'
import iconOk from './images/icons/ok.svg'
import iconCancel from './images/icons/cancel.svg'
import iconSearch from './images/icons/search.svg'

const getFontMainCss = () => join ('\n\n', [
  fontFace (
    'Lora',
    [
      [
        fontLora,
        'woff',
      ],
    ],
    {
      weight: 'normal',
      style: 'normal',
      stretch: 'normal',
    },
  ),
  fontFace (
    'Walter Turncoat',
    [
      [
        fontWalterTurncoat,
        'truetype',
      ],
    ],
    {
      weight: 'normal',
      style: 'normal',
      stretch: 'normal',
    },
  ),
])

export default {
  appElement: '#app',
  alwaysMobile: true,
  // --- 'do-stretch', 'simple-start', 'simple-center'
  responsiveInputScroll: 'do-stretch',
  doAbridgedListView: true,
  debug: {
    render: debugRenders && {
      Main: false,
      Assignee: false,
      Key: true,
      KeyRow: true,
    },
    reducers: debugReducers && {
      domain: true,
    },
    selectors: debugSelectors && {
      domain: {
        counter: true,
        error: true,
      },
      Key: {
        slice: true,
      },
    },
  },
  layout: {
    keySummary: {
      nameTruncate: 23,
      // --- set to '0px' to not show extra info in the key row
      keyExtraInfoWidth: '100px',
    }
  },
  misc: {
  },
  font: {
    main: {
      css: getFontMainCss (),
      // family: 'ReenieBeanie',
      family: 'Lora',
    },
  },
  images: {
    borderScruffy: imgBorderScruffy,
    lineScruffyHorizontal: imgLineScruffyHorizontal,
    house: imgBackground,
    slider: imgSlider,
    monkey: imgMonkey,
    gorilla: imgGorilla,
  },
  placeholders: {
    'key-custom': {
      mobile: '--none--',
      default: '<extra info>',
    },
  },
  icons: {
    clock: iconClock,
    edit: iconEdit,
    info: iconInfo,
    key: iconKey,
    lock: iconLock,
    more: iconMore,
    trashBinRed: iconTrashBinRed,
    ok: iconOk,
    cancel: iconCancel,
    c: {
      blocked: iconCBlocked,
      check: iconCCheck,
      clear: iconCClear,
      question: iconCQuestion,
    },
    search: iconSearch,
  },
  colors: {
    highlight: '#fd60b5',
    header: {
      color1: '#A62900',
      color2: '#00A629',
    },
    theme2: '#000000',
    key: {
      blue: '#2244AA',
      red: '#AA2222',
      yellow: 'rgb(251, 251, 77)',
      black: '#000',
      green: '#090',
    },
    'selected-key': '#fd60b599',
    'selected-key-lighter': '#fffbfd',
    'selected-key-lighter-with-opacity': '#fd60b506',
  },
  domain: {
    // --- whether to automatically set stale keys to inactive, also on the server.
    staleKeyAutoInactive: false,
  },
}
