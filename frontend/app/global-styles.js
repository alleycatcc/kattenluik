import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { createGlobalStyle, } from 'styled-components'

import 'react-tooltip/dist/react-tooltip.css'

import configure from 'alleycat-js/es/configure'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { mediaPhoneOnly, mediaTablet, mediaDesktop, } from './common'
import config from './config'

const configTop = config | configure.init

const fontMainCss = 'font.main.css' | configTop.get
const fontMainFamily = 'font.main.family' | configTop.get

// --- remember to use normal syntax for injecting variables (normal strings, not functions).

export const GlobalStyle = createGlobalStyle`
  ${fontMainCss}

  // --- note: on mobile, height = 100vh includes url bar height and soft buttons, while 100%
  // doesn't.

  html, body {
    width: 100%;
  }

  // --- webkit only, but good enough for now
  body::-webkit-scrollbar {
    ${mediaQuery (
      mediaPhoneOnly ('display: none'),
    )};
    display: none;
  }

  body {
    font-family: ${fontMainFamily};
    color: #333;
  }

  h1, h2, h3, h4, p {
    cursor: text;
  }

  #app {
    width: 100%;
    overflow: auto;
  }

  .x--mobile {
    .m--hide {
      display: none;
    }
  }

  /* Only show for given media query */

  .u-query-mobile {
    ${mediaQuery (
      mediaTablet ('display: none !important'),
      mediaDesktop ('display: none !important'),
    )};
  }

  .u-query-not-mobile {
    ${mediaQuery (
      mediaPhoneOnly ('display: none !important'),
    )};
  }

  .u-cursor-pointer {
    cursor: pointer !important;
  }

  .u--display-none {
    display: none !important;
  }
  .u--opacity-0 {
    opacity: 0;
  }
  .u--opacity-1 {
    opacity: 1;
  }
  .u--opacity-50 {
    opacity: 0.5;
  }

  // --- applying this class means hide the element unless hover is supported.
  .u-can-hover {
    @media (any-hover: none) {
      display: none;
    }
  }

  .u--hide {
    visibility: hidden !important;
  }

  // --- use case: a popup which uses position: fixed is open and you don't want the stuff
  // underneath to scroll. Apply this class to document.body while the popup is open.
  // \`overscroll-behavior: none\` works well if the popup is tall enough to get its own scrollbar,
  // but not otherwise.
  // --- note that this may cause some horizontal reflow as the scrollbar is shown/removed;
  // padding-right may fix this.
  .u--vertical-clip {
    height: 100vh;
    overflow-y: hidden;
  }
`
