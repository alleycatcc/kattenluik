import React from 'react'

import { withDisplayName, } from 'alleycat-js/es/react'

export default withDisplayName ('NotFound') (
  () => <article>
    Pagina niet gevonden.
  </article>
)
