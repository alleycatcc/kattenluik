import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { all, } from 'redux-saga/effects'

import {} from './actions'
import {} from './selectors'

export default function *sagaRoot () {
  yield all ([
  ])
}
