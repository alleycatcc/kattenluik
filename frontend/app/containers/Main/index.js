import {
  pipe, compose, composeRight,
  prop, eq, invoke, ifTrue, whenTrue, not,
} from 'stick-js/es'

import React, { useEffect, useState, } from 'react'

import { useParams as useRouteParams, } from 'react-router-dom'
import styled from 'styled-components'

import 'csshake/dist/csshake-hard.min.css'

// @todo we don't get this to work nicely together with useNavigate from react-router
// import { Parallax, } from 'react-parallax'

import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import {} from 'alleycat-js/es/general'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { useSaga, } from 'alleycat-js/es/redux-hooks'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {} from '../App/actions/main'
import { selectAppInitted, selectAdminAuthenticate, selectAdminCallPending, } from '../App/store/domain/selectors'
import { selectInterfaceLocked, } from '../App/store/ui/selectors'

import saga from './saga'

import { Button, MainSpinner, } from '../../components/shared'
import Admin, { CheckKeyButton, } from '../../containers/Admin'
import Assignee from '../../containers/Assignee'
import Key from '../../containers/Key'

import { container, useWhy, mediaPhone, mediaPhoneOnly, mediaTablet, useNavigateWithLocal, } from '../../common'

import config from '../../config'

import { foldWhenRequestResults, foldIfRequestResults, } from '../../types'

const configTop = config | configure.init

const { imgHouse, imgMonkey, imgGorilla, imgLock, } = configTop.gets ({
  imgHouse: 'images.house',
  imgMonkey: 'images.monkey',
  imgGorilla: 'images.gorilla',
  imgLock: 'icons.lock',
})

const MainS = styled.div`
  > .x__lock {
    display: none;
    padding: 30px;
    position: fixed;
    top: 70px;
    bottom: 70px;
    left: 70px;
    right: 70px;
    z-index: 10;
    background: white;
    box-shadow: 1px 1px 5px 1px;
    text-align: center;
    font-size: 30px;
    &.x--call-pending {
      > .x__main {
        display: none;
      }
      > .x__spinner {
        display: block;
      }
    }
    > .x__spinner {
      display: none;
    }
    > .x__main {
      > .x__button {
        width: 60%;
        margin: auto;
        margin-top: 30px;
      }
      > .x__text {
        padding-left: 2%;
        padding-right: 2%;
        width: 100%;
      }
    }
    img {
      width: 10%;
      margin: auto;
      margin-top: 2%;
      margin-bottom: 5%;
      display: block;
    }
  }
  &.x--locked {
    > .x__main {
      filter: blur(10px);
      pointer-events: none;
      height: 100vh;
    }
    > .x__lock {
      display: block;
    }
  }
  .x__mask {
    height: 100%;
    width: 100%;
    background: white;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1000;
  }
  width: 100%;
  margin: auto;
  // font-size: 16px;
  overflow-y: hidden;
`

const sketchyBorder1 = `
  border: 3px solid #41403E;
  border-radius: 2% 6% 5% 4% / 1% 1% 2% 4%;
`

const sketchyBorder = sketchyBorder1

const ContentS = styled.div`
  background-image: url(${imgHouse});
  background-size: 100%;
  .x__section {
    position: relative;
    // --- @todo what was this for?
    // z-index: 10;
    margin: auto;
    margin-bottom: 100px;
    padding: 20px;
    ${sketchyBorder}
    background: white;
    :nth-child(1) {
    }
    :nth-child(2) {
    }
    :last-child {
      margin-bottom: 0px;
    }
    .x__section-contents {
    }
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      .x__section {
        padding-left: 3%;
        width: 100%;
        .x__section-contents {
          margin-right: 2%;
        }
        &.x--first {
        }
      }
    `),
    mediaTablet (`
      .x__section {
        padding-left: 5%;
        width: 80%;
        .x__section-contents {
          margin-top: 30px;
        }
        &.x--first {
          margin-top: 100px !important;
        }
      }
    `),
  )}
`

export const Title = invoke (() => {
  const TitleS = styled.div`
    font-family: Walter Turncoat;
    ${mediaQuery (
      mediaPhone ('font-size: 65px'),
      mediaTablet ('font-size: 35px'),
    )};
    color: #fd60b5;
  `

  return ({ title, }) => <TitleS>
    {title}
  </TitleS>
})

const AdminAuthenticateS = styled.div`
  width: 100vw;
  height: 100vh;
  font-size: 100px;
  .x__bg {
    position: absolute;
    top: 0;
    left: 0;
    background: ${prop ('success') >> ifTrueV (
      '#20ff20', 'red',
    )};
    height: 100%;
    width: 100%;
    z-index: 10;
    opacity: 0.5;
  }
  .x__yes, .x__no {
    position: fixed;
    height: 100%;
    width: 100%;
    z-index: 11;
    opacity: 1;
  }
  .x__yes {
    top: 0;
    left: 0;
  }
  .x__yes img, .x__no {
    position: absolute;
    left: 50vw;
    top: 50vh;
    transform: translateX(-50%) translateY(-50%);
  }
  .x__yes {
    animation-name: main-monkey;
    animation-duration: 500ms;
    @keyframes main-monkey {
      from {
        transform: skew(10deg, 50deg);
      }
      to {
        transform: skew(0deg, 0deg);
      }
    }
  }
  .x__no {
    width: 300px;
    height: 380px;
    animation-name: main-gorilla;
    animation-duration: 2s;
    background-image: url(${imgGorilla});
    background-size: contain;
    background-repeat-y: no-repeat;

    @keyframes main-gorilla {
      from {
        background-position-y: 200px;
      }
      to {
        background-position-y: 0px;
      }
    }
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      .x__yes {
        img { width: 50%; }
      }
    `),
  )}
`

const AdminAuthenticate = ({ success, }) => {
  if (success === null) return null
  const bgCls = clss (
    'x__bg',
    success ? 'x--yes' : 'x--no',
  )
  return <AdminAuthenticateS success={success}>
    <div className={bgCls}/>
    <div className='x__img'>
      {success | ifTrue (
        () => <div className='x__yes'>
          <img src={imgMonkey}/>
        </div>,
        () => <div className='x__no'/>
      )}
    </div>
  </AdminAuthenticateS>
}


const dispatchTable = {
}

const selectorTable = {
  appInitted: selectAppInitted,
  adminAuthenticate: selectAdminAuthenticate,
  adminCallPending: selectAdminCallPending,
  interfaceLocked: selectInterfaceLocked,
}

const InnerS = styled.div`
  // --- @todo reuse styling from Admin
  > .x__buttons {
    width: 80%;
    margin: auto;
    margin-bottom: 80px;
    button {
      width: 100%;
    }
  }
`

const Inner = ({ isLocal, isMobile, routeDeviceId=null, }) => {
  const reload = useCallbackConst (() => document.location.reload ())
  const navigate = useNavigateWithLocal (isLocal)
  return <InnerS>
    <div className='x__section x--first'>
      <Title title='Admin'/>
      <div className='x__section-contents'>
        <Admin isLocal={isLocal} isMobile={isMobile} navigate={navigate}/>
      </div>
    </div>

    <div className='x__section'>
      <Title title='Keys'/>
      <div className='x__section-contents'>
        <Key isMobile={isMobile} routeDeviceId={routeDeviceId} navigate={navigate}/>
      </div>
    </div>

    <div className='x__section'>
      <Title title='Assignees'/>
      <div className='x__section-contents'>
        <Assignee isMobile={isMobile} navigate={navigate}/>
      </div>
    </div>

    <div className='x__buttons'>
      {/* --- @todo reuse from error page */}
      <Button onClick={reload}>Restart interface</Button>
    </div>
  </InnerS>
}

const MainSpinnerWrapperS = styled.div`
  > div {
    display: flex;
    align-items: center;
    width: 100%;
    > * {
      width: 50%;
      margin: auto;
    }
  }
`

const MainSpinnerWrapper = ({ scale, }) => <MainSpinnerWrapperS>
  <div>
    <MainSpinner scale={scale}/>
  </div>
</MainSpinnerWrapperS>

export default container (
  ['Main', dispatchTable, selectorTable, null],
  (props) => {
    const {
      isMobile,
      appInitted, adminAuthenticate, adminCallPending, interfaceLocked,
      isLocal, withKeyParam,
    } = props

    useSaga ({ saga, key: 'Main', })

    const cls = clss (
      adminAuthenticate | foldWhenRequestResults (
        ({ authenticated, isUnlockingScreen, ... _}) => authenticated && !isUnlockingScreen && 'shake-hard shake-constant',
      ),
    )

    const params = useRouteParams ('deviceId')
    const routeDeviceId = withKeyParam | ifTrue (
      () => params.deviceId,
      () => "")

    const [maskEnabled, setMaskEnabled] = useState (true)
    // --- hide everything for a moment so we don't see the image jump
    useEffect (() => {
      setTimeout (() => setMaskEnabled (false), 100)
    }, [])
    useWhy ('Main', props)

    if (not (appInitted)) return <MainSpinnerWrapper scale={10}/>
    return <MainS className={clss (interfaceLocked && 'x--locked')}>
      <div className={clss ('x__lock', adminCallPending && 'x--call-pending')}>
        <img src={imgLock}/>
        <div className='x__spinner'>
          <MainSpinner scale={2}/>
        </div>
        <div className='x__main'>
          <div className='x__text'>
            Use an admin key to unlock the screen.
          </div>
          <div className='x__button'>
            <CheckKeyButton unlockScreen={true} text='Scan key'/>
          </div>
        </div>
      </div>
      <div className='x__main'>
        {maskEnabled && <div className='x__mask'/>}
        <ContentS className={cls}>
          <Inner isLocal={isLocal} isMobile={isMobile} routeDeviceId={routeDeviceId}/>
        </ContentS>
        <AdminAuthenticate success={adminAuthenticate | foldIfRequestResults (
          ({ authenticated, isUnlockingScreen, }) => isUnlockingScreen ? null : authenticated,
          () => null,
        )}/>
      </div>
    </MainS>
  }
)
