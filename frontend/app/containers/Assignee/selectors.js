import {
  pipe, compose, composeRight,
  map, reduce, assocM, prepend,
  mapTuples, lets, tap, repeatF,
} from 'stick-js/es'

import { length, logWith, mapX, } from 'alleycat-js/es/general'

import { initialState, } from './reducer'
import { sortKeyData, sortKeySortFunctions, } from '../../sort'

import { initSelectors, } from '../../common'

import sortWith from 'ramda/es/sortWith'

const { select, selectTop, selectVal, } = initSelectors (
  'Assignee',
  initialState,
)

export const selectT = selectVal ('assignees')
export const selectSortKey = selectVal ('sortKey')
export const selectSortKeys = selectVal ('sortKeys')

export const selectSortKeyData = select (
  'sortKeyData',
  [selectSortKeys],
  (sks) => sks | map (sortKeyData),
)

// --- note: be sure to use `keyId` as the React key, and not the list index, when rendering.
const selectTSorted = select (
  'assigneesSorted',
  [selectT, selectSortKey],
  (assignees, sortAssignee) => lets (
    () => sortAssignee | sortKeySortFunctions,
    (fs) => assignees | sortWith (fs),
  ),
)

const selectNumsPerPage = selectVal ('numsPerPage')
const selectNumPerPageIdx = selectVal ('numPerPageIdx')
export const selectCurPage = selectVal ('curPage')

export const selectNumPerPage = select (
  'numPerPage',
  [selectNumPerPageIdx, selectNumsPerPage],
  (idx, nums) => nums [idx],
)

const selectTSlice = select (
  'slice',
  [selectTSorted, selectCurPage, selectNumPerPage],
  (keys, cur, num) => keys.slice (cur*num, cur*num + num),
)

export const selectTComponent = select (
  'assigneesComponent',
  [selectTSlice],
  map (({ id: assigneeId, name, custom, }) => ({
    assigneeId, name, custom,
  })),
)

// export const selectAssigneesLookup = select (
  // 'assigneesLookup',
  // [selectT],
  // (assignees) => new Map (assignees | map (({ id, name, }) => [id, name])),
// )

export const selectTReverse = select (
  'assigneesReverse',
  [selectT],
  (assignees) => assignees | mapTuples ((_, { id, name, }) => [name, id]),
)

// --- this is the list we want when editing the assignee of a key.
export const selectTEdit = select (
  'assigneesEdit',
  [selectT],
  prepend ({ id: null, name: '(unassigned)', custom: null, }),
)

// export const selectNumAssignees = select (
  // 'numAssignees',
  // [selectT],
  // length,
// )

export const selectNameExists = select (
  'assigneeNameExists',
  [selectT],
  (assignees) => (assigneeName) => {
    return new Set (assignees | map ((assignee => assignee.name))).has(assigneeName)
  },
)

export const selectNumsPerPageComponent = select (
  'numsPerPageComponent',
  [selectNumPerPageIdx, selectNumsPerPage],
  (idx, nums) => nums | mapX ((n, m) => ({
    n, idx: m, selected: m === idx,
  })),
)

const selectTLength = select (
  'numKeys',
  [selectT],
  length,
)

const selectNumPages = select (
  'numPages',
  [selectNumPerPage, selectTLength],
  (npp, num) => lets (
    () => num / npp,
    (n) => Math.floor (n),
    (n, m) => n === m ? n : m + 1,
  ),
)

export const selectPageComponent = select (
  'pageComponent',
  [selectCurPage, selectNumPages],
  (cur, num) => num | repeatF ((idx) => ({
    idx, n: idx + 1, selected: cur === idx,
  })),
)
