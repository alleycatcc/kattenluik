import {
  pipe, compose, composeRight,
  noop, prop, merge, assoc, tap,
} from 'stick-js/es'

import sortWith from 'ramda/es/sortWith'

import { cata, } from 'alleycat-js/es/bilby'
import { logWith, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import {
  assigneesFetchCompleted,
  assigneeSort,
  dbImport, dbReset,
  setAssigneesNumPerPageIdxReal,
  setAssigneesCurPage,
} from '../App/actions/main'
import { reducer, } from '../../common'

import {
  ascend, descend, ascendWithNullPrefer,
  mkSortKey,
} from '../../sort'

const SortKeyNone = mkSortKey (
  null, '--none--', noop, ascend,
)

const SortKeyNameAscend = mkSortKey (
  'name-ascending', 'Name ↑', prop ('name'), ascend,
)

const SortKeyNameDescend = mkSortKey (
  'name-descending', 'Name ↓', prop ('name'), descend,
)

const SortKeyCustom = mkSortKey (
  'custom', 'Extra info', prop ('custom'), ascend,
)

const SortKeyCustomNone = mkSortKey (
  'custom-none', 'Extra info (empty on top)', prop ('custom'), ascendWithNullPrefer (null),
)

const SortKeyNumKeysAscend = mkSortKey (
  'numkeys-ascending', 'Number of keys ↑', prop ('numKeys'), ascend,
)

const SortKeyNumKeysDescend = mkSortKey (
  'numkeys-descending', 'Number of keys ↓', prop ('numKeys'), descend,
)

export const initialState = {
  assignees: void 8,
  curPage: 0,
  numPerPageIdx: 1,
  numsPerPage: [5, 10, 25, 50],
  sortKey: SortKeyNameAscend,
  sortKeys: [
    SortKeyNone,
    SortKeyNameAscend,
    SortKeyNameDescend,
    SortKeyCustom,
    SortKeyCustomNone,
    SortKeyNumKeysAscend,
    SortKeyNumKeysDescend,
  ],
}

const clearData = merge ({
  assignees: null,
})

const reducerTable = makeReducer (
  assigneesFetchCompleted, (rcomplete) => assoc (
    'assignees', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results | sortWith (
        [ascend (prop ('name'))],
      ),
    }),
  ),
  assigneeSort, (ks) => assoc ('sortKey', ks),
  dbImport, () => clearData (),
  dbReset, () => clearData (),
  setAssigneesNumPerPageIdxReal, assoc ('numPerPageIdx'),
  setAssigneesCurPage, assoc ('curPage'),
)

export default reducer ('Assignee', initialState, reducerTable)
