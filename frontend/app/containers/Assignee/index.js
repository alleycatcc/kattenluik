import {
  pipe, compose, composeRight,
  tap, map, id, noop, ok, nil, ifOk, ifNil,
  ifFalse, whenTrue, whenOk, not,
  T, F, lets, sprintfN, sprintf1,
  ifPredicate, eq, prop, defaultToV, ifTrue, plus,
} from 'stick-js/es'

import React, { useCallback, useEffect, useMemo, useRef, useState, } from 'react'

import styled from 'styled-components'

import { Tooltip, } from 'react-tooltip'

import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, mapX, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { useSaga, } from 'alleycat-js/es/redux-hooks'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  assigneeAdd,
  assigneeCustomSet,
  assigneeNameSet,
  assigneeRemove,
  assigneeSort,
  keyGoToPageForKey, keySetSelectedDeviceId,
  setAssigneesNumPerPageIdx, setAssigneesCurPage,
} from '../App/actions/main'

import {
  selectTComponent as selectAssigneesComponent,
  selectNameExists as selectAssigneeNameExists,
  selectSortKeyData, selectSortKey,
  selectNumsPerPageComponent as selectAssigneesNumsPerPageComponent,
  selectPageComponent as selectAssigneesPageComponent,
} from './selectors'

import {
  selectAssigneeHasKeys,
  selectGetKeysForAssigneeSimple, selectGetNumKeysForAssignee
} from '../App/store/domain/selectors'

import { AreYouSureDialog, Button, mkPagination, SelectWithText, Text, } from '../../components/shared'

import saga from './saga'

import { component, container, getAnchor, isNotEmptyString, useWhy, toastWarn, mediaPhone, mediaPhoneOnly, mediaTablet, mediaDesktop, scrollIntoViewWithOptions, } from '../../common'
import config from '../../config'

import {} from '../../types'

const configTop = config | configure.init

const {
  imgTrashBinRed, imgKey, iconMore,
  responsiveInputScroll, doAbridgedListView, selectedKeyColor2,
} = configTop.gets ({
  imgTrashBinRed: 'icons.trashBinRed',
  imgKey: 'icons.key',
  iconMore: 'icons.more',
  responsiveInputScroll: 'responsiveInputScroll',
  doAbridgedListView: 'doAbridgedListView',
  selectedKeyColor2: 'colors.selected-key-lighter',
})

const ifZero = eq (0) | ifPredicate
const ifOne = eq (1) | ifPredicate

const getColor = lets (
  () => ({
    blue: 'blue',
    red: 'red',
    black: 'black',
    yellow: '#d8d800',
    green: 'green',
  }),
  (o) => (x) => o [x] | defaultToV ('grey'),
)

const AssigneeS = styled.div`
  margin-top: 50px;
  margin-bottom: 70px;
  > .x__controls {
    width: 120%;
    margin-bottom: 60px;
    position: relative;
    // --- sorry
    left: -150px;
    > .x__select {
      padding-left: 10px;
      display: inline-block;
    }
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      padding-left: 21%;
      padding-bottom: 175px;
    `),
  )}
`

const AssigneesS = styled.div`
`

const KeyIconS = styled.div`
  cursor: pointer;
  display: inline-block;
  background: ${prop ('color') >> defaultToV ('black')};
`

const ImgS = styled.img`
  height: 51px;
`

const KeyIcon = container ([
  'KeyIcon',
  {
    keyGoToPageForKeyDispatch: keyGoToPageForKey,
    keySetSelectedDeviceIdDispatch: keySetSelectedDeviceId,
  },
  {},
], ({
  deviceId, color,
  keyGoToPageForKeyDispatch, keySetSelectedDeviceIdDispatch,
}) => {
  const onClick = useCallbackConst (() => {
    /* To navigate to a key, we
     *   1) set the route (this may be slow, but will catch up eventually, which is fine; it won't
     *   work if the route is already in the address bar though;
     *   2) fire the action immediately (in case we need to change curPage)
     *   and 3) try to scroll there immediately using an anchor, which violates the spirit of
     *   React but is also the most responsive.
     */
    // --- @todo navigate disabled for now -- produces an annoying delay, and it works fine without it.
    // navigate ('/key/' + String (deviceId))
    getAnchor ('key', deviceId) | whenOk (scrollIntoViewWithOptions ({
      block: 'center',
    }))
    keySetSelectedDeviceIdDispatch (deviceId)
    keyGoToPageForKeyDispatch (deviceId)
  })
  return <KeyIconS color={color} onClick={onClick}>
    <ImgS src={imgKey}/>
  </KeyIconS>
})

// @todo: adjust the width of x__wrapper in mobile case (depends on the
// size of the screen).
const AssigneeRowFields = styled.div`
  display: flex;
  flex-flow: row wrap;
  .x__wrapper {
    display: flex;
  }
  // --- @todo duplicated from key
  > .x__controls {
    > .x__show-more {
      display: flex;
      align-items: center;
      &.x--showing {
        filter: brightness(0%);
      }
      > .x__img {
        width: 40px;
        text-align: center;
        transform: rotate(90deg);
      }
      ${mediaQuery (
        mediaTablet (`
          cursor: pointer;
          &:hover {
            filter: brightness(0%);
          }
        `),
      )}
    }

  }
  ${mediaQuery (
    mediaPhoneOnly (`
      .x__wrapper {
        display: flex;
        flex-flow: column wrap;
        width: 80%;
        .x__name {
          width: 100%;
          margin-bottom: 20px;
        }
        .x__custom {
          width: 100%;
          margin-bottom: 15px;
        }
        .x__keys {
          width: 100%;
          margin-top: 20px;
          margin-bottom: 20px;
        }
      }
      > .x__controls {
        margin-left: 20px;
      }
    `),
    mediaTablet (`
      .x__wrapper {
        width: 80%;
        .x__name {
          width: 200px;
        }
        .x__custom {
          margin-left: 5%;
          width: 200px;
        }
        .x__keys {
          margin-left: 5%;
          width: 33%;
        }
      }
      > .x__controls {
        margin-left: 5%;
      }
    `),
  )}
`

const AssigneeRowBaseS = styled (AssigneeRowFields) `
  margin-top: 5%;
  margin-bottom: 5%;
  .x__name {
    cursor: pointer;
  }
  .x__custom {
    cursor: pointer;
  }
`

const AddAssigneeRowS = styled (AssigneeRowBaseS) `
  button {
    width: 30px;
    height: 30px;
    font-weight: bold;
    position: relative;
    top: 4px;
  }
  ${mediaQuery (
    mediaPhone (`
      >.x__remove {
        margin-left: 20px;
      }
      .x__minus {
        position: relative;
        top: -5px;
      }
    `),
    mediaTablet (`
      .x__minus {
        position: static;
      }
    `),
  )}
`

const AssigneeRowS = styled (AssigneeRowBaseS) `
  min-height: 60px;
  &.x--expanded {
    position: fixed;
    z-index: 100;
    left: 0;
    top: 0;
    bottom: 0;
    padding-bottom: 30px;
    padding-top: 30px;
    background: ${selectedKeyColor2};
    // --- trial and error
    padding-left: 182px;
    width: 100%;
    padding-right: 39px;
    margin-top: 0px;
    margin-bottom: 0px;
    overflow-y: scroll;
    // --- don't scroll the stuff beneath the popup
    overscroll-behavior: none;
    .x__wrapper {
    }
  }
  .x__removeButton {
    cursor: pointer;
    width: 35px;
    margin-top: 15px;
    img {
      width: 17px;
      &.x--with-alert {
        filter: none;
      }
      filter: brightness(0%);
    }
    ${mediaQuery (
      mediaPhoneOnly (`
        img {
          width: 25px;
        }
      `),
    )}
  }
`

const TextContents = styled.div`
  // --- for a long string without spaces, since it can't be wrapped
  overflow: hidden;
  text-overflow: ellipsis;
`

// todo this repeats in Key
const WithKeyLabelS = styled.div`
  width: 100%;
  display: inline-block;
  position: relative;
  min-height: 50px;
  > .x__label {
    position: absolute;
    opacity: 0.7;
    font-size: 80%;
    left: -150px;
    top: 8px;
    ${mediaQuery (
      mediaTablet ('display: none;'),
    )}
  }
`

const WithKeyLabelM = ({ children, label, }) => <WithKeyLabelS>
  {children}
  <div className='x__label'>
    {label}
  </div>
</WithKeyLabelS>

const Name = component (
  ['Name'],
  ({ abridged, isMobile, value, onAccept, }) => {
    return <WithKeyLabelM label='name'>
      {abridged | ifTrue (
        () => value,
        () => <Text
          isMobile={isMobile}
          border={0}
          defaultValue={value}
          blur='cancel'
          showButtons={false}
          placeholder={isMobile ? '' : '<name>'}
          onAccept={onAccept}
          onCancel={noop}
        />,
      )}
    </WithKeyLabelM>
  },
)

const Custom = component (
  ['Custom'],
  ({ abridged, isMobile, value, onAccept, }) => {
    return <WithKeyLabelM label='extra info'>
      {abridged | ifTrue (
        () => value,
        () => <Text
          isMobile={isMobile}
          border={0}
          defaultValue={value}
          blur='cancel'
          showButtons={false}
          placeholder={isMobile ? '' : '<extra info>'}
          onAccept={onAccept}
          onCancel={noop}
        />,
      )}
    </WithKeyLabelM>
  },
)

const Keys = component (
  ['Keys'],
  ({ keys, navigate, }) => keys | mapX (
    ({ id: keyId, deviceId, color, }, idx) => <KeyIcon
      idx={idx} key={keyId}
      color={getColor (color)} deviceId={deviceId}
      navigate={navigate}
    />,
  ),
)

// --- @todo lots of duplication from Key
const InfoS = styled.div`
  width: 100%;
/*
  >.x__main {
    padding-left: 180px;
    .x__wrapper {
      width: 80%;
    }
  }
  */
`

const Info = component (
  ['Info'],
  (props) => {
    const {
      isMobile,
      NameC, CustomC,
      onRequestRemove,
      numKeys,
    } = props
    const removeText = numKeys === 0 ? 'Remove this assignee' :
      numKeys === 1 ? 'Remove this assignee and convert key to unassigned' :
      'Remove this assignee and convert keys to unassigned'
    return <InfoS>
      <div className='x__main'>
        <div className='x__wrapper'>
          {/*
          <div className='x__name'>
            <NameC abridged={false}/>
          </div>
          <div className='x__custom'>
            <CustomC abridged={false}/>
          </div>
          */}
          <Button width='100%' onClick={onRequestRemove}>
            {removeText}
          </Button>
        </div>
      </div>
    </InfoS>
  },
)

const AssigneeRow = container (
  ['AssigneeRow', {
    assigneeCustomSetDispatch: assigneeCustomSet,
    assigneeNameSetDispatch: assigneeNameSet,
    assigneeRemoveDispatch: assigneeRemove,
  }, {
    assigneeHasKeys: selectAssigneeHasKeys,
    assigneeNameExists: selectAssigneeNameExists,
    getNumKeysForAssignee: selectGetNumKeysForAssignee,
    getKeysForAssignee: selectGetKeysForAssigneeSimple,
  }], ({
    isMobile, abridged, navigate,
    assigneeId, name: nameProp, custom: customProp,
    assigneeHasKeys, getKeysForAssignee, getNumKeysForAssignee, assigneeNameExists,
    assigneeCustomSetDispatch, assigneeNameSetDispatch, assigneeRemoveDispatch,
  }) => {
    const [name, setName] = useState (nameProp)
    const [custom, setCustom] = useState (customProp)
    const [removeDialogOpen, setRemoveDialogOpen] = useState (false)
    const [showInfo, setShowInfo] = useState (false)

    useEffect (() => { setName (nameProp) }, [nameProp])
    useEffect (() => { setCustom (customProp) }, [customProp])

    const closeRemoveDialog = useCallbackConst (F >> setRemoveDialogOpen)
    const openRemoveDialog = useCallbackConst (T >> setRemoveDialogOpen)

    const onAcceptName = useCallback ((name) => {
      if (name === '' || (assigneeNameExists (name) && name !== nameProp))
        return toastWarn ("The name must be unique and can't be empty")
      assigneeNameSetDispatch (assigneeId, name)
    }, [nameProp, assigneeId, assigneeNameExists])

    const onAcceptCustom = useCallback ((custom) => {
      setCustom (custom)
      assigneeCustomSetDispatch (assigneeId, custom)
    }, [assigneeId])

    const clickDialogNo = closeRemoveDialog

    const clickDialogYes = useCallback (
      (hasKeys) => () => {
        assigneeRemoveDispatch ({ id: assigneeId, allowAssignedKeys: hasKeys, })
        closeRemoveDialog ()
      },
      [assigneeId, closeRemoveDialog],
    )

    const hasKeys = assigneeHasKeys (assigneeId)
    const keys = getKeysForAssignee (assigneeId)
    const numKeysForThisAssignee = getNumKeysForAssignee (assigneeId)

    // --- returns [extraWarn, contents]
    const dialogContents = useCallback (
      () => numKeysForThisAssignee | ifZero (
        () => [false, name | sprintf1 ('This will delete assignee %s from the system.')],
        (n) => [true, lets (
          () => n | ifOne (() => ['', 'is', 'it', 'an unassigned key'], () => ['s', 'are', 'they', 'unassigned keys']),
          ([s, is, it, akey]) => <>
            {[is, n, s, name] | sprintfN ('There %s still %s key%s assigned to %s.')}
            <br/>
            {[it, akey] | sprintfN ('If you proceed, %s will be converted to %s.')}
          </>,
        )],
      ), [name, numKeysForThisAssignee]
    )

    const onClickMore = useCallback (
      () => not (showInfo) | setShowInfo,
      [showInfo],
    )

    // --- @todo splitting Name/NameC and Custom/CustomC liked this turned out not to be necessary

    const NameC = useMemo (() => component (
      ['NameC'],
      ({ abridged, }) => <Name
        abridged={abridged}
        isMobile={isMobile}
        value={name}
        onAccept={onAcceptName}
      />,
    ), [isMobile, name, onAcceptName])

    const CustomC = useMemo (() => component (
      ['CustomC'],
      ({ abridged, }) => <Custom
        abridged={abridged}
        isMobile={isMobile}
        value={custom ?? ''}
        onAccept={onAcceptCustom}
      />,
    ), [isMobile, custom, onAcceptCustom])

    const confirm = useRef ([false, ''])
    useEffect (() => {
      confirm.current = dialogContents ()
    }, [name, numKeysForThisAssignee, dialogContents])

    useEffect (() => {
      const f = showInfo ? 'add' : 'remove'
      document.body.classList [f] ('u--vertical-clip')
    }, [showInfo])

    return <AssigneeRowS className={clss (showInfo && 'x--expanded')}>
      <div className='x__wrapper'>
        <AreYouSureDialog
          extraWarn={confirm.current [0]}
          isOpen={removeDialogOpen}
          onRequestClose={closeRemoveDialog}
          onYes={clickDialogYes (hasKeys)}
          onNo={clickDialogNo}
          contents={confirm.current [1]}
          isMobile={isMobile}
        />
        {/* @todo we could store the 'input' and 'setInput' in AssigneeRow (then we don't need state in Input*/}
        <div className='x__name'>
          <NameC abridged={abridged && not (showInfo)}/>
        </div>
        <div className='x__custom'>
          <CustomC abridged={abridged && not (showInfo)}/>
        </div>
        <div className='x__num-keys'>
          <WithKeyLabelM label='# of keys'>
            {numKeysForThisAssignee}
          </WithKeyLabelM>
        </div>
        <div className='x__keys'>
          {(abridged && not (showInfo)) || <Keys keys={keys} navigate={navigate}/>}
        </div>
      </div>
      <div className='x__controls'>
        {abridged | ifTrue (
          () => <div
            onClick={onClickMore}
            className={clss ('x__show-more', showInfo && 'x--showing')}
          >
            <img className='x__img' src={iconMore}/>
          </div>,
          () => <span
            className='x__removeButton'
            onClick={openRemoveDialog}
          >
            <img className={clss (hasKeys && 'x--with-alert')} src={imgTrashBinRed}
              data-tooltip-id='tooltip-assignee'
              data-tooltip-content={hasKeys | ifFalse (
               () => 'Delete assignee.',
               () => lets (
                 () => numKeysForThisAssignee,
                 (num) => num === 1 ? 'key' : 'keys',
                 (num, keyString) => [num, keyString, 'name'] | sprintfN (
                   'Delete assignee with %s assigned %s.',
                 ),
               ),
              )}
            />
          </span>,
        )}
      </div>
      {showInfo && <Info
        isMobile={isMobile}
        onRequestRemove={openRemoveDialog}
        numKeys={numKeysForThisAssignee}
        // --- @todo turned out not to be necessary
        NameC={NameC}
        CustomC={CustomC}
      />}
    </AssigneeRowS>
  },
)

const Assignees = component (['Assignees'], (props) => {
  const { isMobile, navigate, assignees, } = props
  const abridged = doAbridgedListView && assignees.length > 5
  const rows = useMemo (() => assignees | map (
    ({ assigneeId, name, custom, }) => <AssigneeRow
      key={assigneeId}
      isMobile={isMobile}
      abridged={abridged}
      navigate={navigate}
      assigneeId={assigneeId}
      name={name}
      custom={custom}
    />,
  ), [assignees, isMobile, abridged, navigate])
  useWhy ('Assignees', props)
  return <AssigneesS>
    {rows}
  </AssigneesS>
})

const AssigneeRowsHeaderS = styled (AssigneeRowFields) `
  > div {
    display: inline-block;
    text-decoration: underline;
  }
`

const AssigneeRowsHeader = ({ classes=[], }) =>
  <AssigneeRowsHeaderS className={clss (... classes)}>
    <div className='x__wrapper'>
      <div className='x__name'>
        Name
      </div>
      <div className='x__custom'>
        Extra info
      </div>
      <div className='x__keys'>
      </div>
    </div>
  </AssigneeRowsHeaderS>

const ButtonRow = styled.div`
  // --- @todo unify styling buttons
  .x__add {
    display: inline-block;
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      button {
        width: 300px;
      }
      .x__add {
        padding-top: 5px;
        padding-bottom: 5px;
      }
    `),
    mediaTablet (`
      padding-left: 85%;
      button {
        width: 30px;
        height: 30px;
        font-weight: bold;
        position: relative;
        top: 4px;
      }
      .x__add {
        position: static;
      }
    `),
  )}
`

// --- @todo refactor
const AssigneeRowNameContentS = styled.span`
  padding-left: 10px;
  padding-right: 10px;
`

const AddAssigneeRow = container (
  ['AddAssigneeRow', {
    assigneeAddDispatch: assigneeAdd,
  }, {
    assigneeNameExists: selectAssigneeNameExists,
  }],
  ({ assigneeNameExists, assigneeAddDispatch, assigneeAddToggle, isMobile, }) => {
  const [name, setName] = useState ()
  const [custom, setCustom] = useState ()

  const assigneeAdd = useCallback (
    () => {
      if (name && name !== '' && !assigneeNameExists (name)) {
        assigneeAddDispatch (name, custom)
        assigneeAddToggle ()
      } else {
        toastWarn ('The name must be unique and cannot be empty')
      }
    },
    [assigneeAddToggle, assigneeNameExists, name, custom],
  )

  const onAccept = useCallback (
    () => assigneeAdd (),
    [assigneeAdd]
  )

  const canAccept = (name ?? '') | isNotEmptyString

  const ref = useRef ()
  useEffect (() => {
    if (responsiveInputScroll === 'simple-center')
      ref.current.scrollIntoView ({
        block: 'center',
      })
    else if (responsiveInputScroll === 'simple-start')
      ref.current.scrollIntoView ({
        block: 'start',
      })
  })

  return <AddAssigneeRowS ref={ref}>
    <div className='x__wrapper'>
      <div className='x__name'>
        <Text
          isMobile={isMobile}
          border={0}
          autoFocus={true}
          defaultValue={name}
          placeholder='name'
          blur='cancel'
          showButtons={false}
          canAccept={canAccept}
          onAccept={onAccept}
          onCancel={noop}
          onUpdate={setName}
        />
      </div>
      <div className='x__custom'>
        <Text
          isMobile={isMobile}
          border={0}
          defaultValue={custom}
          placeholder='custom'
          blur='cancel'
          showButtons={false}
          canAccept={canAccept}
          onAccept={onAccept}
          onCancel={noop}
          onUpdate={setCustom}
        />
      </div>
      <div className='x__keys'/>
    </div>
    <div className='x__remove'>
      <Button
        onClick={assigneeAddToggle}
        style={{ padding: '4px', }}
      >
        <span className='x__minus'>-</span>
      </Button>
    </div>
</AddAssigneeRowS>
})

const dispatchTable = {
  assigneeSortDispatch: assigneeSort,
}

const selectorTable = {
  assignees: selectAssigneesComponent,
  sortKey: selectSortKey,
  sortKeyData: selectSortKeyData,
}

export default container (
  ['Assignee', dispatchTable, selectorTable],
  (props) => {
    const {
      isMobile, navigate, assignees,
      sortKey, sortKeyData,
      assigneeSortDispatch,
    } = props

    useSaga ({ saga, key: 'Assignee', })

    const [addAssigneeVisible, setAddAssigneeVisible] = useState (false)
    // --- use an integer key to force tooltips to rerender (otherwise a row that has just been
    // added won't show the tooltips)
    const [tooltipKey, setTooltipKey] = useState (0)

    const assigneeAddToggle = useCallback (
      () => {
        setAddAssigneeVisible (!addAssigneeVisible)
      },
      [addAssigneeVisible],
    )

    // --- @todo refactor options for all selects
    const sortOptions = sortKeyData | map (
      ([sk, _tag, text]) => ({
        key: sk,
        value: sk,
        contentString: text,
        ContentElement: ({ children, }) => <AssigneeRowNameContentS>{children}</AssigneeRowNameContentS>,
      }),
    )

    const onSelectSort = useCallbackConst (
      ({ id: sortKey, }) => assigneeSortDispatch (sortKey),
    )

    const PaginationC = useMemo (() => mkPagination ({
      setNumPerPageIdx: setAssigneesNumPerPageIdx,
      setCurPage: setAssigneesCurPage,
      selectNumsPerPage: selectAssigneesNumsPerPageComponent,
      selectPage: selectAssigneesPageComponent,
    }), [])

    useEffect (() => {
      setTooltipKey (plus (1))
    }, [assignees])

    useWhy ('Assignee', props, { tooltipKey, addAssigneeVisible, })

    return <AssigneeS>
      <div className='x__controls'>
        Sort on:
        <div className='x__select'>
          <SelectWithText
            selectKey={sortKey}
            isMobile={isMobile}
            selectedValue={sortKey}
            options={sortOptions}
            onSelect={onSelectSort}
          />
        </div>
        <PaginationC collectionName='assignees'/>
      </div>
      <AssigneeRowsHeader classes={['u-query-not-mobile']}/>
      <Assignees
        isMobile={isMobile} navigate={navigate}
        assignees={assignees}
      />
      <ButtonRow>
        <Button
          onClick={assigneeAddToggle}
          className={clss (addAssigneeVisible && 'u--display-none')}
          style={{ padding: '4px', }}
        >
          <div className='x__add'>
            <span className='u-query-not-mobile'>+</span>
            <span className='u-query-mobile'>
              Add assignee
            </span>
          </div>
        </Button>
      </ButtonRow>
      <Tooltip
        id="tooltip-assignee"
        key={tooltipKey}
        effect='solid'
        openOnClick={false}
        style={{
          // this should be possible with hidden={isMobile} as a prop for Tooltip, but somehow it
          // doesn't work.
          display: isMobile ? 'none' : 'inline-block',
          width: '300px',
      }}/>
      {addAssigneeVisible | whenTrue (() =>
      <AddAssigneeRow
        isMobile={isMobile}
        assigneeAddToggle={assigneeAddToggle}
      />
      )}
    </AssigneeS>
  }
)
