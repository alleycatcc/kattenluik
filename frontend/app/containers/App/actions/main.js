import {
  pipe, compose, composeRight,
  noop, tap, lets,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { action, } from 'alleycat-js/es/redux'

import { StatusInactive, } from '../../../types'

export const halt = action (
  () => {},
  'halt',
)

export const localeChange = action (
  (locale) => locale,
)

export const adminKeyAuthenticateCompleted = action (
  (rcomplete, isUnlockingScreen) => ({ rcomplete, isUnlockingScreen, }),
  'adminKeyAuthenticateCompleted',
)

export const adminKeyReadCompleted = action (
  (rcomplete) => rcomplete,
  'adminKeyReadCompleted',
)

export const appMounted = action (
  () => {},
  'appMounted',
)

export const assigneeAdd = action (
  (name, custom) => ({ name, custom, }),
  'assigneeAdd',
)

export const assigneeCustomSet = action (
  (id, custom) => ({ id, custom }),
  'assigneeCustomSet',
)

export const assigneeNameSet = action (
  (id, name) => ({ id, name }),
  'assigneeNameSet',
)

export const assigneeRemove = action (
  ({ id, allowAssignedKeys }) => ({ id, allowAssignedKeys }),
  'assigneeRemove',
)

export const assigneesFetch = action (
  () => {},
  'assigneesFetch',
)

export const assigneesFetchCompleted = action (
  (rcomplete) => rcomplete,
  'assigneesFetchCompleted',
)

export const colorsFetch = action (
  () => {},
  'colorsFetch',
)

export const colorsFetchCompleted = action (
  (rcomplete) => rcomplete,
  'colorsFetchCompleted',
)

export const doorsFetch = action (
  noop,
  'doorsFetch',
)

export const doorsFetchCompleted = action (
  (rcomplete) => rcomplete,
  'doorsFetchCompleted',
)

export const keyAssigneeSet = action (
  (deviceId, assigneeId) => ({ deviceId, assigneeId }),
  'keyAssigneeSet',
)

export const keyAuthorizationAdd = action (
  // --- [[keyId, doorId]]
  (tuples) => tuples,
  'keyAuthorizationAdd',
)

export const keyAuthorizationRemove = action (
  (ids) => ids,
  'keyAuthorizationRemove',
)

export const keyAuthorizationsFetch = action (
  noop,
  'keyAuthorizationsFetch',
)

export const keyAuthorizationsFetchCompleted = action (
  (rcomplete) => rcomplete,
  'keyAuthorizationsFetchCompleted',
)

export const keyColorSet = action (
  (deviceId, colorId) => ({ deviceId, colorId }),
  'keyColorSet',
)

export const keyCustomSet = action (
  (deviceId, custom) => ({ deviceId, custom }),
  'keyCustomSet',
)

export const keysFetch = action (
  () => {},
  'keysFetch',
)

export const keysFetchCompleted = action (
  (rcomplete) => rcomplete,
  'keysFetchCompleted',
)

export const keyCheck = action (
  (unlockScreen) => ({ unlockScreen, }),
  'keyCheck',
)

export const keyPrivilegeAdd = action (
  // --- [[keyId, privilegeId]],
  (tuples) => tuples,
  'keyPrivilegeAdd',
)

export const keyPrivilegeRemove = action (
  (ids) => ids,
  'keyPrivilegeRemove',
)

export const keyPrivilegesFetch = action (
  noop,
  'keyPrivilegesFetch',
)

export const keyPrivilegesFetchCompleted = action (
  (rcomplete) => rcomplete,
  'keyPrivilegesFetchCompleted',
)

export const keySort = action (
  (keySort) => keySort,
  'keySort',
)

export const assigneeSort = action (
  (assigneeSort) => assigneeSort,
  'assigneeSort',
)

export const keyRegenerate = action (
  () => {},
  'keyRegenerate',
)

export const keyRead = action (
  () => {},
  'keyRead',
)

export const keyRemove = action (
  (deviceId) => ({ deviceId, fetch: false, }),
  'keyRemove',
)

export const keyStatusSet = action (
  (status, deviceId, fetch) => ({
    status, deviceId, fetch, updateActivityTimestamp: false,
  }),
  'keyStatusSet',
)

// --- used to mark stale keys but not automatically change their status
export const keyMarkAsStale = action (
  (deviceIds) => ({ deviceIds, }),
  'keyMarkAsStale',
)

export const checkShouldLock = action (
  () => {},
  'checkShouldLock',
)

export const checkStaleKeys = action (
  () => {},
  'checkStaleKeys',
)

export const dbReset = action (
  () => {},
  'dbReset',
)

export const dbImport = action (
  (dbFile) => dbFile,
  'importDB',
)

export const lockInterface = action (
  () => {},
  'lockInterface',
)

export const unlockInterface = action (
  () => {},
  'unlockInterface',
)

export const uptimeApproximateByMonthFetch = action (
  () => {},
  'uptimeApproximateByMonthFetch',
)

export const uptimeApproximateByMonthFetchCompleted = action (
  (rcomplete) => rcomplete,
  'uptimeApproximateByMonthFetchCompleted',
)

export const setKeysNumPerPageIdx = action (
  (n) => n,
  'setKeysNumPerPageIdx',
)

export const setKeysNumPerPageIdxReal = action (
  (n) => n,
  'setKeysNumPerPageIdxReal',
)

export const setKeysCurPage = action (
  (n) => n,
  'setKeysCurPage',
)

export const setAssigneesNumPerPageIdx = action (
  (n) => n,
  'setAssigneesNumPerPageIdx',
)

export const setAssigneesNumPerPageIdxReal = action (
  (n) => n,
  'setAssigneesNumPerPageIdxReal',
)

export const setAssigneesCurPage = action (
  (n) => n,
  'setAssigneesCurPage',
)

export const keySetSelectedDeviceId = action (
  (deviceId) => deviceId,
  'keySetSelectedDeviceId',
)

export const keyGoToPageForKey = action (
  (deviceId) => deviceId,
  'keyGoToPageForKey',
)

export const updateCanLockInterface = action (
  (canLock) => canLock,
  'updateCanLockInterface',
)

// ------ not real actions, i.e. don't try to listen on them (listen instead on the action they
// call)

// --- used in the case that we want to automatically set stale keys to inactive (see config)
export const keyStatusStaleNoFetch = (deviceId) => keyStatusSet (StatusInactive, deviceId, false)
