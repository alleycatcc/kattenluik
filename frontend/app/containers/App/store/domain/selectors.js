// ------ sending a 'slice' parameter to a selector function is a way to get it to accept a more
// specific chunk of the reducer: useful for calling the selector from the reducer.

import {
  pipe, compose, composeRight,
  filter, whenOk,
  tap, map, id, noop, ok, nil, ifOk, ifNil,
  eq, prop, reduce, assocM, prepend, list,
  anyAgainst, deconstruct, find,
  mapTuples, lets, updateM, defaultTo,
  recurry, addIndex, appendM,
} from 'stick-js/es'

import { weakMapMemoize as memoize, } from 'reselect'

import { ierror, logWith, length, pluckN, } from 'alleycat-js/es/general'

import { selectT as selectAssignees, } from '../../../Assignee/selectors'
import {
  selectAuthorizations as selectKeyAuthorizations,
  selectPrivileges as selectKeyPrivileges,
  selectT as selectKeys,
  selectSlice as selectKeysSlice,
} from '../../../Key/selectors'

import { initialState, } from './reducer'
import { benchmark, initSelectors, allOk, reduceX, } from '../../../../common'
import { requestIsPending, foldIfRequestResults, statusTag, } from '../../../../types'

const { select, selectTop, selectVal, } = initSelectors ('domain', initialState)

const latestBench = { current: null, }

const bench = (tag, f) => benchmark (
  tag, f, (tag, delta) => latestBench.current = [tag, delta],
)

export const selectLatestBench = select (
  'latestBench',
  [],
  () => latestBench.current
)

// --- @todo stick

const remapAsMap = (f) => (xs) => {
  const m = new Map
  for (const x of xs) {
    const [k, v] = f (x)
    m.set (k, v)
  }
  return m
}

const mapUpdateM = recurry (3) (
  (k) => (f) => (m) => m.set (k, f (m.get (k))),
)
const mapPath = recurry (2) (
  (xs) => (m) => {
    let j = m
    for (const x of xs) {
      if (!ok (j)) return j
      else j = j.get (x)
    }
  return j
  },
)

const mapSetM = recurry (3) (
  (k) => (v) => (m) => m.set (k, v),
)
const updateWithDefaultM = recurry (4) (
  (key) => (defaultF) => (f) => updateM (key) (defaultTo (defaultF) >> f),
)
const updateWithDefaultVM = recurry (4) (
  (key) => (defaultV) => updateWithDefaultM (key, () => defaultV),
)
const updateMapWithDefaultM = recurry (4) (
  (key) => (defaultF) => (f) => mapUpdateM (key) (defaultTo (defaultF) >> f),
)
const remapMapTuples = (f) => (m) => {
  const ret = []
  m.forEach ((v, k) => {
    ret.push (f (k, v))
  })
  return ret
}
export const reduceMap = recurry (3) (
  (f) => (acc) => (m) => {
    let curAcc = acc
    for (const [k, v] of m)
      curAcc = f (curAcc, [k, v])
    return curAcc
  }
)

const remapMapTuplesX = addIndex (remapMapTuples)

export const selectError = selectVal ('error')

export const selectAdminAuthenticate = selectVal ('adminAuthenticate')
export const selectAdminRead = selectVal ('adminRead')
export const selectAdminReadKeyId = select (
  'adminReadKeyId',
  [selectAdminRead],
  foldIfRequestResults (
    prop ('deviceId'), () => null,
  ),
)

export const selectAdminCallPending = select (
  'adminCallPending',
  [selectVal ('adminAuthenticate'), selectVal ('adminRead')],
  list >> anyAgainst (requestIsPending),
)

export const selectColors = selectVal ('colors')
export const selectDoors = selectVal ('doors')

// --- @future this happens every time a slider gets moved; possible to improve?
const selectKeyAuthorizationsByX = select (
  'keyAuthorizationsByX',
  [selectKeyAuthorizations],
  (authorizations) => authorizations | reduce (
    ([byKeyId, byDoorId], auth) => auth | deconstruct (
      ({ keyId, doorId, }) => [
        byKeyId | updateMapWithDefaultM (keyId, () => new Map, mapSetM (doorId, auth)),
        byDoorId | updateMapWithDefaultM (doorId, () => new Map, mapSetM (keyId, auth)),
      ],
    ),
    [new Map, new Map],
  ),
)

export const selectKeyAuthorizationsByKeyId = select (
  'keyAuthorizationsByKeyId',
  [selectKeyAuthorizationsByX],
  ([byKeyId, _]) => byKeyId,
)

const selectKeyPrivilegesByKeyId = select (
  'keyPrivilegesByKeyId',
  [selectKeyPrivileges],
  (privileges) => privileges | reduce (
    (m, priv) => priv | deconstruct (
      ({ keyId, privilegeId, ... _ }) => m | updateMapWithDefaultM (
        keyId, () => new Map, mapSetM (privilegeId, priv),
      ),
    ),
    new Map,
  ),
)

// --- this is the list we want when editing the color of a key.
export const selectColorsEdit = select (
  'colors',
  [selectColors],
  prepend ({ id: null, color: null, }),
)

export const selectColorsEditReverse = select (
  'colorsEditReverse',
  [selectColorsEdit],
  mapTuples (
    (colorId, { color, }) => [color, Number (colorId)],
  ),
)

const selectGetAuthorizationsForKey = select (
  'getAuthorizationsForKey',
  [selectKeyAuthorizationsByKeyId],
  // --- @todo profile
  (authorizationsByKeyId) => memoize (
    (keyId) => authorizationsByKeyId.get (keyId) | ifNil (
      () => [],
      (byDoorId) => byDoorId | remapMapTuples (
        // --- authId can be null
        (doorId, auth) => ({ doorId, authId: auth | ifOk (prop ('id'), () => null), })
      )
    ),
  )
)

const selectGetPrivilegesForKey = select (
  'getPrivilegesForKey',
  [selectKeyPrivilegesByKeyId],
  (privilegesByKeyId) => memoize (
    (keyId) => privilegesByKeyId.get (keyId) | ifNil (
      () => [],
      (m) => m | remapMapTuplesX (
        (_, { keyPrivilegeId, privilegeId, privilege: privilegeText, }, idx) => ({
          key: idx,
          privilegeId,
          privilegeText,
          // --- can be null
          keyPrivilegeId,
        }),
      ),
    ),
  ),
)

export const selectStatuses = selectVal ('statuses')

const selectStatusesByTag = select (
  'statusesByTag',
  [selectStatuses],
  (statuses) => statuses | remapAsMap (
    (status) => [status | statusTag, status],
  ),
)

// --- prepare `keys` list for use in the component
// --- at the moment it's actually just a map written as a reduce, but could be used to prepare
// useful lookups at some point if that's needed.
const selectKeysPrepareComponent = select (
  'keysPrepareComponent',
  [selectKeysSlice, selectStatusesByTag, selectAdminReadKeyId, selectGetAuthorizationsForKey, selectGetPrivilegesForKey],
  (keys, statusesByTag, readDeviceId, getAuthorizationsForKey, getPrivilegesForKey) => keys | reduceX (
    ([keys], {
      id: keyId, assigneeId, color, custom, deviceId, hasSecret, name, status: statusTag, activityTimestamp,
    }, _idx) => [keys | appendM (
      lets (
        () => readDeviceId === deviceId,
        () => getAuthorizationsForKey (keyId),
        () => getPrivilegesForKey (keyId),
        (isRead, authorizations, privileges) => ({
          // --- derived
          authorizations, isRead, privileges,
          // --- from db, with id renamed to keyId
          keyId, assigneeId, color, custom, deviceId, hasSecret, name, status: statusesByTag.get (statusTag), activityTimestamp,
        }),
      ),
    )],
    [[]],
  ),
)

export const selectKeysComponent = select (
  'keysComponent',
  [selectKeysPrepareComponent],
  ([keys]) => keys,
)

export const selectAppInitted = select (
  'appInitted',
  [selectAssignees, selectKeyAuthorizations, selectColors, selectKeys, selectKeyPrivileges],
  list >> allOk,
)

const selectKey = select (
  'key',
  [selectKeys],
  (keys) => memoize ((deviceId) => keys | find (
    prop ('deviceId') >> eq (deviceId),
  )),
)

// --- assumes valid key id.
const selectAssigneeIdForKeyId = select (
  'assigneeIdForKeyId',
  [selectKey],
  // --- whenOk for the race condition in the instant when the key id being checked was received
  // through the read call but the key hasn't been added to the list yet.
  (keyById) => keyById >> whenOk (prop ('assigneeId')),
)

export const selectGetKeysForAssignee = select (
  'getKeysForAssignee',
  [selectKeys],
  (keys) => memoize ((assigneeId) => keys | filter (
    ({ assigneeId: assigneeIdFromKey, }) => assigneeIdFromKey === assigneeId,
  )),
)

// --- assumes valid key id.
export const selectAssigneeNameByKeyId = select (
  'assigneeNameByKeyId',
  [selectKey],
  // --- whenOk for the race condition in the instant when the key id being checked was received
  // through the read call but the key hasn't been added to the list yet.
  (keyById) => keyById >> whenOk (prop ('name')),
)

export const selectGetKeysForAssigneeSimple = select (
  'getKeysForAssigneeSimple',
  [selectGetKeysForAssignee],
  (f) => memoize (f >> map (pluckN (['id', 'deviceId', 'color']))),
)

// --- @todo this can disappear (it's now part of the assignee list)
export const selectGetNumKeysForAssignee = select (
  'numKeysForAssignee',
  [selectGetKeysForAssignee],
  (f) => memoize (f >> length),
)

export const selectNumKeysForAssigneeByKeyId = select (
  'numKeysForAssigneeByKeyId',
  [selectGetNumKeysForAssignee, selectAssigneeIdForKeyId],
  (getNumKeysForAssignee, assigneeIdForKeyId) => memoize (
    assigneeIdForKeyId >> getNumKeysForAssignee,
  ),
)

// --- reverse mapping: given doorId and keyId, find the entry in the auth table.
export const selectFindAuthorizationId = select (
  'selectFindAuthorizationId',
  [selectKeyAuthorizationsByKeyId],
  (keyAuthorizationsByKeyId) => memoize (
    (doorId, keyId) => keyAuthorizationsByKeyId | mapPath ([keyId, doorId])
    | whenOk (prop ('id'))
  ),
)

export const selectCheckKeyAuthorized = select (
  'checkKeyAuthorized',
  [selectFindAuthorizationId],
  (findAuthorizationId) => memoize (
    (doorId, keyId) => ok (findAuthorizationId (doorId, keyId)),
  )
)

export const selectGrantedDeniedAllDoors = select (
  'grantedDeniedAllDoors',
  [selectDoors, selectFindAuthorizationId],
  (doors, findAuthorizationId) => memoize (
    (keyId) => doors | reduce (
      ([allGranted, allDenied], { id: doorId, }) => lets (
        () => ok (findAuthorizationId (doorId, keyId)),
        (isAuthorized) => [
          allGranted && isAuthorized,
          allDenied && !isAuthorized,
        ],
      ),
      [true, true],
    ),
  ),
)

export const selectGrantedDeniedAllPrivileges = select (
  'grantedDeniedAllPrivileges',
  [selectKeyPrivilegesByKeyId],
  (keyPrivilegesByKeyId) => memoize (
    (keyId) => keyPrivilegesByKeyId.get (keyId) | ifNil (
      () => [false, true],
      (m) => m | reduceMap (
        ([allGranted, allDenied], [_, { keyPrivilegeId, }]) => lets (
          () => keyPrivilegeId | ok,
          (granted) => [
            allGranted && granted,
            allDenied && !granted,
          ],
        ),
        [true, true],
      ),
    ),
  ),
)

export const selectStaleKeysAsSet = selectVal ('staleKeys')

export const selectStaleKeysAsList = select (
  'selectStaleKeysAsList',
  [selectStaleKeysAsSet],
  (keysSet) => [... keysSet.keys ()],
)

export const selectUptimeApproximateByMonth = selectVal ('uptimeApproximateByMonth')

const selectAssigneeHasKeysMap = select (
  'assigneeHasKeysMap',
  [selectAssignees, selectKeys],
  (assignees, keys) => {
    return assignees | reduce (
      (acc, assignee) => acc | assocM (
        assignee.id, new Set (keys | map (key => key.name)).has (assignee.name)),
      {},
    )
  },
)

export const selectAssigneeHasKeys = select (
  'assigneeHasKeys',
  [selectAssigneeHasKeysMap],
  (hasKeysMap) => (assigneeId) => hasKeysMap [assigneeId],
)
