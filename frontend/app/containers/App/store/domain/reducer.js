import {
  pipe, compose, composeRight,
  tap, map, id, noop, ok, nil, ifOk, ifNil,
  assoc, merge, ifFalse, ifTrue, update,
  ifPredicate, recurry,
} from 'stick-js/es'

import { cata, Nothing, } from 'alleycat-js/es/bilby'
import { RequestInit, RequestLoading, RequestError, RequestResults, } from 'alleycat-js/es/fetch'
import { logWith, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import {
  halt,
  adminKeyAuthenticateCompleted,
  adminKeyReadCompleted,
  colorsFetchCompleted,
  dbImport,
  dbReset,
  doorsFetchCompleted,
  keyCheck,
  keyMarkAsStale,
  keyRead,
  keyRegenerate,
  uptimeApproximateByMonthFetchCompleted,
} from '../../actions/main'

import { reducer, } from '../../../../common'
import { StatusActive, StatusInactive, StatusMissing, } from '../../../../types'

// --- @todo stick / common
const setContainsExactly = recurry (2) (
  (xs) => (s) => {
    if (s.size !== xs.length) return false
    for (const x of xs)
      if (!s.has (x))
        return false
    return true
  },
)

const ifSetContainsExactly = setContainsExactly >> ifPredicate

export const initialState = {
  // --- `error=true` means the reducer is totally corrupted or some other fatal error has occurred
  // and the app should halt.
  error: false,

  // --- indicates result of 'check key'.
  // :: null | Request Bool
  adminAuthenticate: RequestInit,
  // :: Request { Bool |isNewKey|, Bool |isNewSecret|, Bool |readOk|, String |deviceId| }
  adminRead: RequestInit,
  colors: void 8,
  doors: void 8,
  statuses: [StatusActive, StatusInactive, StatusMissing],
  // --- we keep track of the stale keys in this structure, which ought to be cleaner than adding a
  // property to the objects in `keys`
  staleKeys: new Set,
  uptimeApproximateByMonth: void 8,
}

const clearData = merge ({
  colors: null,
  doors: null,
})

const reducerTable = makeReducer (
  halt, () => assoc ('error', true),
  adminKeyAuthenticateCompleted, ({ rcomplete, isUnlockingScreen, }) => assoc (
    'adminAuthenticate', rcomplete | ifNil (
      () => RequestInit,
      cata ({
        RequestCompleteError: (_) => RequestError (null),
        RequestCompleteSuccess: ({ data: { authenticated, readOk, }}) => readOk | ifFalse (
          () => RequestError (null),
          () => RequestResults ({ authenticated, isUnlockingScreen, }),
        ),
      }),
    ),
  ),
  adminKeyReadCompleted, (rcomplete) => assoc (
    'adminRead', rcomplete | cata ({
      RequestCompleteError: (_) => RequestError (null),
      RequestCompleteSuccess: ({ data: { isNewKey, isNewSecret, readOk, deviceId, }}) => readOk | ifTrue (
        () => RequestResults ({
          isNewKey, isNewSecret, deviceId,
        }),
        () => RequestError (null),
      ),
    }),
  ),
  colorsFetchCompleted, (rcomplete) => assoc (
    'colors', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results,
    }),
  ),
  dbImport, () => clearData (),
  dbReset, () => clearData (),
  doorsFetchCompleted, (rcomplete) => assoc (
    'doors', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results,
    }),
  ),
  keyCheck, (... _) => merge ({
    adminAuthenticate: RequestLoading (Nothing),
    adminRead: RequestInit,
  }),
  keyRead, () => merge ({
    adminAuthenticate: RequestInit,
    adminRead: RequestLoading (Nothing),
  }),
  // --- regenerate is basically just a read call, but erases the secret in the db first.
  keyRegenerate, () => merge ({
    adminAuthenticate: RequestInit,
    adminRead: RequestLoading (Nothing),
  }),
  keyMarkAsStale, ({ deviceIds, }) => update (
    'staleKeys',
    (curStaleKeys) => curStaleKeys | ifSetContainsExactly (deviceIds) (
      () => curStaleKeys,
      () => new Set (deviceIds),
    ),
  ),
  uptimeApproximateByMonthFetchCompleted, (rcomplete) => assoc (
    'uptimeApproximateByMonth', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results,
    }),
  )
)

export default reducer ('domain', initialState, reducerTable)
