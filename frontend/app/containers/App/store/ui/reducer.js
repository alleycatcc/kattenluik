import {
  pipe, compose, composeRight,
  assoc,
} from 'stick-js/es'

import { logWith, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import {
  lockInterface, unlockInterface,
  updateCanLockInterface,
} from '../../actions/main'

import { reducer, } from '../../../../common'

export const initialState = {
  canLockInterface: null,
  interfaceLocked: false,
}

const reducerTable = makeReducer (
  lockInterface, () => assoc ('interfaceLocked', true),
  unlockInterface, () => assoc ('interfaceLocked', false),
  updateCanLockInterface, assoc ('canLockInterface'),
)

export default reducer ('ui', initialState, reducerTable)
