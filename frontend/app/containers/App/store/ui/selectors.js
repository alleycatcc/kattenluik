import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { initialState, } from './reducer'
import { initSelectors, } from '../../../../common'

const { select, selectTop, selectVal, } = initSelectors (
  'ui',
  initialState,
)

export const selectInterfaceLocked = selectVal ('interfaceLocked')

export const selectCanLockInterface = selectVal ('canLockInterface')
