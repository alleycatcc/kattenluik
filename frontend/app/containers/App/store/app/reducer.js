import {
  pipe, compose, composeRight,
  merge,
} from 'stick-js/es'

import { makeReducer, } from 'alleycat-js/es/redux'

import { localeChange, } from '../../actions/main'
import { reducer, } from '../../../../common'

export const initialState = {
  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,
  locale: 'en',
}

const reducerTable = makeReducer (
  localeChange, (locale) => merge ({ locale, }),
)

export default reducer ('app', initialState, reducerTable)
