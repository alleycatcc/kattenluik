import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import { initialState, } from './reducer'
import { initSelectors, } from '../../../../common'

const { select, selectTop, selectVal, } = initSelectors (
  'app',
  initialState,
)

export const selectLocale = selectVal ('locale')
