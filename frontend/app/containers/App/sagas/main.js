import {
  pipe, compose, composeRight,
  map, prop, ifTrue, sprintf1,
  defaultTo, path, tap, recurry, ok,
  compact, whenTrue,
} from 'stick-js/es'

import { all, call, put, select, takeEvery, takeLatest, delay, } from 'redux-saga/effects'

import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { requestJSON, defaultOpts, resultFoldMap, resultFold, } from 'alleycat-js/es/fetch'
import { info, iwarn, logWith, } from 'alleycat-js/es/general'
import {} from 'alleycat-js/es/predicate'
import { EffAction, EffSaga, EffNoEffect, } from 'alleycat-js/es/saga'

import {
  halt as a_halt,
  adminKeyAuthenticateCompleted as a_adminKeyAuthenticateCompleted,
  adminKeyReadCompleted as a_adminKeyReadCompleted,
  appMounted as a_appMounted,
  assigneeAdd as a_assigneeAdd,
  assigneeCustomSet as a_assigneeCustomSet,
  assigneeNameSet as a_assigneeNameSet,
  assigneeRemove as a_assigneeRemove,
  assigneeSort as a_assigneeSort,
  assigneesFetch as a_assigneesFetch,
  assigneesFetchCompleted as a_assigneesFetchCompleted,
  checkShouldLock as a_checkShouldLock,
  checkStaleKeys as a_checkStaleKeys,
  colorsFetch as a_colorsFetch,
  colorsFetchCompleted as a_colorsFetchCompleted,
  dbImport as a_dbImport,
  dbReset as a_dbReset,
  doorsFetch as a_doorsFetch,
  doorsFetchCompleted as a_doorsFetchCompleted,
  keyAssigneeSet as a_keyAssigneeSet,
  keyAuthorizationAdd as a_keyAuthorizationAdd,
  keyAuthorizationRemove as a_keyAuthorizationRemove,
  keyAuthorizationsFetch as a_keyAuthorizationsFetch,
  keyAuthorizationsFetchCompleted as a_keyAuthorizationsFetchCompleted,
  keyCheck as a_keyCheck,
  keyColorSet as a_keyColorSet,
  keyCustomSet as a_keyCustomSet,
  keyGoToPageForKey as a_keyGoToPageForKey,
  keyPrivilegeAdd as a_keyPrivilegeAdd,
  keyPrivilegeRemove as a_keyPrivilegeRemove,
  keyPrivilegesFetch as a_keyPrivilegesFetch,
  keyPrivilegesFetchCompleted as a_keyPrivilegesFetchCompleted,
  keyRead as a_keyRead,
  keyRegenerate as a_keyRegenerate,
  keyRemove as a_keyRemove,
  keySort as a_keySort,
  keysFetch as a_keysFetch,
  keysFetchCompleted as a_keysFetchCompleted,
  keySetSelectedDeviceId as a_keySetSelectedDeviceId,
  keyStatusSet as a_keyStatusSet,
  keyStatusStaleNoFetch as a_keyStatusStaleNoFetch,
  keyMarkAsStale as a_keyMarkAsStale,
  lockInterface as a_lockInterface,
  setKeysNumPerPageIdx as a_setKeysNumPerPageIdx,
  setKeysNumPerPageIdxReal as a_setKeysNumPerPageIdxReal,
  setKeysCurPage as a_setKeysCurPage,
  setAssigneesNumPerPageIdx as a_setAssigneesNumPerPageIdx,
  setAssigneesNumPerPageIdxReal as a_setAssigneesNumPerPageIdxReal,
  setAssigneesCurPage as a_setAssigneesCurPage,
  unlockInterface as a_unlockInterface,
  updateCanLockInterface as a_updateCanLockInterface,
  uptimeApproximateByMonthFetch as a_uptimeApproximateByMonthFetch,
  uptimeApproximateByMonthFetchCompleted as a_uptimeApproximateByMonthFetchCompleted,
} from '../actions/main'
import {} from '../store/app/selectors'
import {
  selectAdminReadKeyId,
} from '../store/domain/selectors'
import {
  selectCurPage as selectAssigneesCurPage,
  selectNumPerPage as selectAssigneesNumPerPage,
} from '../../Assignee/selectors'
import {
  selectCurPage as selectKeysCurPage,
  selectNumPerPage as selectKeysNumPerPage,
  selectListLocationForDeviceId as selectKeyListLocationForDeviceId,
} from '../../Key/selectors'

import { doApiCall, saga, toastError, toastInfo, toastWarnOptions, } from '../../../common'
import config from '../../../config'
import { statusTag, } from '../../../types'

const configTop = config | configure.init
const staleKeyAutoInactive = configTop.get ('domain.staleKeyAutoInactive')

const delayEffect = recurry (2) (
  (ms) => function * (effect) {
    yield delay (ms)
    yield effect
  }
)

// --- to avoid showing the warning too often
let lastWarnedCanNotLock = 0

// ------ @todo config
const WARN_FREQUENCY_CAN_NOT_LOCK = 60000
const UPTIME_FETCHER_INTERVAL = 10000
const CHECK_STALE_INTERVAL = 10000
const CHECK_SHOULD_LOCK_INTERVAL = 5000

function *s_initData () {
  yield put (a_assigneesFetch ())
  yield put (a_colorsFetch ())
  yield put (a_doorsFetch ())
  yield put (a_keyAuthorizationsFetch ())
  yield put (a_keyPrivilegesFetch ())
  yield put (a_keysFetch ())
}

function *s_adminKeyReadCompleted (_) {
  const deviceId = yield select (selectAdminReadKeyId)
  yield put (a_keySetSelectedDeviceId (deviceId))
}

function *s_appMounted () {
  yield put (a_checkShouldLock ())
  yield call (s_initData)
  // --- long-running (infinite) tasks: launch in parallel
  yield all ([
    call (s_uptimeFetcher),
    call (s_staleKeysChecker),
    call (s_shouldLockChecker),
  ])
}

function *s_staleKeysChecker () {
  yield put (a_checkStaleKeys ())
  yield delayEffect (CHECK_STALE_INTERVAL, call (s_staleKeysChecker))
}

function *s_shouldLockChecker () {
  yield put (a_checkShouldLock ())
  yield delayEffect (CHECK_SHOULD_LOCK_INTERVAL, call (s_shouldLockChecker))
}

function *s_assigneesFetch () {
  yield call (doApiCall, {
    url: '/api/assignees',
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_assigneesFetchCompleted),
    imsgDecorate: 'Error fetching assignees',
  })
}

function* s_assigneeAdd ({ name, custom, }) {
  yield call (doApiCall, {
    url: '/api/assignee',
    optsMerge: {
      method: 'PUT',
      body: JSON.stringify ({
        data: { name, custom },
      }),
    },
    continuation: EffSaga (s_assigneeModifyCompleted),
    imsgDecorate: 'Error setting assignee custom',
  })
}

function *s_assigneeCustomSet ({ id, custom }) {
  yield call (s_assigneeUpdate, { id, custom })
}

function *s_assigneeModifyCompleted (rcomplete) {
  yield all (map (put, rcomplete | cata ({
    RequestCompleteError: (_) => [a_halt ()],
    RequestCompleteSuccess: (_) => [a_assigneesFetch (), a_keysFetch ()],
  })))
}

function *s_assigneeNameSet ({ id, name }) {
  yield call (s_assigneeUpdate, { id, name })
}

function *s_assigneeRemove ({ id, allowAssignedKeys }) {
  yield call (doApiCall, {
    url: '/api/assignee/',
    optsMerge: {
      method: 'DELETE',
      body: JSON.stringify ({
        data: { id, allowAssignedKeys },
      }),
    },
    continuation: EffSaga (s_assigneeModifyCompleted),
    imsgDecorate: 'Error removing assignee',
  })
}

function *s_assigneeUpdate ({ id, name, custom, }) {
  yield call (doApiCall, {
    url: '/api/assignee/',
    optsMerge: {
      method: 'PATCH',
      body: JSON.stringify ({
        data: { id, name, custom },
      }),
    },
    continuation: EffSaga (s_assigneeModifyCompleted),
    imsgDecorate: 'Error setting assignee',
  })
}

// --- this also stores 'can lock' in the reducer
function *s_checkShouldLock () {
  function *process (rcomplete) {
    yield all (rcomplete | cata ({
      RequestCompleteError: (_) => [call (console.error, 'error while checking should lock interface')],
      RequestCompleteSuccess: ({ data: { shouldLock, }}) => {
        if (shouldLock === null) {
          const now = Date.now ()
          if (now - lastWarnedCanNotLock > WARN_FREQUENCY_CAN_NOT_LOCK) {
            toastWarnOptions ({ autoClose: 50000, }, 'Warning, screen locking disabled. Please ensure there is at least one key with both status and the “unlock-interface” privilege set to active.')
            lastWarnedCanNotLock = now
          }
          return [put (a_updateCanLockInterface (false))]
        }
        lastWarnedCanNotLock = 0

        // --- @future we should make a new call called 'can lock interface', and call it when the
        // lock-interface privileges change -- how it is now it takes a few seconds for the lock
        // icon to update.
        return compact ([
          put (a_updateCanLockInterface (true)),
          // --- note, no effect on shouldLock=false: unlocking after an admin key was used happens
          // in _s_keyAuthenticateCompleted, so it happens immediately, not here, which would
          // involve waiting for the next timeout.
          shouldLock && put (a_lockInterface ()),
        ])
      },
    }))
  }
  yield call (doApiCall, {
    url: '/api/should-lock-interface',
    continuation: EffSaga (process),
    oops: console.error,
  })
}

function *s_checkStaleKeys () {
  function *process (rcomplete) {
    yield all (rcomplete | cata ({
      RequestCompleteError: (_) => [call (console.error, 'error while checking stale keys')],
      RequestCompleteSuccess: ({ data: deviceIds, }) => {
        const n = deviceIds.length
        return staleKeyAutoInactive | ifTrue (
          () => {
            if (n === 0) return []
            info (n | sprintf1 ('setting %d stale key(s) to inactive'))
            // --- a list of actions which each update one key
            return deviceIds | map (put << a_keyStatusStaleNoFetch)
          },
          () => {
            if (n !== 0) info (n | sprintf1 ('marking %d stale keys'))
            // --- a list with one element, a batched action
            return [put (a_keyMarkAsStale (deviceIds))]
          },
        )
      },
    }))
  }
  // --- @todo why not make a selector?
  const interfaceLocked = yield select ((state) =>
    state
    | path (['ui', 'interfaceLocked'])
    | defaultTo (() => iwarn ('s_checkStaleKeys (): unable to determine `interfaceLocked`'))
  )
  if (interfaceLocked) return
  info ('checking for stale keys')
  yield call (doApiCall, {
    url: '/api/stale-keys',
    continuation: EffSaga (process),
    oops: console.error,
  })
}

// --- [keyId, doorId]
function *s_keyAuthorizationAdd (tuples) {
  yield call (doApiCall, {
    url: '/api/key-authorizations',
    optsMerge: {
      method: 'PUT',
      body: JSON.stringify ({
        data: { data: tuples, }
      }),
    },
    continuation: EffSaga (s_keyAuthorizationModifyCompleted),
    imsgDecorate: 'Error setting key authorizations',
  })
}

function *s_keyAuthorizationsFetch () {
  yield call (doApiCall, {
    url: '/api/key-authorizations',
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_keyAuthorizationsFetchCompleted),
    imsgDecorate: 'Error fetching key authorizations',
  })
}

function *s_keyAuthorizationModifyCompleted (rcomplete) {
  yield all (map (put, rcomplete | cata ({
    RequestCompleteError: (_) => [a_halt ()],
    RequestCompleteSuccess: (_) => [a_keyAuthorizationsFetch ()],
  })))
}

function *s_keyAuthorizationRemove (ids) {
  yield call (doApiCall, {
    url: '/api/key-authorizations',
    optsMerge: {
      method: 'DELETE',
      body: JSON.stringify ({
        data: { ids, },
      }),
    },
    continuation: EffSaga (s_keyAuthorizationModifyCompleted),
    imsgDecorate: 'Error removing key authorizations',
  })
}

function *s_colorsFetch () {
  yield call (doApiCall, {
    url: '/api/colors',
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_colorsFetchCompleted),
    imsgDecorate: 'Error fetching colors',
  })
}

function *s_doorsFetch () {
  yield call (doApiCall, {
    url: '/api/doors',
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_doorsFetchCompleted),
    imsgDecorate: 'Error fetching doors',
  })
}

function *s_keyAssigneeSet ({ deviceId, assigneeId, }) {
  yield call (doApiCall, {
    url: '/api/key/' + deviceId,
    optsMerge: {
      method: 'PATCH',
      body: JSON.stringify ({
        data: { deviceId, assigneeId, },
      }),
    },
    continuation: EffSaga (s_keyModifyCompleted),
    imsgDecorate: 'Error setting assignee',
  })
}

function *s_keyAuthenticate ({ unlockScreen, }) {
  yield call (doApiCall, {
    url: '/api/admin/check',
    continuation: EffSaga (unlockScreen ? s_keyAuthenticateCompletedUnlockScreen : s_keyAuthenticateCompletedAdmin),
    imsgDecorate: 'Error checking key',
  })
}

function *s_keyAuthenticateCompletedUnlockScreen (rcomplete) {
  yield call (_s_keyAuthenticateCompleted, rcomplete, true)
}

function *s_keyAuthenticateCompletedAdmin (rcomplete) {
  yield call (_s_keyAuthenticateCompleted, rcomplete, false)
}

function *_s_keyAuthenticateCompleted (rcomplete, isUnlockingScreen) {
  yield put (a_adminKeyAuthenticateCompleted (rcomplete, isUnlockingScreen))
  yield all (rcomplete | cata ({
    // --- no need to crash the app on error -- the oops bubble is enough.
    RequestCompleteError: (_) => [],
    RequestCompleteSuccess: ({ data: { readOk, canUnlockScreen, ..._ }}) => [
      ... readOk | ifTrue (
        () => isUnlockingScreen | ifTrue (
          () => canUnlockScreen | ifTrue (
            () => [put (a_unlockInterface ())],
            () => {
              // --- @todo sometimes we use call and sometimes not
              toastError ('This key is not authorized to unlock the screen.')
              return []
            },
          ),
          () => [],
        ),
        () => [call (toastError, isUnlockingScreen ? 'Read error or internal I/O error.' : 'Read error.')],
      ),
      // --- i.e., show the monkey for 2 seconds
      call (delayEffect, 2000, put (a_adminKeyAuthenticateCompleted (null))),
    ],
  }))
}

// --- @todo combine color set and custom into generic function?
function *s_keyColorSet ({ deviceId, colorId, }) {
  yield call (doApiCall, {
    url: '/api/key/' + deviceId,
    optsMerge: {
      method: 'PATCH',
      body: JSON.stringify ({
        data: { deviceId, colorId, },
      }),
    },
    continuation: EffSaga (s_keyModifyCompleted),
    imsgDecorate: 'Error setting color',
  })
}

function *s_keyCustomSet ({ deviceId, custom, }) {
  yield call (doApiCall, {
    url: '/api/key/' + deviceId,
    optsMerge: {
      method: 'PATCH',
      body: JSON.stringify ({
        data: { deviceId, custom, },
      }),
    },
    continuation: EffSaga (s_keyModifyCompleted),
    imsgDecorate: 'Error setting color',
  })
}

function *s_keyGoToPageForKey (deviceId) {
  const lookupPage = yield select (selectKeyListLocationForDeviceId)
  const [page, _] = lookupPage (deviceId)
  yield put (a_setKeysCurPage (page))
}

function *s_keyModifyCompleted (rcomplete, fetch=true) {
  yield all (map (put, rcomplete | cata ({
    RequestCompleteError: (_) => [a_halt ()],
    RequestCompleteSuccess: (_) => fetch | ifTrue (
      () => [a_keysFetch ()],
      () => [],
    ),
  })))
}

/* The 'no fetch' version of this action / saga updates the key directly in the store and doesn't
 * result in a new fetch of all the keys after that. We need this at the moment so that the logic
 * which sets a stale key to inactive doesn't cause a race condition if someone updates that key at
 * the same moment.
 *
 * The only thing to be careful about is if the REST call which updates the status fails. This
 * results in a `halt` and a force reload of the page, so we're good.
 *
 * @todo Make more or even most of the key update functionality 'no fetch' by default, which will
 * also result in a more responsive UI.
 */

function *s_keyModifyCompletedNoFetch (rcomplete) {
  yield call (s_keyModifyCompleted, rcomplete, false)
}

function* s_keyPrivilegeAdd (data) {
  yield call (doApiCall, {
    url: '/api/key-privileges',
    optsMerge: {
      method: 'PUT',
      body: JSON.stringify ({
        data: { data, }
      }),
    },
    continuation: EffSaga (s_keyPrivilegeModifyCompleted),
    imsgDecorate: 'Error setting key privileges',
  })
}

function *s_keyPrivilegesFetch (strategy='full') {
  yield call (doApiCall, {
    url: '/api/key-privileges?strategy=' + String (strategy),
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_keyPrivilegesFetchCompleted),
    imsgDecorate: 'Error fetching key privileges',
  })
}

function *s_keyPrivilegeModifyCompleted (rcomplete) {
  yield all (map (put, rcomplete | cata ({
    RequestCompleteError: (_) => [a_halt ()],
    RequestCompleteSuccess: (_) => [a_checkShouldLock (), a_keyPrivilegesFetch ()],
  })))
}

function *s_keyPrivilegeRemove (ids) {
  yield call (doApiCall, {
    url: '/api/key-privileges',
    optsMerge: {
      method: 'DELETE',
      body: JSON.stringify ({
        data: { ids, },
      }),
    },
    continuation: EffSaga (s_keyPrivilegeModifyCompleted),
    imsgDecorate: 'Error removing key privileges',
  })
}

function *s_keyRead () {
  yield call (doApiCall, {
    url: '/api/admin/read',
    continuation: EffSaga (s_keyReadCompleted),
    imsgDecorate: 'Error reading key',
  })
}

function *s_keyReadCompleted (rcomplete) {
  yield put (a_adminKeyReadCompleted (rcomplete))
  yield all (rcomplete | cata ({
    // --- no need to crash the app on error -- the oops bubble is enough.
    RequestCompleteError: (_) => [],
    RequestCompleteSuccess: ({ data: { readOk, ..._ }}) => [
      ... readOk | ifTrue (
        () => [],
        // --- when unlocking screen, this failure could also be due to a db error.
        () => [call (toastError, 'Read error.')],
      ),
      put (a_keysFetch ()),
      put (a_keyPrivilegesFetch ()),
    ],
  }))
}

function *s_keyRegenerate () {
  yield call (doApiCall, {
    url: '/api/admin/regenerate-secret',
    // --- very similar to read
    continuation: EffSaga (s_keyReadCompleted),
    imsgDecorate: 'Error regenerating secret',
  })
}

function *s_keysFetch () {
  yield call (doApiCall, {
    url: '/api/keys',
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_keysFetchCompleted),
    imsgDecorate: 'Error fetching keys',
  })
}

function *s_lockInterface () {
  yield call (s_initData)
}

function *s_unlockInterface () {
  yield call (s_initData)
}

// ------ when changing the number of results per page, we try to adjust curPage so that the first
// visible item before changing it stays visible after the adjustment.
// --- @todo then fix the logic which scrolls to the selected item, if any.

function *s_setKeysNumPerPageIdx (idx) {
  const npp = yield select (selectKeysNumPerPage)
  const cur = yield select (selectKeysCurPage)
  const keyIdx = npp * cur
  yield put (a_setKeysNumPerPageIdxReal (idx))
  const nppNew = yield select (selectKeysNumPerPage)
  yield put (a_setKeysCurPage (Math.floor (keyIdx / nppNew)))
}

function *s_setAssigneesNumPerPageIdx (idx) {
  const npp = yield select (selectAssigneesNumPerPage)
  const cur = yield select (selectAssigneesCurPage)
  const keyIdx = npp * cur
  yield put (a_setAssigneesNumPerPageIdxReal (idx))
  const nppNew = yield select (selectAssigneesNumPerPage)
  yield put (a_setAssigneesCurPage (Math.floor (keyIdx / nppNew)))
}

// ------

function *s_uptimeApproximateByMonthFetch () {
  yield call (doApiCall, {
    url: '/api/uptime-approximate-by-month',
    resultsModify: map ('data' | prop),
    continuation: EffAction (a_uptimeApproximateByMonthFetchCompleted),
    imsgDecorate: 'Error fetching uptime',
  })
}

function *s_uptimeFetcher () {
  yield put (a_uptimeApproximateByMonthFetch ())
  yield delayEffect (UPTIME_FETCHER_INTERVAL, call (s_uptimeFetcher))
}

function *s_keyRemove ({ deviceId, fetch, }) {
  yield call (doApiCall, {
    url: '/api/key/' + deviceId,
    optsMerge: {
      method: 'DELETE',
    },
    continuation: EffSaga (fetch ? s_keyModifyCompleted : s_keyModifyCompletedNoFetch),
    imsgDecorate: 'Error removing key',
  })
}

function *s_keySort () {
  yield put (a_setKeysCurPage (0))
}

function *s_assigneeSort () {
  yield put (a_setAssigneesCurPage (0))
}

function *s_keyStatusSet ({ deviceId, status, fetch, updateActivityTimestamp=false, }) {
  yield call (doApiCall, {
    url: '/api/key/' + deviceId,
    optsMerge: {
      method: 'PATCH',
      body: JSON.stringify ({
        data: { deviceId, status: status | statusTag, updateActivityTimestamp },
      }),
    },
    continuation: EffSaga (fetch ? s_keyModifyCompleted : s_keyModifyCompletedNoFetch),
    imsgDecorate: 'Error setting status',
  })
}

function *s_dbImportSuccess () {
  toastInfo ('Database import successful')
  yield call (s_initData)
}

function *s_dbImport (dbFile) {
  const formData = new FormData ()
  formData.append ('db-import-file', dbFile)
  yield call (doApiCall, {
    url: '/api/db-import',
    optsMerge: {
      method: 'POST',
      body: formData,
      headers: {
        contentType: 'multipart/form-data',
      },
    },
    continuation: EffSaga (s_dbImportSuccess),
    imsgDecorate: 'Error import database',
    oops: () => toastError ('Unable to import database'),
  })
}

function *s_dbReset () {
  yield call (doApiCall, {
    url: '/api/all',
    optsMerge: {
      method: 'DELETE',
    },
    continuation: EffSaga (s_initData),
    imsgDecorate: 'Error resetting database',
    oops: () => toastError ('Unable to reset database: the data is unchanged!'),
  })
}

export default function *sagaRoot () {
  yield all ([
    saga (takeLatest, a_adminKeyReadCompleted, s_adminKeyReadCompleted),
    saga (takeLatest, a_appMounted, s_appMounted),
    saga (takeLatest, a_assigneeAdd, s_assigneeAdd),
    saga (takeLatest, a_assigneeCustomSet, s_assigneeCustomSet),
    saga (takeLatest, a_assigneeNameSet, s_assigneeNameSet),
    saga (takeLatest, a_assigneeRemove, s_assigneeRemove),
    saga (takeLatest, a_assigneeSort, s_assigneeSort),
    saga (takeLatest, a_assigneesFetch, s_assigneesFetch),
    saga (takeLatest, a_checkShouldLock, s_checkShouldLock),
    saga (takeLatest, a_checkStaleKeys, s_checkStaleKeys),
    saga (takeLatest, a_colorsFetch, s_colorsFetch),
    saga (takeLatest, a_dbImport, s_dbImport),
    saga (takeLatest, a_dbReset, s_dbReset),
    saga (takeLatest, a_doorsFetch, s_doorsFetch),
    saga (takeLatest, a_keyAssigneeSet, s_keyAssigneeSet),
    saga (takeLatest, a_keyAuthorizationAdd, s_keyAuthorizationAdd),
    saga (takeLatest, a_keyAuthorizationsFetch, s_keyAuthorizationsFetch),
    saga (takeLatest, a_keyAuthorizationRemove, s_keyAuthorizationRemove),
    saga (takeLatest, a_keyCheck, s_keyAuthenticate),
    saga (takeLatest, a_keyColorSet, s_keyColorSet),
    saga (takeLatest, a_keyCustomSet, s_keyCustomSet),
    saga (takeLatest, a_keyGoToPageForKey, s_keyGoToPageForKey),
    saga (takeLatest, a_keyPrivilegeAdd, s_keyPrivilegeAdd),
    saga (takeLatest, a_keyPrivilegesFetch, s_keyPrivilegesFetch),
    saga (takeLatest, a_keyPrivilegeRemove, s_keyPrivilegeRemove),
    saga (takeLatest, a_keyRead, s_keyRead),
    saga (takeLatest, a_keyRegenerate, s_keyRegenerate),
    saga (takeLatest, a_keyRemove, s_keyRemove),
    saga (takeLatest, a_keySort, s_keySort),
    saga (takeLatest, a_keyStatusSet, s_keyStatusSet),
    saga (takeLatest, a_keysFetch, s_keysFetch),
    saga (takeLatest, a_lockInterface, s_lockInterface),
    saga (takeLatest, a_setKeysNumPerPageIdx, s_setKeysNumPerPageIdx),
    saga (takeLatest, a_setAssigneesNumPerPageIdx, s_setAssigneesNumPerPageIdx),
    saga (takeLatest, a_unlockInterface, s_unlockInterface),
    saga (takeLatest, a_uptimeApproximateByMonthFetch, s_uptimeApproximateByMonthFetch),
  ])
}
