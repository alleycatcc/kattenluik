import {
  pipe, compose, composeRight,
  map, dot, die, sprintf1,
  prop, whenOk, ifFalse, ifTrue, ok,
} from 'stick-js/es'

import React, { useEffect, useState, } from 'react'
import { BrowserRouter, useRoutes, } from 'react-router-dom'
import styled from 'styled-components'

import FontFaceObserver from 'fontfaceobserver'

import { then, recover, allP, } from 'alleycat-js/es/async'
import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { logWith, iwarn, setIntervalOn, } from 'alleycat-js/es/general'
import { whenTrueV, } from 'alleycat-js/es/predicate'
import { useMeasureWithCb, } from 'alleycat-js/es/react'
import { useReduxReducer, useSaga, } from 'alleycat-js/es/redux-hooks'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { createReducer, } from '../../redux'

import domainReducer from './store/domain/reducer'
import { selectError, } from './store/domain/selectors'
import uiReducer from './store/ui/reducer'
import appReducer from './store/app/reducer'
import keyReducer from '../Key/reducer'
import assigneeReducer from '../Assignee/reducer'

import { appMounted, } from './actions/main'
import saga from './sagas/main'

import Error from '../../components/Error'
import { ErrorBoundary, } from '../../components/ErrorBoundary'
import { Main, } from '../../containers/Main/Loadable'
import NotFoundPage from '../../containers/NotFoundPage'
import Toast from '../../components/Toast'

import { container, mediaPhone, mediaTablet, isMobileWidth, useWhy, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

const fontMainFamily = 'font.main.family' | configTop.get

const fontStyles = [
  ['normal', 'normal'],
  ['normal', 'italic'],
  ['bold', 'normal'],
  ['bold', 'italic'],
]

const loadFont = 'load' | dot

const startFontObserver = fontFamily => fontStyles | map (
    ([weight, style]) => new FontFaceObserver (fontFamily, { weight, style, }),
  )
  | map (loadFont)
  | allP
  // --- doesn't always die on failure @todo
  | recover ((fontDetails) => die (
    fontFamily | sprintf1 ('timed out waiting for font %s'),
    fontDetails | JSON.stringify,
  ))

const AppWrapper = styled.div`
  ${mediaQuery (
    mediaPhone ('font-size: 30px'),
    mediaTablet ('font-size: 16px'),
  )};
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0 auto;
  height: 100%;
`

const dispatchTable = {
  appMountedDispatch: appMounted,
}

const selectorTable = {
  error: selectError,
}

const Routes = ({ passProps, }) => useRoutes ([
  { path: '/', element: <Main {... passProps}/> },
  { path: '/key/:deviceId', element: <Main withKeyParam={true} {... passProps}/> },
  { path: '*', element: <NotFoundPage/> },
])

export default container (
  ['App', dispatchTable, selectorTable],
  (props) => {
    const { error, appMountedDispatch, } = props

    useReduxReducer ({ createReducer, key: 'domain', reducer: domainReducer, })
    useReduxReducer ({ createReducer, key: 'ui', reducer: uiReducer, })
    useReduxReducer ({ createReducer, key: 'app', reducer: appReducer, })
    useReduxReducer ({ createReducer, key: 'Key', reducer: keyReducer, })
    useReduxReducer ({ createReducer, key: 'Assignee', reducer: assigneeReducer, })
    // --- 'key' is only used so that hot reloading works properly with sagas.
    useSaga ({ saga, key: 'App', })

    const params = new URLSearchParams (document.location.search)

    const [isMobile, setIsMobile] = useState (void 8)
    // --- meaning we're using the hardware screen, not a laptop.
    // We expect the url to be host[:port]/?local[...] for this to work.
    const isLocal = params.get ('local') | ok
    const [fontLoaded, setFontLoaded] = useState (false)

    const [width, ref] = useMeasureWithCb (
      (_node, check) => window.addEventListener ('resize', check),
      prop ('width'),
    )

    useEffect (() => {
      fontMainFamily
        | startFontObserver
        // --- if the font fails, keep going.
        | recover (console.error)
        | then (() => setFontLoaded (true))
    }, [])

    useEffect (() => {
      width | whenOk (
        isMobileWidth >> setIsMobile,
      )
    }, [width])

    useEffect (() => {
      appMountedDispatch ()
      return () => iwarn ('App unmounting or rerendering, unexpected')
    }, [])

    useWhy ('App', props, { isMobile, isLocal, fontLoaded, width, ref, })

    // --- note that after mounting, isMobile is still false for an instant, even on mobile.
    const passProps = { isMobile, isLocal, }
    const cls = clss (
      isMobile | whenTrueV ('x--mobile'),
    )

    return error | ifTrue (
      () => <Error/>,
      () => <ErrorBoundary>
        <Toast/>
        <AppWrapper ref={ref} className={cls}>
          {fontLoaded | ifFalse (
            () => '',
            () => <BrowserRouter>
              <Routes passProps={passProps}/>
            </BrowserRouter>,
          )}
        </AppWrapper>
      </ErrorBoundary>,
    )
  },
)
