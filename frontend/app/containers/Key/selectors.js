import {
  pipe, compose, composeRight,
  map, tap, prop, lets, repeatF,
  defaultTo,
} from 'stick-js/es'

import { weakMapMemoize as memoize, } from 'reselect'

import { ierror, length, logWith, mapX, } from 'alleycat-js/es/general'

import sortWith from 'ramda/es/sortWith'

import { initialState, } from './reducer'
import { sortKeyData, sortKeySortFunctions, } from '../../sort'

import { benchmark, initSelectors, reduceX, } from '../../common'

const { select, selectTop, selectVal, } = initSelectors (
  'Key',
  initialState,
)

const latestBench = { current: null, }
const bench = (tag, f) => benchmark (
  tag, f, (tag, delta) => latestBench.current = [tag, delta],
)

// --- 'T' to be like 't' like in OCaml, as in, the main collection we manage (here keys, elsewhere
// assignees, etc.)

export const selectT = selectVal ('keys')
export const selectSortKey = selectVal ('sortKey')
export const selectSortKeys = selectVal ('sortKeys')
// --- @future these pull in the entire tables, more intelligent way to access?
export const selectPrivileges = selectVal ('privileges')
export const selectAuthorizations = selectVal ('authorizations')

// --- note: be sure to use `keyId` as the React key, and not the list index, when rendering.
const selectTSorted = select (
  'sorted',
  [selectT, selectSortKey],
  bench ('sort keys', (keys, sortKey) => lets (
    () => sortKey | sortKeySortFunctions,
    (fs) => keys | sortWith (fs),
  )),
)

const selectSortedLookups = select (
  'sortedLookups',
  [selectTSorted],
  (keys) => keys | reduceX (
    ([idxByDeviceId], { deviceId, ... _ }, idx) => [
      idxByDeviceId.set (deviceId, idx),
    ],
    [new Map],
  ),
)

export const selectKeyIdxByDeviceId = select (
  'keyIdxByDeviceId',
  [selectSortedLookups],
  ([idxByDeviceId]) => idxByDeviceId,
)

export const selectSortKeyData = select (
  'sortKeyData',
  [selectSortKeys],
  (sks) => sks | map (sortKeyData),
)

const selectNumsPerPage = selectVal ('numsPerPage')
const selectNumPerPageIdx = selectVal ('numPerPageIdx')
export const selectCurPage = selectVal ('curPage')

export const selectNumPerPage = select (
  'numPerPage',
  [selectNumPerPageIdx, selectNumsPerPage],
  (idx, nums) => nums [idx],
)

export const selectSlice = select (
  'slice',
  [selectTSorted, selectCurPage, selectNumPerPage],
  (keys, cur, num) => keys.slice (cur*num, cur*num + num),
)

export const selectNumsPerPageComponent = select (
  'numPerPageComponent',
  [selectNumPerPageIdx, selectNumsPerPage],
  (idx, nums) => nums | mapX ((n, m) => ({
    n, idx: m, selected: m === idx,
  }))
)

const selectTLength = select (
  'numKeys',
  [selectT],
  length,
)

const selectNumPages = select (
  'numPages',
  [selectNumPerPage, selectTLength],
  (npp, num) => lets (
    () => num / npp,
    (n) => Math.floor (n),
    (n, m) => n === m ? n : m + 1,
  ),
)

export const selectPageComponent = select (
  'pageComponent',
  [selectCurPage, selectNumPages],
  (cur, num) => num | repeatF ((idx) => ({
    idx, n: idx + 1, selected: cur === idx,
  })),
)

const selectKeyIdxForDeviceId = select (
  'keyIdxForDeviceId',
  [selectKeyIdxByDeviceId],
  (lookup) => memoize (
    (deviceId) => lookup.get (deviceId) | defaultTo (() => {
      ierror ('selectKeyIdxForDeviceId')
      return 0
    }),
  ),
)

export const selectListLocationForDeviceId = select (
  'listLocationForDeviceId',
  [selectNumPerPage, selectKeyIdxForDeviceId],
  (npp, doLookup) => memoize (
    (deviceId) => lets (
      () => doLookup (deviceId),
      (idx) => Math.floor (idx / npp),
      (idx, page) => idx - page*npp,
      (_, page, idxInPage) => [page, idxInPage],
    ),
  ),
)

export const selectSelectedDeviceId = selectVal ('selectedDeviceId')
