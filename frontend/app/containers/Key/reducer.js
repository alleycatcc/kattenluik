import {
  pipe, compose, composeRight,
  assoc, prop, map, update,
  merge, filter, noop,
} from 'stick-js/es'

import { cata, } from 'alleycat-js/es/bilby'
import { RequestInit, RequestLoading, RequestError, RequestResults, } from 'alleycat-js/es/fetch'
import { logWith, ierror, } from 'alleycat-js/es/general'
import { makeReducer, } from 'alleycat-js/es/redux'

import { statusTag, } from '../../types'

import {
  dbImport,
  dbReset,
  keyAuthorizationsFetchCompleted,
  keyPrivilegesFetchCompleted,
  keyRemove,
  keySetSelectedDeviceId,
  keySort,
  keyStatusSet,
  keysFetchCompleted,
  setKeysNumPerPageIdxReal,
  setKeysCurPage,
} from '../App/actions/main'

import {
  ascend, ascendWithNull,
  descend, descendWithNull,
  ascendPrefer, ascendWithNullPrefer,
  descendPrefer,
  mkSortKey,
} from '../../sort'

// --- @future would be nice if order by none could keep the last ordering
const SortKeyNone = mkSortKey (
  null, '--none--', noop, ascend,
)
const SortKeyStatusActive = mkSortKey (
  // --- active, inactive, missing
  'status-active', 'Status (active on top)', prop ('status'), ascendPrefer ('active'),
)
const SortKeyStatusInactive = mkSortKey (
  // --- inactive, missing, active
  'status-inactive', 'Status (inactive on top)', prop ('status'), descendPrefer ('inactive'),
)
// --- missing, inactive, active
const SortKeyStatusMissing = mkSortKey (
  'status-missing', 'Status (missing on top)', prop ('status'), descendPrefer ('missing'),
)
// ------ @todo colors hardcoded
const SortKeyColorBlue = mkSortKey (
  'color-blue', 'Color (blue on top)', prop ('color'), ascendWithNullPrefer ('blue'),
)
const SortKeyColorRed = mkSortKey (
  'color-red', 'Color (red on top)', prop ('color'), ascendWithNullPrefer ('red'),
)
const SortKeyColorYellow = mkSortKey (
  'color-yellow', 'Color (yellow on top)', prop ('color'), ascendWithNullPrefer ('yellow'),
)
const SortKeyColorBlack = mkSortKey (
  'color-black', 'Color (black on top)', prop ('color'), ascendWithNullPrefer ('black'),
)
const SortKeyColorGreen = mkSortKey (
  'color-green', 'Color (green on top)', prop ('color'), ascendWithNullPrefer ('green'),
)
const SortKeyColorNone = mkSortKey (
  'color-none', 'Color (uncolored on top)', prop ('color'), ascendWithNullPrefer (null),
)
const SortKeyAssigneeAscend = mkSortKey (
  'assignee-ascending', 'Assignee ↑', prop ('name'), ascendWithNull,
)
const SortKeyAssigneeDescend = mkSortKey (
  'assignee-descending', 'Assignee ↓', prop ('name'), descendWithNull,
)
const SortKeyAssigneeNone = mkSortKey (
  'assignee-none', 'Assignee (unassigned on top)', prop ('name'), ascendWithNullPrefer (null),
)
const SortKeyCustomAscend = mkSortKey (
  'custom', 'Extra info ↑', prop ('custom'), ascendWithNull,
)
const SortKeyCustomDescend = mkSortKey (
  'custom', 'Extra info ↓', prop ('custom'), descendWithNull,
)
const SortKeyCustomNone = mkSortKey (
  'custom-none', 'Extra info (empty on top)', prop ('custom'), ascendWithNullPrefer (null),
)
const SortKeyLastUsedAscend = mkSortKey (
  'last-used-ascending', 'Last used (oldest first)', prop ('activityTimestamp'), ascend,
)
const SortKeyLastUsedDescend = mkSortKey (
  'last-used-descending', 'Last used (latest first)', prop ('activityTimestamp'), descend,
)
// --- @future this currently relies on the order of the database id, though at some point that
// could change (migrations etc.)
const SortKeyAddedDescend = mkSortKey (
  'last-added-descending', 'Last added (latest first)', prop ('id'), descend,
)
const SortKeyAddedAscend = mkSortKey (
  'last-added-ascending', 'Last added (oldest first)', prop ('id'), ascend,
)

import {} from './actions'
import { reducer, } from '../../common'

export const initialState = {
  authorizations: void 8,
  curPage: 0,
  keys: void 8,
  privileges: void 8,
  numPerPageIdx: 1,
  numsPerPage: [5, 10, 25, 50],
  selectedDeviceId: null,
  sortKey: SortKeyAssigneeAscend,
  sortKeys: [
    SortKeyNone,
    SortKeyAssigneeAscend,
    SortKeyAssigneeDescend,
    SortKeyAssigneeNone,
    SortKeyCustomAscend,
    SortKeyCustomDescend,
    SortKeyCustomNone,
    SortKeyStatusActive,
    SortKeyStatusInactive,
    SortKeyStatusMissing,
    SortKeyLastUsedDescend,
    SortKeyLastUsedAscend,
    SortKeyAddedDescend,
    SortKeyAddedAscend,
    SortKeyColorBlue,
    SortKeyColorRed,
    SortKeyColorYellow,
    SortKeyColorBlack,
    SortKeyColorGreen,
    SortKeyColorNone,
    // SortKeyCanUnlockScreen,
  ],
}

const clearData = merge ({
  keys: null,
})

const reducerTable = makeReducer (
  dbImport, () => clearData (),
  dbReset, () => clearData (),
  keyAuthorizationsFetchCompleted, (rcomplete) => assoc (
    'authorizations', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results,
    }),
  ),
  keyPrivilegesFetchCompleted, (rcomplete) => assoc (
    'privileges', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results,
    }),
  ),
  keyRemove, ({ deviceId: deleted, ... _ }) => update (
    'keys', (keys) => keys | filter (
      ({ deviceId: cur, }) => deleted !== cur,
    ),
  ),
  keySetSelectedDeviceId, (deviceId) => assoc (
    'selectedDeviceId', deviceId,
  ),
  keySort, (ks) => assoc ('sortKey', ks),
  keyStatusSet, ({ status, deviceId, ... _ }) => update (
    'keys', (keys) => keys | map (
      (key) => {
        const { deviceId: deviceIdFromKey, } = key
        if (deviceIdFromKey !== deviceId) return key
        return key | assoc ('status', status | statusTag)
      }
    )
  ),
  keysFetchCompleted, (rcomplete) => assoc (
    'keys', rcomplete | cata ({
      RequestCompleteError: (_) => {},
      RequestCompleteSuccess: (results) => results | map (
        update ('hasSecret', Boolean),
      ),
    }),
  ),
  setKeysNumPerPageIdxReal, assoc ('numPerPageIdx'),
  setKeysCurPage, assoc ('curPage',)
)

export default reducer ('Key', initialState, reducerTable)
