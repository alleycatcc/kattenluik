import {
  pipe, compose, composeRight,
  map, prop, not, cond, each, filter,
  tap, id, noop, ok, nil, ifOk, ifNil,
  ifTrue, ifFalse, whenTrue, whenOk, eq, invoke,
  T, compact, appendM, reduce, lets, die, letS,
  addIndex2,
} from 'stick-js/es'

import React, { useCallback, useEffect, useMemo, useRef, useState, } from 'react'

import styled from 'styled-components'

import 'react-widgets/styles.css'
import 'react-responsive-select/dist/react-responsive-select.css'

import configure from 'alleycat-js/es/configure'
import { flatMap, } from 'alleycat-js/es/bilby'
import { clss, } from 'alleycat-js/es/dom'
import { defaultToV, ierror, iwarn, logWith, mapX, } from 'alleycat-js/es/general'
import { ifEmptyString, whenNotEquals, } from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'
import { useSaga, } from 'alleycat-js/es/redux-hooks'

import {
  keyAssigneeSet, keyColorSet, keyCustomSet,
  keyAuthorizationAdd, keyAuthorizationRemove,
  keyGoToPageForKey, keySetSelectedDeviceId,
  keyPrivilegeAdd, keyPrivilegeRemove,
  keyRemove,
  keySort,
  keyStatusSet,
  setKeysNumPerPageIdx, setKeysCurPage,
} from '../App/actions/main'

import {
  selectColorsEdit, selectColorsEditReverse,
  selectDoors, selectKeysComponent,
  selectCheckKeyAuthorized,
  selectGrantedDeniedAllDoors,
  selectGrantedDeniedAllPrivileges,
  selectFindAuthorizationId,
  selectStaleKeysAsSet,
  selectStatuses,
  selectLatestBench,
  selectUptimeApproximateByMonth,
} from '../App/store/domain/selectors'
import {
  selectSortKey,
  selectSortKeyData,
  selectNumsPerPageComponent as selectKeysNumsPerPageComponent,
  selectPageComponent as selectKeysPageComponent,
  selectSelectedDeviceId as selectKeySelectedDeviceId,
} from './selectors'
import {
  selectTEdit as selectAssigneesEdit,
  selectTReverse as selectAssigneesReverse,
} from '../Assignee/selectors'

import saga from './saga'

import { AreYouSureDialog, Button, LinkLike, mobilePopup, mkPagination, Select, SelectWithText, Text, } from '../../components/shared'
import ClockIcon from '../../components/shared/svg/clock'

import { component, container, getAnchor, ifZero, mediaDesktop, mediaPhone, mediaPhoneOnly, mediaTablet, mkAnchor, useWhy, lookupOn, truncate, whenOkAndNotEmptyString, listFlatMap, scrollIntoViewWithOptions, whenEq, } from '../../common'
import { StatusActive, StatusInactive, StatusMissing, statusIsActive, statusFold, statusTag, statusText, } from '../../types'

const remapMapTuples = (f) => (m) => {
  const ret = []
  for (const [k, v] of m.entries ())
    ret.push (f (k, v))
  return ret
}

const remapMapKeys = (f) => (m) => {
  const ret = []
  for (const k of m.keys ())
    ret.push (f (k))
  return ret
}

const remapMapTuplesX = addIndex2 (remapMapTuples)

import config from '../../config'

const configTop = config | configure.init
const doAbridgedListView = configTop.get ('doAbridgedListView')
const configKeyColors = configTop.get ('colors.key')
const highlightColor = configTop.get ('colors.highlight')
const selectedKeyColor = configTop.get ('colors.selected-key')
const selectedKeyColor2 = configTop.get ('colors.selected-key-lighter')
const selectedKeyColor2WithOpacity = configTop.get ('colors.selected-key-lighter-with-opacity')
const sliderImg = configTop.get ('images.slider')
const moreIcon = configTop.get ('icons.more')

const cQuestionIcon = configTop.get ('icons.c.question')
const cBlockedIcon = configTop.get ('icons.c.blocked')
const cCheckIcon = configTop.get ('icons.c.check')

const keyCustomPlaceholder = configTop.get ('placeholders.key-custom')
const assigneeTruncate = configTop.get ('layout.keySummary.nameTruncate')
const keyExtraInfoWidth = configTop.get ('layout.keySummary.keyExtraInfoWidth')

// @todo move to config?
const keyRowStatusInfo = new Map ([
  [StatusActive, {
    imgLeft: '-375px',
    labelLeft: '0%',
    label: 'active',
  }],
  [StatusInactive, {
    imgLeft: '-228px',
    labelLeft: '36%',
    label: 'inactive',
  }],
  [StatusMissing, {
    imgLeft: '-65px',
    labelLeft: '75%',
    label: 'missing',
  }],
])

const allowDenySliderInfo = new Map ([
  [true, {
    imgLeft: '-375px',
    labelLeft: '0%',
    label: 'allow',
  }],
  [false, {
    imgLeft: '-65px',
    labelLeft: '84%',
    label: 'deny',
  }],
])

const SwatchS = styled.div`
  width: 100%;
  height: 100%;
  display: inline-block;
  border-radius: 1000px;
  border: 1px solid black;
  &.x--no-color {
    background: white;
  }
  &.x--no-hex {
    background: white;
  }
  &.x--normal {
    background: ${prop ('background')};
  }
  > div {
    display: inline-block;
    position: relative;
    left: 5px;
    top: -3px;
  }
`

const Swatch = ({ color, height='20px', width='20px', style={}, classes=[], }) => {
  const hex = color | lookupOn (configKeyColors)
  const className = clss (... [cond (
    [() => color | nil, () => 'x--no-color'],
    [() => hex | nil, () => 'x--no-hex'],
    [T, () => 'x--normal'],
  ), ... classes])
  const f = (name) => whenOk ((x) => ({ [name]: x, }))
  return <SwatchS
    background={hex}
    className={className}
    style={{
      ... f ('height') (height),
      ... f ('width') (width),
      ... style,
    }}
  />
}

const KeyRowS = styled.div`
  position: relative;
  margin-bottom: 8%;
  margin-top: 8%;
  padding-left: 30px;
  &.x--expanded {
    position: fixed;
    z-index: 100;
    left: 0;
    // right: 0;
    width: 100%;
    top: 0;
    bottom: 0;
    overflow-y: scroll;
    // --- don't scroll the stuff beneath the popup
    overscroll-behavior: none;
    background: ${selectedKeyColor2};
    >* {
      background: ${selectedKeyColor2};
    }
    margin-bottom: 0%;
    margin-top: 0%;
    padding-top: 30px;
    padding-bottom: 30px;
    .x__selection-cursor {
      display: none;
    }
    > .x__main {
      // --- this is a bit ugly -- trial & error to make the row not shift when expanding the info
      // box.
      padding-left: 3%;
      padding-right: 5%;
      .x__stale-clock {
        // --- ditto
        left: 10px;
        top: 28px;
      }
    }
  }
  .x__selection-cursor {
    position: absolute;
    --vert-offset: -20px;
    left: ${prop ('left')};
    width: 100vw;
    top: var(--vert-offset);
    bottom: var(--vert-offset);
    ${prop ('isSelected') >> whenTrue (
      () => `
        border: 4px solid ${selectedKeyColor};
        background: ${selectedKeyColor2WithOpacity};
      `,
    )};
  }
  /* @future not currently used (need a nice way to display the link)
  &:hover .x__clear {
    opacity: 1;
  }
  .x__clear {
    position: absolute;
    opacity: 0;
    transition: opacity .1s;
    right: 1%;
    top: 5px;
    padding: 10px;
    cursor: pointer;
    color: #3333c2;
    font-size: 13px;
    font-family: sans;
    .x__selected {
      display: inline-block;
    }
    &:hover {
      text-decoration: underline;
    }
  }
  */
  > .x__main {
    width: 100%;
    display: flex;
    > * {
      justify-content: space-between;
      margin-right: 2%;
    }
    .x__stale-clock {
    }
    .x__status-icon {
    }
    .x__assignee {
      // --- note that we use text-overflow for a long string without spaces, since it can't be
      // wrapped; in general we also we need to use \`truncate\`
      overflow-x: hidden;
      text-overflow: ellipsis;
    }
    .x__assignee, .x__color {
      // --- cursor=pointer causes an annoying blue flash on mobile (and isn't necessary anyway)
      ${mediaQuery (
        mediaDesktop ('cursor: pointer'),
      )}
    }
    .x__extra-info {
      overflow-x: hidden;
      text-overflow: ellipsis;
    }
    .x__status-icon, .x__color {
      .x__option > .x__text {
        margin-left: 20px;
      }
      .rrs {
        &.rrs--options-visible {
          z-index: 10;
          width: 200px;
        }
        .rrs__button {
          box-shadow: none;
        }
        .rrs__label {
          border: 0px;
          padding: 0px;
          /*
          ${prop ('isSelected') >> whenTrue (
            () => `background: ${selectedKeyColor};`
          )}
          */
        }
        .rrs__options {
          border: 2px solid black;
          max-height: none;
          left: 0;
          right: 0;
          position: fixed;
          top: 0;
          bottom: 0;
        }
      }
    }
    > .x__show-more {
      display: flex;
      align-items: center;
      &.x--showing {
        filter: brightness(0%);
      }
      > .x__img {
        width: 40px;
        text-align: center;
        transform: rotate(90deg);
      }
      ${mediaQuery (
        mediaTablet (`
          cursor: pointer;
          &:hover {
            filter: brightness(0%);
          }
        `),
      )}
    }
    .x__key-id {
      display: none;
    }
    .x__has-secret {
      display: none;
      &.x--yes { color: green; }
      &.x--no {
        color: white;
        background: #ff4d4d;
      }
      font-size: 80%;
      margin-left: 10px;
      margin-right: 10px;
    }
  }
  ${mediaQuery (
    mediaPhone (`
      > .x__main {
        > * {
          margin-right: 4%;
        }
        > div:nth-child(1) {
          position: absolute;
          top: 0px;
          left: -12px;
        }
        > div:nth-child(2) {
          flex: 0 0 50px;
        }
        > div:nth-child(3) {
          flex: 0 0 50px;
        }
        > div:nth-child(4) {
          flex: 1 0 40px;
        }
        > div:nth-child(5) {
          flex: 0.5 0 40px;
        }
        > div:nth-child(6) {
          flex: 0 0 40px;
        }
      }
      .x__clear {
        opacity: 1;
        padding: 5px;
      }
    `),
    mediaTablet (`
      > .x__main {
        > div:nth-child(1) {
          flex: 0.1 0 50px;
          margin-right: 5px;
        }
        > div:nth-child(2) {
          flex: 0.1 0 50px;
        }
        > div:nth-child(3) {
          flex: 0.6 0 40px;
          margin-right: 4%;
        }
        > div:nth-child(4) {
          flex: 0.4 0 40px;
        }
        > div:nth-child(5) {
          flex: 0 0;
        }
        > div:nth-child(6) {
          flex: 0.3 0 50px;
        }
      }
    `),
  )}
`

const InfoSliderRowS = styled.div`
  display: flex;
  align-items: center;
  // --- matches var --height in .x__main
  height: 100px;
  width: 100%;
  margin-left: 50px;
  > .x__inner {
    display: inline-block;
  }
`

// --- @todo sliderProps is ugly
const InfoSliderRow = ({ sliderProps, }) => {
  const { cur: curProp, onSet, } = sliderProps

  // --- cur is a copy of curProp which is stored in the state to make the sliders react quicker.
  const [cur, setCur] = useState (curProp)

  useEffect (() => {
    setCur (curProp)
  }, [curProp])

  if (nil (curProp)) return iwarn ('InfoSliderRow: cur is nil')

  const passProps = {
    ... sliderProps,
    cur: cur,
    onSet: (n) => n | tap (setCur) | onSet,
  }

  return <InfoSliderRowS>
    <div className='x__inner'>
      <Slider {... passProps}/>
    </div>
  </InfoSliderRowS>
}

const InfoS = styled.div`
  margin-top: 20px;
  margin-bottom: 50px;
  >.x__main {
    > .x__custom {
      height: 150px;
    }
    > .x__separator {
      position: relative;
      left: -15%;
      width: 65%;
    }
    .x__deviceId {
      white-space: nowrap;
      > div:nth-child(1) {
        font-weight: bold;
      }
    }
    .x__doorsHeader {
      > div {
        font-weight: bold;
      }
    }
    .x__last-used {
      padding-bottom: 20px;
      margin-left: -30px;
    }
    .x__abridgedControls {
      .rrs {
        // --- @todo repeated
        .rrs__button {
          box-shadow: none;
        }
        .rrs__label {
          padding: 0px;
        }
      }
      .x__colorWrapper {
        margin-bottom: 54px;
        // --- @todo ugly, needs to match margin of assigneeWrapper
        margin-left: 50px;
      }
    }
  }
  >.x__remove-button {
    width: 60%;
    margin: auto;
    button { width: 100%; }
  }
  >.x__buttons {
    .x__allButtons {
      margin-bottom: 50px;
      .x__buttons {
        position: relative;
        height: 30px;
        .x__allowAllButton {
          float: left;
        }
        .x__denyAllButton {
          float: right;
        }
      }
    }
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      >.x__main {
        &.x__first {
          margin-top: 60px;
        }
        --height: 100px;
        --width: 300px;
        --left-edge-contents: 42%;
        margin-left: var(--left-edge-contents);
        .x__assigneeWrapper, .x__customWrapper {
          height: var(--height);
          width: calc(var(--width) - 50px);
          margin-left: 50px;
        }
        .x__doorName {
          display: none;
        }
        .x__doorRow {
          display: flex;
          align-items: center;
          height: var(--height);
        }
      }
      >.x__buttons {
        width: 530px;
        margin: auto;
        position: relative;
        left: 25px;
      }
    `),
    // @todo: --col-width
    mediaTablet (`
      margin-left: 10%;
      width: 80%;
      --col1-width: calc(100% - 300px);
      --col2-width: 300px;
      .x__allButtons {
        .x__buttons {
        }
      }
    `),
  )}
`

const DeviceIdS = styled.div`
  font-size: 15px;
  font-family: monospace;
  ${mediaQuery (
    mediaPhoneOnly (`margin-left: 50px;`),
  )}
`

const DeviceId =  ({ deviceId, }) => <DeviceIdS>
  {deviceId}
</DeviceIdS>

const DeviceInfo = ({ deviceId, }) =>
  <div className='u-query-not-mobile x__deviceId'>
    <div>Device ID:</div>
    <DeviceId deviceId={deviceId}/>
  </div>

const DoorsHeader = () =>
  <div className='u-query-not-mobile x__doorsHeader'>
    <div>Door</div>
    <div>State</div>
  </div>

const WithKeyLabelS = styled.div`
  position: relative;
  // --- we force display flex on the contents to make vertical aligning easier
  display: flex;
  align-items: center;
  > .x__label {
    display: flex;
    align-items: center;
    position: absolute;
    top: 0px;
    height: var(--height);
    overflow: hidden;
    // --- left & width should correspond roughly to --left-edge-contents of the parent
    left: -42%;
    width: 42%;
    opacity: 0.7;
    font-size: 80%;
    ${mediaQuery (
      mediaTablet ('display: none;'),
    )}
  }
`

const WithKeyLabelM = ({ children, label, classes=[] }) => {
  return <WithKeyLabelS className={clss (... classes)}>
    {children}
    <div className='x__label'>
      <span>
        {label | truncate (30)}
      </span>
    </div>
  </WithKeyLabelS>
}

const AllowDenyAllButtons = ({ what, allGranted, allowAll, allDenied, denyAll, }) =>
  <div className='x__allButtons'>
    <div className='x__buttons'>
      <LinkLike
        disabled={allGranted}
        className={clss ('x__allowAllButton', allGranted && 'u--opacity-50')}
        onClick={allowAll}>
        allow all {what}
      </LinkLike>
      <LinkLike
        disabled={allDenied}
        className={clss ('x__denyAllButton', allDenied && 'u--opacity-50')}
        onClick={denyAll}>
        deny all {what}
      </LinkLike>
    </div>
  </div>

/* @todo the commented out code is not used atm, should we remove it?
const ListDownArrowS = styled.div`
  ${prop ('withBorder') >> whenTrue (
    () => 'border-left: 1px solid grey',
  )};
  display: inline-block;
  width: 30px;
  height: calc(100% + 6px);
  position: relative;
  ${({ right='0px', top='0px', }) => [right, top] | sprintfN (`
    right: %s;
    top: %s;
  `)}
  > div {
    position: relative;
    top: 18px;
    width: 10px;
  }
  ${mediaQuery (
    mediaPhone (`
      > div { left: 8px; }
    `),
    mediaDesktop (`
      > div { left: 50%; transform: translateX(-50%); }
    `),
  )}
`

const TriangleEquiDownS = styled.div`
  width: 0;
  height: 0;
  ${({ side, color, }) => [side, side, side, color] | sprintfN (
    `border-left: %s solid transparent;
     border-right: %s solid transparent;
     border-top: %s solid %s;
    `,
  )}
`

const DropdownListS = styled (DropdownList)`
  z-index: 1;
`

const DropdownListWrapperS = styled.div`
  height: 50px;
  background: white;
  z-index: 1;
  position: relative;
  .x__main {
    margin: auto;
    position: relative;
    top: 50%;
    transform: translateY(-50%);
  }
  ${mediaQuery (
    mediaPhone (`
      width: 80vw;
      .x__main {
        width: 100%;
      }
    `),
    mediaDesktop (`
      width: 300px;
      .x__main {
        width: 80%;
      }
    `),
  )}
`

const DropdownListWrapper = (props) => <DropdownListWrapperS>
  <div className='x__main'>
    <DropdownListS {... props}/>
  </div>
</DropdownListWrapperS>

const TriangleEquiDown = ({ side=5, color='black', }) => <TriangleEquiDownS
  side={String (side) + 'px'}
  color={color}
/>

const ListDownArrow = ({ withBorder=false, right='0px', top='0px', style, }) => (
  <ListDownArrowS withBorder={withBorder} right={right} top={top} style={style}>
    <div>
      <TriangleEquiDown side={5} color='#666'/>
    </div>
  </ListDownArrowS>
)

const ListDownArrowCombo = (props) => <ListDownArrow
  {...props}
  right='-2px' top='-4px' withBorder={false}
/>
*/

const KeyColorDropdownOptionS = styled.div`
  .x__swatch {
    width: 20px;
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
  }
`

const KeyColorDropdownOption = ({ color, }) => <KeyColorDropdownOptionS
>
  <div className='x__swatch'>
    <Swatch color={color}/>
  </div>
  {color | defaultToV ('(none)')}
</KeyColorDropdownOptionS>

const Custom = ({ isMobile, defaultValue, onAccept=noop, wrapperRef=null, classes=[], }) => {
  const placeholder = isMobile ? keyCustomPlaceholder.mobile : keyCustomPlaceholder.default
  return <Text
    classes={classes}
    isMobile={isMobile}
    defaultValue={defaultValue}
    border={0}
    blur='cancel'
    showButtons={false}
    stretchElementRef={wrapperRef}
    placeholder={placeholder}
    onAccept={onAccept}
    onCancel={noop}
  />
}

const SliderS = styled.div`
  position: relative;
  width: 350px;
  height: 67px;
  overflow: hidden;
  .x__content {
    position: relative;
    display: inline-block;
    width: calc(100% - 0px);
    height: 100%;
    vertical-align: middle;
    overflow: hidden;
    .x__img {
      position: absolute;
      top: 45px;
      /*
      left: ${prop ('imgLeft')};
      */
      width: 750px;
      height: 20px;
      transition: left 100ms;
    }
    > .x__label {
      display: inline-block;
      position: absolute;
      font-size: 25px;
      font-family: arial;
      top: 0;
      &.x--active {
        opacity: 1;
      }
      &.x--inactive {
        opacity: 0.7;
        &:hover {
          opacity: 1;
          cursor: pointer;
        }
      }
      /*
      ${({ labelLefts }) => labelLefts | mapX ((left, n) => `
          &:nth-child(${n+2}) {
            left: ${left};
          }
        `
      )}
      */
  }
`

const StatusIconS = styled.div`
  display: inline-block;
  position: relative;
  width: 50px;
  font-size: 120%;
  vertical-align: middle;
  img {
    width: 100%;
    position: relative;
    border: 2px solid black;
    border-radius: 10000px;
    vertical-align: top;
  }
  .x__active {
    img {
      background: #066506;
    }
  }
  .x__inactive {
    img {
      background: #af0000;
    }
  }
  .x__missing {
    img {
      background: #af0000;
    }
  }
  ${mediaQuery (
    mediaPhone (`
      .x__active, .x__inactive, .x__missing {
        width: 50px;
        height: 50px;
      }
    `),
    mediaTablet (`
      .x__active, .x__inactive, .x__missing {
        width: 20px;
        height: 20px;
      }
    `),
  )}
`

const StatusIcon = ({ status, classes=[], }) => {
  const [cls, src] = status | statusFold (
    () => ['x__active', cCheckIcon],
    () => ['x__inactive', cBlockedIcon],
    () => ['x__missing', cQuestionIcon],
  )
  return <StatusIconS className={clss (... classes)}>
    <div key={status} className={cls}>
      <img src={src}/>
    </div>
  </StatusIconS>
}

const Slider = ({ info, cur, onSet, }) => {
  const onClicks = useMemo (() => info | remapMapKeys (
    (infoKey) => (_) => infoKey | whenNotEquals (cur) (onSet),
  ), [info, cur, onSet])
  const imgLeft = info.get (cur).imgLeft
  const cls = ['x--inactive', 'x--active']
  return <SliderS>
    <div className='x__content'>
      <img className='x__img' src={sliderImg} style={{ left: imgLeft, }}/>
      {info | remapMapTuplesX (
        (infoKey, { labelLeft, label, ... _ }, idx) => <div
          key={idx}
          style={{ left: labelLeft, }}
          className={clss ('x__label', cls [Number (infoKey === cur)])}
          onClick={onClicks [idx]}>
          {label}
        </div>,
      )}
    </div>
  </SliderS>
}

const InfoSeparator = styled.hr`
  background-color: ${highlightColor};
`

const Privileges = container ([
  'Privileges',
  {
    keyPrivilegeAddDispatch: keyPrivilegeAdd,
    keyPrivilegeRemoveDispatch: keyPrivilegeRemove,
  },
  {},
], (props) => {
  const { keyId, privileges, keyPrivilegeAddDispatch, keyPrivilegeRemoveDispatch, } = props
  useWhy ('Privileges', props)
  return privileges | map ((priv) => {
    const { key, keyPrivilegeId, privilegeId, privilegeText, } = priv
    const cur = ok (keyPrivilegeId)
    const onSet = (set) => {
      if (set) keyPrivilegeAddDispatch ([[keyId, privilegeId]])
      else keyPrivilegeRemoveDispatch ([keyPrivilegeId])
    }

    return <WithKeyLabelM key={key} label={privilegeText}>
      {/* @todo doorRow */}
      <div className='x__doorRow'>
        <InfoSliderRow sliderProps={{ onSet, cur, info: allowDenySliderInfo, }} />
      </div>
    </WithKeyLabelM>
  })
})

const lastUsedText = (activityTimestamp, uptimeApproximateByMonth) => lets (
  () => uptimeApproximateByMonth - activityTimestamp,
  ifZero (
    () => 'within the last month',
    (n) => Math.floor (n / 3600 / 24 / 30) | letS ([
      (m) => m === 1,
      (_, one) => one ? 'month' : 'months',
      (m, _, months) => 'approximately ' + String (m) + ' ' + months + ' ago',
    ]),
  ),
)

const LastUsed = container ([
  'LastUsed', {},
  { uptimeApproximateByMonth: selectUptimeApproximateByMonth, },
], (props) => {
  const { activityTimestamp, uptimeApproximateByMonth, } = props
  const text = lastUsedText (activityTimestamp, uptimeApproximateByMonth)
  useWhy ('LastUsed', props)
  return <span>Last used: {text}</span>
})

const Info = container (
  [
    'Info',
    {
      keyAuthorizationAddDispatch: keyAuthorizationAdd,
      keyAuthorizationRemoveDispatch: keyAuthorizationRemove,
      keyPrivilegeAddDispatch: keyPrivilegeAdd,
      keyPrivilegeRemoveDispatch: keyPrivilegeRemove,
      keyRemoveDispatch: keyRemove,
    },
    {
      checkKeyAuthorized: selectCheckKeyAuthorized,
      grantedDeniedAllDoors: selectGrantedDeniedAllDoors,
      grantedDeniedAllPrivileges: selectGrantedDeniedAllPrivileges,
      findAuthorizationId: selectFindAuthorizationId,
      // findPrivilegeId: selectFindPrivilegeId,
    },
  ],
  (props) => {
    const {
      isMobile,
      abridged,
      doors, keyId, deviceId, activityTimestamp,
      authorizations, privileges,
      checkKeyAuthorized,
      grantedDeniedAllDoors,
      grantedDeniedAllPrivileges,
      findAuthorizationId,
      keyAuthorizationAddDispatch, keyAuthorizationRemoveDispatch,
      keyPrivilegeAddDispatch, keyPrivilegeRemoveDispatch,
      keyRemoveDispatch,
      CustomInput,
      AssigneeSelect,
      ColorSelect,
      statusSliderProps,
    } = props
    const [removeDialogOpen, setRemoveDialogOpen] = useState (false)
    const authsToAdd = useMemo (() => doors | listFlatMap (
      ({ id: doorId, }) => checkKeyAuthorized (doorId, keyId) ? [] : [[keyId, doorId]],
    ), [doors, checkKeyAuthorized, keyId])
    const authsToRemove = useMemo (() => compact (authorizations | map (
      ({ authId, }) => authId,
    )), [authorizations])
    const allowAllDoors = useCallback (() => {
      keyAuthorizationAddDispatch (authsToAdd)
    }, [authorizations, authsToAdd])
    const denyAllDoors = useCallback (() => {
      keyAuthorizationRemoveDispatch (authsToRemove)
    }, [authorizations, authsToRemove])
    const [allDoorsGranted, allDoorsDenied] = grantedDeniedAllDoors (keyId)

    const [privilegesToAdd, privilegesToRemove] = useMemo (() => privileges | reduce (
      ([toAdd, toRemove], { keyPrivilegeId, privilegeId, }) => keyPrivilegeId | ifOk (
        () => [toAdd, toRemove | appendM (keyPrivilegeId)],
        () => [toAdd | appendM ([keyId, privilegeId]), toRemove],
      ),
      [[], []],
    ), [keyId, privileges])
    const allowAllPrivileges = useCallback (() => {
      keyPrivilegeAddDispatch (privilegesToAdd)
    }, [privileges, privilegesToAdd])
    const denyAllPrivileges = useCallback (() => {
      keyPrivilegeRemoveDispatch (privilegesToRemove)
    }, [privileges, privilegesToRemove])
    const [allPrivilegesGranted, allPrivilegesDenied] = grantedDeniedAllPrivileges (keyId)

    const doRemove = useCallback (
      () => keyRemoveDispatch (deviceId),
      // --- this shouldn't change, but just to be safe.
      [deviceId],
    )

    const onNoRemove = useCallbackConst (
      () => setRemoveDialogOpen (false)
    )

    const onClickRemove = useCallbackConst (
      () => setRemoveDialogOpen (true),
    )

    const haveMultipleDoors = useMemo (() => doors.length > 1, [doors])
    const haveMultiplePrivileges = useMemo (() => privileges.length > 1, [privileges])

    const onSetSlider = useCallbackConst ((keyId, doorId, keyAuthorizationId) => {
      return (set) => {
        if (set) keyAuthorizationAddDispatch ([[keyId, doorId]])
        else keyAuthorizationRemoveDispatch ([keyAuthorizationId])
      }
    })

    const authorizationRows = useMemo (() => doors | map ((door) => {
      // --- @todo can we loop through authorizations here instead of through doors and thus
      // get rid of findAuthorizationId?

      const { id: doorId, name, } = door

      const [keyAuthorizationId, cur] = lets (
        () => findAuthorizationId (doorId, keyId),
        ok,
        (authId, has) => [authId, has],
      )
      const onSet = onSetSlider (keyId, doorId, keyAuthorizationId)

      return <WithKeyLabelM key={doorId} label={name}>
        <div className='x__doorRow'>
          <InfoSliderRow sliderProps={{ onSet, cur, info: allowDenySliderInfo, }} />
        </div>
      </WithKeyLabelM>
    }), [doors, findAuthorizationId, keyId, onSetSlider])

    useWhy ('Info', props)

    return <InfoS>
      <div className='x__main x__first'>
        <DeviceInfo deviceId={deviceId}/>
        <DoorsHeader/>
        {abridged && <div className='x__abridgedControls'>
          <WithKeyLabelM label='color' classes={['u-query-mobile']}>
            <div className='x__colorWrapper'>
              <ColorSelect editable={true}/>
            </div>
          </WithKeyLabelM>
          <WithKeyLabelM label='assignee' classes={['u-query-mobile']}>
            <div className='x__assigneeWrapper'>
              <AssigneeSelect/>
            </div>
          </WithKeyLabelM>
        </div>}
        <WithKeyLabelM label='extra info' classes={['x__custom', 'u-query-mobile']}>
          <div className='x__customWrapper'>
            <CustomInput/>
          </div>
        </WithKeyLabelM>
        <div className='x__separator'>
          <InfoSeparator/>
        </div>
        <WithKeyLabelM label='status' classes={['u-query-mobile']}>
          <InfoSliderRow sliderProps={statusSliderProps}/>
        </WithKeyLabelM>
        <div className='x__separator'>
          <InfoSeparator/>
        </div>
        {authorizationRows}
      </div>
      <div className='x__buttons'>
        {haveMultipleDoors && <AllowDenyAllButtons
          what='doors'
          allGranted={allDoorsGranted}
          allowAll={allowAllDoors}
          allDenied={allDoorsDenied}
          denyAll={denyAllDoors}
        />}
      </div>
      <div className='x__main'>
        <div className='x__separator'>
          <InfoSeparator/>
        </div>
        <Privileges keyId={keyId} privileges={privileges}/>
        <div className='x__separator'>
          <InfoSeparator/>
        </div>
        <div className='x__last-used'>
          <LastUsed activityTimestamp={activityTimestamp}/>
        </div>
      </div>
      <div className='x__buttons'>
        {haveMultiplePrivileges && <AllowDenyAllButtons
          what='privileges'
          allGranted={allPrivilegesGranted}
          allowAll={allowAllPrivileges}
          allDenied={allPrivilegesDenied}
          denyAll={denyAllPrivileges}
        />}
      </div>
      <div className='x__remove-button'>
        <Button onClick={onClickRemove}>Remove this key</Button>
      </div>
      <AreYouSureDialog
        isOpen={removeDialogOpen}
        onYes={doRemove}
        onNo={onNoRemove}
        contents={<span>
          This will permanently delete this key from the system.
        </span>}
        isMobile={isMobile}
      />
    </InfoS>
  },
)

const KeyRowNameContentS = styled.div`
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
`

const StatusAbridged = component (
  ['StatusAbridged'],
  ({ customLabelRenderer, selectedValue, }) => customLabelRenderer ({ value: selectedValue, }),
)

const StatusC = component (
  ['StatusC'],
  ({ abridged=false, ... props }) => abridged | ifTrue (
    () => <StatusAbridged {... props}/>,
    () => <Select {... props}/>,
  ),
)

const ColorNotEditable = component (
  ['ColorNotEditable'],
  ({ customLabelRenderer, selectedValue, }) => {
    return <div>
      {customLabelRenderer ({ value: selectedValue, })}
    </div>
  },
)

const Color = component (
  ['Color'],
  ({ editable=true, ... props }) => editable | ifFalse (
    () => <ColorNotEditable {... props}/>,
    () => <Select {... props}/>,
  ),
)

const AssigneeAbridged = component (
  ['AssigneeAbridged'],
  ({ name, }) => {
    return <div>
      {name}
    </div>
  },
)

const Assignee = component (
  ['Assignee'],
  ({ abridged=false, AssigneeSelect, ... props }) => abridged | ifTrue (
    () => <AssigneeAbridged {... props}/>,
    () => <AssigneeSelect {... props}/>,
  ),
)

const KeyRow = container (
  [
    'KeyRow',
    {
      keyAssigneeSetDispatch: keyAssigneeSet,
      keyColorSetDispatch: keyColorSet,
      keyCustomSetDispatch: keyCustomSet,
      keyStatusSetDispatch: keyStatusSet,
    },
    {
      assigneesEdit: selectAssigneesEdit,
      assigneesReverse: selectAssigneesReverse,
      colorsEdit: selectColorsEdit,
      colorsEditReverse: selectColorsEditReverse,
      staleKeys: selectStaleKeysAsSet,
      statuses: selectStatuses,
      uptimeApproximateByMonth: selectUptimeApproximateByMonth,
    },
  ],
  (props) => {
    const {
      isMobile,
      abridged,
      deviceId, status: statusProp,
      keyId, color, custom: customProp,
      activityTimestamp,
      wrapperRef=null,
      // --- null means key is not assigned
      name=null,
      assigneesEdit,
      assigneesReverse,
      authorizations,
      colorsEdit, colorsEditReverse,
      doors,
      privileges,
      uptimeApproximateByMonth,
      isSelected, clearSelected,
      onInteract: onInteractProp=noop,
      staleKeys,
      statuses,
      keyAssigneeSetDispatch, keyColorSetDispatch, keyCustomSetDispatch,
      keyStatusSetDispatch,
    } = props

    const [status, setStatus] = useState (statusProp)
    const [showInfo, setShowInfo] = useState (false)
    const [offsetLeft, setOffsetLeft] = useState (null)

    const onClickMore = useCallback (
      () => not (showInfo) | setShowInfo,
      [showInfo],
    )

    // --- @future not currently used (need a nice way to display the 'clear' link)
    const onClickClearSelected = useCallback (
      () => clearSelected (),
      [clearSelected],
    )

    const mainRef = useRef ()

    // --- this effect is called when entering the page using a url with /key/:deviceId, when
    // the link in Admin is clicked after a 'Read key', or when clicking on a key icon in the
    // assignees.
    useEffect (() => {
      if (isSelected) {
        // --- options should match options in Assignee/KeyIcon for best UX.
        mainRef.current.scrollIntoView ({
          block: 'center',
        })
      }
    }, [isSelected])

    const onAcceptCustom = useCallback ((custom) => {
      keyCustomSetDispatch (deviceId, custom)
    }, [deviceId])

    const onSetStatus = useCallback (
      (status) => {
        setStatus (status)
        keyStatusSetDispatch (status, deviceId, false)
      },
      [deviceId],
    )

    const onChangeStatusIcon = useCallback (
      ({ value: status, }) => {
        setStatus (status)
        onSetStatus (status)
      },
      [onSetStatus],
    )

    const onChangeColor = useCallback (
      ({ value, }) => {
        const color = value | ifEmptyString (() => null, id)
        const colorId = colorsEditReverse [color]
        keyColorSetDispatch (deviceId, colorId)
      },
      [colorsEditReverse, deviceId],
    )

    const swatchProps = (color) => ({ color, style: { verticalAlign: 'top', }})

    // --- value is undefined for an instant when it's first mounted -- not sure if it's a bug in
    // the library but `whenOk` is necessary.
    const statusIconSelectedLabelRenderer = useCallbackConst (
      ({ value: status, }) => status | whenOk (
        () => <StatusIcon status={status}/>,
      ),
    )

    const colorSelectedLabelRenderer = useCallbackConst (
      ({ value: color, }) => color | whenOk (() => <>
        <Swatch {... swatchProps (color)} height='50px' width='50px' classes={['u-query-mobile']}/>
        <Swatch {... swatchProps (color)} classes={['u-query-not-mobile']}/>
      </>),
    )

    const optionsStatusIcon = useMemo (() => statuses | map (
      (status) => ({
        text: null,
        // --- note that values get compared using JSON.stringify by the library
        value: status,
        markup: <div className='x__option'>
          <StatusIcon status={status}/>
          <span className='x__text'>
            {status | statusText}
          </span>
        </div>,
      }),
    ), [statuses])

    const optionsColor = colorsEdit | map (({ color, }) => ({
      value: color ?? '',
      markup: <KeyColorDropdownOption color={color}/>,
    }))

    // --- `null` means the key is unassigned.
    const assigneeId = name | ifNil (
      () => null,
      () => assigneesReverse [name],
    )

    const optionsName = useMemo (() => assigneesEdit | flatMap (
      ({ id: assigneeId, name, }) => ({
        // --- `null` is not a valid (React) key, so we use -1 for unassigned keys.
        key: assigneeId | defaultToV (-1),
        value: assigneeId,
        contentString: name,
        ContentElement: ({ children, }) => <KeyRowNameContentS>{children}</KeyRowNameContentS>,
      }),
    ), [assigneesEdit])

    const onSelectName = useCallback (
      ({ id: assigneeId, }) => {
        keyAssigneeSetDispatch (deviceId, assigneeId)
      }, [deviceId],
    )

    const onInteract = useCallback (
      (... _) => onInteractProp (deviceId),
      [deviceId, onInteractProp],
    )
    const onClickClock = useCallback (
      () => lets (
        () => lastUsedText (activityTimestamp, uptimeApproximateByMonth),
        () => (status | statusIsActive) ?
          [', and is still active.', 'Set it to inactive/missing or delete it if necessary.'] :
          [',', 'Delete it if necessary.'],
        (text, [activeText, whatToDo]) => mobilePopup (<span>
          This key was last used {text}{activeText}
          <p>
            {whatToDo}
          </p>
        </span>),
      ),
      [activityTimestamp, uptimeApproximateByMonth, status],
    )
    const onListenSelectOpen = useCallback (
      // --- only fire when select is opened (`onListen` fires for various things, including
      // initialize)
      (_, __, action) => action | whenEq ('SET_OPTIONS_PANEL_OPEN') (
        () => onInteract (),
      ),
      [onInteract],
    )

    /* --- @todo reimplement this?
     *
     * when we change the name in assignee, there will be an instant where `name` in the state
     * is the old name, causing `assigneeId` to be undefined. To avoid a warning we block the render
     * until useEffect has had a chance to fire and resync `name` to `nameProp`.
    if (notDefined (assigneeId)) return
    */

    const CustomInput = useMemo (() => {
      return component (
        ['CustomInput'],
        (props) => {
          const { classes=[], } = props
          useWhy ('CustomInput', props)
          return <Custom
            isMobile={isMobile}
            defaultValue={customProp}
            onAccept={onAcceptCustom}
            classes={classes}
            wrapperRef={wrapperRef}
          />
        },
      )},
      [isMobile, customProp, onAcceptCustom, wrapperRef],
    )

    const AssigneeSelect = useMemo (() => {
      return component (
        ['AssigneeSelect', []],
        (_props) => {
          useWhy ('AssigneeSelect', _props)
          return <SelectWithText
            isMobile={isMobile}
            classes={['x__assignee']}
            selectKey={name}
            selectedValue={assigneeId}
            // selectedLabelContentTransform={truncate (assigneeTruncate)}
            options={optionsName}
            onSelect={onSelectName}
          />
        },
      )},
      [isMobile, name, assigneeId, optionsName, onSelectName],
    )

    const ColorSelect = useMemo (() => {
      return component (
        ['ColorSelect', []],
        (props) => {
          const { editable, } = props
          useWhy ('ColorSelect', props)
          return <Color
            editable={editable}
            classes={['x__color']}
            decorateMobile={false}
            selectKey={color}
            // --- random string (see above)
            name='color-jzlk9aj'
            selectedValue={color}
            customLabelRenderer={colorSelectedLabelRenderer}
            options={optionsColor}
            onChange={onChangeColor}
            onListen={onListenSelectOpen}
          />
        },
      )},
      [color, colorSelectedLabelRenderer, optionsColor, onChangeColor, onListenSelectOpen],
    )

    const outerRef = useRef (null)
    const cls = clss (showInfo && 'x--expanded')
    const clockColor = lets (
      () => status | statusIsActive,
      (warning) => warning ? 'red' : 'black',
    )

    useEffect (() => {
      const { x, } = outerRef.current.getBoundingClientRect ()
      setOffsetLeft (String (-1 * x) + 'px')
    }, [])

    useEffect (() => {
      const f = showInfo ? 'add' : 'remove'
      document.body.classList [f] ('u--vertical-clip')
    }, [showInfo])

    useEffect (() => {
      setStatus (statusProp)
    }, [statusProp])

    invoke (() => {
      // --- wrapperRef causes problems if we try to log it as JSON
      const { wrapperRef: _, ... otherProps } = props
      useWhy ('KeyRow', otherProps, { status, showInfo, offsetLeft, })
    })

    return <KeyRowS
      ref={outerRef}
      isSelected={isSelected}
      onClick={onInteract}
      left={offsetLeft}
      className={cls}
    >
      {mkAnchor ('key', deviceId)}
      <div className='x__selection-cursor'/>
      <div ref={mainRef} className='x__main'>
        <div className={'x__stale-clock'}>
          {staleKeys.has (deviceId) | whenTrue (
            () => <ClockIcon color={clockColor} width='30px' onClick={onClickClock}/>,
          )}
        </div>
        <div>
          <div className='x__status-icon u-query-mobile'>
            <StatusC
              abridged={abridged}
              decorateMobile={false}
              selectKey={status | statusTag}
              // selectKey={statusNForStatus (status)}
              // --- random string at the end because the name doesn't actually matter
              name='status-icon-lskdun1'
              selectedValue={status}
              customLabelRenderer={statusIconSelectedLabelRenderer}
              options={optionsStatusIcon}
              onChange={onChangeStatusIcon}
              onListen={onListenSelectOpen}
            />
          </div>
          {/*
          <div className='x__status-icon u-query-not-mobile'>
            <StatusIcon statusN={statusN}/>
          </div>
          */}
        </div>
        <ColorSelect editable={not (abridged)}/>
        <Assignee
          abridged={abridged}
          name={name}
          isMobile={isMobile}
          assigneeId={assigneeId}
          optionsName={optionsName}
          onSelectName={onSelectName}
          AssigneeSelect={AssigneeSelect}
        />
        {abridged ? <div>{customProp}</div> : <CustomInput/>}
        <div
          onClick={onClickMore}
          className={clss ('x__show-more', showInfo && 'x--showing')}
        >
          <img className='x__img' src={moreIcon}/>
        </div>
      </div>
      {showInfo && <Info
        isMobile={isMobile}
        abridged={abridged}
        deviceId={deviceId}
        authorizations={authorizations}
        doors={doors}
        keyId={keyId}
        activityTimestamp={activityTimestamp}
        privileges={privileges}
        CustomInput={CustomInput}
        AssigneeSelect={AssigneeSelect}
        ColorSelect={ColorSelect}
        statusSliderProps={{
          info: keyRowStatusInfo,
          cur: status,
          onSet: onSetStatus,
        }}
      />}
      {/*
      <div
        className={clss ('x__clear', isSelected || 'u--display-none')}
        onClick={onClickClearSelected}
      >
        clear
      </div>
      */}
    </KeyRowS>
  }
)

const KeyRows = component (
  ['KeyRows'], ({
    keys, isMobile, routeDeviceId, wrapperRef, doors, isSelected, clearSelected, onInteract,
  }) => {
  const abridged = doAbridgedListView && keys.length > 5
  return keys | map (
    ({ keyId, authorizations, privileges, deviceId, color, custom, name, status, activityTimestamp, }) => <KeyRow
      key={keyId}
      isMobile={isMobile}
      abridged={abridged}
      routeDeviceId={routeDeviceId}
      wrapperRef={wrapperRef}
      keyId={keyId}
      authorizations={authorizations}
      privileges={privileges}
      deviceId={deviceId}
      color={color}
      custom={custom ?? ''}
      name={name}
      status={status}
      activityTimestamp={activityTimestamp}
      doors={doors}
      isSelected={isSelected (deviceId)}
      clearSelected={clearSelected}
      onInteract={onInteract}
    />,
  )
})

const KeyS = styled.div`
  > .x__sort {
    > .x__select {
      padding-left: 10px;
      display: inline-block;
    }
  }
  .x__jump-button {
    width: 60%;
    margin: auto;
    margin-top: 25px;
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      margin-top: 50px;
      margin-bottom: 50px;
    `),
  )}
`

const dispatchTable = {
  keySortDispatch: keySort,
  keyGoToPageForKeyDispatch: keyGoToPageForKey,
  keySetSelectedDeviceIdDispatch: keySetSelectedDeviceId,
}

const selectorTable = {
  doors: selectDoors,
  keys: selectKeysComponent,
  latestBench: selectLatestBench,
  selectedDeviceId: selectKeySelectedDeviceId,
  sortKey: selectSortKey,
  sortKeyData: selectSortKeyData,
}

export default container (
  ['Key', dispatchTable, selectorTable],
  (props) => {
    const {
      isMobile, routeDeviceId=null, navigate,
      doors, keys, sortKey,
      selectedDeviceId: selectedDeviceIdProp=null,
      sortKeyData,
      latestBench,
      keySortDispatch, keyGoToPageForKeyDispatch, keySetSelectedDeviceIdDispatch,
    } = props

    if (!keys) return null

    // --- a key gets selected by:
    //   - clicking on it
    //   - navigating to it via route
    //   - reading it through the reader (`isRead` is received via the API call)
    const selectedKeyDeviceId = selectedDeviceIdProp
    const wrapperRef = useRef (null)
    const clearSelected = () => {navigate ('/')}

    const isSelected = useCallback (
      (deviceId) => selectedKeyDeviceId === deviceId,
      [selectedKeyDeviceId],
    )

    // --- used for selecting the row; means the row was clicked. We can't just use onClick because
    // react-responsive-select doesn't use it (it uses onListen instead).
    const onInteract = useCallbackConst (
      (deviceId) => keySetSelectedDeviceIdDispatch (deviceId),
    )

    useEffect (() => {
      keySetSelectedDeviceIdDispatch (selectedDeviceIdProp)
    }, [selectedDeviceIdProp])

    // @todo not use anymore, remove?
    useEffect (() => {
      routeDeviceId | whenOkAndNotEmptyString ((deviceId) => {
        keySetSelectedDeviceIdDispatch (deviceId)
        getAnchor ('key', deviceId) | whenOk (scrollIntoViewWithOptions ({
          block: 'center',
        }))
        keyGoToPageForKeyDispatch (deviceId)
      })
    }, [routeDeviceId])

    useSaga ({ saga, key: 'Key', })
    useWhy ('Key', props)

    // --- @todo refactor options for all selects
    const sortOptions = sortKeyData | map (
      ([sk, _tag, text]) => ({
        // key: tag,
        key: sk,
        value: sk,
        contentString: text,
        ContentElement: ({ children, }) => <KeyRowNameContentS>{children}</KeyRowNameContentS>,
      }),
    )

    // --- @todo repeated, need general goTo function
    const onClickJumpToSelected = useCallback (() => {
      const deviceId = selectedDeviceIdProp
      getAnchor ('key', deviceId) | whenOk (scrollIntoViewWithOptions ({
        block: 'center',
      }))
      keyGoToPageForKeyDispatch (deviceId)
    }, [selectedDeviceIdProp])

    const onSelectSort = useCallbackConst (
      ({ id: sortKey, }) => keySortDispatch (sortKey),
    )

    const PaginationC = useMemo (() => mkPagination ({
      setNumPerPageIdx: setKeysNumPerPageIdx,
      setCurPage: setKeysCurPage,
      selectNumsPerPage: selectKeysNumsPerPageComponent,
      selectPage: selectKeysPageComponent,
    }), [])

    return <KeyS ref={wrapperRef}>
      {/*<p>Latest bench: {latestBench}</p>*/}
      <div className='x__sort'>
        Sort on: <div className='x__select'>
          <SelectWithText
            selectKey={sortKey}
            isMobile={isMobile}
            selectedValue={sortKey}
            options={sortOptions}
            onSelect={onSelectSort}
          />
        </div>
      </div>

      <PaginationC collectionName='keys'/>

      <div className='x__jump-button'>
        <Button width='100%' onClick={onClickJumpToSelected} disabled={nil (selectedDeviceIdProp)}>
          Jump to selected key
        </Button>
      </div>

      <KeyRows
        keys={keys}
        isMobile={isMobile}
        routeDeviceId={routeDeviceId}
        wrapperRef={wrapperRef}
        doors={doors}
        isSelected={isSelected}
        clearSelected={clearSelected}
        onInteract={onInteract}
      />

    </KeyS>
  }
)
