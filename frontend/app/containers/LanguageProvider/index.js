import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import React, {} from 'react'

import { IntlProvider, } from 'react-intl'

import {} from '../App/actions/main'
import { selectLocale, } from '../App/store/app/selectors'

import { container, } from '../../common'

import {} from '../../types'

const dispatchTable = {
}

const selectorTable = {
  locale: selectLocale,
}

export default container (
  ['LanguageProvider', dispatchTable, selectorTable],
  (props) => {
    const { children, locale='en', messages, } = props
    return <IntlProvider
      locale={locale}
      key={locale}
      messages={messages [locale]}
    >
      { /* the production build will fail if this is { ... children} */ }
      { React.Children.only (children) }
    </IntlProvider>
  },
)
