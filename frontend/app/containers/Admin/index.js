import {
  pipe, compose, composeRight,
  join, sprintfN, lets, cond, nil, whenOk, noop,
  whenFalse, tap, map, not, flip,
} from 'stick-js/es'

import React, { useCallback, useEffect, useRef, useState, } from 'react'

import styled from 'styled-components'

import { Tooltip, } from 'react-tooltip'

import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { iwarn, logWith, mapX, } from 'alleycat-js/es/general'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { useSaga, } from 'alleycat-js/es/redux-hooks'
import { mediaQuery, } from 'alleycat-js/es/styled'

import {
  dbReset, dbImport,
  keyRead, keyCheck, keyGoToPageForKey, keyRegenerate, lockInterface,
} from '../App/actions/main'
import {
  selectAdminRead,
  selectAdminCallPending,
  selectAssigneeNameByKeyId,
  selectNumKeysForAssigneeByKeyId,
} from '../App/store/domain/selectors'
import {
  selectCanLockInterface,
} from '../App/store/ui/selectors'

import { AreYouSureDialog, Button as ButtonParent, Dialog, LinkLike, MainSpinner, mobilePopup, Separator, ButtonLink, } from '../../components/shared'
import { Input, } from '../../components/shared/Input'
import {} from './actions'
import saga from './saga'
import {} from './selectors'

import { addEventListener, container, getAnchor, isNilOrEmptyString, mediaPhone, mediaTablet, scrollIntoViewWithOptions, useWhy, } from '../../common'
import config from '../../config'

import { foldIfRequestResults, foldWhenRequestResults, } from '../../types'

const configTop = config | configure.init
const { imgInfo, imgLock, } = configTop.gets ({
  imgInfo: 'icons.info',
  imgLock: 'icons.lock',
})

const ButtonMobileS = styled (ButtonParent)`
  margin: 5px;
`

const ButtonMobile = (props) => <ButtonMobileS
  width='100%'
  {... props}
>
  {props.children}
</ButtonMobileS>

const ButtonNormal = ({ 'tip-content': content, ... props}) => <ButtonMobile
  data-tooltip-id="tooltip"
  data-tooltip-delay-show="300"
  data-tooltip-content={content}
  {... props}
>
  {props.children}
</ButtonMobile>

const Button = (props) => <>
  <ButtonMobile className='u-query-mobile' {... props}>
    {props.children}
  </ButtonMobile>
  <ButtonNormal className='u-query-not-mobile' {... props}>
    {props.children}
  </ButtonNormal>
</>

export const ReadKeyButton = container (
  ['ReadKeyButton', { keyReadDispatch: keyRead, }, {}, null],
  ({ disabled=false, keyReadDispatch, }) => {
    const onClick = useCallbackConst (
      () => keyReadDispatch (),
    )
    return <Button
      disabled={disabled}
      tip-content={adminText.read}
      onClick={onClick}>
      Read key
    </Button>
  },
)

export const CheckKeyButton = container (
  ['CheckKeyButton', { keyCheckDispatch: keyCheck, }, {}, null],
  ({ text='Check access for key', disabled=false, unlockScreen=false, keyCheckDispatch, }) => {
    const onClick = useCallbackConst (
      () => keyCheckDispatch (unlockScreen),
    )
    return <Button
      disabled={disabled}
      tip-content={adminText.check}
      onClick={onClick}
    >
      {text}
    </Button>
  },
)

const AdminS = styled.div`
  >.x__lock {
    position: absolute;
    top: 30px;
    right: 30px;
    img {
      width: 60px;
    }
    &.x--disabled {
      opacity: 0.5;
      cursor: not-allowed;
    }
  }
  >.x__buttons, >.x__results {
  }
  >.x__buttons {
    vertical-align: middle;
  }
  >.x__buttons >.x__buttons-wrapper {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    >.x__button-row {
      flex: 0 1 auto;
      >.x__info {
        display: inline-block;
        width: 30px;
        img {
          width: 100%;
        }
      }
    }
  }
  >.x__results {
    height: 40px;
    width: calc(100% - 300px - 30px - 30px);
    vertical-align: top;
  }
  ${mediaQuery (
    mediaPhone (`
      margin: auto;
      width: 80%;
      >.x__buttons {
        >.x__buttons-wrapper {
          >.x__spinner-wrapper {
            margin: auto;
          }
          >.x__button-row {
            margin-top: 5px;
            margin-bottom: 5px;
            >.x__info {
              width: 40px;
            }
            button, .x__button-box {
              width: calc(100% - 60px - 20px);
              margin-right: 30px;
            }
          }
        }
      }
    `),
    mediaTablet (`
      margin: initial;
      width: 100%;
      >.x__buttons, >.x__results {
        margin-right: 30px;
        display: inline-block;
      }
      >.x__buttons {
        width: 300px;
        >.x__buttons-wrapper {
          >.x__button-row {
            button {
              margin-right: 5px;
            }
          }
        }
      }
    `),
  )}
`

const ReportS = styled.div`
  .x__key-id, .x__secret {
    display: inline-block;
  }
  .x__key-id {
    text-decoration: underline;
    cursor: pointer;
  }
  .x__secret {
  }
`

const ImportDialogS = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  >.x__main >form >.x__buttons {
    width: 100%;
  }
`

const mkReport = ({ isNewKey, assigneeName, numKeysForAssignee, isNewSecret, }) => lets (
  () => cond (
    [() => isNewKey, () => 'a new key'],
    [() => assigneeName | nil, () => 'an existing, unassigned key'],
    [() => numKeysForAssignee === 1, () => [assigneeName, numKeysForAssignee] | sprintfN (
      `%s's key`,
    )],
    [() => numKeysForAssignee, () => [assigneeName, numKeysForAssignee] | sprintfN (
      `one of %s's %s keys`,
    )],
  ),
  () => isNewSecret ? ', and a new secret has been generated and stored' : '',
  (aNew, secret) => `This is ${aNew}${secret}.`,
    (aNew, secret, text) => ({ aNew, secret, text }),
)

const Report = ({ scrollTo, isNewSecret, isNewKey, deviceId, numKeysForAssignee, assigneeName, }) => {
  const { aNew, secret, } = mkReport ({ isNewKey, assigneeName, numKeysForAssignee, isNewSecret, })

  return <ReportS>
    This is {aNew}
    &nbsp;(<div className='x__key-id' onClick={
      () => scrollTo (deviceId)
    }>
      {deviceId}
    </div>){secret}.
  </ReportS>
}

const dispatchTable = {
  dbResetDispatch: dbReset,
  dbImportDispatch: dbImport,
  keyGoToPageForKeyDispatch: keyGoToPageForKey,
  keyRegenerateDispatch: keyRegenerate,
  lockInterfaceDispatch: lockInterface,
}

const selectorTable = {
  adminCallPending: selectAdminCallPending,
  adminRead: selectAdminRead,
  numKeysForAssigneeByKeyId: selectNumKeysForAssigneeByKeyId,
  assigneeNameByKeyId: selectAssigneeNameByKeyId,
  canLockInterface: selectCanLockInterface,
}

const probeText = (t) => <span>
  {[... t, '(Place the key on the admin probe while the blue LED is lit).'] | mapX (
    (x, idx) => <span key={idx}><p>{x}</p></span>,
  )}
</span>
const adminText = {
  read: probeText ([`Use this to get information about a key or to add a new key to the system.`]),
  check: probeText ([`Use this to check whether a key is known to the system and has a status of “active”.`, `Note that it does not check whether the key is actually authorized for any doors.`]),
  regenerate: probeText ([`Use this if a key has been out of the owner's possession and may have fallen into the wrong hands.`, `The secret stored in the key will be regenerated from scratch and automatically updated in the system. This action is safe to perform as often as you like.`]),
  exportDB: `Download a copy of the entire database. Be careful as this will contain all key secrets and personal information. Store in a safe place, preferably on an encrypted drive.`,
  importDB: `Replace the current database with a previously downloaded backup. This will irrevocably discard the current keys and assignees.`,
  resetDB: `Reset the database to factory settings. This will irrevocably remove all keys and assignees.`,
}

const FileInput = ({ onChange, height, name, accept=void 8, }) => {
  const inputRef = useRef (null)
  useEffect (() => {
    const remove = inputRef.current | whenOk (
      (el) => {
        // --- this element keeps unmounting and changing for some reason -- here we restore its
        // value at startup and it seems to work.
        onChange (el.value)
        return el | addEventListener ('change', () => onChange (el.value))
      },
    )
    return () => {
      onChange (null)
      if (remove) remove ()
    }
  }, [onChange])
  return <Input theRef={inputRef} type='file' accept={accept} height={height} name={name}/>
}

export default container (
  ['Admin', dispatchTable, selectorTable],
  (props) => {
    const {
      isLocal, isMobile, navigate,
      adminCallPending, adminRead,
      canLockInterface,
      numKeysForAssigneeByKeyId, assigneeNameByKeyId,
      keyGoToPageForKeyDispatch, keyRegenerateDispatch, lockInterfaceDispatch,
      dbImportDispatch, dbResetDispatch,
    } = props

    useSaga ({ saga, key: 'Admin', })

    const scrollTo = useCallback ((deviceId) => {
      getAnchor ('key', deviceId) | whenOk (scrollIntoViewWithOptions ({
        block: 'center',
      }))
      keyGoToPageForKeyDispatch (deviceId)
    }, [adminRead])

    const [resetDialogOpen, setResetDialogOpen] = useState (false)
    const [importConfirmDialogOpen, setImportConfirmDialogOpen] = useState (false)
    const [importDialogOpen, setImportDialogOpen] = useState (false)
    const [importFile, setImportFile] = useState (null)
    const importUploadButtonDisabled = importFile | isNilOrEmptyString

    const onClickAdminInfo = {
      read: useCallbackConst (() => mobilePopup (adminText.read)),
      check: useCallbackConst (() => mobilePopup (adminText.check)),
      regenerate: useCallbackConst (() => mobilePopup (adminText.regenerate)),
      exportDB: useCallbackConst (() => mobilePopup (adminText.exportDB)),
      importDB: useCallbackConst (() => mobilePopup (adminText.importDB)),
      resetDB: useCallbackConst (() => mobilePopup (adminText.resetDB)),
    }
    const onClickKeyRegenerate = useCallbackConst (
      () => keyRegenerateDispatch (),
    )
    const onClickDBExport = useCallbackConst (
      // --- note that this breaks the react hot reload in dev mode
      () => document.location = '/api/db-export/?ts=' + Number (new Date ()),
    )
    const onClickLock = useCallback (
      () => canLockInterface && lockInterfaceDispatch (),
      [canLockInterface],
    )

    const doDBReset = useCallbackConst (
      () => {
        dbResetDispatch ()
        closeResetDialog ()
      }
    )
    const closeResetDialog = useCallbackConst (
      () => setResetDialogOpen (false),
    )
    const onClickDBReset = useCallbackConst (
      () => setResetDialogOpen (true),
    )
    const onYesImportConfirmDialog = useCallbackConst (
      () => {
        setImportDialogOpen (true)
        setImportConfirmDialogOpen (false)
      }
    )
    const closeImportConfirmDialog = useCallbackConst (
      () => setImportConfirmDialogOpen (false),
    )
    const onClickDBImport = useCallbackConst (
      () => setImportConfirmDialogOpen (true),
    )
    const closeImportDialog = useCallbackConst (
      () => setImportDialogOpen (false),
    )
    const onImportFormClickButton = useCallbackConst ((event) => {
      // --- prevent the form from submitting
      event.preventDefault ()
      closeImportDialog ()
      // --- @todo hardcoded names
      const form = document.forms ['db-import-form']
      if (!form) return iwarn ("Can't get form db-import-form")
      dbImportDispatch (form ['db-import-file'].files [0])
    })

    const clsExportButtonRow = clss (
      'x__button-row',
      isLocal && 'u--display-none',
    )

    const readPopupShown = useRef (false)

    useWhy ('Admin', props)

    useEffect (() => {
      if (not (isMobile)) return
      adminRead | flip (foldIfRequestResults) (
        // --- no results
        () => { readPopupShown.current = false },
        // --- results
        ({ deviceId, isNewSecret, isNewKey, ... _}) => {
          if (readPopupShown.current) return
          readPopupShown.current = true
          const { text, } = mkReport ({
            isNewKey, isNewSecret,
            assigneeName: assigneeNameByKeyId (deviceId),
            numKeysForAssignee: numKeysForAssigneeByKeyId (deviceId),
          })
          mobilePopup (
            <>
              <div>
                {text}
              </div>
              <div style={{ marginTop: '10px', }}>
                <Button onClick={() => scrollTo (deviceId)}>
                  Go to key
                </Button>
              </div>
            </>,
          )
        },
      )
    }, [
      adminRead, isMobile, assigneeNameByKeyId, numKeysForAssigneeByKeyId, scrollTo,
    ])

    const ReportX = () => adminRead | foldWhenRequestResults (
      ({ deviceId, isNewSecret, isNewKey, isUnlockingScreen, ... _ }) => isUnlockingScreen | whenFalse (
        () => <Report
        scrollTo={scrollTo}
        deviceId={deviceId}
        isNewSecret={isNewSecret}
        isNewKey={isNewKey}
        numKeysForAssignee={numKeysForAssigneeByKeyId (deviceId)}
        assigneeName={assigneeNameByKeyId (deviceId)}
      />
    ))

    return <AdminS>
      <AreYouSureDialog
        extraWarn
        isOpen={resetDialogOpen}
        onRequestClose={closeResetDialog}
        onYes={doDBReset}
        onNo={closeResetDialog}
        contents={<span>
          This will irrevocably remove all keys and assignees and reset the entire database. Use this only if you know what you are doing.
        </span>}
        isMobile={isMobile}
      />
      <AreYouSureDialog
        extraWarn
        isOpen={importConfirmDialogOpen}
        onRequestClose={closeImportConfirmDialog}
        onYes={onYesImportConfirmDialog}
        onNo={closeImportConfirmDialog}
        contents={<span>
          This will restore a previously made backup, irrevocably discarding any new keys and assignees. Use this only if you know what you are doing.
        </span>}
        isMobile={isMobile}
      />
      <Dialog
        isOpen={importDialogOpen}
        onRequestClose={closeImportDialog}
        isMobile={isMobile}
      >
        <ImportDialogS>
          <div className='x__main'>
            <form
              name='db-import-form'
              method='POST'
            >
              Upload .db file
              <FileInput
                onChange={setImportFile}
                height={57}
                name='db-import-file'
                accept='application/vnd.sqlite3'
              />
              <div className='x__buttons'>
                <Button width='30%' disabled={importUploadButtonDisabled} onClick={onImportFormClickButton}>
                  Upload
                </Button>
                <Button width='30%' onClick={closeImportDialog}>
                  Cancel
                </Button>
              </div>
            </form>
          </div>
        </ImportDialogS>
      </Dialog>

      <div className={clss ('x__lock', canLockInterface || 'x--disabled')}>
        <img src={imgLock} onClick={onClickLock}/>
      </div>
      <div className='x__buttons'>
        <div className='x__buttons-wrapper'>
          <div className='x__spinner-wrapper u-query-mobile'>
            <MainSpinner scale={2} spinning={adminCallPending}/>
          </div>
          <div className='x__button-row'>
            <ReadKeyButton disabled={adminCallPending} unlockScreen={false}/>
            <div className='x__info u-query-mobile' onClick={onClickAdminInfo.read}>
              <img src={imgInfo}/>
            </div>
          </div>
          <div className='x__button-row'>
            <CheckKeyButton disabled={adminCallPending} unlockScreen={false}/>
            <div className='x__info u-query-mobile' onClick={onClickAdminInfo.check}>
              <img src={imgInfo}/>
            </div>
          </div>
          <div className='x__button-row'>
            <div className='x__button-box'>
              <Separator/>
            </div>
          </div>
          <div className='x__button-row'>
            <Button
              disabled={adminCallPending}
              tip-content={adminText.regenerate}
              onClick={onClickKeyRegenerate}
            >
              Regenerate secret for key
            </Button>
            <div className='x__info u-query-mobile' onClick={onClickAdminInfo.regenerate}>
              <img src={imgInfo}/>
            </div>
          </div>
          <div className={clsExportButtonRow}>
            <Button onClick={onClickDBExport} disabled={adminCallPending}>
              Backup database
            </Button>
            <div className='x__info u-query-mobile' onClick={onClickAdminInfo.exportDB}>
              <img src={imgInfo}/>
            </div>
          </div>
          {/* @todo proper alignment of <a> with <button>, for now a pixel nudge */}
          <div className={clsExportButtonRow} style={{
            position: 'relative',
            left: '5px',
          }}>
            <Button onClick={onClickDBImport} disabled={adminCallPending}>
              Restore database backup
            </Button>
            <div className='x__info u-query-mobile' onClick={onClickAdminInfo.importDB}>
              <img src={imgInfo}/>
            </div>
          </div>
          <div className='x__button-row'>
            <div className='x__button-box'>
              <Separator/>
            </div>
          </div>
          <div className='x__button-row'>
            <Button onClick={onClickDBReset} disabled={adminCallPending}>
              Reset database
            </Button>
            <div className='x__info u-query-mobile' onClick={onClickAdminInfo.resetDB}>
              <img src={imgInfo}/>
            </div>
          </div>
        </div>
      </div>
      <div className='x__results'>
        <div className='u-query-not-mobile'>
          <MainSpinner spinning={adminCallPending}/>
        </div>
        <div className='u-query-not-mobile'>
          <ReportX/>
        </div>
      </div>
      <Tooltip id="tooltip" effect='solid' openOnClick={false} style={{
        display: 'inline-block',
        width: '300px',
      }}/>
    </AdminS>
  }
)
