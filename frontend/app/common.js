import {
  pipe, compose, composeRight,
  concatTo, lt, bindProp, noop, ifTrue,
  bindTryProp, defaultTo, lets, invoke, ifOk, id,
  T, F, prop, condS, gt, guard, sprintf1, arg0, divideBy, reduce,
  tap, otherwise, recurry, concat, side2, remapTuples, mergeToM,
  ne, not, ifPredicate, always, allAgainst, ok, whenPredicate,
  mergeTo, die, againstEither, dot2, nil, addIndex2, eq,
  sprintfN, ampersandN,
} from 'stick-js/es'

import React, { useState, } from 'react'

import { useNavigate, } from 'react-router-dom'
import { Bounce as ToastBounce, toast, } from 'react-toastify'
import { call as sagaCall, all as sagaAll, put as sagaPut, } from 'redux-saga/effects'
const sagaEffects = {
  call: sagaCall,
  all: sagaAll,
  put: sagaPut,
}

import { cata, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { doApiCall as _doApiCall, } from 'alleycat-js/es/fetch'
import { getQueryParams, length, logWith, } from 'alleycat-js/es/general'
import { all, allV, ifArray, ifFalseV, isEmptyString, isEmptyList, notDefined, } from 'alleycat-js/es/predicate'
import { componentTell, containerTell, useCallbackConst, useWhyTell, } from 'alleycat-js/es/react'
import { prepareIntlComponent, } from 'alleycat-js/es/react-intl'
import { reducerTell, } from 'alleycat-js/es/redux'
import { saga as _saga, } from 'alleycat-js/es/saga'
import { initSelectorsTell, } from 'alleycat-js/es/select'
import { mediaRule, mgt, mlt, } from 'alleycat-js/es/styled'

import { spinner, } from './alleycat-components'
import config from './config'
import { envIsDev, } from './env'
import { defaultMessages, } from './translations'

const configTop = config | configure.init

const alwaysMobile = configTop.get ('alwaysMobile')

const slice = dot2 ('slice')

export const log = console | bindProp ('log')
export const debug = console | bindTryProp ('debug') | defaultTo (() => log)

export const debugIt = (...args) => debug ('[debug] *', ...args)
export const debugDev = envIsDev ? debugIt : noop
export const debugDevWith = header => (...args) => debugDev (... [header, ...args])

/* These are mostly based on the Bootstrap 4 breakpoints, with the tablet breakpoint a bit higher so
 * that a small 480x800 screen in landscape mode is still considered a 'phone'.
 * Note that these are from the given width *and up*. If you use `mediaPhone` then you probably also
 * want to provide `mediaTablet`, `mediaDesktop`, etc., or else it will apply to every width. To
 * make it really apply to only the given width, use the 'only' variant, e.g. `mediaPhoneOnly`.
 */

const [mediaRulePhone, mediaRuleTablet, mediaRuleDesktop, mediaRuleDesktopBig, mediaRulePhoneOnly] = lets (
  () => [mgt (0), mlt (0)],
  ([always, never]) => alwaysMobile | ifTrue (
    () => [always, never, never, never, always],
    () => [mgt (0), mgt (801), mgt (992), mgt (1200), mlt (801)],
  ),
)

export const mediaPhone      = mediaRulePhone      | mediaRule
export const mediaTablet     = mediaRuleTablet     | mediaRule
export const mediaDesktop    = mediaRuleDesktop    | mediaRule
export const mediaDesktopBig = mediaRuleDesktopBig | mediaRule
export const mediaPhoneOnly  = mediaRulePhoneOnly  | mediaRule
export const isMobileWidth = 992 | lt

// --- note, user agent tests are never totally reliable.
// --- see mdn docs for a pretty good break-down.

export const isSafari = () => lets (
  () => window.navigator.userAgent,
  (ua) => allV (
    ua.indexOf ('Safari') !== -1,
    ua.indexOf ('Chrome') === -1,
    ua.indexOf ('Chromium') === -1,
  ),
)

/* Momentum scroll seems to be causing problems (flickering, bouncing weird & possibly causing other
 * problems).
 * Seen on iPhone 5 (iOS11 / 605) and iPhone SE.
 * For now, disabling on all (mobile) Safaris.
 */
export const shouldDisableMomentumScroll = invoke (() => {
  const cutoff = Infinity

  const q = getQueryParams ()
  if (q.momentum === '1') return F

  return () => lets (
    () => window.navigator.userAgent,
    (ua) => all (
      () => isSafari (),
      () => ua.match (/AppleWebKit\/(\d+)/),
      (_, m) => m | prop (1) | Number | lt (cutoff),
    ),
  )
})

export const checkUploadFilesSize = ({
  file,
  maxFileSize: max = 1024 * 1024,
  prettyBytesDecimalPlaces: places = 0,
  alertFunc = noop,
}) => file.size | condS ([
  max | gt | guard (_ => max
    | prettyBytes (places)
    | sprintf1 ('File too large! (max = %s)')
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

export const prettyBytes = invoke (() => {
  const row = recurry (4) (
    fmt => pred => n => suffix =>
      Math.pow (1024, n + 1) | pred | guard (
        arg0 >> divideBy (Math.pow (1024, n)) >> sprintf1 (fmt) >> concat (' ' + suffix)
      )
  )

  return numDecimals => lets (
    _ => numDecimals | sprintf1 ('%%.%sf'),
    (fmt) => row (fmt),
    (_, rowFmt) => condS ([
      rowFmt (lt, 0, 'b'),
      rowFmt (lt, 1, 'k'),
      rowFmt (lt, 2, 'M'),
      rowFmt (_ => T, 3, 'G'),
    ]),
  )
})

export const makeFormData = invoke (() => {
  const app = side2 ('append')
  return (data) => data
    | remapTuples (app)
    | reduce (pipe, new FormData ())
})

export const useWhy = 'debug.render' | configTop.get | useWhyTell

// const debug  = (...args) => console.debug (...args)

export const component = 'debug.render' | configTop.get | componentTell
export const container = 'debug.render' | configTop.get | containerTell

export const getMessages = (idPref) => defaultMessages [idPref] | prepareIntlComponent (idPref)

export const reducer = 'debug.reducers' | configTop.get | reducerTell
export const initSelectors = 'debug.selectors' | configTop.get | initSelectorsTell

export const N = null | always
export const V = void 8 | always

const toastXOptions = recurry (3) (
  (f) => (opts) => (msg) => toast [f] (msg, opts | mergeToM ({
    transition: ToastBounce,
    autoClose: 5000,
    closeOnClick: true,
  })),
)

export const toastErrorOptions = toastXOptions ('error')
export const toastWarnOptions = toastXOptions ('warn')
export const toastSuccessOptions = toastXOptions ('success')
export const toastInfoOptions = toastXOptions ('info')

export const toastError = toastErrorOptions ({})
export const toastWarn = toastWarnOptions ({})
export const toastSuccess = toastSuccessOptions ({})
export const toastInfo = toastInfoOptions ({})

export function saga (...args) {
  return _saga (sagaEffects, ...args)
}

export function *doApiCall (opts) {
  yield sagaCall (_doApiCall, sagaEffects, opts | mergeTo ({
    oops: toastError,
  }))
}

// --- returns the corresponding remove function
// @todo alleycat-js
export const addEventListener = recurry (3) (
  (eventName) => (f) => (el) => {
    el.addEventListener (eventName, f)
    return () => el.removeEventListener (eventName, f)
  }
)

export const requestResults = invoke (() => {
  const SpinnerComet = spinner ('comet')
  return ({
    Spinner=SpinnerComet,
    onError=ifOk (
      String >> concatTo ('Error: '),
      () => 'Error',
    ),
    onResults=id,
    spinnerProps={ color: '#033966', },
  } = {}) => cata ({
    RequestInit: () => null,
    RequestLoading: (_) => <Spinner {... spinnerProps}/>,
    RequestError: (err) => err | onError,
    RequestResults: (res) => res | onResults,
  })
})

export const defined = void 8 | ne
export const ifUndefined = (defined >> not) | ifPredicate

// :: (a -> b) -> Maybe a -> b | undefined
export const foldWhenJust = recurry (2) (
  (f) => (mb) => mb | fold (f, void 8),
)

export const lookupOn = recurry (2) (
  o => k => o [k],
)
export const lookup = recurry (2) (
  k => o => lookupOn (o, k),
)
export const lookupOnOr = recurry (3) (
  (f) => (o) => (k) => lookupOn (o, k) | ifUndefined (f, id),
)
export const lookupOr = recurry (3) (
  (f) => (k) => (o) => lookupOnOr (f, o, k),
)
export const lookupOnOrV = recurry (3) (
  (x) => lookupOnOr (x | always),
)
export const lookupOrV = recurry (3) (
  (x) => lookupOr (x | always),
)
export const lookupOrDie = recurry (3) (
  (msg) => (k) => (o) => lookupOnOr (
    () => die (msg),
    o, k,
  )
)
export const lookupOnOrDie = recurry (3) (
  (msg) => (o) => (k) => lookupOrDie (msg, k, o),
)

export const allOk = allAgainst (ok)

export const whenGt = recurry (2) (
  (x) => gt (x) | whenPredicate,
)

export const ifGt = recurry (3) (
  (x) => gt (x) | ifPredicate,
)

export const truncate = recurry (2) (
  (n) => (s) => lets (
    () => length (s) > n,
    (doIt) => doIt | ifFalseV (id, slice (0, n) >> concat ('…')),
    (_, f) => f (s),
  ),
)

export const isNilOrEmptyString = againstEither (nil, isEmptyString)
export const isNotNilOrEmptyString = not << isNilOrEmptyString
export const ifNilOrEmptyString = isNilOrEmptyString | ifPredicate
export const whenOkAndNotEmptyString = isNotNilOrEmptyString | whenPredicate

export const isNotEmptyList = not << isEmptyList
export const isNotEmptyString = not << isEmptyString

export const allNil = allAgainst (nil)
export const ifAllNil = allNil | ifPredicate
export const allUndefined = allAgainst (notDefined)
export const ifAllUndefined = allUndefined | ifPredicate

export const reduceX = reduce | addIndex2

export const hasFocus = (el) => el | eq (window.document.activeElement)

// --- @todo alleycat-js
// --- this is a hook which implements what we can call a "flipper" -- a piece of state which we use
// when we don't care about the value, but just the fact that it changes, for example to force a
// component to rerender.

export const useFlipState = () => {
  const [val, setVal] = useState (false)
  return ({
    get () { return val },
    flip () { setVal (not (val)) },
  })
}

// --- usage: [1, 2, 3] | elementAt (1) // -> 2
export const elementAt = prop

export const listFlatMap = recurry (2) (
  (f) => (o) => o.flatMap ((x) => f (x)),
)

export const useNavigateWithLocal = (isLocal) => {
  const navigate = useNavigate ()
  return useCallbackConst ((to) => navigate ({
    pathname: to,
    search: isLocal ? 'local' : null,
  })
)}

export const benchmark = (tag, f, onBench=null) => (... args) => {
  const d = Date.now ()
  const ret = f (... args)
  const delta = Date.now () - d
  if (onBench) onBench (tag, delta)
  console.log ([tag, delta] | sprintfN ('[bench] %s took %.1fms'))
  return ret
}

export const ifZero = eq (0) | ifPredicate
export const toListSingleton = ifArray (id, (x) => [x])
// --- `pam` is like the reverse of `map`: given a list of functions and a value, return a list
// consisting of each function applied to the value. The annoying thing is remembering the order of
// the arguments (we need to think of something). For now it's: [double, triple] | pam (3) to get
// [6, 9]
export const pam = recurry (2) (
  (x) => (fs) => ampersandN (fs, x),
)

export const whenEq = recurry (3) (
  eq >> whenPredicate,
)

export const scrollIntoView = (el) => el.scrollIntoView ()
export const scrollIntoViewWithOptions = recurry (2) (
  (opts) => (el) => el.scrollIntoView (opts),
)

export const [getAnchor, mkAnchor] = invoke (() => {
  const mkName = (what, which) => [what, what, which] | sprintfN (
    '%ss-%s-%s',
  )
  return [
    (what, which) => lets (
      () => [mkName (what, which)] | sprintfN ('a[name=%s]'),
      (sel) => document.querySelector (sel),
    ),
    (what, which, props) => <a name={mkName (what, which)} {... props}/>,
  ]
})
