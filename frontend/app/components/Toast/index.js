import React from 'react';
import {ToastContainer, cssTransition as toastCssTransition} from 'react-toastify';

import styled from 'styled-components'

import 'react-toastify/dist/ReactToastify.css'

import { mediaQuery, } from 'alleycat-js/es/styled'
import { mediaPhone, mediaPhoneOnly, mediaTablet, mediaDesktop, } from '../../common'

const ToastTransition = toastCssTransition ({
  enter: 'Toastify__slide-enter',
  exit: 'Toastify__slide-exit',
  duration: [300, 100],
  appendPosition: true,
})

const ToastContainerS = styled (ToastContainer)`
  --toastify-toast-width: 90vw;
  --toastify-toast-min-height: 90vh;
  @keyframes toast-mobile-popup-enter {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  .Toastify {
    &__toast {
      right: 1em;
      border-radius: 100px;
      padding: 30px;
      font-size: 25px;
      &.mobile-popup-enter {
        animation: toast-mobile-popup-enter 0.3s;
      }
      // --- this is important for if you click on the popup rather than swiping it away (otherwise
      // it turns invisible but gets in the way)
      &.mobile-popup-exit {
        display: none;
      }
    }
    // @todo Does this have an effect?
    &__toast-container {
      width: 80%;
      height: 80%;
    }
    &__toast--warning {
      background: var(--toastify-color-warning);
      color: var(--toastify-color-dark);
      button {
        color: var(--toastify-color-dark);
        opacity: 1;
      }
    }
    &__toast-icon {
      display: none;
    }
    &__close-button {
      display: none;
    }
    &__toast--error {
      background: #b21515;
      color: var(--toastify-color-light);
      button {
        color: var(--toastify-color-light);
        opacity: 1;
      }
    }
    &__toast--error, &__toast--warning {
      font-size: 20px;
    }
    ${mediaQuery (
      mediaPhoneOnly (`
        &__toast--error, &__toast--warning, &__toast--info {
          font-size: 40px;
        }
      `),
    )};
  }
`

export default function ToastComponent() {
  return <ToastContainerS
    transition={ToastTransition}
    hideProgressBar={true}
    autoClose={8000}
    draggablePercent={20}
  />
}
