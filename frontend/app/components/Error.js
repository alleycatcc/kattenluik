import {
  pipe, compose, composeRight,
  eq, ifPredicate, sprintfN,
} from 'stick-js/es'

import React, { useCallback, useEffect, useRef, useState, } from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { logWith, setTimeoutOn, } from 'alleycat-js/es/general'
import {} from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { useWhy, mediaPhone, mediaTablet, mediaDesktop, component, } from '../common'
import config from '../config'
import { Button, SpinnerText, } from './shared'

const RELOAD_SECS = 10

const configTop = config | configure.init

const ifZero = eq (0) | ifPredicate
const ifOne = eq (1) | ifPredicate
const reload = () => document.location.reload ()
const getS = ifOne (() => '', () => 's')

const ErrorS = styled.div`
`

export default component (
  ['Error', null],
  (props) => {
    const {} = props
    const [secs, setSecs] = useState (RELOAD_SECS)
    const tick = useCallback (() => {
      const s = secs - 1
      if (secs === 0) return reload ()
      setSecs (s)
    }, [secs])
    const onClick = useCallbackConst (() => reload ())
    useEffect (
      () => { 1000 | setTimeoutOn (tick) },
      [secs, tick],
    )
    useWhy ('Error', props)
    return <ErrorS>
      <p>
        Sorry, but we’ve encountered a fatal error.
        <br/>
        {secs | ifZero (
          () => <SpinnerText/>,
          () => [secs, getS (secs)] | sprintfN ('The page will automatically reload in %d second%s, or you can use the button below.'),
        )}
      </p>

      <Button onClick={onClick}>reload</Button>

    </ErrorS>
  },
)
