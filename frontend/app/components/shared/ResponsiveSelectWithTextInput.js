import {
  pipe, compose, composeRight,
  find, whenOk, appendM, lets, not,
  noop, ifNil, defaultTo, prop, eq, reduce, ok,
} from 'stick-js/es'

import React, { useCallback, useEffect, useMemo, useRef, useState, } from 'react'

import memoizeOne from 'memoize-one'
import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { length, ierror, logWith, } from 'alleycat-js/es/general'
import { ifEmptyString, ifPredicateResult, ifDefined, } from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import ResponsiveTextInput from './ResponsiveTextInput'

import { mediaPhoneOnly, useWhy, component, ifAllUndefined, } from '../../common'
import config from '../../config'

const configTop = config | configure.init

// --- one of `selectedIdx` or `selectedValue` is defined
const getCurrentOption = memoizeOne ((options, selectedIdx, selectedValue) => lets (
  () => selectedIdx | ifDefined (
    (idx) => prop (idx),
    () => find (prop ('value') >> eq (selectedValue)),
  ),
  (f) => options | f | defaultTo (() => {
    ierror ('currentLabel')
    return <span>&nbsp;</span>
  }),
))

const matchesFilter = (filterString) => (contentString) => {
  if (filterString === '') return false
  const idx = contentString.toLowerCase ().indexOf (filterString.toLowerCase ())
  if (idx === -1) return false
  return { start: idx, end: idx + length (filterString), }
}
const ifMatchesFilter = matchesFilter >> ifPredicateResult

// --- the styling here is meant to match react-responsive-select as closely as possible
const ResponsiveSelectWithTextInputS = styled.div`
  > .x__popup {
    display: none;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 2;
    background: rgba(0, 0, 0, 0.5);
    > .x__popup-contents {
      position: absolute;
      overflow-y: auto;
      background: white;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      border: 2px solid black;
      padding: 4px;
      ul {
        margin-top: 0;
        margin-bottom: 0;
        list-style: none;
        padding: 0;
      }
      li {
        padding: 0.75rem 1rem;
        font-size: 1.25rem;
        &.x--disabled {
          opacity: 0.5;
        }
        &.x--selected {
          color: #0273b5;
          background: #f1f8fb;
          outline: 1px solid rgb(224, 224, 224);
        }
      }
      > *::nth-child(1) {
        margin-top: 10px;
      }
      > .x__options {
        .x__highlight {
          color: green;
        }
        // border-bottom: 2px solid black;
      }
    }
  }
  &.x--show {
    .x__popup {
      display: block;
    }
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      >.x__popup >.x__popup-contents li {
        font-size: 1.8rem;
      }
    `),
  )};
`

// --- @todo enable escape
// --- @todo non-mobile version should use Combobox
export default component (
  ['ResponsiveSelectWithTextInput', null],
  (props) => {
    const {
      isMobile,
      initialInput='',
      autoFocus=false,
      // ------ one of `selectedIdx` or `selectedValue` must be given.
      // --- if given, this option will be plucked from the options array (most efficient).
      selectedIdx: selectedIdxProp,
      // --- otherwise, the values will be looped through to search for `selectedValue`.
      selectedValue,
      options, onSelect: onSelectProp=noop,
      customLabelRenderer=({ contentString, ContentElement, }) => <ContentElement>{contentString}</ContentElement>,
      style={},
    } = props

    const [show, setShow] = useState (false)
    const [inputValue, setInputValue] = useState (initialInput)

    const selectedIdx = [selectedIdxProp, selectedValue] | ifAllUndefined (
      () => {
        ierror ('selectedIdx and selectedValue are both undefined, choosing 0')
        return 0
      },
      () => selectedIdxProp,
    )

    const currentLabel = getCurrentOption (options, selectedIdx, selectedValue) | customLabelRenderer
    const onUpdateInput = useCallbackConst ((value) => { setInputValue (value) })

    const onSelect = useCallback (
      (value) => {
        onSelectProp ({ id: value, })
        setShow (false)
      },
      [onSelectProp],
    )

    const Li = useMemo (() => {
      return ({ theKey, disabled=false, value, contentString, ContentElement, highlight=null, }) => {
        const content = highlight | ifNil (
          () => [contentString],
          () => [
            contentString.slice (0, highlight.start),
            <span key={theKey} className='x__highlight'>
              {contentString.slice (highlight.start, highlight.end)}
            </span>,
            contentString.slice (highlight.end),
          ]
        )
        const onClick = useCallback (
          () => disabled || onSelect (value),
          // --- @todo linter says onSelect is unnecessary, but is it?
          [onSelect, value, disabled],
        )
        return <li
          key={theKey}
          onClick={onClick}
          className={clss (
            (value === selectedValue) && 'x--selected',
            disabled && 'x--disabled',
          )}
        >
          <ContentElement key={theKey}>{content}</ContentElement>
        </li>
      }
    }, [selectedValue, onSelect])

    // --- matches and rest will never both be necessary, but calculating them simultaneously like
    // this is a bit easier.
    // --- we assume that all options have a unique `value`.
    // --- `matchValue` is the `value` of the last option to match, and is useful in the case that
    // there is exactly one match.
    const [matches, rest, matchValue] = options | reduce (
      ([matches, rest, matchValue], { key=value, value, contentString, ContentElement, }) => {
        const match = contentString | matchesFilter (inputValue)
        if (match && ok (value)) {
          const highlight = match
          return [matches | appendM (
            <Li key={key} theKey={key} disabled={false} value={value} contentString={contentString} ContentElement={ContentElement} highlight={highlight}/>,
          ), rest, value]
        }
        else {
          return [matches, rest | appendM (
            <Li key={key} theKey={key} disabled={false} value={value} contentString={contentString} ContentElement={ContentElement}/>,
          ), matchValue]
        }
      },
      [[], [], null],
    )

    const theOptions = inputValue | ifEmptyString (() => rest, () => matches)
    const canAccept = length (theOptions) === 1
    const onAccept = useCallback (
      () => canAccept && onSelect (matchValue),
      [canAccept, onSelect, matchValue],
    )
    const onCancel = useCallbackConst (() => setShow (false))
    const onClickValue = useCallbackConst (() => setShow (true))
    const onClickPopup = useCallbackConst (() => {
      setShow (false)
      setInputValue ('')
    })
    const onClickPopupContents = useCallbackConst ((event) => { event.stopPropagation ()})
    const inputRef = useRef (null)
    const stretchElementRef = useRef (null)

    useEffect (() => {
      if (show && autoFocus) inputRef.current | whenOk ((ref) => ref.focus ())
    }, [
      // --- @todo linter says unnecessary, but it seems necessary
      inputRef.current,
      show, autoFocus,
    ])

    useWhy ('ResponsiveSelectWithTextInput', props)
    return <ResponsiveSelectWithTextInputS className={clss (show && 'x--show')} style={style}>
      <div className='x__value' onClick={onClickValue}>
        {currentLabel}
      </div>
      <div className='x__popup' onClick={onClickPopup}>
        <div className='x__popup-contents' onClick={onClickPopupContents}>
          <ResponsiveTextInput
            autoFocus={false}
            inputRef={inputRef}
            isMobile={isMobile}
            withIcon={['search', 'left']}
            defaultValue={inputValue}
            scrollInputToTop={false}
            canAccept={canAccept}
            onAccept={onAccept}
            onCancel={onCancel}
            onUpdate={onUpdateInput}
            stretchElementRef={stretchElementRef}
            blur='allow'
            showButtons={isMobile}
          />
          <div ref={stretchElementRef} className='x__options'>
            <ul>{theOptions}</ul>
          </div>
        </div>
      </div>
    </ResponsiveSelectWithTextInputS>
  },
)
