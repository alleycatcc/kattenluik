import {
  pipe, compose, composeRight,
  map, prop, not, cond, each, filter,
  tap, id, noop, ok, nil, ifOk, ifNil,
  ifTrue, whenTrue, whenOk, eq, invoke,
  T, compact, appendM, reduce, lets, die, letS,
  sprintf1, merge, defaultToV,
} from 'stick-js/es'

import React, { useCallback, useMemo, useRef, useState, } from 'react'
import styled from 'styled-components'
import { Link, } from 'react-router-dom'

import Modal from 'react-modal'
import { Select as ResponsiveSelect, } from 'react-responsive-select'
import { cssTransition as toastifyCssTransition, } from 'react-toastify'

import configure from 'alleycat-js/es/configure'
import { clss, } from 'alleycat-js/es/dom'
import { ifTrueV, } from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { component, container, mediaPhone, mediaPhoneOnly, mediaTablet, mediaDesktop, toastInfoOptions, useWhy, } from '../../common'

import { spinner, } from '../../alleycat-components'
import ResponsiveTextInput from './ResponsiveTextInput'
import ResponsiveSelectWithTextInput from './ResponsiveSelectWithTextInput'

import config from '../../config'

const configTop = config | configure.init

Modal.setAppElement (configTop.get ('appElement'))

const imgEdit = configTop.get ('icons.edit')

export const SpinnerComet = spinner ('comet')
export const SpinnerText = spinner ('textblocks')

// --- fakeDisabled means the onClick handler is called, but the button doesn't move when clicked.
// --- it's useful for e.g. letting the click event bubble to an outer component.
const ButtonBaseS = styled.button`
  border: 1px solid black;
  border-radius: 2px;
  border-radius: 2% 6% 5% 4% / 1% 1% 2% 4%;
  display: inline-block;
  &::before {
	content: '';
	border: 2px solid #35353599;
	display: block;
	width: 100%;
	height: 100%;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate3d(-50%, -50%, 0) scale(1.015) rotate(0.5deg);
	border-radius: 1% 1% 2% 4% / 2% 6% 5% 4%;
  }
  ${prop ('fakeDisabled') >> ifTrueV (
    ``,
    `
    &:not(:disabled) {
      cursor: pointer;
      &:active {
        transition: all .03s;
        transform: translateY(1px) translateX(1px);
        opacity: 0.8;
      }
    }
    `,
  )}
  &:focus {
    outline: none;
  }
`

export const ButtonS = styled (ButtonBaseS)`
  background: #fd60b511;
  padding: 10px;
  position: relative;
  opacity: 0.5;
  width: ${prop ('width') >> defaultToV ('200px')};
  &:not(:disabled) {
    opacity: 1.0;
    &:active {
      border-style: solid;
    }
  }
`

export const Button = ({ children, ...props}) => <ButtonS type='submit' {...props}>
  {children}
</ButtonS>

export const Button2 = styled (ButtonBaseS)`
  border-radius: 5px;
  padding: 7px;
`

const RouterLinkS = styled (Link)`
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
`

const RouterLinkDarkS = styled (RouterLinkS)`
  color: black;
`

export const BlueLink = styled (Link)`
  color: darkblue;
`

// --- make it look like other buttons (on Chromium at least)
const ButtonLinkS = styled (Link)`
  color: rgb(51, 51, 51);
`
export const ButtonLink = ({ children, manual=false, to, style={}, buttonStyle={}, ... restProps }) => {
  const linkProps = {
    to,
    style,
    // --- see react-router-docs: this means make a normal <a> and don't try to route within the
    // application.
    reloadDocument: manual,
  }
  return <ButtonLinkS {... linkProps}>
    <Button {... restProps} style={buttonStyle}>
      {children}
    </Button>
  </ButtonLinkS>
}

export const RouterLink = RouterLinkS
export const RouterLinkDark = RouterLinkDarkS

const TextDivS = styled.div`
  cursor: text;
`

export const TextDiv = (props) => <TextDivS {...props}/>

const linkBaseMixinDisabled = `
  pointer-events: none;
`

const linkBaseMixin = `
  color: darkblue;
  text-decoration: underline;
  cursor: pointer;
`

const linkMixin = (disabled) => sprintf1 (`
  ${disabled | ifTrue (
    () => linkBaseMixinDisabled,
    () => linkBaseMixin,
  )}
`)

export const LinkLike = styled.span`
  ${prop ('disabled') >> linkMixin}
`

// --- trivial styled component to allow passing ref.
export const Div = styled.div`
`

const ButtonMS = styled (Button2)`
  ${prop ('selected') >> ifTrueV (
    `
      background: #44A;
      color: white;
    `,
    `
      background: #FFF;
      color: black;
    `,
  )}
  ${prop ('greyed') >> ifTrueV (
    'opacity: 0.5;', '',
  )}
  width: ${prop ('width')};
  flex: 1 0 auto;
`

export const ButtonM = ({ text, selected, disabled, greyed, onClick, width='100px', }) =>
  <ButtonMS
    selected={selected}
    disabled={disabled}
    fakeDisabled={true}
    greyed={greyed}
    onClick={onClick}
    width={width}
  >
    {text}
  </ButtonMS>

const SpinnerS = styled.div`
  color: #370707;
  .x__kid-m, .x__kid-t, .x__kid-d {
    text-align: center;
  }
  ${mediaQuery (
    mediaPhone (`
      .x__kid-t, .x__kid-d { display: none; }
      .x__kid-m { display: block; }
    `),
    mediaTablet (`
      .x__kid-m, .x__kid-d { display: none; }
      .x__kid-t { display: block; }
    `),
    mediaDesktop (`
      .x__kid-m, .x__kid-t { display: none; }
      .x__kid-d { display: block; }
    `),
  )}
`

/* `useSpace` means the component takes up space even when it's not spinning.
*/
export const MainSpinner = ({ scale=1, shrink=1, spinning=true, useSpace=true, }) => lets (
  // --- coefficient chosen so that shrink works fairly well with integer values
  () => scale * 1 / (shrink * 1.3),
  (shrinker) => <SpinnerS useSpace={useSpace}>
    <div className='x__kid-m'>
      <SpinnerComet size={20 * shrinker} spinning={spinning} useSpace={useSpace}/>
    </div>
    <div className='x__kid-t'>
      <SpinnerComet size={30 * shrinker} spinning={spinning} useSpace={useSpace}/>
    </div>
    <div className='x__kid-d'>
      <SpinnerComet size={30 * shrinker} spinning={spinning} useSpace={useSpace}/>
    </div>
  </SpinnerS>,
)

const styleOverlayBase = {
  backgroundColor: '',
  zIndex: 1000,
}

const styleContentBase = {
  opacity: 1,
  top: '50%',
  left: '50%',
  right: 'auto',
  bottom: 'auto',
  marginRight: '-50%',
  transform: 'translate(-50%, -50%)',
  padding: '0',
  maxWidth: '40%',
  borderRadius: '10px',
}

const ModalContentsS = styled.div`
  height: 100%;
  font-size: 30px;
  .x__close {
    position: absolute;
    right: 10px;
    top: 10px;
    font-size: 20px;
    font-family: sans-serif;
    font-weight: bold;
    cursor: pointer;
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      box-shadow: 1px 1px 5px 1px;
      border: 3px solid black;
      border-radius: 50px;
    `),
    mediaTablet (`
      border: 10px solid black;
      border-radius: 10px;
    `),
  )}
`

export const Dialog = ({
  children,
  isMobile,
  isOpen, onRequestClose,
  closeOnOverlayClick=false, showCloseButton=false,
  styleOverlay={},
  styleContent=isMobile ? { width: '100%', maxWidth: '100%', height: '100%'} : {},
}) => <Modal
  isOpen={isOpen}
  onRequestClose={onRequestClose}
  shouldCloseOnOverlayClick={closeOnOverlayClick}
  style={{
    content: styleContentBase | merge (styleContent),
    overlay: styleOverlayBase | merge (styleOverlay),
  }}
>
  <ModalContentsS>
    <ModalContents2S>
      {showCloseButton &&
        <div className='x__close' onClick={onRequestClose}>
          x
        </div>}
      {children}
    </ModalContents2S>
  </ModalContentsS>
</Modal>

// --- wrapper component to show a little pencil icon on hover (desktop) or a box with shadow and
// padding (mobile).

const EditableS = styled.div`
  position: relative;
  // width: 90%;
  .x__pencil {
    /* use media query here because our touchscreen is generating both touch and mouse events for
     * some reason (we only expect touch), which means that the pencil sometimes gets shown
     * otherwise.
     */
    ${mediaQuery (
      mediaPhoneOnly ('display: none;'),
    )}
    position: absolute;
    transition: opacity 200ms;
    left: -25px;
    top: 18px;;
    transform: translateY(-50%);
    img {
      width: ${prop ('imgWidth') >> defaultToV ('15px')};
    }
  }
  > .x__wrapper {
    position: relative;
    width: 100% !important;
    height: 100%;
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      &.x--decorate {
        box-shadow: 1px 1px 5px 1px;
        padding: 4px;
      }
    `),
  )}
`

export const Editable = ({ children, style, iconStyle, classes=[], decorateMobile=true, }) => {
  const [visible, setVisible] = useState (false)
  const show = useCallbackConst (() => { setVisible (true) })
  const hide = useCallbackConst (() => { setVisible (false) })
  return <EditableS
    style={style}
    onMouseOver={show} onMouseOut={hide}
    className={clss (decorateMobile && 'x--decorate', ... classes)}
  >
    <div className='x__wrapper'>
      {children}
      <span className={clss ('x__pencil', 'u-can-hover', visible || 'u--opacity-0')} style={iconStyle}>
        <img src={imgEdit}/>
      </span>
    </div>
  </EditableS>
}

export const Text = ({
  classes=[], decorateMobile=void 8,
  ... restProps
}) => <Editable
  classes={classes} decorateMobile={decorateMobile}
>
  <ResponsiveTextInput {... restProps}/>
</Editable>

export const SelectWithText = ({
  classes=[], decorateMobile=void 8, isMobile,
  selectKey=void 8,
  // --- only used if `selectedLabelRenderer` isn't given
  selectedLabelContentTransform=id,
  selectedLabelRenderer: selectedLabelRendererProp=null,
  ... restProps
}) => {
  const selectedLabelRenderer = useCallback (
    ({ value, contentString, ContentElement, }) => {
      return selectedLabelRendererProp | ifOk (
        (f) => f ({ value, contentString, ContentElement, }),
        () => (isMobile && nil (value)) | ifTrue (
          () => <span>&nbsp;</span>,
          () => <ContentElement>{contentString | selectedLabelContentTransform}</ContentElement>,
        ),
      )
    },
    [isMobile, selectedLabelRendererProp, selectedLabelContentTransform],
  )

  return <Editable
    classes={classes} decorateMobile={decorateMobile}
  >
    <ResponsiveSelectWithTextInput
      key={selectKey}
      customLabelRenderer={selectedLabelRenderer}
      isMobile={isMobile}
      {... restProps}
    />
  </Editable>
}

export const Select = ({
  classes=[], decorateMobile=void 8,
  selectKey=void 8, ... restProps
}) => <Editable
  classes={classes} decorateMobile={decorateMobile}
>
  <ResponsiveSelect key={selectKey} {... restProps}/>
</Editable>

const toastTransitionMobilePopup = toastifyCssTransition ({
  enter: 'mobile-popup-enter',
  exit: 'mobile-popup-exit',
})

export const mobilePopup = (contents) => toastInfoOptions ({
  autoClose: false,
  transition: toastTransitionMobilePopup,
  closeButton: false,
}, <div className='.x__mobile-popup'>
  {contents}
</div>)

// --- @todo merge with ModalContentsS
const ModalContents2S = styled.div`
  height: 100%;
  padding: 20px;
  button {
    display: inline-block;
    min-width: 100px;
    margin: 10px;
  }
  >.x__title {
    font-family: Walter Turncoat;
    color: #fd60b5;
    margin: 10px;;
    text-align: center;
  }
  >.x__buttons {
    text-align: center;
  }
  >.x__contents {
    margin-top: 20px;
    margin-bottom: 25px;
    margin-left: 10px;
    margin-right: 10px;
    .x__warning-sign {
      text-align: center;
      margin-bottom: 20px;
      &::before {
        content: '⚠';
        font-size: 35px;
        background: pink;
        padding-left: calc(40% - 10px);
        padding-right: calc(40% - 10px - 10px);
        color: black;
      }
    }
  }
  ${mediaQuery (
    mediaPhoneOnly (`
      >.x__title {
        font-size: 35px;
      }
      >.x__buttons {
      }
      >.x__contents {
        font-size: 30px;
      }
  }
    `),
    mediaTablet (`
      >.x__title {
        font-size: 25px;
      }
    `),
  )}
`

export const AreYouSureDialog = ({
  isMobile,
  isOpen, onRequestClose,
  closeOnOverlayClick=void 8,
  onYes, onNo,
  contents='',
  extraWarn=false,
}) => <Dialog
  isOpen={isOpen}
  onRequestClose={onRequestClose}
  closeOnOverlayClick={closeOnOverlayClick}
  isMobile={isMobile}
>
  <div className='x__title'>Are you sure?</div>
  <div className={clss ('x__contents', contents || 'u--display-none')}>
    {extraWarn && <div className='x__warning-sign'/>}
    {contents}
  </div>
  <div className='x__buttons'>
    <Button onClick={onYes}>Yes</Button>
    <Button onClick={onNo}>No</Button>
  </div>
</Dialog>

const SeparatorS = styled.div`
  width: 80%;
  margin: auto;
  height: 1px;
  border-top: 2px solid black;
  margin-top: 15px;
  margin-bottom: 15px;
`
export const Separator = () => <SeparatorS/>

const PaginationS = styled.div`
  line-height: 2em;

  >.x__num-per-page, >.x__cur-page >.x__page {
    >.x__num {
      display: inline-block;
      padding: 0px 6px 0px 6px;
      min-width: 35px;
      text-align: center;
      &:nth-child(1) {
        border: 0;
      }
      >.x__cursor {
        display: none;
        position: relative;
        top: 55px;
        height: 0px;
        width: 100%;
        border-top: 2px solid black;
      }
      &.x--selected >.x__cursor {
        display: block;
      }
    }
  }
  >.x__cur-page {
    display: flex;
    > *:nth-child(1) {
      flex: 0 0 50px;
    }
    > *:nth-child(2) {
      flex: 1 0 50px;
    }
    >.x__page {
      display: inline-block;
      overflow-x: auto;
      overflow-y: hidden;
      width: 650px;
      height: 80px;
      white-space: nowrap !important;
    }
  }
`

const Pagination = component ([
  'Pagination',
], (props) => {
  const {
    collectionName,
    numsPerPage, page,
    setNumPerPageIdxDispatch, setCurPageDispatch,
  } = props

  const onClickNpp = useCallbackConst (
    (idx) => () => { setNumPerPageIdxDispatch (idx) },
  )

  const onClicksNpp = useMemo (
    () => numsPerPage | map (({ idx, }) => onClickNpp (idx)),
    [numsPerPage, onClickNpp],
  )

  const onClickPage = useCallbackConst (
    (idx) => () => { setCurPageDispatch (idx) },
  )

  const onClicksPage = useMemo (
    () => page | map (({ idx, }) => onClickPage (idx)),
    [page, onClickPage],
  )

  useWhy ('Pagination', props)
  return <PaginationS>
    <div className='x__num-per-page'>
      Number of {collectionName} per page:
      {numsPerPage | map (({ n, idx, selected, }) => {
        const onClick = { onClick: onClicksNpp [idx], }
        const cls = clss ('x__num', selected && 'x--selected')
        return <div key={idx} className={cls} {... selected || onClick}>
          <div className='x__cursor'/>
          {n}
        </div>
      })}
    </div>
    {page.length > 1 && <div className='x__cur-page'>
      <div>
        Page:
      </div>
      <div className='x__page'>
        {page | map (({ n, idx, selected, }) => {
          const onClick = { onClick: onClicksPage [idx], }
          const cls = clss ('x__num', selected && 'x--selected')
          return <div key={idx} className={cls} {... selected || onClick}>
            <div className='x__cursor'/>
            {n}
          </div>
        })}
      </div>
    </div>}
  </PaginationS>
})

// --- a function which returns a React component (a Redux container)
export const mkPagination = ({
  setNumPerPageIdx, setCurPage,
  selectNumsPerPage, selectPage,
}) => container ([
  'PaginationWrapper',
  {
    setNumPerPageIdxDispatch: setNumPerPageIdx,
    setCurPageDispatch: setCurPage,
  },
  {
    numsPerPage: selectNumsPerPage,
    page: selectPage,
  },
], ({
    setNumPerPageIdxDispatch, setCurPageDispatch, numsPerPage, page,
    ... restProps
  }) => <Pagination
    {... restProps}
    setNumPerPageIdxDispatch={setNumPerPageIdxDispatch}
    setCurPageDispatch={setCurPageDispatch}
    numsPerPage={numsPerPage}
    page={page}
  />
)
