import {
  pipe, compose, composeRight,
  prop, lets, noop, not,
  whenOk, condS, eq, guard, otherwise, id, tap,
  ifNil, invoke,
} from 'stick-js/es'

import React, { Fragment, memo, useCallback, useEffect, useRef, useState, } from 'react'

import styled from 'styled-components'

import Keyboard from 'react-simple-keyboard'
import "react-simple-keyboard/build/css/index.css";

import configure from 'alleycat-js/es/configure'
import { logWith, iwarn, } from 'alleycat-js/es/general'
import { ifTrueV, whenTrueV,} from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'
import { mediaQuery, } from 'alleycat-js/es/styled'

import { addEventListener, component, useWhy, } from '../../../common'
import config from '../../../config'

// --- @todo, our small waveshare display is cutting off some pixels on the right edge of the screen
// --- update, seems to be fixed, check & remove this
const KEYBOARD_WIDTH = '100vw'

const configTop = config | configure.init

// some variables used to define the keyboard
const display = {
  '{shift}': 'shift',
  '{capslock}': 'lock',
  '{bksp}': '⬅',
  '{switch}': '!#1',
  '{abc}': 'ABC',
  '{enter}': 'enter',
  '{space}': 'space',
  '{acc}': 'éà',
  '{unacc}': 'ea',
  '{shift_acc}': 'ÉÀ',
  '{shift_unacc}': 'EA',
  '{show_hide}': '⯆',
}

// --- @todo
const memoize = id

const getLayout = memoize ((canAccept=true) => {
  const enter = canAccept ? ' {enter}' : ''
  return {
    default: [
      'q w e r t y u i o p {show_hide}',
      '{acc} a s d f g h j k l',
      '{shift} z x c v b n m {bksp}',
      `{switch} , {space} .${enter}`,
    ],
    shift: [
      'Q W E R T Y U I O P {show_hide}',
      '{shift_acc} A S D F G H J K L',
      '{capslock} Z X C V B N M {bksp}',
      `{switch} , {space} .${enter}`,
    ],
    accents: [
      'á à ä ã â ą é è ë ẽ {show_hide}',
      'ê ě ę í ì ï ĩ ı į î',
      '{unacc} ó ò ö õ ô ú ù ü ũ',
      '{shift} û ų ý ỳ ÿ ỹ ȳ {bksp}',
      `{switch} , {space} .${enter}`,
    ],
    accents_shift: [
      'Á À Ä Ã Â Ā Ȧ Ą É È Ë Ẽ {show_hide}',
      'Ê Ě Ė Ę Í Ì Ï Ĩ Ī I Į Î',
      '{shift_unacc} Ó Ò Ö Õ Ô Ȯ Ǫ Ú Ù Ü Ũ Ū',
      '{capslock} Û Ų Ý Ỳ Ÿ Ỹ Ȳ {bksp}',
      `{switch} , {space} .${enter}`,
    ],
    symbols: [
      '1 2 3 4 5 6 7 8 9 0 {show_hide}',
      '! @ # % & * ( ) / €',
      '+ × ÷ = - \' " : ; ? {bksp}',
      `{abc} , {space} .${enter}`,
    ]
  }
})

const KeyboardWrapperS = styled.div`
  position: fixed;
  bottom: 0px;
  left: 0px;
  z-index: 10;
  font-size: 35px;
  width: ${KEYBOARD_WIDTH ?? '100vw'};
  ${prop ('capslock') >> whenTrueV (`
    .x__shift-highlight {
    background: lightblue !important;
    color: white;
    }`
  )}
  .hg-button {
    height: 60px !important;
  }
  .hg-button-bksp {
    flex-basis: 60px;
    flex-grow: 0 !important;
    font-size: 90%;
  }
  .hg-button-switch, .hg-button-abc {
    flex-basis: 100px;
    flex-grow: 0 !important;
  }
  .hg-button-space {
  }
  .hg-activeButton {
    background: lightcoral !important;
  }
  .button-adjacent-to-space {
    flex-basis: 70px;
    flex-grow: 0 !important;
  }
  .hg-button-enter {
    flex-basis: 140px !important;
    flex-grow: 0 !important;
  }
`

// when capslock is true, the capslock button will be light blue
const buttonTheme = [
  {
    class: "x__shift-highlight",
    buttons: '{capslock}',
  },
  {
    class: 'button-adjacent-to-space',
    buttons: ', .',
  },
]

export default component (
  ['Keyboard', null],
  (props) => {
    const {
      input, setInput,
      onDimensions=noop,
      canAccept=true,
      onEnter=noop, onInit=noop,
      onRequestClose=noop, onUpdate=noop,
      keyboardRef: keyboardRefProp=null,
      keyboardWrapperRef=null,
    } = props

    const [layoutName, setLayoutName] = useState ("default")
    const [capslock, setCapslock] = useState (false)

    const keyboardRefDefault = useRef (null)
    const keyboardRef = keyboardRefProp ?? keyboardRefDefault

    useEffect (() => {
      onDimensions ({
        height: keyboardRef.current?.keyboardDOM.clientHeight,
      })
    }, [
      layoutName,
      onDimensions,
      keyboardRef,
      keyboardRef.current?.keyboardDOM.clientHeight,
    ])

    const onChangeKeyboard = useCallback ((input) => {
      setInput (input)
      onUpdate (input)
    }, [onUpdate, setInput])

    /* If we use the `onKeyPress` listener of react-simple-keyboard to listen for the show/hide
     * button and hide the keyboard, it leads to strange behavior where the underlying element also
     * gets a click event. So, we manually add an event to the DOM node here and that seems to work
     * fine.
     */
    useEffect (() => {
      const remove = keyboardRef.current | ifNil (
        () => iwarn ('keyboardRef prop missing, unable to add show/hide listener'),
        (r) => lets (
          () => r.keyboardDOM.querySelector ('.hg-button-show_hide'),
          (showHideButton) => showHideButton | addEventListener ('click', (_) => {
            onRequestClose ()
          }),
        ),
      )
      return () => remove | whenOk (invoke)
    },
      [
        keyboardRef, layoutName, onRequestClose,
        // --- @todo the linter says this should be removed because it can not cause a rerender but
        // that doesn't seem right -- say we rerender for whatever other reason, and
        // keyboardRef.current changed, and we want to the effect to fire again, then it seems we
        // need it
        keyboardRef.current,
        // --- we need to listen on canAccept, because it causes the layout to change, which causes
        // the DOM elements for the buttons to change.
        // --- @todo that's hard to reason about -- is there a bettery way?
        canAccept,
      ],
    )

    const onKeyPress = useCallback ((button) => {
      button | condS ([
        // --- don't hide the keyboard here: see above.
        "{show_hide}" | eq | guard (() => {
        }),
        "{shift}" | eq | guard (() => {
          if (layoutName | eq ('default'))
            setLayoutName ('shift')
          else if (layoutName | eq ('accents'))
            setLayoutName ('accents_shift')
        }),
        "{capslock}" | eq | guard (() => {
          if (not (capslock))
            setCapslock (true)
          else if (capslock && layoutName | eq ('shift')) {
            setLayoutName ('default')
            setCapslock (false)
          } else if (capslock && layoutName | eq ('accents_shift')) {
            setLayoutName ('accents')
            setCapslock (false)
          }
        }),
        "{switch}" | eq | guard (() => setLayoutName ('symbols')),
        "{abc}" | eq | guard (() => {
          if (capslock) setLayoutName ('shift')
          else setLayoutName ('default')
        }),
        "{acc}" | eq | guard (() => setLayoutName ('accents')),
        "{unacc}" | eq | guard (() => setLayoutName ('default')),
        "{shift_acc}" | eq | guard (() => setLayoutName ('accents_shift')),
        "{shift_unacc}" | eq | guard (() => setLayoutName ('shift')),
        "{enter}" | eq | guard (() => canAccept && onEnter ()),
        otherwise | guard (() => {
          if (not (capslock) && layoutName | eq ('shift'))
            setLayoutName ('default')
          else if (not (capslock) && layoutName | eq ('accents_shift'))
            setLayoutName ('accents')
        }),
      ])
    }, [capslock, layoutName, onEnter, canAccept])

    const layout = getLayout (canAccept)
    useWhy ('Keyboard', props)

    {/* Adding tabIndex makes it so that this element can appear as the event.relatedTarget of the
        onBlur event of the input field. Making it -1 then prevents it from getting focused when
        you press tab. */}
    return <KeyboardWrapperS
      tabIndex={-1}
      ref={keyboardWrapperRef}
      capslock={capslock}>
      <Keyboard
        onChange={onChangeKeyboard}
        onInit={onInit}
        keyboardRef={(r) => keyboardRef | whenOk (
          () => keyboardRef.current = r,
        )}
        layoutName={layoutName}
        layout={layout}
        display={display}
        onKeyPress={onKeyPress}
        buttonTheme={buttonTheme}
        // --- this prevents the keyboard from stealing the focus from the input field
        preventMouseDownDefault={true}
        stopMouseDownPropagation={true}
      />
    </KeyboardWrapperS>
  },
)

