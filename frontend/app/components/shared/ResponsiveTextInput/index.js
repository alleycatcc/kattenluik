import {
  pipe, compose, composeRight,
  noop, ifTrue, not, whenOk, invoke, nil,
  defaultTo, tap, ifOk,
} from 'stick-js/es'

import React, { memo, useCallback, useEffect, useMemo, useRef, useState, } from 'react'

import styled from 'styled-components'

import configure from 'alleycat-js/es/configure'
import { clss, keyPressListen, } from 'alleycat-js/es/dom'
import { ierror, setTimeoutOn, iwarn, logWith, length, } from 'alleycat-js/es/general'
import { whenNotEquals, ifEquals, allV, } from 'alleycat-js/es/predicate'
import { useCallbackConst, } from 'alleycat-js/es/react'

import Keyboard from '../Keyboard'
import { InputText as InputTextNormal, } from '../Input'

import { component, hasFocus, lookupOnOr, lookupOnOrDie, useWhy, useFlipState, } from '../../../common'

import config from '../../../config'

const configTop = config | configure.init
const { iconOk, iconCancel, responsiveInputScroll, } = configTop.gets ({
  iconOk: 'icons.ok',
  iconCancel: 'icons.cancel',
  responsiveInputScroll: 'responsiveInputScroll',
})

// --- @todo this can disappear when we're happy with the new behavior (see below)
const STRETCH_BEHAVIOR = 'new' // 'old'

const INPUT_HEIGHT = '70px'

/* `onChange` gets `event` as its argument, and is passed to the <input/> element. It does not get
 * called when you use the virtual keybaord.
 * `onUpdate` gets sent to the keyboard and gets called by the keyboard's `onChange` handler. It
 * also gets called during our `onChange`. It takes the current value of the input as its argument.
 *
 * When the keyboard is shown, a 'stretchElement' gets stretched vertically by an
 * amount sufficient to allow the input to get scrolled to the top.
 * This can get passed as a prop (probably a close ancestor that can scroll), or explictly set to
 * `null` if this behavior isn't desired, and otherwise we'll create one. The downside if we create
 * one is that it will create lots of vertical space after the input field.
 */

const InputTextWithKeyboard = ({
  withKeyboard=true,
  canAccept,
  defaultValue='',
  inputRef: inputRefProp=null,
  border=void 8,
  width='100%',
  withIcon=void 8,
  scrollInputToTop=true,
  onAccept: onAcceptProp=noop,
  onBlur: onBlurProp=noop,
  onCancel: onCancelProp=noop,
  onChange: onChangeProp=noop,
  onClick: onClickProp=noop,
  onFocus: onFocusProp=noop,
  onKeyPress: onKeyPressProp=noop,
  onUpdate: onUpdateProp=noop,
  stretchElementRef={},
  ... restProps
}) => {
  const restoreHeightRef = useRef (null)
  const stretchElementInitHeight = useRef (null)
  const keyboardRef = useRef ()
  const keyboardWrapperRef = useRef ()
  const siblingRef = useRef ()
  const inputRefDefault = useRef (null)
  const accepted = useRef (false)
  const caretPos = useRef (null)
  const curInput = useRef (null)

  const [input, setInput] = useState (defaultValue)
  const [requestShowKeyboard, setRequestShowKeyboard] = useState (false)
  const [keyboardHeight, setKeyboardHeight] = useState (null)
  const showKeyboard = withKeyboard && requestShowKeyboard

  /* We retrieve and set the caret position manually because react-simple-keyboard doesn't handle
   * it correctly (it's not really documented how to sync a particular input field with a keyboard
   * -- not just the value of the text but also the caret position -- and the examples on the site
   * don't seem to work right, and the documented `inputName` prop also doesn't seem to work right.)
   *
   * So our solution is to use mouse up and key up to detect changes to the caret, store this in a
   * ref, and then sync it to the keyboard. Then we use an effect to keep track of when the text in
   * the keyboard is changed, and update the caret position accordingly (when text is entered or
   * backspace is pressed it needs to move forwards or backwards respectively).
   *
   * `caretPosSync` is a flipper which gets flipped when the input has been selected and the caret
   * position has been retrieved and stored.
   *
   * The effect which scrolls the input field listens for changes on this value, not on the
   * mouse event which selects the field, because to get the caret position we need to a mouse up or
   * key up event, and we need to make sure the scrolling doesn't happen until the up event has
   * finished processing (otherwise the field could scroll away from the mouse cursor and the up
   * event won't fire).
   */
  const caretPosSync = useFlipState ()

  const inputRef = inputRefProp ?? inputRefDefault

  const onCancel = useCallback ((... args) => {
    onCancelProp (... args)
    setInput (defaultValue)
    if (showKeyboard)
      keyboardRef.current.setInput (defaultValue)
  }, [onCancelProp, defaultValue, showKeyboard])

  const onUpdate = useCallback ((... args) => {
    onUpdateProp (... args)
    accepted.current = false
  }, [onUpdateProp])

  const onChangeNormal = useCallback ((event) => {
    const inputValue = event.target.value
    setInput (inputValue)
    if (showKeyboard)
      keyboardRef.current.setInput (inputValue)
    accepted.current = false
    onUpdate (inputValue)
    onChangeProp (event)
  }, [onUpdate, onChangeProp, showKeyboard])

  const onClick = useCallback ((event) => {
    setRequestShowKeyboard (true)
    // showKeyboardSyncKey.flip ()
    onClickProp (event)
  }, [setRequestShowKeyboard, onClickProp])

  /* Old behavior: we stretch the height of stretchElement by exactly the distance between the input
   * field and the top of the screen. The idea is to allow the focused input to scroll all the way
   * to the top. In some cases, it can't scroll to the top, and this value may be an overshoot (for
   * example the input could be inside a scrollable container which is itself inside a scrollable
   * container and then the behavior of `scrollIntoView` is pretty tricky. But it works pretty
   * alright.
   *
   * New behavior: the old behavior doesn't help us in the case of a full-screen select component
   * with lots of options, when it's not necessarily an input field we're concerned with, but with
   * the fact that some of the options could get hidden by the keyboard. So we simply stretch
   * stretchElement by the height of the keyboard. This also works fine in the case of an input --
   * it might not be able to scroll all the way to the top but it will scroll itself to a visible
   * spot.
   */
  const stretchCur = stretchElementRef | whenOk ((ref) => ref.current)
  const doStretchElement = useCallback ((inputElement) => stretchElementRef | whenOk ((ref) => {
    if (responsiveInputScroll !== 'do-stretch') return
    const theStretchElement = ref.current ?? inputElement.nextSibling
    const curHeight = stretchElementInitHeight.current | defaultTo (
      () => theStretchElement.getBoundingClientRect ().height,
    )
    if (curHeight === 0) return
    stretchElementInitHeight.current = curHeight
    const deltaHeight = STRETCH_BEHAVIOR | ifEquals ('old') (
      () => inputElement.getBoundingClientRect ().top,
      // --- it's ok if it's zero -- it will be correct on the next render.
      () => keyboardHeight ?? 0,
    )
    const newHeight = deltaHeight + curHeight
    theStretchElement.style.height = String (newHeight) + 'px'
    if (nil (restoreHeightRef.current)) restoreHeightRef.current = () => {
      theStretchElement.style.height = String (curHeight) + 'px'
    }
  }), [
    keyboardHeight,
    stretchElementRef,
    // --- linter says we don't need these, but we want to be sure we listen on them so we don't
    // miss any changes to the sizes of DOM elements.
    showKeyboard, stretchCur,
  ])

  const restoreStretchElement = useCallback (() => {
    if (restoreHeightRef.current) {
      restoreHeightRef.current ()
      restoreHeightRef.current = null
    }
  }, [])

  const onFocus = useCallback ((event) => {
    if (not (withKeyboard)) return onFocusProp (event)
    setRequestShowKeyboard (true)
    onFocusProp (event)
  }, [withKeyboard, onFocusProp])

  const cleanup = useCallback ((event, { cancel, }) => {
    restoreStretchElement ()
    setRequestShowKeyboard (false)
    if (cancel) {
      setInput (defaultValue)
      onCancel (event)
    }
  }, [restoreStretchElement, onCancel, defaultValue])

  const onAccept = useCallback ((event) => {
    onAcceptProp (input, event)
    accepted.current = true
    // --- it may be the case that the keyboard has already been hidden due to logic in
    // `onAcceptProp`, ok.
    cleanup (event, { cancel: false, })
    // @todo after blurring the input scrolls again, avoid this
    inputRef.current?.blur ()
  }, [input, inputRef, onAcceptProp, cleanup])

  const onKeyPress = useCallback ((event) => {
    onKeyPressProp (event)
    event | keyPressListen (
      () => canAccept && onAccept (),
      'Enter',
    )
  }, [onKeyPressProp, canAccept, onAccept])

  const onBlur = useCallback ((event) => {
    // --- this is important to determine properly, because if it's true then a component as
    // ResponsiveSelectWithTextInput won't work properly (you can't choose any of the options
    // because the component collapses too quickly)
    const cancelOnBlur = onBlurProp (event)
    if (not (withKeyboard)) return onCancel (event)
    if (allV (
      cancelOnBlur,
      not (accepted.current),
      showKeyboard ? (event.relatedTarget !== keyboardWrapperRef.current) : true,
    )) {
      cleanup (event, { cancel: true, })
    }
  }, [onBlurProp, withKeyboard, onCancel, cleanup, showKeyboard])

  const onKeyboardDimensions = useCallbackConst (
    ({ height, }) => setKeyboardHeight (height),
  )

  const onInit = useCallback ((keyboard) => {
    keyboard.setInput (defaultValue)
  }, [defaultValue])

  const onRequestClose = useCallbackConst (() => {
    setRequestShowKeyboard (false)
  })

  const theCaretPos = caretPosSync.get ()
  const getCaretPosFromInputField = useCallback (() => {
    caretPos.current = inputRef.current.selectionStart
    if (showKeyboard)
      keyboardRef.current.setCaretPosition (caretPos.current)
    caretPosSync.flip ()
  }, [
    caretPosSync, inputRef, showKeyboard, showKeyboard,
    // --- for extra paranoia
    theCaretPos,
  ])

  const updateAndRestoreCaret = useCallback ((moveCursor) => {
    caretPos.current += moveCursor
    inputRef.current.selectionStart = inputRef.current.selectionEnd = caretPos.current
    if (showKeyboard)
      keyboardRef.current.setCaretPosition (caretPos.current)
  }, [showKeyboard, inputRef])

  const onMouseOrKeyUp = useCallback ((_) => {
    getCaretPosFromInputField ()
  }, [getCaretPosFromInputField])

  const onMouseUp = onMouseOrKeyUp
  const onKeyUp = onMouseOrKeyUp

  useEffect (() => {
    const cur = siblingRef.current
    if (!cur) return ierror ()
    const inputWrapper = cur.nextSibling
    const inputElement = inputWrapper.querySelector ('input')
    if (!inputElement) return ierror ('inputElement')
    if (!hasFocus (inputElement)) return
    doStretchElement (inputWrapper)
    if (scrollInputToTop) inputWrapper.scrollIntoView ({ block: 'start', behavior: 'smooth' })
  }, [
    theCaretPos, doStretchElement, showKeyboard, scrollInputToTop,
    // --- @todo linter complains, but it seems we should keep this.
    siblingRef.current,
  ])

  useEffect (() => {
    // --- how many positions and which direction to move the caret by.
    // --- in principle this can also handle multiple characters being added or removed, since it
    // just compares the lengths, though currently it's only one character at a time.
    const deltaInput = curInput.current | ifOk (
      (cur) => length (input) - length (cur),
      () => 0,
    )
    updateAndRestoreCaret (deltaInput)
    curInput.current = input
  }, [input, updateAndRestoreCaret])

  return <div>
    {/* We take a ref to this element to avoid an autofocus trap: this element is sure to be mounted
        before input is mounted, so it is ok to use it in onFocus */}
    <div ref={siblingRef}/>
    <InputTextNormal
      withIcon={withIcon}
      // height={INPUT_HEIGHT}
      width={width}
      border={border}
      value={input}
      onBlur={onBlur}
      onChange={onChangeNormal}
      onFocus={onFocus}
      onKeyPress={onKeyPress}
      onClick={onClick}
      theRef={inputRef}
      onMouseUp={onMouseUp}
      onKeyUp={onKeyUp}
      {... restProps}
    />
    {/* The div that will get stretched */}
    <div/>
    {showKeyboard && <Keyboard
      onDimensions={onKeyboardDimensions}
      input={input}
      setInput={setInput}
      canAccept={canAccept}
      onEnter={onAccept}
      onInit={onInit}
      onRequestClose={onRequestClose}
      onUpdate={onUpdate}
      keyboardRef={keyboardRef}
      keyboardWrapperRef={keyboardWrapperRef}
    />}
  </div>
}

const ResponsiveTextInputS = styled.div`
  display: flex;
  align-items: center;
  // --- given as style prop, default 100%
  // width: 100%;
  // padding-right: 10px;
  .x__input {
    flex: 1 1 100%;
  }
  .x__buttons {
    margin-left: 4%;
    flex: 1 0 25%;
    display: flex;
    justify-content: space-around;
    background: green;
    border: 2px solid white;
    background: white;
    &.x--highlight {
      border: 2px solid steelblue;
      background: aliceblue;
    }
    > .x__button {
      .x__img-wrapper-accept, .x__img-wrapper-cancel {
        width: ${INPUT_HEIGHT};
        height: ${INPUT_HEIGHT};
        img {
          width: 100%;
          height: 100%;
        }
      }
      .x__img-wrapper-accept {
        background: #025c02;
      }
      .x__img-wrapper-cancel {
        background: #a50000;
      }
    }
    img {
      width: 30px;
      height: 30px;
      vertical-align: top;
      &.x__accept {
        border: 1px solid #025c02;
        background: #025c02;
      }
      &.x__cancel {
        border: 1px solid #a50000;
        background: #a50000;
      }
    }
    &.x--can-not-accept {
      .x__img-wrapper-accept {
        opacity: 0.2;
      }
      img.x__accept {
        background: grey;
      }
    }
  }
`

const getCancelOnBlur = (blur) => blur | lookupOnOrDie ('invalid blur ' + blur) ({
  block: false,
  allow: false,
  cancel: true,
})

export default component (
  ['ResponsiveTextInput', null],
  (props) => {
    const {
      children, isMobile,
      border=0,
      canAccept=true,
      // --- 'allow' (default), 'cancel', or 'block'
      blur='allow',
      width='100%',
      // --- @experimental
      // --- the buttons do not currently work correctly in many cases (there are a lot of tricky
      // edge cases) -- keep on `false` for now.
      // --- `true` is ok in the case of ResponsiveSelectWithTextInput, because it's a modal dialog
      // (you can't accidentally click on something else), and the strech element is simpler / not
      // applicable.
      showButtons=false,
      withIcon=void 8,
      scrollInputToTop=void 8,
      inputRef,
      onBlur: onBlurProp=noop,
      onAccept=noop,
      onChange=noop,
      onCancel=noop,
      onKeyPress: onKeyPressProp=noop,
      onUpdate=noop,
      ... restProps
    } = props

    const common = {
      marginTopOnFocus: '-2px',
    }
    // --- this is to keep of track of the normal (non-mobile) input
    const [input, setInput] = useState ()
    const buttonsRef = useRef (null)
    const flashButtons = useCallback (
      () => {
        const { current: ref, } = buttonsRef
        ref.classList.add ('x--highlight')
        100 | setTimeoutOn (() =>
          ref.classList.remove ('x--highlight'),
        )
      },
      [buttonsRef],
    )
    const handleBlur = useCallback (
      (blur) => (event) => invoke (blur | lookupOnOr (
        () => (ierror ('invalid value for blur', blur), noop),
        ({
          allow: noop,
          // --- @todo you can still click on other input fields unfortunately, or on the trash can
          // for an assignee, how to stop this?
          block: () => {
            event.target.focus ()
            event.preventDefault ()
            event.stopPropagation ()
            if (showButtons) flashButtons ()
          },
          cancel: () => onCancel (),
        })
      )),
      [showButtons, flashButtons, onCancel],
    )
    const onBlur = useCallback (
      (event) => {
        onBlurProp (event)
        event | handleBlur (blur)
        return getCancelOnBlur (blur)
      },
      [onBlurProp, handleBlur, blur],
    )
    const onKeyPressNormal = useCallback (
      (event) => {
        onKeyPressProp (event)
        return event | keyPressListen (
          () => canAccept && onAccept (input, event),
          'Enter',
        )
      },
      [onKeyPressProp, canAccept, onAccept, input],
    )
    const onKeyPressWithKeyboard = useCallback (
      onKeyPressProp,
      [onKeyPressProp],
    )
    const onChangeNormal = useCallback (
      (event) => {
        const input = event.target.value
        setInput (input)
        onUpdate (input)
        onChange (event)
      },
      [onUpdate, onChange],
    )
    useWhy ('ResponsiveTextInput', props)
    return <ResponsiveTextInputS
      style={{ width, }}
    >
      <div className='x__input'>
        {isMobile | ifTrue (
          () => <InputTextWithKeyboard
            {... common}
            {... restProps}
            inputRef={inputRef}
            border={border}
            scrollInputToTop={scrollInputToTop}
            withIcon={withIcon}
            canAccept={canAccept}
            onAccept={onAccept}
            onBlur={onBlur}
            onCancel={onCancel}
            onUpdate={onUpdate}
            onKeyPress={onKeyPressWithKeyboard}
          >
            {children}
          </InputTextWithKeyboard>,
          () => <InputTextNormal
            {... common}
            {... restProps}
            theRef={inputRef}
            border={border}
            onBlur={onBlur}
            onChange={onChangeNormal}
            onKeyPress={onKeyPressNormal}
          >
            {children}
          </InputTextNormal>,
        )}
      </div>
      <div
        ref={buttonsRef}
        className={clss ('x__buttons', showButtons || 'u--display-none', canAccept || 'x--can-not-accept')}
      >
        <div className='x__button' onClick={onAccept}>
          <div className='x__img-wrapper-accept'><img className='x__accept' src={iconOk} /></div>
        </div>
        <div className='x__button' onClick={onCancel}>
          <div className='x__img-wrapper-cancel'><img className='x__cancel' src={iconCancel} /></div>
        </div>
      </div>
    </ResponsiveTextInputS>
  },
)
