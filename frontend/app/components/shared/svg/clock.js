import React from 'react'
// import styled from 'styled-components'

export default ({ width, height=width, color='black', onClick, }) => <svg
   onClick={onClick}
   width={width}
   height={ height}
   viewBox="0 0 128.38811 128.38811"
   version="1.1"
   style={{ stroke: color, fill: color, }}
              id="svg5">
       <defs
     id="defs2" /><g
     id="layer1"
     transform="translate(-14.754303,-124.55222)"><circle
       style={{  strokeWidth: 8, strokeOpacity: 1, fill: 'none', }}
       id="path289"
       cx="78.948357"
       cy="188.74628"
       r="60.194054" /><rect
         style={{  strokeWidth: 0, strokeOpacity: 1, }}
       id="rect1211"
       width="8.8604269"
       height="34.190418"
       x="72.840477"
       y="155.39856" /><rect
         style={{  strokeWidth: 0, strokeOpacity: 1, }}
       id="rect1211-3"
       width="8.8604269"
       height="34.190418"
       x="132.18182"
       y="-180.33006"
       transform="rotate(115)" /></g></svg>

