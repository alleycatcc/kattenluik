import {
  pipe, compose, composeRight,
  prop, lets,
} from 'stick-js/es'

import daggy from 'daggy'

import { pam, toListSingleton, } from './common'

const SortKeyTypeRef = daggy.taggedSum ('SortKey', {
  /* `sortFunction` takes the item dictionary returned in the keys array and produces a sort
   * key.
   *
   * ramda's `ascend` and `descend` functions compares these using the < and > operators, so bools
  /* sort as `false` and then `true`, numbers compare as expected, strings compare alphanumerically.
   *
   * Be careful with `null`: it can not be compared using < and > and will produce bizarre results.
   */
  SortKey: ['tag', 'text', 'sortFunctions'],
})

export const { SortKey, } = SortKeyTypeRef

// --- we pass the variant type as the first element, which is useful as a React key for a Select or
// a collection.
export const sortKeyData = (sk) => [sk, sk.tag, sk.text]
export const sortKeyTag = prop ('tag')
export const sortKeyText = prop ('text')
export const sortKeySortFunctions = prop ('sortFunctions')

// ------ using two separate functions here for an infinitesimally small performance boost.
// Use the `withNull` variant if the list might contain `null`: it will push `null` values to the
// end. (This is because `null` produces undefined results when compared using < and >, see above).
// Then you can use a second comparator function to deal with those using for example `ok` or
// `nil`.

// --- note that `f (a)` and `f (b)` can be strings or anything really
const xscend = (ascending) => lets (
  () => ascending ? [-1, 1] : [1, -1],
  ([l, r]) => (f) => (a, b) => {
    const aa = f (a)
    const bb = f (b)
    if (aa === bb) return 0
    return aa < bb ? l : r
  },
)

// --- note that `f (a)` and `f (b)` can be strings or anything really, including `null`
const xscendWithNull = (ascending) => lets (
  () => ascending ? [-1, 1] : [1, -1],
  ([l, r]) => (f) => (a, b) => {
    const aa = f (a)
    const bb = f (b)
    // --- send null to the bottom
    if (aa === null) return 1
    if (bb === null) return -1
    if (aa === bb) return 0
    return aa < bb ? l : r
  },
)

export const ascend = xscend (true)
export const ascendWithNull = xscendWithNull (true)
export const descend = xscend (false)
export const descendWithNull = xscendWithNull (false)

// ------ for sorting for example all black keys on top, and the rest descending or ascending.

const prefer = (ascending) => lets (
  () => ascending ? [-1, 1] : [1, -1],
  ([l, r]) => (n) => (f) => (a, b) => {
    const aa = f (a)
    const bb = f (b)
    if (aa === n) return -1
    if (bb === n) return 1
    if (aa === bb) return 0
    return aa < bb ? l : r
  },
)

const preferWithNull = (ascending) => lets (
  () => ascending ? [-1, 1] : [1, -1],
  ([l, r]) => (n) => (f) => (a, b) => {
    const aa = f (a)
    const bb = f (b)
    if (aa === n) return -1
    if (bb === n) return 1
    if (bb === null) return -1
    if (aa === null) return 1
    if (aa === bb) return 0
    return aa < bb ? l : r
  },
)

export const ascendPrefer = prefer (true)
export const ascendWithNullPrefer = preferWithNull (true)
export const descendPrefer = prefer (false)
export const descendWithNullPrefer = preferWithNull (false)

// --- tag is currently not used, but could be useful for example for requesting a particular sort
// from the server.
// --- `orderBy` can be a list or a single function
export const mkSortKey = (tag, text, f, orderBy) => SortKey (tag, text, toListSingleton (orderBy) | pam (f))
