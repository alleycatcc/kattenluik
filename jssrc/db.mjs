import {
  pipe, compose, composeRight,
  map, tap, lets, sprintfN, join, always, die,
  ifOk, sprintf1, reduceObj, appendM, prop, ne,
  eq, flip, filter, ifTrue, allAgainst,
  remapTuples, tryCatch, invoke, ok, compact,
} from 'stick-js/es'

import { dirname, } from 'path'

import { Left, Right, fold, flatMap, isJust, } from 'alleycat-js/es/bilby'
import { S, SB, getApi, } from 'alleycat-js/es/bsqlite3'
import configure from 'alleycat-js/es/configure'
import { decorateRejection, length, logWith, } from 'alleycat-js/es/general'
import { ifEquals, ifEmptyList, } from 'alleycat-js/es/predicate'

import { config, } from './config.mjs'
import { yellow, info, mkdirIfNeeded, warnX, } from './io.mjs'
import { bifirst, doEither, foldWhenLeft, lookupEitherOn, nilToLeft, } from './util.mjs'
import { siteSpecLock, siteSpecName, } from './types.mjs'

const configTop = config | configure.init
const { dbPath, getSiteSpecs, keyColors, } = configTop.gets ('dbPath', 'getSiteSpecs', 'keyColors')

// --- @todo config
// --- a key is considered stale (and will be set to inactive) if its last activity timestamp is
// this many seconds ago or more
// --- note that this must be larger than a month, since the timestamp is
// rounded down by month, or else you may be unable to refresh the key.
const staleKeySeconds = 6 * 30 * 24 * 3600 // 6 months
// const staleKeySeconds = 30

const activityTimestampTypes = {
  uptime: 1,
  system: 2,
}

/* If a row doesn't exist, `getPluck` and `get` both return undefined.
 * If it exists but a column is null, `getPluck` returns null while `get` returns { <keyname>: null, }
 */

let sqliteApi

export const init = ({ initSchema, initTestData, }) => {
  const sites = getSiteSpecs ()
  info ('opening db file at', dbPath | yellow)
  mkdirIfNeeded (dirname (dbPath))
  sqliteApi = getApi (dbPath, {})
  if (initSchema) initialiseSchema (sites) | foldWhenLeft (
    decorateRejection ("Couldn't initialise schema: ") >> die,
  )
  if (initTestData) initialiseTestData () | foldWhenLeft (
    decorateRejection ("Couldn't initialise test data: ") >> die,
  )
}

const initialiseSchema = (sites) => doEitherWithTransaction (
  () => sqliteApi.allPluck (getTables),
  (tables) => sqliteApi.runs (dropTables (tables)),
  () => sqliteApi.runs (createTables),
  () => sqliteApi.runs (initActivityTimestampType),
  () => sqliteApi.runs (initColor),
  () => sqliteApi.runs (initDoor (sites)),
  () => sqliteApi.run (initPrivilegeTables),
  () => sqliteApi.run (initStatus),
  () => sqliteApi.run (initUptime),
)

const initialiseTestData = () => doEither (
  () => sqliteApi.runs (initTestData),
)

// --- @todo migrations

const getTables = S (
  `select name from sqlite_master where type is 'table'
  and name not in ('sqlite_master', 'sqlite_sequence')`
)

const dropTables = (tables) => tables | map (
  (table) => S (table | sprintf1 (`drop table %s`)),
)

const createTables = [
  S (`drop table if exists activityTimestampType`),
  S (`drop table if exists assignee`),
  S (`drop table if exists color`),
  S (`drop table if exists door`),
  S (`drop table if exists key`),
  S (`drop table if exists keyAuthorization`),
  S (`drop table if exists status`),

  S (`create table activityTimestampType (
    id integer primary key,
    type text not null
  )`),
  S (`create table assignee (
    id integer primary key autoincrement,
    name text unique not null,
    custom text
  )`),
  S (`create table color (
    id integer primary key autoincrement,
    color text not null
  )`),
  S (`create table door (
    id integer primary key autoincrement,
    name text unique not null
  )`),
  /* `activityTimestamp`:
   *    In 'uptime' mode: a tick value in seconds, measured relative to an
   *    arbitrary starting point. It gets set to the current tick value in 3
   *    cases: a key is created, a key is used, or the status of the key is changed.
   *    In 'system' mode: a timestamp in seconds.
   */
  S (`create table key (
    id integer primary key autoincrement,
    deviceId text not null,
    secret64 text,
    statusId int not null,
    colorId int,
    assigneeId int,
    custom text,
    activityTimestamp int not null,
    activityTimestampTypeId int not null
  )`),
  S (`create index key_statusId on key (statusId)`),
  S (`create index key_assigneeId on key (assigneeId)`),
  S (`create index key_custom on key (custom)`),
  S (`create index key_activityTimestamp on key (activityTimestamp)`),
  S (`create table keyAuthorization (
    id integer primary key autoincrement,
    keyId int not null,
    doorId int not null,
    unique (keyId, doorId)
  )`),
  S (`create index keyAuthorization_keyId on keyAuthorization (keyId)`),
  S (`create index keyAuthorization_doorId on keyAuthorization (doorId)`),
  S (`create table keyPrivilege (
    id integer primary key autoincrement,
    keyId int not null,
    privilegeId int not null,
    unique (keyId, privilegeId)
  )`),
  S (`create index keyPrivilege_keyId on keyPrivilege (keyId)`),
  S (`create index keyPrivilege_privilegeId on keyPrivilege (privilegeId)`),
  S (`create table privilege (
    id integer primary key autoincrement,
    privilege text not null
  )`),
  S (`create table status (
    id integer primary key,
    status text not null
  )`),
  S (`create table uptime (
    tick integer not null
  )`),
]

const initStatus = S (
  `insert into status (id, status) values (1, 'active'), (2, 'inactive'), (3, 'missing')`,
)
// --- 'system' means the timestamp is a real date (assumes system time is
// accurate)
// --- 'uptime' means use kattenluik's internal clock (which measures
// uptime)
const initActivityTimestampType = activityTimestampTypes | remapTuples (
  (s, n) => SB (
    `insert into activityTimestampType (id, type) values (?, ?)`, [n, s],
  ),
)
const initUptime = S (
  `insert into uptime (tick) values (0)`,
)
const initOneColor = (color) => SB ('insert into color (color) values (?)', color)
const initColor = keyColors | map (initOneColor)
const initPrivilegeTables = S (
  `insert into privilege (privilege) values ('unlock-interface')`,
)

const initOneDoor = (name) =>
  SB ('insert into door (name) values (?)', name)

const isDoor = site => site | siteSpecLock | isJust

const initDoor = (sites) => sites
  | filter (isDoor)
  | map ((site) => site | siteSpecName)
  | map (initOneDoor)

// --- @todo error
const initTestData = [
  S (`insert into assignee (name, custom) values
    ('femke', NULL), ('eberhardt', '0653423232'), ('jesse', 'jij weet wel')
  `),
  S (`insert into key (deviceId, secret64, statusId, colorId, assigneeId, custom, activityTimestamp, activityTimestampTypeId) values
    ('33CBC47A0500008F', 'Oy08CSdWKAxeFSAGBH1UCw==', 1, 1, 1, 'mock key = z', 0, 1),
    ('3300000000000000', 'ZZZZZZdWKAxeFSAGBH1UCw==', 1, NULL, NULL, 11, 0, 1),
    ('33AFC77A050000DE', 'Ngo7OxxAMXgGbgo5ShcVNQ==', 1, NULL, NULL, 12, 0, 1),
    ('3300000000000001', NULL, 1, NULL, NULL, NULL, 0, 1),
    ('3300000000000002', 'some-secret', 2, 2, 2, NULL, 0, 1),
    ('3300000000000003', 'some-secret', 2, 3, 2, NULL, 0, 1),
    ('3300000000000004', 'some-secret', 3, 4, 3, 5, 0, 1),
    ('3300000000000005', 'some-secret', 3, 5, 3, 10, 0, 1),
    ('3300000000000006', 'FzFgIQE1MX4CV01dM0sWPA==', 1, 1, 1, 1, 0, 1)
  `),
  S (`insert into keyAuthorization (keyId, doorId) values
    (1,1),
    (1,2),
    (2,1)
  `),
  S (`insert into privilege (privilege) values ('zwem richting wal'), ('zwem richting schip')`),
]

export const keyAuthorizationAdd = (data) => sqliteApi.runs (
  data | map (([keyId, doorId]) =>
    SB (`insert into keyAuthorization (keyId, doorId) values (?, ?)`, [keyId, doorId]),
  ),
)

export const keyAuthorizationDelete = (ids) => sqliteApi.runs (
  ids | map ((id) =>
    SB (`delete from keyAuthorization where id = ?`, id),
  ),
)

export const keyAuthorizationsGet = () => sqliteApi.all (
  S (`select id, keyId, doorId from keyAuthorization`),
)

export const doorsGet = () => sqliteApi.all (S (`
  select id, name from door`),
)

export const keyExistsFail = (deviceId) => lets (
  () => sqliteApi.get (SB (`select id from key where deviceId = ?`, deviceId)),
  (res) => res | nilToLeft ('Key not found: ' + deviceId),
  (_, either) => either | map (Boolean),
)

export const keyExists = (deviceId) => lets (
  () => sqliteApi.get (SB (`select id from key where deviceId = ?`, deviceId)),
  (either) => either | map (Boolean),
)

export const keyStatusGet = (deviceId) => sqliteApi.getPluckFail (
  SB (
    `select s.status from key k join status s on k.statusId = s.id where k.deviceId = ?`,
    deviceId,
  ),
)

export const keyIsActiveFail = (deviceId) => keyStatusGet (deviceId) | flatMap (
  (statusText) => statusText | ifEquals ('active') (
    () => Right (),
    (s) => Left (s | sprintf1 ('Status is <%s>')),
  )
)

export const keyIsAuthorizedFail = (deviceId, siteName) => sqliteApi.getPluck (
  SB (`
    select a.id from keyAuthorization a
    join key k on k.id = a.keyId
    join door d on d.id = a.doorId
    where d.name = ? and k.deviceId = ?`,
    [siteName, deviceId])
) | nilToLeft (
  [deviceId, yellow (siteName)] | sprintfN ('key with device ID %s not authorized for site %s)')
)

export const keyPrivilegeAdd = (data) => sqliteApi.runs (
  data | map (([keyId, privilegeId]) =>
    SB (`insert into keyPrivilege (keyId, privilegeId) values (?, ?)`, [keyId, privilegeId]),
  ),
)

export const keyPrivilegeDelete = (ids) => sqliteApi.runs (
  ids | map ((id) =>
    SB (`delete from keyPrivilege where id = ?`, id),
  ),
)

export const keyPrivilegesGet = invoke (() => {
  const join = (j) => `
    select kp.id, keyId, p.id as privilegeId, privilege
    from privilege p ${j} keyPrivilege kp on kp.privilegeId = p.id
  `
  const s = {
    // --- this is not efficient (@future improve), but it gets us the data in the most useful way
    // for building up our selectors in one pass: every combination of keyId and privilegeId is
    // present, and keyPrivilegeId is then either null or a valid id.
    full: S (`
      select k.id as keyId, p.id as privilegeId, p.privilege, kp.id as keyPrivilegeId
      from key k, privilege p left join keyPrivilege kp
      on k.id = kp.keyId and p.id = kp.privilegeId
    `),
    // --- this is for getting the minimal set of existing keyPrivileges
    simple: S (join ('inner join')),
    // --- this is like minimal but with a left join instead of inner join, in case it's useful.
    outer: S (join ('left join')),
  }
  return (strategy='full') => doEither (
    () => strategy | lookupEitherOn (s),
    (stmt) => sqliteApi.all (stmt),
  )
})

export const secretGet = (deviceId) => sqliteApi.getPluck (
  SB (`select secret64 from key where deviceId = ?`, deviceId)
)

export const secretGetFail = (deviceId) => deviceId | secretGet | nilToLeft (
  'Secret not found for device ID ' + deviceId,
)

export const secretUpdate = (deviceId, secret64) => sqliteApi.run (
  SB (`update key SET secret64 = ? where deviceId = ?`, [secret64, deviceId])) | map (always (secret64))

export const keyUpdate = (deviceId, vals, shouldUpdateActivityTimestamp=false) => {
  const { status: statusTag, } = vals
  delete vals.status
  const [clauses, bindings] = vals | flip (reduceObj) (
    [[], []],
    ([clauses, bindings], [k, v]) => [
      clauses | appendM (k | sprintf1 ('%s = ?')),
      bindings | appendM (v),
    ],
  )
  if (ok (statusTag)) {
    clauses.push ('statusId = (select id from status where status = ?)')
    bindings.push (statusTag)
  }
  const sql = `update key set ` + (clauses | join (', ')) + ` where deviceId = ?`
  return doEither (
    () => shouldUpdateActivityTimestamp | ifTrue (
      () => updateActivityTimestamp (deviceId),
      () => Right (null),
    ),
    () => sqliteApi.run (
      SB (sql, bindings | appendM (deviceId)),
    ),
  )
}

const updateActivityTimestampUptime = (deviceId) => doEither (
  () => getUptimeApproximateByMonth (),
  (tick) => sqliteApi.run (
    SB (
      `update key set activityTimestamp = ?, activityTimestampTypeId = ? where deviceId = ?`,
      [tick, 1, deviceId],
    ),
  ),
)

// --- @todo we assume uptime mode
export const updateActivityTimestamp = updateActivityTimestampUptime

export const keyDeleteByDeviceId = (deviceId) => sqliteApi.run (
  SB (`delete from key where deviceId = ?`, deviceId)
) | fold (
  Left,
  prop ('changes') >> eq (1) >> ifTrue (
    () => Right (null),
    () => Left ('Unable to delete key with device ID ' + deviceId),
  ),
)

export const secretDelete = (deviceId) => sqliteApi.run (
  SB (`update key set secret64 = null where deviceId = ?`, deviceId),
)

export const assigneeAdd = (name, custom) => sqliteApi.run (
  SB (`insert into assignee (name, custom) values (?, ?)`, [name, custom])
)

export const assigneeDelete = (id, allowAssignedKeys) => {
  const doit = () => sqliteApi.run (SB (`delete from assignee where id = ?`, id))
  return doEither (
    () => sqliteApi.all (SB (`select id from key where assigneeId = ?`, id)),
    (res) => res | ifEmptyList (
      // if there ar no keys assigned to the assignee, we can just remove
      () => doit (),
      () => allowAssignedKeys | ifTrue (
        () => doEither (
          () => sqliteApi.runs (res | map (x =>
            SB (`update key set assigneeId = NULL where id = ?`, x.id))),
          // @future a transaction would be cleaner
          // we check whether all 'update' statements created changes in the db
          (ress) => ress | allAgainst (prop ('changes') >> eq(1)) | ifTrue (
            () => doit (),
            () => Left (id | sprintf1 (
              'assigneeDelete: refusing to remove assignee (%d), update key set failed')),
          )
        ),
        // if there are keys assigned to the assignee, and allowAssignedKeys is false,
        // we generate an error, because such a request should not come from the frontend.
        () => Left ([id, res | length] | sprintfN (
          'assigneeDelete: refusing to remove assignee (%d) with %d keys still assigned')),
      )
    )
  )
}

export const assigneeUpdate = (id, vals) => {
  const [clauses, bindings] = vals | flip (reduceObj) (
    [[], []],
    ([clauses, bindings], [k, v]) => [
      clauses | appendM (k | sprintf1 ('%s = ?')),
      bindings | appendM (v),
    ],
  )
  const sql = `update assignee set ` + (clauses | join (', ')) + ` where id = ?`
  return sqliteApi.run (
    SB (sql, bindings | appendM (id)),
  )
}

export const assigneesGet = () => sqliteApi.all (
  S (`select id, name, custom from assignee`),
)

export const assigneesGetWithKeyInfo = () => sqliteApi.all (S (`
  select a.id, a.name, count(k.id) as numKeys
  from assignee a left join key k
  on a.id = k.assigneeId
  group by a.id
`))

export const colorsGet = () => sqliteApi.all (
  S (`select id, color from color`),
)

export const keysGet = () => sqliteApi.all (S (`
  select k.id, k.deviceId, k.assigneeId, k.custom, k.activityTimestamp,
  (case when k.secret64 is not null then 1 else 0 end) as hasSecret,
  s.status, c.color, a.name
  from key k
  join status s on k.statusId = s.id
  left join color c on k.colorId = c.id
  left join assignee a on k.assigneeId = a.id
`))

export const privilegesGet = () => sqliteApi.all (
  S (`select id, privilege from privilege`),
)

export const statusesGet = () => sqliteApi.all (
  S (`select id, status from status`),
)

/* If the key doesn't exist, it's added, with an empty secret, no color or assignee, and a
 * status of inactive.
 * On success, returns Right (`true`) if a a new row was inserted, Right (`false`) otherwise.
 */

// --- @todo add dateAdded with actual uptime tick

// :: String |deviceId| -> Either String Bool |wasCreated|
export const keyCreateIfNotExists = (deviceId, activityTimestampType) => {
  if (activityTimestampType !== 'uptime')
    warnX ('keyCreateIfNotExists (): only uptime mode is supported, switching to uptime mode')
  let activityTimestampTypeId, uptime
  return doEither (
    () => activityTimestampTypes [activityTimestampType] | ifOk (
      Right,
      () => Left ('internal error: bad activityTimestampType'),
    ),
    (_activityTimestampTypeId) => {
      activityTimestampTypeId = _activityTimestampTypeId
      return getUptimeApproximateByMonth ()
    },
    (_uptime) => {
      uptime = _uptime
      return sqliteApi.getPluck (
        SB (`select deviceId from key where deviceId = ?`, deviceId),
      )
    },
    (deviceIdFromDb) => deviceIdFromDb | ifOk (
      () => Right (false),
      () => sqliteApi.run (
        SB (`insert into key
          (deviceId, secret64, statusId, colorId, assigneeId, custom, activityTimestamp, activityTimestampTypeId)
          values (?, NULL, 2, NULL, NULL, NULL, ?, ?)`,
          [deviceId, uptime, activityTimestampTypeId],
        ),
      ) | map (prop ('changes') >> ne (0)),
    ),
  )
}

export const getUptime = () => sqliteApi.getPluck (
  S (`select tick from uptime`),
)

// --- for privacy, we want to round down to the nearest month
export const getUptimeApproximateByMonth = () => {
  const x = 3600 * 24 * 30
  return doEither (
    () => getUptime (),
    (tick) => Right (lets (
      () => tick / x,
      (div) => x * Math.floor (div),
    )),
  )
}

export const setUptime = (tick) => sqliteApi.run (
  SB (`update uptime set tick = ?`, tick),
)

// --- @todo: assumes uptime mode
export const getStaleKeys = (includeInactive=true) => {
  const where = compact ([
    includeInactive || `status != 'inactive'`,
    `activityTimestamp <= ?`,
  ]) | join (' and ')
  return doEither (
    () => getUptime (),
    (tick) => sqliteApi.allPluck (SB (`
      select deviceId from key k join status s
      on k.statusId = s.id
      where ${where}
    `, tick - staleKeySeconds)),
  )
}

// --- @todo find a good place for this
/* The bsqlite3 library lets us wrap an arbitrary function in db.transaction () and then run it.
 * It's fine if those functions themselves open and close transactions (which means we are free to
 * use `runs`).
 * When the function exits the transaction is committed and if an error is thrown it rolls back.
 * This functin lets us wrap a series of Either functions in a transaction by making sure that an
 * error is first seen by the `transaction` function, so that the roll back happens, and then
 * wrapped in Left (or Right for ok).
 */
const doEitherWithTransaction = (... fs) => tryCatch (
  (res) => Right (res),
  (err) => Left (err),
  () => sqliteApi.db.transaction (() => doEither (... fs) | fold (
    (e) => die ('Rolling back transaction due to error:', e),
    (r) => r,
  )) (),
)

export const reset = () => lets (
  () => getSiteSpecs (),
  (sites) => initialiseSchema (sites) | bifirst (
    decorateRejection ("Couldn't reset DB: "),
  ),
)

// --- it's ok to keep mutating the db while the backup is in progress (the changes will be
// propagated to the backup)
// :: String, {} -> Promise
export const backup = (path, opts) => sqliteApi.db.backup (path, opts)

export const close = () => sqliteApi.db.close ()

// --- this doesn't check whether the key is active (that happens as part of the auth flow, though
// we could add it here as well as an extra check @future)
export const keyCanUnlockScreen = (deviceId) => {
  return sqliteApi.getPluck (SB (`
    select kp.id from key k
    join keyPrivilege kp on k.id = kp.keyId
    where k.deviceId = ? and kp.privilegeId = 1
  `, deviceId))
  | map (ok)
}

// --- this checks whether there is at least one key with the unlock-interface privilege, and a status of
// `active`
export const screenCanBeUnlocked = () => sqliteApi.getPluck (S (`
  select k.id from key k join keyPrivilege kp on k.id = kp.keyId
  join privilege p on kp.privilegeId = p.id
  join status s on k.statusId = s.id
  where p.privilege = 'unlock-interface'
  and status = 'active'
  /* we shouldn't have any empty secrets anyway, but this is an extra check.
   * note that we can not check if it's the RIGHT secret, so there is still a possibility of getting
   * locked out of the interface.
   */
  and k.secret64 is not null
  limit 1
`)) | map (ok)

export const getSqliteApi = () => sqliteApi
