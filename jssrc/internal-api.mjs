// ------ provides tables for the internal API -- mostly useful for easily swapping out calls with
// mocks.

import {
  pipe, compose, composeRight,
  whenOk, noop, lets, not, nil,
  invoke, condS, eq, guard, otherwise, die,
  ifTrue, sprintfN, sprintf1,
} from 'stick-js/es'

import { init as initW1Druppel, } from 'w1druppel/js/lib/index.mjs'

import { fold, cata, Just, Nothing, } from 'alleycat-js/es/bilby'

import {
  assigneeAdd as dbAssigneeAdd,
  assigneeDelete as dbAssigneeDelete,
  assigneeUpdate as dbAssigneeUpdate,
  assigneesGet as dbAssigneesGet,
  assigneesGetWithKeyInfo as dbAssigneesGetWithKeyInfo,
  backup as dbBackup,
  close as dbClose,
  colorsGet as dbColorsGet,
  doorsGet as dbDoorsGet,
  getSqliteApi as dbGetSqliteApi,
  getStaleKeys as dbGetStaleKeys,
  getUptime as dbGetUptime,
  getUptimeApproximateByMonth as dbGetUptimeApproximateByMonth,
  setUptime as dbSetUptime,
  keyAuthorizationsGet as dbKeyAuthorizationsGet,
  keyAuthorizationAdd as dbKeyAuthorizationAdd,
  keyAuthorizationDelete as dbKeyAuthorizationDelete,
  keyCanUnlockScreen as dbKeyCanUnlockScreen,
  keyCreateIfNotExists as dbKeyCreateIfNotExists,
  keyExistsFail as dbKeyExistsFail, keyExists as dbKeyExists,
  keyIsActiveFail as dbKeyIsActiveFail,
  keyIsAuthorizedFail as dbKeyIsAuthorizedFail,
  keyPrivilegesGet as dbKeyPrivilegesGet,
  keyPrivilegeAdd as dbKeyPrivilegeAdd,
  keyPrivilegeDelete as dbKeyPrivilegeDelete,
  keyUpdate as dbKeyUpdate,
  keyDeleteByDeviceId as dbKeyDeleteByDeviceId,
  keysGet as dbKeysGet,
  privilegesGet as dbPrivilegesGet,
  reset as dbReset,
  screenCanBeUnlocked as dbScreenCanBeUnlocked,
  secretDelete as dbSecretDelete,
  secretGet as dbSecretGet, secretGetFail as dbSecretGetFail,
  secretUpdate as dbSecretUpdate,
  statusesGet as dbStatusesGet,
  updateActivityTimestamp as dbUpdateActivityTimestamp,
} from './db.mjs'

import {
  gpioInit, gpioInitOutput, gpioOn, gpioOff,
  I2CInit, I2CInitTarget,
  knalDoI2C,
} from './knal-io.mjs'

import { info, magenta, } from './io.mjs'

import {
  unlock as lockIOUnlock,
  lock as lockIOLock,
  unlockRelock as lockIOUnlockRelock,
} from './lock-io.mjs'

import { i2cSdaPin, i2cSclPin, i2cBaudrate, i2cAddress, lockTypeFold, } from './types.mjs'
import { foldWhenJust, } from './util.mjs'

export const getOwApi = (_mockKeySpecs) => ({
  owInitDruppel: (config, opts) => initW1Druppel (config, opts),
})

export const getLockApi = (knalGpio, knalI2CTarget) => ({
  lockIOLock: (... args) => lockIOLock (knalGpio, knalI2CTarget, ... args),
  lockIOUnlock: (... args) => lockIOUnlock (knalGpio, knalI2CTarget, ... args),
  lockIOUnlockRelock: (... args) => lockIOUnlockRelock (knalGpio, knalI2CTarget, ... args),
  lockIOInit: (lock) => lock | cata ({
    // --- note that this assumes external type; ok, harmless in the case of
    // auto.
    LockGpio: (pinNumber, lockType) => {
      // --- @future remove once we're sure this isn't necessary
      if (nil (pinNumber)) return iwarn ('pinNumber is nil')
      lockType | lockTypeFold (
        () => Nothing,
        () => Nothing,
        () => Just (true),
        () => Just (false),
      ) | foldWhenJust (
        (value) => gpioInitOutput (knalGpio, value, pinNumber),
      )
    },
    LockI2C: (_i2cSpec, subtargetId, lockType) => {
      lockType | lockTypeFold (
        () => Nothing,
        () => Nothing,
        () => Just (true),
        () => Just (false),
      ) | foldWhenJust (
        (value) => knalDoI2C (knalI2CTarget, subtargetId, 'relay', 'external', value),
      )
    },
    LockGeneric: noop,
  })
})

export const getDbApi = () => ({
  dbAssigneeAdd,
  dbAssigneeDelete,
  dbAssigneeUpdate,
  dbAssigneesGet,
  dbAssigneesGetWithKeyInfo,
  dbBackup,
  dbClose,
  dbColorsGet,
  dbKeyCanUnlockScreen,
  dbKeyCreateIfNotExists,
  dbDoorsGet,
  dbGetSqliteApi,
  dbGetStaleKeys,
  dbGetUptime,
  dbGetUptimeApproximateByMonth,
  dbSetUptime,
  dbKeyAuthorizationsGet,
  dbKeyAuthorizationAdd,
  dbKeyAuthorizationDelete,
  dbKeyDeleteByDeviceId,
  dbKeyExists,
  dbKeyExistsFail,
  dbKeyIsActiveFail,
  dbKeyIsAuthorizedFail,
  dbKeyPrivilegesGet,
  dbKeyPrivilegeAdd,
  dbKeyPrivilegeDelete,
  dbKeyUpdate,
  dbKeysGet,
  dbPrivilegesGet,
  dbReset,
  dbScreenCanBeUnlocked,
  dbSecretDelete,
  dbSecretGet,
  dbSecretGetFail,
  dbSecretUpdate,
  dbStatusesGet,
  dbUpdateActivityTimestamp,
})

const ledIcon = (color, state) => lets (
  () => state === 'on' ? '●' : '○',
  (icon) => color (icon),
)

export const getKnalApi = (i2cSpec) => {
  const knalGpio = gpioInit ()
  const sda = i2cSdaPin (i2cSpec)
  const scl = i2cSclPin (i2cSpec)
  const baud = i2cBaudrate (i2cSpec)
  const addr = i2cAddress (i2cSpec)
  const knalI2C = I2CInit (sda, scl, baud)
  const knalI2CTarget = I2CInitTarget (knalI2C, addr)
  return { knalI2CTarget, knalGpio, }
}

export const getLedApi = invoke (() => {
  const mockOperate = ([type, ...typeArgs], colorFunc, name, state) => lets (
    () => type | condS ([
      eq ('gpio') | guard (() => lets (
        () => typeArgs,
        ([pinNumber]) => pinNumber | sprintf1 ('GPIO (pin = %d)')),
      ),
      eq ('i2c') | guard (() => lets (
        () => typeArgs,
        ([subtargetId]) => [subtargetId] | sprintfN ('I2C (subtargetId = %d)')),
      ),
      otherwise | guard (() => die ('invalid')),
    ]),
    (via) => [
      colorFunc | fold ((f) => ledIcon (f, state) + ' ', ''),
      magenta (name),
      state,
      via,
    ] | sprintfN ('[led] [mock] %sled %s %s via %s') | info,
  )

  return (mock, knalGpio, knalI2CTarget) => (mock) | ifTrue (
    () => ({
      onGpio: (pinNumber, name, colorFunc) => mockOperate (['gpio', pinNumber], colorFunc, name, 'on'),
      offGpio: (pinNumber, name, colorFunc) => mockOperate (['gpio', pinNumber], colorFunc, name, 'off'),
      // --- @todo i2cSpec not used
      onI2C: (_i2cSpec, subtargetId, _type, name, colorFunc) => mockOperate (['i2c', subtargetId], colorFunc, name, 'on'),
      offI2C: (_i2cSpec, subtargetId, _type, name, colorFunc) => mockOperate (['i2c', subtargetId], colorFunc, name, 'off'),
    }),
    () => ({
      onGpio: (pinNumber, ..._) => pinNumber | gpioOn (knalGpio),
      offGpio: (pinNumber, ..._) => pinNumber | gpioOff (knalGpio),
      // --- @todo i2cSpec not used
      onI2C: (_i2cSpec, subtargetId, type) => knalDoI2C (knalI2CTarget, subtargetId, 'led-' + type, true),
      offI2C: (_i2cSpec, subtargetId, type) => knalDoI2C (knalI2CTarget, subtargetId, 'led-' + type, false),
    }),
  )
})
