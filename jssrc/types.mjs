import {
  pipe, compose, composeRight,
  recurry, map, each,
  dot, condS, guard, otherwise,
  T, F, prop, lets, die, eq, sprintfN,
} from 'stick-js/es'

import daggy from 'daggy'

import { Just, Nothing, cata, } from 'alleycat-js/es/bilby'

import { N, } from './util.mjs'

/**
 * All locks have an associated LockType, described here.
 *
 * Locks also have 3 variants: GPIO, I2C, and generic. We don't use GPIO
 * locks any more since eendraad version 1.3 and the configuration functions
 * (e.g. `mkW1DruppelV1_3` and family) produce I2C locks by default. But the
 * GPIO code is still here, in case it's useful during testing or in an
 * ununusal situation. The GPIO code can handle both the auto and external
 * variants. It will require some manual bypassing/wiring on the eendraad
 * board though because the output of the microcontroller is wired directly
 * to the RJ45 connector.
 *
 * A generic lock ignores its lockType and calls a generic callback when
 * locked/unlocked (which may not actually lock / unlock anything).
 *
 * 'Auto' locks refer to locks which we signal by a square wave which the
 * eendraad microcontroller sends. These relock automatically using logic on
 * the microcontroller on the reader side (previously called 'hardware'
 * locks).
 *
 * 'External' locks are controlled not with a square wave but with a simple
 * on/off signal sent by the Pi (in the GPIO variant) or by the eendraad
 * microcontroller (I2C variant). 'External' means external from the point
 * of the view of the reader/lock.
 *
 * They differ in whether they are signaled by a falling/rising edge, the
 * number of peaks & crests of the wave (given by `numBits`), and the
 * timing.
 *
 * `halfPeriod` is how long we wait between pulses, i.e. half the period of
 * the wave.
 *
 * `unblockTimeout` is how long we wait in milliseconds before allowing the
 * lock to be operated again.
 *
 * `windowDuration` is the number of milliseconds in which the window stays
 * open for sending the complete sequence. Useful for analysing / debugging
 * the sequence.
 *
 * They all use a preamble of 1-0 (for falling) or 0-1 (for rising). The
 * number of bits which come after this is then given by `numBits`.
 *
 * The simplest auto lock listens for exactly one rise/fall event. This is
 * what we call `numBits=0` and it looks like 1 - (halfPeriod) - 0 (for a
 * falling trigger, vice versa for rising). The first bit is there so that
 * we are sure the second bit triggers an interrupt, in case the pin is
 * currently in the active position.
 *
 * `numBits=5`, for example, looks like 1-0-1-0-1-0-1 (falling) or
 * 0-1-0-1-0-1-0 (rising).
 *
 * `relockTimeout` determines when the external lock gets locked again in
 * the case of an unlock-relock action.
 */

/* :: data LockType = LockAutoTriggerLow { numBits :: Num, halfPeriod :: Num, unblockTimeout :: Num, windowDuration :: Num }
 *                  | LockAutoTriggerHigh { numBits :: Num, halfPeriod :: Num, unblockTimeout :: Num, windowDuration :: Num }
 *                  | LockExternalTriggerLow -- 0
 *                  | LockExternalTriggerHigh -- 1
 */

const LockType = daggy.taggedSum ('LockType', {
  LockAutoTriggerLow: ['numBits', 'halfPeriod', 'unblockTimeout', 'windowDuration'],
  LockAutoTriggerHigh: ['numBits', 'halfPeriod', 'unblockTimeout', 'windowDuration'],
  LockExternalTriggerLow: ['relockTimeout'],
  LockExternalTriggerHigh: ['relockTimeout'],
})

// --- returns ['auto'/'external', 'high'/'low']
// const lockTypeInfo = cata ({
  // LockAutoTriggerLow: (..._) => ['auto', 'low'],
  // LockAutoTriggerHigh: (..._) => ['auto', 'high'],
  // LockExternalTriggerLow: (..._) => ['external', 'low'],
  // LockExternalTriggerHigh: (..._) => ['external', 'high'],
// })

const { LockAutoTriggerLow, LockAutoTriggerHigh, LockExternalTriggerLow, LockExternalTriggerHigh, } = LockType
export { LockAutoTriggerLow, LockAutoTriggerHigh, LockExternalTriggerLow, LockExternalTriggerHigh, }

// :: data I2CSpecType = I2CSpec { sdaPin :: Num, sclPin :: Num, baudrate :: Num, address :: Num }
const I2CSpecType = daggy.taggedSum ('I2CSpec', {
  I2CSpec: ['sdaPin', 'sclPin', 'baudrate', 'address'],
})

const { I2CSpec, } = I2CSpecType
export { I2CSpec, }

I2CSpecType.prototype.toString = function () {
  return this | cata ({
    I2CSpec: (sdaPin, _sclPin, _baudrate, address) => [sdaPin, address] | sprintfN ('sda %d|addr 0x%02x'),
  })
}

export const i2cSdaPin = prop ('sdaPin')
export const i2cSclPin = prop ('sclPin')
export const i2cBaudrate = prop ('baudrate')
export const i2cAddress = prop ('address')

// :: data Lock = LockGpio { pinNumber :: Num, lockType :: LockType }
//              , LockI2C { i2cSpec :: I2CSpec, subtargetId :: Num, lockType :: LockType }
//              , LockGeneric { cb :: Function }

export const Lock = daggy.taggedSum ('Lock', {
  LockGpio: ['pinNumber', 'lockType'],
  LockI2C: ['i2cSpec', 'subtargetId', 'lockType'],
  LockGeneric: ['cb'],
})

export const lockType = (l) => l | cata ({
  LockGpio: (_) => l.lockType,
  LockI2C: (_) => l.lockType,
  LockGeneric: (_) => die ('attempted to retrieve lock type of a generic lock'),
})

export const lockTypeMb = (l) => l | cata ({
  LockGpio: (_) => Just (l.lockType),
  LockI2C: (_) => Just (l.lockType),
  LockGeneric: (_) => Nothing,
})

export const lockTypeTriggerDir = cata ({
  LockAutoTriggerLow: (_) => false,
  LockAutoTriggerHigh: (_) => true,
  LockExternalTriggerLow: (_) => false,
  LockExternalTriggerHigh: (_) => true,
})

export const lockTypeFold = (f, g, h, i) => cata ({
  LockAutoTriggerLow: (... p) => f (... p),
  LockAutoTriggerHigh: (... p) => g (... p),
  LockExternalTriggerLow: (... p) => h (... p),
  LockExternalTriggerHigh: (... p) => i (... p),
})

const { LockGpio, LockI2C, LockGeneric, } = Lock
export { LockGpio, LockI2C, LockGeneric, }

// :: data SiteSpecLocationType = SiteSpecLocation { ctrlNum :: Int
//                                                 , busNum :: Int }
const SiteSpecLocationType = daggy.taggedSum ('SiteSpecLocationType', {
  SiteSpecLocation: ['ctrlNum', 'busNum'],
})

// --- it's a sum type with one variant so prop always does what we want.
const siteSpecLocationCtrlNum = prop ('ctrlNum')
const siteSpecLocationBusNum = prop ('busNum')

const { SiteSpecLocation, } = SiteSpecLocationType
export { SiteSpecLocation, siteSpecLocationCtrlNum, siteSpecLocationBusNum, }

// :: data SiteType = SiteSpec { name :: String
//                             , locations :: [SiteSpecLocation]
//                             , lock :: Maybe Lock }

const SiteType = daggy.taggedSum ('SiteType', {
  SiteSpec: ['name', 'locations', 'lock'],
})

// --- sum type with one variant, see above
const siteSpecName = prop ('name')
const siteSpecLocations = prop ('locations')
const siteSpecLock = prop ('lock')

const { SiteSpec, } = SiteType
export { SiteSpec, siteSpecName, siteSpecLocations, siteSpecLock, }

// :: data Mode = Mode { modeReader :: ModeReader
//                     , modeLed :: ModeLed }
export const Mode = daggy.tagged ('Mode', ['modeReader', 'modeLed'])

// :: data ModeReader = ModeReaderSimulate
//                    | ModeReaderSimulateKeys { keySpecs :: [SimulateKeySpec] }
//                    | ModeReaderReal

const ModeReader = daggy.taggedSum ('ModeReader', {
  // --- ModeReaderSimulate corresponds to the `ks` option. It's mostly
  // useful for development off the Pi. It's like the `ksx`, `ksz` etc.
  // options, in that it causes the Onewire API to get mocked, but doesn't
  // require (and doesn't allow) any keyboard-triggered mock events.
  ModeReaderSimulate: [],
  ModeReaderSimulateKeys: ['keySpecs'],
  ModeReaderReal: [],
})

/*
 * :: data SimulateKeySpec = SimulatePresenceSpec { description :: String
 *                                                , keyboardKey :: String
 *                                                , busNumber :: Number }
 *                         | SimulateReadSpec { description :: String
 *                                            , keyboardKey :: String
 *                                            , deviceId :: String
 *                                            , secret64 :: String
 *                                            , busNumber :: Number }
 */

const SimulateKeySpec = daggy.taggedSum ('SimulateKeySpec', {
  SimulatePresenceSpec: ['description', 'keyboardKey', 'busNumber'],
  SimulateReadSpec: ['description', 'keyboardKey', 'deviceId', 'secret64', 'busNumber'],
})

const { SimulatePresenceSpec, SimulateReadSpec, } = SimulateKeySpec
export { SimulatePresenceSpec, SimulateReadSpec, }

export const simulateKeySpecKeyboardKey = prop ('keyboardKey')
export const simulateKeySpecSecret64 = prop ('secret64')
export const simulateKeySpecBusNumber = prop ('busNumber')

// :: data ModeLed = ModeLedSimulate | ModeLedReal
const ModeLed = daggy.taggedSum ('ModeLed', {
  ModeLedSimulate: [],
  ModeLedReal: [],
})

/* `serverM` is an object created by the server factory in server.js -- M for mutable.
 */
// :: data Server = Server { serverM :: ServerFactoryObject
//                         , eventHandlers :: [EventHandler] }
export const Server = daggy.tagged ('Server', ['serverM', 'eventHandlers'])
export const serverServerM = prop ('serverM')
export const serverEventHandlers = prop ('eventHandlers')

export const { ModeReaderSimulate, ModeReaderSimulateKeys, ModeReaderReal, } = ModeReader
export const { ModeLedSimulate, ModeLedReal, } = ModeLed

// :: data Event = Event { eventEventData :: EventData }
export const Event = daggy.tagged ('Event', ['eventData'])
export const eventEventData = prop ('eventData')

/* The constructors of `EventData` are what we can call 'event types'.
 * We don't provide accessors for the event payloads (e.g. busNumber) -- these can be retrieved by
 * deconstructing or using `prop`.
 */

// :: data EventData = EventLockOpen | EventLockClose
//                   | EventAdminUnblocked | EventAdminBlocked
//                   | EventAdminReadSuccess (...) | EventAdminReadFailure (...)
//                   | EventAccessDenied (...) | EventAccessDeniedClear (...)
//                   | EventAccessGranted (...) | EventAccessGrantedClear (...)
//                   | EventPresence (...) | EventPresenceClear (...)

export const EventData = daggy.taggedSum ('EventData', {
  // not currently used
  EventLockOpen: [],
  EventLockClose: [],
  EventAdminUnblocked: [],
  EventAdminBlocked: [],
  EventAdminReadSuccess: ['siteName', 'deviceId', 'isNewKey', 'isNewSecret'],
  EventAdminReadFailure: ['siteName'],
  EventAccessDenied: ['siteName'],
  EventAccessDeniedClear: ['siteName'],
  // --- second argument is a Maybe deviceId: in the case of the door hold open we really don't need
  // to keep track of which key was the cause, so then it's Nothing.
  EventAccessGranted: ['siteName', 'mbDeviceId'],
  EventAccessGrantedClear: ['siteName'],
  EventPresence: ['siteName'],
  EventPresenceClear: ['siteName'],
})

export const {
  EventLockOpen, EventLockClose,
  EventAdminUnblocked, EventAdminBlocked,
  EventAdminReadSuccess, EventAdminReadFailure,
  EventAccessDenied, EventAccessDeniedClear,
  EventAccessGranted, EventAccessGrantedClear,
  EventPresence, EventPresenceClear,
} = EventData

// :: data EventHandler = EventHandler { eventHandlerType :: EventDataConstructor,
//                                     , eventHandlerCb :: Function }
export const EventHandler = daggy.tagged ('EventHandler', ['type', 'cb'])

export const eventHandlerType = prop ('type')
export const eventHandlerCb = prop ('cb')

const LedOperateI2CType = daggy.taggedSum ('LedOperateI2CType', {
  LedOperateI2CTypeGrant: [],
  LedOperateI2CTypeDeny: [],
})

const { LedOperateI2CTypeGrant, LedOperateI2CTypeDeny, } = LedOperateI2CType
export { LedOperateI2CTypeGrant, LedOperateI2CTypeDeny, }

// :: data LedOperate = LedOperateGpio { pinNumber :: Num }
//                    | LedOperateI2C { i2cSpec :: I2CSpec, subtargetId: Num, type :: LedOperateI2CType}
//                    | LedOperateGeneric { cbOn :: Function
//                                        , cbOff :: Function, }

export const LedOperate = daggy.taggedSum ('LedOperate', {
  // --- for an LED which is operated by turning a GPIO pin on and off.
  LedOperateGpio: ['pinNumber'],
  LedOperateI2C: ['i2cSpec', 'subtargetId', 'type'],
  // --- for an LED which is operated by running an arbitrary callback
  // (not currently used).
  LedOperateGeneric: ['cbOn', 'cbOff'],
})

export const { LedOperateGpio, LedOperateI2C, LedOperateGeneric, } = LedOperate

/* `operate` is a list, meaning that one `Led` structure can operate
 * multiple (physical) leds. This is most useful when a 'yellow' led for
 * example is lit by lighting adjacent red and green ones. You could also
 * use it for multiple (non-adjacent) physical leds but that's probably not
 * useful.
 *
 * `colorFunc` is an optional console color function for logging and/or
 * mocked leds.
 *
 * :: data Led = Led { name :: String
 *                   , operate :: [LedOperate]
 *                   , eventHandlers :: [EventHandler] }
 *                   , colorFunc :: Maybe Function
 */

export const Led = daggy.tagged ('Led', ['name', 'operate', 'eventHandlers', 'colorFunc'])
export const ledName = prop ('name')
export const ledOperate = prop ('operate')
export const ledEventHandlers = prop ('eventHandlers')
export const ledColorFunc = prop ('colorFunc')

export const AdminMode = daggy.taggedSum ('AdminMode', {
  AdminModeBlocked: [],
  /* `forceGenerate` = `false`: generate a secret if there isn't one in the db.
   * `forceGenerate` = `true`: always (re)generate a secret.
   */
  AdminModeRead: ['forceGenerate'],
  AdminModeAuthenticate: [],
})

const { AdminModeBlocked, AdminModeRead, AdminModeAuthenticate, } = AdminMode
export { AdminModeBlocked, AdminModeRead, AdminModeAuthenticate, }

export const adminModeIsAuthenticate = cata ({
  AdminModeAuthenticate: () => true,
  AdminModeRead: (_) => false,
  AdminModeBlocked: () => false,
})

export const adminModeIsRead = cata ({
  AdminModeAuthenticate: () => false,
  AdminModeRead: (_) => true,
  AdminModeBlocked: () => false,
})

export const adminModeFoldRead = (no, yes) => cata ({
  AdminModeAuthenticate: () => no (),
  AdminModeRead: (forceGenerate) => yes (forceGenerate),
  AdminModeBlocked: () => no (),
})

// :: data DoorMode = -- door is (momentarily) blocked as the result of an authenticate
//                    DoorModeBlockedAuthenticate
//                    -- door is (indefintely) blocked as the result of being manually held open
//                  | DoorModeBlockedHold
//                    -- door is ready to read
//                  | DoorModeReady
export const DoorMode = daggy.taggedSum ('DoorMode', {
  DoorModeBlockedAuthenticate: [],
  DoorModeBlockedHold: [],
  DoorModeReady: [],
})

const { DoorModeBlockedAuthenticate, DoorModeBlockedHold, DoorModeReady, } = DoorMode
export { DoorModeBlockedAuthenticate, DoorModeBlockedHold, DoorModeReady, }

export const doorIsBlockedAuthenticate = cata ({
  DoorModeBlockedAuthenticate: T,
  DoorModeBlockedHold: F,
  DoorModeReady: F,
})

/* class c a where
 *   siteIsBlocked :: a -> Bool
 *
 * instance c AdminMode where ...
 * instance c DoorMode where ...
 */

AdminMode.prototype.siteIsBlocked = function () {
  return this | cata ({
    AdminModeBlocked: T,
    AdminModeRead: F,
    AdminModeAuthenticate: F,
  })
}

DoorMode.prototype.siteIsBlocked = function () {
  return this | cata ({
    DoorModeBlockedAuthenticate: T,
    DoorModeBlockedHold: T,
    DoorModeReady: F,
  })
}

export const siteIsBlocked = dot ('siteIsBlocked')

/* Types which can receive events need to implement the method `getEventHandlers`.
 * We can think of it like a Haskell-style typeclass (or a Java interface).
 *
 *     class EventTarget a where
 *       getEventHandlers :: a -> [EventHandler]
 */

Led.prototype.getEventHandlers = function () {
  return this | ledEventHandlers
}

Server.prototype.getEventHandlers = function () {
  return this | serverEventHandlers
}

export const getEventHandlers = dot ('getEventHandlers')

// --- @todo move out of types.mjs

// --- Turn an LED on or off.
// :: LedApi -> 'on' | 'off' -> (Led, _) -> IO ()

const ledOperateTurn = recurry (3) (
  (ledApi) => (state) => (led) => lets (
    () => led | ledName,
    () => led | ledColorFunc,
    (name, colorFunc) => led | ledOperate | each (cata ({
      LedOperateGpio: (pinNumber) => state | condS ([
        'on'      | eq | guard (_ => ledApi.onGpio (pinNumber, name, colorFunc)),
        'off'     | eq | guard (_ => ledApi.offGpio (pinNumber, name, colorFunc)),
        otherwise      | guard (_ => die ()),
      ]),
      LedOperateI2C: (i2cSpec, subtargetId, type) => state | condS ([
        'on'      | eq | guard (_ => ledApi.onI2C (i2cSpec, subtargetId, type, name, colorFunc)),
        'off'     | eq | guard (_ => ledApi.offI2C (i2cSpec, subtargetId, type, name, colorFunc)),
        otherwise      | guard (_ => die ()),
      ]),
      LedOperateGeneric: (cbOn, cbOff) => state | condS ([
        'on'      | eq | guard (cbOn),
        'off'     | eq | guard (cbOff),
        otherwise      | guard (_ => die ()),
      ]),
    })),
  ),
)

// :: LedApi -> (Led, _) -> IO ()
export const ledOperateTurnOn = recurry (2) (
  (ledApi) => ledOperateTurn (ledApi, 'on'),
)
// :: LedApi -> (Led, _) -> IO ()
export const ledOperateTurnOff = recurry (2) (
  (ledApi) => ledOperateTurn (ledApi, 'off'),
)

// :: Led -> [Num | null]
export const ledGpioPinNumbers = ledOperate >> map (cata ({
  LedOperateGpio: (pinNumber) => pinNumber,
  LedOperateI2C: N,
  LedOperateGeneric: N,
}))

/* Τhis is for leds which turns on and off when ANY access or deny event is
 * fired.
 * Not currently used.
 */
const mkAccessGeneralLed = (isGrant, ledApi, name, pinNumbers, mbColorFunc) => lets (
  () => isGrant ? [EventAccessGranted, EventAccessGrantedClear] : [EventAccessDenied, EventAccessDeniedClear],
  ([setEvent, clearEvent]) => Led (
    name,
    pinNumbers | map (LedOperateGpio),
    [
      EventHandler (setEvent, (_, led) => ledOperateTurnOn (ledApi, led)),
      EventHandler (clearEvent, (_, led) => ledOperateTurnOff (ledApi, led)),
    ],
    mbColorFunc,
  ),
)

export const mkAccessGeneralLedDeny = (ledApi) => (...args) => mkAccessGeneralLed (
  false, ledApi, 'general-denied', ...args,
)
export const mkAccessGeneralLedGrant = (ledApi) => (...args) => mkAccessGeneralLed (
  true, ledApi, 'general-granted', ...args,
)
