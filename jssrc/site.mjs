/* This is an object factory for a 'site' and contains some mutable information (the state of the
 * door/admin site, whether it's blocked etc.) and can access general information like the name and
 * type.
 */

import {
  pipe, compose, composeRight,
  ifOk, map, sprintf1, T,
  ifTrue, ifFalse,
  die, raise, not,
  factory, factoryProps,
} from 'stick-js/es'

import { startP, then, recover, resolveP, } from 'alleycat-js/es/async'
import { Right, flatMap, } from 'alleycat-js/es/bilby'
import { decorateRejection, } from 'alleycat-js/es/general'
import { any, all, } from 'alleycat-js/es/predicate'

import { info, warn, infoSecretMore, } from './io.mjs'
import { base64decodeAsU8Array, base64encode, bifirst, foldWhenLeft, } from './util.mjs'
import {
  AdminModeBlocked, AdminModeAuthenticate, AdminModeRead,
  DoorModeBlockedAuthenticate, DoorModeReady, DoorModeBlockedHold,
  siteIsBlocked, doorIsBlockedAuthenticate,
  adminModeIsAuthenticate, adminModeFoldRead, adminModeIsRead,
} from './types.mjs'

const proto = {
  init () {
    return this
  },

  /* Return false to mean that we shouldn't propagate the event further (basically this means don't
   * turn the LED yellow if the door is blocked, meaning, the door was just recently opened or
   * denied).
   */
  onShort (_owInfo) {
    if (this.isAdmin && siteIsBlocked (this.adminMode)) return false
    if (!this.isAdmin && siteIsBlocked (this.doorMode)) return false
    return true
  },

  /* Return false to mean that we shouldn't propagate the event further.
   */
  onPresence (_owInfo) {
    return not (this._isBlocked ())
  },

  /* :: Either String (isNewKey :: Boolean or null)
   * `isNewKey` = null means we are not an admin site
   */
  onResult (deviceId, _owInfo, { dbKeyCreateIfNotExists, }) {
    return this.isAdmin | ifFalse (
      () => Right (null),
      () => dbKeyCreateIfNotExists (deviceId, 'uptime') | bifirst (
        decorateRejection (`Can't add key to db: `),
      ),
    )
  },

  /* :: Either String [shouldGenerate :: Boolean, isAdminRead :: Boolean]
   * `shouldGenerate` is `true` if we are an admin site and either
   * `generateSecretRequested` is true (meaning the regenerate secret button
   * was clicked on the admin page) or the secret is empty and we are
   * doing a normal read (this is how secrets are first generated for a new
   * key when read key is clicked on the admin page)
   */
  shouldGenerateSecret (deviceId, _owInfo, { dbSecretGet, }) {
    const no = any (
      () => this._isBlocked (),
      () => not (this.isAdmin),
      () => not (this.adminMode | adminModeIsRead),
    )
    return no | ifTrue (
      () => Right ([false, false]),
      () => this.adminMode | adminModeFoldRead (
        () => die ('invariant violation'),
        (generateSecretRequested) => generateSecretRequested | ifTrue (
          // --- 'regenerate secret' button on admin page
          () => Right ([true, true]),
          // --- 'read key' button on admin page
          () => dbSecretGet (deviceId) | map (ifOk (
            // --- secret exists in db.
            () => [false, true],
            // --- no secret in db.
            () => [true, true],
          )),
        ),
      ),
    )
  },

  /* :: Either String (updatedOrStored :: Boolean)
   * `updatedOrStored` is always `true` in the Right case.
   */
  onGenerateSecret (deviceId, secret, isNewKey, _owInfo, { dbSecretUpdate, dbKeyDeleteByDeviceId, }) {
    info ('secret generated and stored in DB for ' + deviceId)
    const secret64 = secret | base64encode
    infoSecretMore ('secret:', secret64, secret)
    return dbSecretUpdate (deviceId, secret64)
    | map (T)
    | bifirst (
      (l) => {
        if (isNewKey) {
          warn ('Failed to generate secret -- removing newly created row')
          dbKeyDeleteByDeviceId (deviceId) | foldWhenLeft (
            decorateRejection ('Remove row failed: ') >> warn,
          )
        }
        return l
      })
  },

  // --- return param `should` = secret for yes, `null` for no
  shouldAuthenticate (deviceId, _owInfo, { dbKeyExistsFail, dbKeyIsActiveFail, dbKeyIsAuthorizedFail, dbSecretGetFail, }) {
    const no = any (
      () => this._isBlocked (),
      () => all (
        () => this.isAdmin,
        () => not (this.adminMode | adminModeIsAuthenticate),
      ),
    )
    return no | ifTrue (
      () => Right (false),
      () => this._validateKey (
        deviceId,
        dbKeyExistsFail, dbKeyIsActiveFail, dbKeyIsAuthorizedFail, dbSecretGetFail,
      )
      | map (base64decodeAsU8Array)
      | bifirst (decorateRejection (
        // --- message should encompass both error and not valid
        'shouldAuthenticate (): Could not validate key for this door: ',
      )),
    )
  },

  onAuthenticate (deviceId, granted, _owInfo, { dbUpdateActivityTimestamp, }) {
    return granted | ifTrue (
      () => dbUpdateActivityTimestamp (deviceId),
      () => Right (null),
    )
  },

  // --- requested = false means secret will be generated if it's empty in db
  adminPrepareRead (generateSecretRequested=false) {
    this.adminMode = AdminModeRead (generateSecretRequested)
  },

  adminPrepareCheck () {
    this.adminMode = AdminModeAuthenticate
  },

  adminPrepareGenerateSecret () {
    this.adminPrepareRead (true)
    return Right (null)
  },

  adminReset () {
    this.adminMode = AdminModeBlocked
  },

  _doorHoldGeneral () {
    if (this.isAdmin) die (
      'manual hold(/cancel) not possible on an admin site',
    )
    if (this._isBlockedDoorAuthenticate ()) die (
      'manual hold(/cancel) failed (door is blocked as the result of an authentication)'
    )
    return resolveP ()
  },

  doorHoldOpen () {
    return startP ()
    | then (() => this._doorHoldGeneral ())
    | then (() => this._doorBlockHold ())
    | then (() => this.onDoorHoldOpen ())
    | recover (decorateRejection ('doorHoldOpen() failed: ') >> raise)
  },

  doorCancelHoldOpen () {
    return startP ()
    | then (() => this._doorHoldGeneral ())
    | then (() => this.doorUnblock ())
    | then (() => this.onDoorCancelHoldOpen ())
    | recover (decorateRejection ('doorCancelHoldOpen() failed: ') >> raise)
  },

  _doorBlockHold () {
    info (this.siteName | sprintf1 ('[site] blocking %s'))
    this.doorMode = DoorModeBlockedHold
    return resolveP ()
  },

  doorBlockAuthenticate () {
    info (this.siteName | sprintf1 ('[site] blocking %s'))
    this.doorMode = DoorModeBlockedAuthenticate
  },

  doorUnblock () {
    info (this.siteName | sprintf1 ('[site] unblocking %s'))
    this.doorMode = DoorModeReady
    return resolveP ()
  },

  _isBlocked () {
    // --- @todo doormode and adminmode really ought to be unified in a
    // datatype
    return siteIsBlocked (this.isAdmin ? this.adminMode : this.doorMode)
  },

  // --- it is a door and its state is DoorModeBlockedAuthenticate
  _isBlockedDoorAuthenticate () {
    return !this.isAdmin && doorIsBlockedAuthenticate (this.doorMode)
  },

  /* _validateKey and _getStoredSecret didn't really have to be in this
   * module, but it's a nice way to keep sites.mjs a bit less heavy.
   */

  /* Returns secret (as Uint8Array) if this key is ok.
   */
  _validateKey (deviceId, dbKeyExistsFail, dbKeyIsActiveFail, dbKeyIsAuthorizedFail, dbSecretGetFail) {
    return Right (null)
      | flatMap (() => dbKeyExistsFail (deviceId) | bifirst (
        decorateRejection (`Can't get key from db: `),
      ))
      | flatMap (() => dbKeyIsActiveFail (deviceId))
      | flatMap (() => this.isAdmin | ifTrue (
        () => Right (null),
        () => dbKeyIsAuthorizedFail (deviceId, this.siteName),
      ))
      | flatMap (() => this._getStoredSecret (deviceId, dbSecretGetFail))
  },

  /* XXX const keyPressed = getKeyboardInput () */

  _getStoredSecret (deviceId, dbSecretGetFail) {
    return dbSecretGetFail (deviceId) | map (base64decodeAsU8Array)
  },
}

const props = {
  // @future these could easily be combined into a single sum type
  adminMode: AdminModeBlocked,
  doorMode: DoorModeReady,

  isAdmin: void 8,
  siteName: void 8,

  onDoorHoldOpen: void 8,
  onDoorCancelHoldOpen: void 8,
}

export default proto | factory | factoryProps (props)
