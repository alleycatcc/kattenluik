import {
  pipe, compose, composeRight,
  ok, whenOk, recurry, map,
  condS, guard, lets, invoke,
  factory, factoryProps, each, find,
  ifNil, die, sprintf1,
} from 'stick-js/es'

import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

import { fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { all, listen, send, sendStatus, setSetting, use, } from 'alleycat-js/es/express'
import { decorateRejection, setTimeoutOn, } from 'alleycat-js/es/general'

import { config, } from './config.mjs'
import { debug, info, spawnSync, warn, warnX, } from './io.mjs'
import { routes, } from './server-routes.mjs'
import {
  EventAccessDenied, EventAccessGranted,
  EventAdminReadSuccess, EventAdminReadFailure,
  eventEventData, siteSpecName,
} from './types.mjs'

import { bifirst, doEither, maybeToEitherWithMsg, whenMapKeyExistsOn, } from './util.mjs'

const configTop = config | configure.init
const timeout = configTop.get ('adminReadTimeout')
const checkScreenStatusCmd = configTop.get ('checkScreenStatusCmd')

/* Returns `constructor` or false.
 */
const eventIs = recurry (2) (
  (constructor) => (constructed) => constructor.is (constructed) ? constructor : false,
)

/* Returns `constructor` or false.
 */
const eventIsAny = recurry (2) (
  (constructors) => (constructed) => constructors | find (
    (constructor) => eventIs (constructor, constructed),
  ),
)

// --- takes a list of events and their associated callbacks. Once any one of the events has fired,
// the entire group is removed from the map.
const getOnce = (onceMap) => (eventSpecs) => {
  eventSpecs | each (
    ([eventType, cb]) => onceMap.set (eventType, cb),
  )
  return () => eventSpecs | each (
    ([eventType, ..._]) => onceMap.delete (eventType),
  )
}

/* `once`: a function of (eventType, callback) which registers a callback for an event which will
 * be called the next time the event is called and then never again. It returns a function which can
 * be used to clear the event listener before the event is fired.
 * `eventSpecs`: a list of tuples of [`eventType`, `mkResponse`], where:
 *   `eventType`: the event type of the event we are listening for.
 *   `mkResponse`: a function for creating the response body.
 */
const eventEndpoint = (once, eventSpecs, cleanup, res) => {
  let sent = false
  let clearOnce

  timeout | setTimeoutOn (() => {
    if (sent) return
    clearOnce ()
    cleanup ()
    res | sendStatus (499, {
      umsg: 'Timed out waiting for read.',
    })
  })

  // :: event payload -> Response
  const successCb = (mkResponse) => (eventData) => {
    clearOnce ()
    sent = true
    cleanup ()
    res | send (mkResponse (eventData))
  }
  clearOnce = once (eventSpecs | map (
    ([eventType, mkResponse]) => [eventType, successCb (mkResponse)],
  ))
}

const corsOptions = {
  // --- reflect the request origin.
  origin: true,
  // --- allow credentials mode 'include' in the request.
  credentials: true,
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
  preflightContinue: false,
  optionsSuccessStatus: 204,
}

const proto = {
  init ({ port, onReady, }) {
    this._express = express ()
    | setSetting ('query parser', 'simple')
    | use (cors (corsOptions))
    | use (bodyParser.json ())
    | all ('*', (_req, _res, next) => {
      next ()
    })
    | routes ({
      dbApi: this.dbApi,
      isAdminSite: () => this.adminSiteSpec | ok,
      adminSiteReset: () => this.adminSiteReset (),
      adminSitePrepareRead: () => this.adminSitePrepareRead (),
      adminSitePrepareCheck: () => this.adminSitePrepareCheck (),
      adminSitePrepareGenerateSecret: () => this.adminSitePrepareGenerateSecret (),
      onWaitResponse: (
        cleanup,
        mkResponseAuthenticateFail, mkResponseAuthenticateSuccess,
        mkResponseReadFail, mkResponseReadSuccess,
      ) => (res) => eventEndpoint (
        getOnce (this._onceMap),
        [
          [EventAccessDenied, mkResponseAuthenticateFail],
          [EventAccessGranted, (eventData) => {
            const { mbDeviceId, } = eventData
            const { dbKeyCanUnlockScreen, } = this.dbApi
            return doEither (
              () => maybeToEitherWithMsg ('deviceId is nil', mbDeviceId),
              (deviceId) => {
                return dbKeyCanUnlockScreen (deviceId)
                | bifirst (decorateRejection ('Error calling dbKeyCanUnlockScreen: '))
              },
            ) | fold (
              (l) => {
                warnX (l | decorateRejection ('Error during EventAccessGranted: '))
                return mkResponseAuthenticateFail (eventData)
              },
              (can) => {
                this._isAPILocked = false
                return mkResponseAuthenticateSuccess (can) (eventData)
              }
            )
          }],
          [EventAdminReadSuccess, mkResponseReadSuccess],
          [EventAdminReadFailure, mkResponseReadFail],
        ],
        cleanup, res,
      ),
      // --- note that this gets called a lot and in principle slows down
      // all API calls (not just /should-lock-interface), because they all
      // check this first.
      isAPILocked: () => {
        if (!this._checkScreenCanUnlock ()) {
          warn ('disabling screen lock (no valid unlock keys)')
          this._isAPILocked = false
          return Promise.resolve (null)
        }
        if (this._isAPILocked)
          return Promise.resolve (true)
        const scrStatus = this._checkScreenStatus ()
        if (scrStatus === 'on') return Promise.resolve (false)
        if (scrStatus === 'off') return Promise.resolve (true)
        warn ("Couldn't determine screen status, locking")
        return Promise.resolve (true)
      },
      canUnlockInterface: () => this._checkScreenCanUnlock (),
    })
    | listen (port) (() => onReady (port))

    return this
  },

  _checkScreenCanUnlock () {
    const { dbScreenCanBeUnlocked, } = this.dbApi
    return dbScreenCanBeUnlocked () | fold (
      (l) => {
        warnX (l | decorateRejection ('Error on dbScreenCanBeUnlocked: '))
        return false
      },
      (can) => can,
    )
  },

  // --- returns 'on' | 'off' | null on error
  _checkScreenStatus () {
    // --- expected to print '1' or '0', with optional newline, or exit with error.
    const { ok, stdout, } = spawnSync (... checkScreenStatusCmd)
    if (!ok) {
      warn ('error running checkScreenStatusCmd')
      return null
    }
    if (stdout === '1\n' || stdout === '1') {
      debug ('screen is on')
      return 'on'
    }
    if (stdout === '0\n' || stdout === '0') {
      debug ('screen is off')
      return 'off'
    }
    warn (stdout | sprintf1 ('Invalid return value from checkScreenStatusCmd (%s)'))
    return null
  },

  // --- called on every event which is fired.
  handleEvent (event) {
    const { adminSiteSpec, _onceMap, } = this
    const runCbIfOnced = (eventData, eventType) => eventType | whenMapKeyExistsOn (
      _onceMap,
      (cb) => cb (eventData),
    )
    if (!adminSiteSpec) return
    whenOk (invoke) (lets (
      () => adminSiteSpec | siteSpecName,
      (adminSiteName) => event | eventEventData | condS ([
        [
          EventAccessDenied, EventAccessGranted,
          EventAdminReadSuccess, EventAdminReadFailure,
        ]
        | eventIsAny | guard ((eventData, eventType) => {
          // @todo improve accessor
          const { siteName: eventSiteName, ..._ } = eventData
          // --- in case of multiple admin sites, only accept admin events from the one which is
          // registered with the server.
          if (eventSiteName !== adminSiteName) return
          return () => runCbIfOnced (eventData, eventType)
        }),
      ]),
    ))
  },

  registerAdminSite (siteSpec, prepareRead, prepareCheck, prepareGenerate, reset) {
    this.adminSiteSpec = siteSpec
    this.adminSitePrepareRead = prepareRead
    this.adminSitePrepareCheck = prepareCheck
    this.adminSitePrepareGenerateSecret = prepareGenerate
    this.adminSiteReset = reset
  },

  stop () {
    info ('stopping web server')
    clearInterval (this._jobCheckScreenLocked)
    this._express | ifNil (
      () => die ("Can't stop web server (never initialized?)"),
      (app) => app.close (),
    )
  }
}

const props = {
  _express: void 8,
  _jobCheckScreenLocked: void 8,
  _onceMap: new Map (),

  /* _isAPILocked is on when the app starts, so we start locked regardless of the
   * screen saver state.
   * If there are no valid unlock keys, _isAPILocked gets set to
   * false, or else you would be locked out of the app. We must be sure to
   * show a prominent warning in this case.
   * _isAPILocked is set to false during the `EventAccessGranted`
   * event, which is to say, when a valid admin key is scanned.
   * _isAPILocked is set to true in 2 cases:
   * 1) when checkScreenStatusCmd indicates that the screen has been blanked
   * 2) after the user clicks the lock icon (TODO)
   */
  _isAPILocked: true,

  adminSiteSpec: void 8,
  adminSitePrepareRead: void 8,
  adminSitePrepareCheck: void 8,
  adminSitePrepareGenerateSecret: void 8,
  adminSiteReset: void 8,
  dbApi: void 8,
}

export default proto | factory | factoryProps (props)
