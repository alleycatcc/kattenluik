import {
  pipe, compose, composeRight,
  whenPredicate, tap,
  side1, side2, bindProp,
  prop, die, notOk,
} from 'stick-js/es'

import readline from 'readline'

const on = side2 ('on')
const setRawMode = side1 ('setRawMode')
const emitKeypressEvents = readline | bindProp ('emitKeypressEvents')
const whenNotTTY = whenPredicate (prop ('isTTY') >> notOk)
const checkTTY = whenNotTTY (() => die (
  'Standard input is not a TTY.',
))

/* @throws on no TTY
 */
export const init = (handleKeyPress) => process.stdin
  | tap (checkTTY)
  | tap (emitKeypressEvents)
  | setRawMode (true)
  | on ('keypress', handleKeyPress)
