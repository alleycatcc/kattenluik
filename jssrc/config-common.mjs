import {
  pipe, compose, composeRight,
  bitwiseOr, map,
  ifOk,
} from 'stick-js/es'

import { Just, Nothing, } from 'alleycat-js/es/bilby'

import {
  Led, LedOperateGpio,
  ledOperateTurnOn, ledOperateTurnOff,
  EventHandler, EventAdminUnblocked, EventAdminBlocked,
  SiteSpec, SiteSpecLocation,
} from './types.mjs'

export const bitAddress_DS2482 = bitwiseOr (0b11000)
export const bitAddressS_DS2482 = (bitstring) => parseInt (bitstring, 2) | bitAddress_DS2482

// `colorFunc` can be `null`
export const mkAdminReadyLed = (ledApi) => (pinNumbers, colorFunc) => Led (
  'admin-ready',
  pinNumbers | map (LedOperateGpio),
  [
    EventHandler (EventAdminUnblocked, (_, led) => ledOperateTurnOn (ledApi, led)),
    EventHandler (EventAdminBlocked, (_, led) => ledOperateTurnOff (ledApi, led)),
  ],
  colorFunc | ifOk (Just, () => Nothing),
)

export const mkLocations = map (([ctrlNum, busNum]) => SiteSpecLocation (ctrlNum, busNum))
export const mkAdminSite = (name, locations) => SiteSpec (name, mkLocations (locations), Nothing)
