import {
  pipe, compose, composeRight,
  map, zipAll, ifOk, filter, ifPredicate,
  dot1, recurry, dot2, addIndex, not,
} from 'stick-js/es'

import { Left, Right, flatMap, } from 'alleycat-js/es/bilby'
import { S, getApi, } from 'alleycat-js/es/bsqlite3'
import { decorateRejection, } from 'alleycat-js/es/general'
import { ifEquals, } from 'alleycat-js/es/predicate'

import { bifirst, doEither,} from './util.mjs'

const pragmaSimple = dot1 ('pragmaSimple')
const pragma = dot1 ('pragma')
const pragma1 = dot2 ('pragma1')

/* from Data.Traversable in Haskell:
 * traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
 * In our case, specific to traversing list (`t a` is list of a) in Either context.
 *
 * Basically it collects the values of several Eithers together. So if `f` is `a -> Either String b`, it
 * takes a list of `a` and makes it into a list of `Either String b`. If any of the eithers is `Left
 * 'some string'`, it returns that. If the result is `[Right 10, Right 11, Right 12]`, it returns
 * `Right [10, 11, 12]`.
 */

const traverseEither = recurry (2) (
  (f) => (xs) => {
    const ret = []
    for (const x of xs) {
      const fx = f (x)
      if (fx.isLeft) return fx
      const [val] = fx.toArray ()
      ret.push (val)
    }
    return Right (ret)
  }
)

// --- on abort it returns the value passed to `abort`, which defaults to `true`
const eachAbort = recurry (2) (
  (f) => (xs) => {
    let quit = false
    let _abortVal
    const abort = (abortVal=true) => {
      quit = true
      _abortVal = abortVal
    }
    for (const x of xs) {
      f (abort, x)
      if (quit) return _abortVal
    }
  }
)

const integrityCheck = (msg) => pragmaSimple ('integrity_check') >> flatMap (
  ifEquals ('ok') (
    () => Right (null),
    () => Left (msg),
  ),
)

const eachAbortX = addIndex (eachAbort)

const sameLengthN = (xss) => {
  let last
  const aborted = xss | eachAbortX (
    (abort, xs, n) => {
      const len = xs.length
      if (n === 0) return last = len
      if (last !== len) abort (true)
      last = len
    },
  )
  return not (aborted)
}

const ifSameLengthN = sameLengthN | ifPredicate

// --- takes a list of N objects and a list of keys and compares the objects key by key
const compareObjectLists = recurry (3) (
  (keys) => (errorF) => ([xs, ys]) => {
    const aborted = zipAll (xs, ys) | eachAbort (
      (abort, [x, y]) => {
        keys | eachAbort (
          (abort2, key) => {
            if (x [key] !== y [key]) {
              abort2 ()
              abort (errorF (key, x, y))
            }
          },
        )
      },
    )
    return aborted | ifOk (
      Left,
      () => Right ([xs, ys]),
    )
  }
)

// --- @future the validation is currently too strict: for example if you
// drop a column and recreate it exactly, this will fail, because it's
// strictly comparing lists. We need to make Maps using the name key and do
// it more carefully if we want to deal with this. Better too strict than
// too lax anyway.

// --- returns Either or @throws
export const validateDb = (sqliteApiControl, dbPath) => {
  // --- @throws
  const sqliteApiTest = getApi (dbPath)
  return doEither (
    () => sqliteApiControl | integrityCheck (
      "Something's wrong: control db failed integrity check",
    ),
    () => sqliteApiTest | integrityCheck (
      'Failed integrity check',
    ),
    () => traverseEither (pragma ('table_list'), [
      sqliteApiControl,
      sqliteApiTest,
    ]),
    ([tl1, tl2]) => [tl1, tl2] | ifSameLengthN (
      () => Right ([tl1, tl2]),
      () => Left ('Unexpected number of tables'),
    ),
    ([tl1, tl2]) => compareObjectLists (
      ['schema', 'name', 'type', 'ncol', 'wr', 'strict'],
      (key, x, _y) => 'table name: ' + x.name + ', key: ' + key,
      ([tl1, tl2]),
    ) | bifirst (
      decorateRejection ('Failed to validate table list'),
    ),
    ([tl1, _tl2]) => traverseEither (
      // --- traverse inner list to produce a single Either (the result of all column comparisons)
      flatMap (([ti1, ti2]) => compareObjectLists (
        ['cid', 'type', 'notnull', 'dflt_value', 'pk'],
        (key, x, _y) => 'column with cid: ' + x.cid + ', key: ' + key,
        ([ti1, ti2]),
      )),
      // --- `traverseEither` returns an Either, so this map returns a list of Eithers (the result
      // of comparing a specific table across both databases).
      tl1
        | filter (({ type, }) => type === 'table')
        | map (({ name, }) => traverseEither (
          pragma1 ('table_info', name), [
            sqliteApiControl,
            sqliteApiTest,
          ]
        )
      )
    ),
    () => sqliteApiTest.get (S (`select * from key`)),
    () => sqliteApiTest.get (S (`select * from assignee`)),
  ) | bifirst (
    decorateRejection ('Unable to validate db: '),
  )
}
