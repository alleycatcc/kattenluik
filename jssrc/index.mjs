#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  cond, condS, guard, otherwise,
  sprintfN, split, join, fromPairs, ne, tryCatch,
  T, lets, compact, whenTrue, remapTuples,
} from 'stick-js/es'

import yargsMod from 'yargs'

import { decorateRejection, repluckN, } from 'alleycat-js/es/general'
import { isNotEmptyObject, } from 'alleycat-js/es/predicate'

import {
  info, setLevelDebug, errorPlain, errorX,
  init as initIO,
  cleanup as cleanupIO,
} from './io.mjs'
import { cleanup as cleanupKnal, } from './knal-io.mjs'
import { start, } from './main.mjs'

import {
  Mode,
  ModeReaderSimulateKeys, ModeReaderSimulate, ModeReaderReal,
  ModeLedSimulate, ModeLedReal,
  SimulatePresenceSpec, SimulateReadSpec,
} from './types.mjs'

import {
  deconstruct, hasProp, length, ifExistsIn, listFlatMap,
} from './util.mjs'

process.title = 'kattenluik'

const yargs = yargsMod
  .usage ('Usage: node $0 [options]')
  .option ('d', {
    boolean: true,
    describe: 'Increase verbosity for debugging. Also print more stack traces.',
  })
  .option ('c', {
    boolean: true,
    conflicts: 'C',
    describe: 'force output colors on',
  })
  .option ('C', {
    boolean: true,
    describe: 'force output colors off',
  })
  .option ('init-db', {
    boolean: true,
    describe: 'Initialise the database: this will erase all data if it exists.',
  })
  .option ('init-test-data', {
    boolean: true,
    // --- this actually means 'requires'.
    implies: 'init-db',
    describe: 'Insert test data into the database (requires init-db)',
  })
  .option ('ks', {
    boolean: true,
    describe: `Simulate the reader bus ('Key Simulate'), but don't allow keyboard keys to simulate key events.`
  })
  .option ('ksp', {
    string: true,
    describe: `Simulate a presence event.
              key-spec is given as: <description>:<key-id>:<bus-number>
                 e.g. some-description:33AAFF830500006E:0`
  })
  .option ('ksP', {
    string: true,
    describe: `See option ksp.`
  })
  .option ('ksz', {
    string: true,
    describe: `Simulate the reader bus & simulate a key event using the z key ('Key Simulate Z').
               key-spec is given as: <description>:<key-id>:<secret-base64>:<bus-number>
                 e.g. some-description:33AAFF830500006E:Oy08CSdWKAxeFSAGBH1UCw==:0
               Note that if the bus number is found in the config this can trigger a real door.`
  })
  .option ('ksZ', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksx', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksX', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksc', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksC', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksv', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksV', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksb', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksB', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksN', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ksn', {
    string: true,
    describe: `See option ksz.`
  })
  .option ('ps', {
    boolean: true,
    describe: `Simulate leds ('lamP Simulate').`
  })
  .option ('s', {
    boolean: true,
    count: true,
    describe: `Log secret messages. Can be provided twice to log even more secrets.
    Don't use this in production.`,
  })
  .conflicts ('ksz', 'ks')
  .conflicts ('ksZ', 'ks')
  .conflicts ('ksx', 'ks')
  .conflicts ('ksX', 'ks')
  .conflicts ('ksc', 'ks')
  .conflicts ('ksC', 'ks')
  .conflicts ('ksv', 'ks')
  .conflicts ('ksV', 'ks')
  .conflicts ('ksb', 'ks')
  .conflicts ('ksB', 'ks')
  .conflicts ('ksn', 'ks')
  .conflicts ('ksN', 'ks')
  .strict ()
  .help ('h')
  .alias ('h', 'help')
  .showHelpOnFail (false, 'Specify --help for available options')

const opt = yargs.argv
// --- showHelp also quits.
if (opt._.length !== 0)
  yargs.showHelp (errorPlain)

initIO (opt.c, opt.C, opt.s)

const exit = (rc=0) => {
  info ('doei')
  cleanupIO ()
  cleanupKnal ()
  process.exit (rc)
}

const sigint = (code) => {
  info ('sigint')
  exit (code)
}

process.on ('SIGINT', () => sigint (1))
process.on ('SIGTERM', () => sigint (1))

const printKey = (string, modifiers) => lets (
  () => modifiers
    | remapTuples ((mod, bool) => bool && mod)
    | compact,
  (mods) => [... mods, string] | join (' + '),
)

const handleKeyPress = ({
  handle_a, handle_A,
  handle_z, handle_Z,
  handle_x, handle_X,
  handle_c, handle_C,
  handle_v, handle_V,
  handle_b, handle_B,
  handle_n, handle_N,
  handle_p, handle_P,
}) =>
  (_chunk, key) => key | deconstruct (
    ({ ctrl, meta, shift, name, sequence, }) => cond (
      (() =>           name === 'q')      | guard (() => exit ()),
      (() =>  ctrl  || meta)              | guard (
        () => info ('ignoring keypress with ctrl/alt'),
      ),
      (() =>  shift && name === 'a')      | guard (handle_A),
      (() =>           name === 'a')      | guard (handle_a),
      (() =>  shift && name === 'z')      | guard (handle_Z),
      (() =>           name === 'z')      | guard (handle_z),
      (() =>  shift && name === 'x')      | guard (handle_X),
      (() =>           name === 'x')      | guard (handle_x),
      (() =>  shift && name === 'c')      | guard (handle_C),
      (() =>           name === 'c')      | guard (handle_c),
      (() =>  shift && name === 'v')      | guard (handle_V),
      (() =>           name === 'v')      | guard (handle_v),
      (() =>  shift && name === 'b')      | guard (handle_B),
      (() =>           name === 'b')      | guard (handle_b),
      (() =>  shift && name === 'n')      | guard (handle_N),
      (() =>           name === 'n')      | guard (handle_n),
      (() =>  shift && name === 'p')      | guard (handle_P),
      (() =>           name === 'p')      | guard (handle_p),
      // --- enter changes name to return for some reason on startup.
      (() =>          name === 'enter')  | guard (() => console.log ('')),
      (() =>          name === 'return') | guard (() => console.log ('')),
      otherwise                          | guard (() => info (
        [printKey (name || sequence, { ctrl, meta, shift, })]
        | sprintfN ('invalid key: %s')
      )),
    ),
  )

const getMode = (opt, mockKeySpecs) => {
  const modeReader = cond (
    [() => mockKeySpecs | isNotEmptyObject, () => ModeReaderSimulateKeys (
      mockKeySpecs,
    )],
    [() => opt | hasProp ('ks'), () => ModeReaderSimulate],
    [T, () => ModeReaderReal],
  )

  const modeLed = opt | condS ([
    'ps' | hasProp | guard (_ => ModeLedSimulate),
    otherwise      | guard (_ => ModeLedReal),
  ])

  return Mode (modeReader, modeLed)
}

const go = () => {
  if (opt.d) setLevelDebug ()
  const [initSchema, initTestData] = opt | repluckN (['init-db', 'init-test-data'])
  const dbOpts = { initSchema, initTestData, }

  const mkSimulatePresencePair = (keyboardKey, [description, bus]) => [
    [keyboardKey, SimulatePresenceSpec (description, keyboardKey, bus)],
  ]
  const mkSimulateReadPair = (keyboardKey, [description, deviceId, secret, bus]) => [
    [keyboardKey, SimulateReadSpec (description, keyboardKey, deviceId, secret, bus)],
  ]

  const mkSimulatePair = (n, f) => (optVal, _, optKey) => lets (
    () => optKey [2],
    () => optVal | split (':'),
    (_, parts) => parts | length | ne (n) | whenTrue (
      () => yargs.showHelp (errorPlain),
    ),
    f,
  )

  const mockPresencePairs = [
    'ksp', 'ksP',
  ] | listFlatMap (
    ifExistsIn (opt, mkSimulatePair (2, mkSimulatePresencePair), () => []),
  )

  const mockReadPairs = [
    'ksz', 'ksZ', 'ksx', 'ksX', 'ksc', 'ksC',
    'ksv', 'ksV', 'ksb', 'ksB', 'ksn', 'ksN',
  ] | listFlatMap (
    ifExistsIn (opt, mkSimulatePair (4, mkSimulateReadPair), () => []),
  )

  const mockKeySpecsPairs = [... mockPresencePairs, ... mockReadPairs]

  const mockKeySpecs = fromPairs (mockKeySpecsPairs)

  tryCatch (
    () => info ('started successfully'),
    decorateRejection ("Couldn't start: ") >> errorX,
    () => start (dbOpts, handleKeyPress, getMode (opt, mockKeySpecs)),
  )
}

go ()
