import {
  pipe, compose, composeRight,
  id, T, F, prop, lets, tryCatch, die, noop,
  ifFalse,
} from 'stick-js/es'

import { startP, then, recover, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { decorateRejection, setIntervalOn, } from 'alleycat-js/es/general'

import { config, } from './config.mjs'
import { getOwApi, getLockApi, getDbApi, getKnalApi, getLedApi, } from './internal-api.mjs'
import { initSites, initKb as _initKb, initLeds, initServer, initUptime, initEvents, initDb, } from './init.mjs'
import { info, errorX, green, warn, } from './io.mjs'
import { serverServerM, } from './types.mjs'
import { delayP, } from './util.mjs'

const configTop = config | configure.init

// ------ shared mutable state
const mut = {
  running: false,
  // --- a task started through `setInterval` which does nothing, but keeps the app alive during
  // restart / stop (otherwise node kills us when the last async task is killed)
  keepAlive: void 8,
}

export let restart = () => {
  warn ('unable to restart: did you forget to call `start` first?')
}

export let stop = () => {
  warn ('unable to stop: did you forget to call `start` first?')
}

const { w1druppel: w1druppelConfig, getDruppelOpts, pollInterval, pollIntervalMock, i2cSpec, serverPort, getLeds, } = tryCatch (
  id,
  decorateRejection ("Couldn't load config: ") >> errorX,
  () => configTop.gets ('w1druppel', 'getDruppelOpts', 'pollInterval', 'pollIntervalMock', 'i2cSpec', 'serverPort', 'getLeds'),
)

const handleMode = (mode) => lets (
  () => mode | prop ('modeReader') | cata ({
    ModeReaderSimulate: () => [true, []],
    ModeReaderSimulateKeys: (keySpecs) => [true, keySpecs],
    ModeReaderReal: () => [false, []],
  }),
  () => mode | prop ('modeLed') | cata ({
    ModeLedSimulate: T,
    ModeLedReal: F,
  }),
  (
    [mockReader, mockKeySpecs],
    mockLed,
  ) => ({ mockReader, mockKeySpecs, mockLed, }),
)

/* `start` decides what to mock, initialises the various apis, and initialises the various
 * subsystems.
 *
 * During `initSites` the onewire polling is started.
 *
 * We do a lot of inversion of control here by passing down functions as arguments to the various
 * init functions. This helps a lot for e.g. keeping track of where IO occurs, mocking, etc.
 *
 * The various init functions throw on error, so we fail fast.
 */

export const start = (dbOpts, handleKeyPress, mode) => {
  const { mockReader, mockKeySpecs, mockLed, } = mode | handleMode

  const { knalI2CTarget, knalGpio, } = getKnalApi (i2cSpec)
  const dbApi = getDbApi ()
  const ledApi = getLedApi (mockLed, knalGpio, knalI2CTarget)
  const lockApi = getLockApi (knalGpio, knalI2CTarget)
  const owApi = getOwApi (mockKeySpecs)

  const leds = getLeds (ledApi)
  const { owInitDruppel, } = owApi

  const thePollInterval = mockReader ? pollIntervalMock : pollInterval

  // @todo useful to separate these?
  const mockOnewire = mockReader

  const drup = tryCatch (
    (drup) => {
      info ('one-wire system ready', green ('✔'))
      return drup
    },
    decorateRejection ("Couldn't init one-wire: ") >> die,
    () => owInitDruppel (w1druppelConfig, getDruppelOpts (mockOnewire)),
  )

  tryCatch (
    () => info ('db ready', green ('✔')),
    decorateRejection ("Couldn't init db: ") >> die,
    () => initDb (dbOpts),
  )

  tryCatch (
    () => info ('leds ready', green ('✔')),
    decorateRejection ("Couldn't init leds: ") >> die,
    // --- @todo can we take out knalGpio here?
    // (we already gave it as an argument when constructing `leds`)
    () => initLeds (mockLed, ledApi, leds),
  )

  // --- `server` is the outer ADT, `serverObj` is the mutable object instance which is stored as a property
  // of `server`
  const [server, serverObj] = tryCatch (
    (server) => {
      info ('server ready', green ('✔'))
      return [server, server | serverServerM]
    },
    decorateRejection ("Couldn't init server: ") >> die,
    () => initServer ({ dbApi, port: serverPort, }),
  )

  const uptime = tryCatch (
    (uptime) => {
      info ('uptime clock started', green ('✔'))
      return uptime
    },
    decorateRejection ("Couldn't init uptime clock: ") >> die,
    () => initUptime ({ dbApi, }),
  )

  const fireEvent = tryCatch (
    (fire) => {
      info ('events ready', green ('✔'))
      return fire
    },
    decorateRejection ("Couldn't init events: ") >> die,
    () => initEvents ([...leds, server]),
  )

  const [drupHandle, _adminApi] = tryCatch (
    ([drupHandle, adminApi]) => {
      info ('sites ready', green ('✔'))
      return [drupHandle, adminApi]
    },
    decorateRejection ("Couldn't init sites: ") >> die,
    () => initSites ({
      drup, mockOnewire: Boolean (mockOnewire), fireEvent,
      dbApi, lockApi,
      pollInterval: thePollInterval,
      registerAdminSiteWithServer: (...args) =>
        serverObj.registerAdminSite (...args),
    }),
  )

  /* @todo deal with keep-open locks (they currently forget their state on restart)
  */
  restart = () => {
    info ('restarting')
    return startP ()
    | then (() => mut.running | ifFalse (
      () => null,
      () => stop () | then (() => info ('restart: stopped successfully')),
    ))
    | recover (decorateRejection ("Restart: couldn't stop: ") >> die)
    | then (() => startP ()
      // --- note, start doesn't return a promise, but that's ok
      | then (() => start (dbOpts, handleKeyPress, mode))
      | recover (decorateRejection ("Restart: couldn't start again: ") >> die)
    )
    | then (() => info ('restarted successfully'))
  }

  /* Note that timeouts don't get cleared, for example an LED which is scheduled to turn off in a
   * few seconds or a SW lock which is scheduled to reclose. This is actually what we want.
   *
   * Returns a promise.
   */
  stop = () => {
    if (!mut.running) return warn ('stop (): not currently running, ignoring request')
    info ('stopping')
    return startP ()
    | then (() => drupHandle.cancel ())
    // --- more than enough to let w1druppel handle any straggling timeouts
    | then (() => delayP (thePollInterval * 5))
    | then (() => {
      uptime.stop ()
      serverObj.stop ()
      dbApi.dbClose ()
      mut.running = false
    })
    | recover (decorateRejection ("Couldn't stop: ") >> die)
  }

  // --- @todo we don't really need to store this
  // --- the timeout doesn't matter at all
  mut.keepAlive ||= 10000 | setIntervalOn (noop)
  mut.running = true

  // XXX
  // tryCatch (
    // () => info ('keyboard ready', green ('✔')),
    // decorateRejection ("Couldn't init keyboard: ") >> die,
    // () => initKb ({
      // handleKeyPress, mockKeySpecs,
      // adminApi.dminPrepareRead, adminApi.dminPrepareCheck, adminApi.dminReset,
    // }),
  // )
}
