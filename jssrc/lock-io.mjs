import {
  pipe, compose, composeRight,
  recurry, noop, lets, invoke, tryCatch, not, sprintfN, raise, repeatF, list,
  join, ifTrue, bindProp, map,
} from 'stick-js/es'

import { then, recover, resolveP, startP, } from 'alleycat-js/es/async'
import { cata, } from 'alleycat-js/es/bilby'
import { decorateRejection, setTimeoutOn, } from 'alleycat-js/es/general'

import { gpioOn, gpioOff, knalDoI2C, } from './knal-io.mjs'
import { brightRed, green, info, iwarn, warn, yellow, } from './io.mjs'
import { lockTypeFold, } from './types.mjs'
import { getBenchmarkAsync, getBenchmarkP, getMeasureMarks, ifFalsey, inspect, lookupOn, promiseDelay, } from './util.mjs'

const DO_BENCHMARK = false
const DO_ANALYSE_WAVE = false

const mark = DO_BENCHMARK | ifFalsey (
  () => () => noop,
  () => (s, i) => (n) => [s, i, n] | sprintfN ('%s [loop=%d] [mark=%d]'),
)

const say = DO_ANALYSE_WAVE | ifTrue (
  () => lets (
    () => process.stdout | bindProp ('write'),
    (write) => list >> map (String) >> join (' ') >> write,
  ),
  () => noop,
)

const [setMark, measurePerformance] = getMeasureMarks (DO_BENCHMARK)
const benchmarkAsync = getBenchmarkAsync (DO_BENCHMARK)
const benchmarkP = getBenchmarkP (DO_BENCHMARK)

const operatePin = (knalGpio, pinNumber, direction) => {
  if (direction) gpioOn (knalGpio, pinNumber)
  else gpioOff (knalGpio, pinNumber)
}

const actions = {
  lock: ['locking', brightRed],
  unlock: ['unlocking', green],
  'unlock-relock': ['unlocking & relocking', yellow],
}

const go = invoke (() => {
  const directionStr = String >> lookupOn ({
    true: 'high',
    false: 'low',
  })

  const explain = (isActiveHigh, siteName, pinNumber) => (action) => info (lets (
    () => action | lookupOn (actions),
    () => isActiveHigh | directionStr,
    ([s, color], active) => [s, color (siteName), pinNumber, active] | sprintfN (
      '%s site %s via pin %d (active %s)',
    ),
  ))

  // --- not worth memoizing: len of 500 takes 2ms on rpi0.
  const mkWave = (n, len) => {
    let x = n
    const code = []
    len | repeatF (() => code.push (Number (x ^= 1)))
    return code
  }

  const sendWave = (knalGpio, pinNumber, isRising, len, timeout, windowDuration) => new Promise ((res, _) => {
    const wave = mkWave (isRising, len)
    const f = (i, done) => {
      const m = mark ('sendWave', i)
      setMark (m (0))
      operatePin (knalGpio, pinNumber, wave [i])
      setMark (m (1))
      if (DO_ANALYSE_WAVE && i === 1) {
        say ('|')
        windowDuration | setTimeoutOn (() => say ('|'))
      }
      say (wave [i] | Number, '')
      if (i === len - 1) return (done (), res ())
      setMark (m (2))
      timeout | setTimeoutOn (() => {
        setMark (m (3))
        f (i+1, done)
      })
    }
    const done = (benchmarkAsyncDone) => {
      benchmarkAsyncDone ()
      if (DO_BENCHMARK) info (inspect (measurePerformance ()))
    }
    benchmarkAsync ('f', (benchmarkAsyncDone) => f (0, () => done (benchmarkAsyncDone)))
  })

  const lockGpio = (knalGpio, action, pinNumber, siteName, lockType) => {
    const operate = (direction) => new Promise ((res, rej) => tryCatch (
      res,
      decorateRejection ([pinNumber, direction | directionStr] | sprintfN ("Couldn't set pin %d %s")) >> rej,
      () => {
        console.log ('operatePin, pinNumber, direction, knalGpio', pinNumber, direction, knalGpio)
        return operatePin (knalGpio, pinNumber, direction)
      }
    ))

    /**
     * These all take `activeHigh` as the first argument.
     * `activeHigh` means high to unlock.
     * `totalNumBits` is 2 more than `numBits` (it includes the preamble).
     */
    // const initAuto = not >> operate
    const unlockRelockAuto = (activeHigh, totalNumBits, halfPeriod, windowDuration) => benchmarkP (
      'sendWave',
      () => sendWave (knalGpio, pinNumber, activeHigh, totalNumBits, halfPeriod, windowDuration),
    )
    const unlockExternal = operate
    const lockExternal = not >> operate
    const unlockRelockExternal = (relockTimeout, activeHigh) => unlockExternal (activeHigh)
      | then (() => promiseDelay (relockTimeout))
      | then (() => lockExternal (activeHigh))

    const auto = (numBits, halfPeriod, unblockTimeout, windowDuration, activeHigh) => {
      if (action !== 'unlock-relock') {
        warn ([action] | sprintfN ('Invalid action for auto (GPIO) lock: %s (only unlock-relock is valid)'))
        return resolveP (null)
      }
      explain (activeHigh, siteName, pinNumber) (action)
      // --- @todo check unused and delete
      // if (action === 'lock') return initAuto (activeHigh)
      return unlockRelockAuto (activeHigh, numBits + 2, halfPeriod, windowDuration)
      // --- this delay has no bearing on the lock but helps us time the
      // LEDs and the unblocking.
      | then (() => promiseDelay (unblockTimeout))
      | then (() => promiseDelay (300))
    }
    const external = (relockTimeout, activeHigh) => {
      explain (activeHigh, siteName, pinNumber) (action)
      if (action === 'unlock') return unlockExternal (activeHigh)
      if (action === 'lock') return lockExternal (activeHigh)
      if (action === 'unlock-relock') return unlockRelockExternal (relockTimeout, activeHigh)
      iwarn ('Unknown action: ' + action)
    }
    return lockType | lockTypeFold (
      // --- auto low
      (numBits, halfPeriod, unblockTimeout, windowDuration) => auto (numBits, halfPeriod, unblockTimeout, windowDuration, false),
      // --- auto high
      (numBits, halfPeriod, unblockTimeout, windowDuration) => auto (numBits, halfPeriod, unblockTimeout, windowDuration, true),
      // --- external low
      (relockTimeout) => external (relockTimeout, false),
      // --- external high
      (relockTimeout) => external (relockTimeout, true),
    )
  }

  const lockI2C = invoke (() => {
    const auto = (knalI2C, subtargetId, action, unblockTimeout, direction) => {
      if (action !== 'unlock-relock') {
        warn ([action] | sprintfN ('Invalid action for auto (GPIO) lock: %s (only unlock-relock is valid)'))
        return resolveP (null)
      }
      return startP ()
      | then (() => knalDoI2C (knalI2C, subtargetId, 'relay', 'auto', direction === 'high'))
      | then (() => promiseDelay (unblockTimeout))
      | then (() => promiseDelay (300))
    }
    // --- we manually promisify here by starting with startP, though of
    // course we could also switch to the promise interface of knal.
    const lockExternal = (knalI2C, subtargetId, direction) => startP ()
      | then (() => knalDoI2C (
        knalI2C, subtargetId, 'relay', 'external', direction === 'low',
      ))
    const unlockExternal = (knalI2C, subtargetId, direction) => startP ()
      | then (() => knalDoI2C (
        knalI2C, subtargetId, 'relay', 'external', direction === 'high',
      ))
    const unlockRelockExternal = (knalI2C, subtargetId, relockTimeout, direction) => startP ()
      | then (() => unlockExternal (knalI2C, subtargetId, direction))
      | then (() => promiseDelay (relockTimeout))
      | then (() => lockExternal (knalI2C, subtargetId, direction))
    const external = (knalI2C, subtargetId, action, relockTimeout, direction) => {
      if (action === 'unlock') return unlockExternal (knalI2C, subtargetId, direction)
      if (action === 'lock') return lockExternal (knalI2C, subtargetId, direction)
      if (action === 'unlock-relock') return unlockRelockExternal (knalI2C, subtargetId, relockTimeout, direction)
      iwarn ('Unknown action: ' + action)
    }

    return (knalI2C, action, subtargetId, _siteName, lockType) => {
      return lockType | lockTypeFold (
        // --- auto low
        (_numBits, _halfPeriod, unblockTimeout, _windowDuration) => auto (knalI2C, subtargetId, action, unblockTimeout, 'low'),
        // --- auto high
        (_numBits, _halfPeriod, unblockTimeout, _windowDuration) => auto (knalI2C, subtargetId, action, unblockTimeout, 'high'),
        // --- external low
        (relockTimeout) => external (knalI2C, subtargetId, action, relockTimeout, 'low'),
        // --- external high
        (relockTimeout) => external (knalI2C, subtargetId, action, relockTimeout, 'high'),
      )
    }
  })

  const lockGeneric = (action, genericData, cb) =>
    cb ({ action, data: genericData, })
    | recover (decorateRejection ("Couldn't operate generic lock: ") >> raise)

  // --- @todo does an error get eaten up? try syntax error / missing import
  return recurry (6) (
    // ------ The `go` function (see end of file)
    // --- `action` = 'lock'|'unlock'|'unlock-relock'
    (action) => (knalGpio) => (knalI2C) => (genericData) => (siteName) => (theLock) => theLock | cata ({
      LockGpio: (pinNumber, lockType) => lockGpio (knalGpio, action, pinNumber, siteName, lockType),
      // --- i2cSpec is currently unused (it gets passed as a global config
      // property instead), but we will need it once we start doing more
      // complicated things with I2C (e.g. multiple targets).
      LockI2C: (_i2cSpec, subtargetId, lockType) => {
        return lockI2C (knalI2C, action, subtargetId, siteName, lockType)
      },
      LockGeneric: (cb) => {
        info ([siteName] | sprintfN ('operating generic lock (via callback) for site %s'))
        return lockGeneric (action, genericData, cb)
      },
    }),
  )
})

/* Use this function to grant access. It means unlock and ensure that the
 * lock gets relocked after a short timeout.
 *
 * It returns a promise, which the poll code should wait on before
 * unblocking the site.
 *
 * For an external lock this will relock after the time given in the config,
 * after which time the promise will resolve.
 *
 * For an auto lock it will send the open sequence and let the
 * microcontroller relock it, and the promise will resolve after a delay.
 *
 * For a generic lock it calls the callback.
 */

export const unlockRelock = go ('unlock-relock')

/* Self-explanatory for an external lock. Results in an error if used for an
 * auto lock.
 *
 * For a generic lock it calls the callback.
 *
 * Returns a promise which resolves immediately.
 */
export const lock = go ('lock')

/* Self-explanatory for an external lock. Results in an error if used for an
 * auto lock.
 *
 * For a generic lock it calls the callback.
 *
 * Returns a promise which resolves immediately.
 */
export const unlock = go ('unlock')
