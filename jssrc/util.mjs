import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenPredicate,
  id, recurry, map, join,
  guard, sprintf1, noop, always,
  prop, path, lets, die, raise, not,
  anyAgainst, againstAny,
  eq, ne, notOk, compactOk, has,
  xMatch, find, flip,
  whenPredicateResults,
  tryCatch, spreadTo, list,
  each, addIndex, reduce, concatM, sprintfN,
  ifNil, updateM, defaultTo,
} from 'stick-js/es'

import { dirname, } from 'path'
import { performance, } from 'node:perf_hooks'
import { fileURLToPath, } from 'url'
import util from 'util'

import { then, } from 'alleycat-js/es/async'
import { Just, Nothing, Left, Right, isLeft, flatMap, fold, } from 'alleycat-js/es/bilby'
import { composeManyRight, decorateRejection, isEmptyList, setTimeoutOn, } from 'alleycat-js/es/general'
import { ifArray, ifDefined, ifTrueV, ifUndefined, } from 'alleycat-js/es/predicate'

import { brightRed, green, info, } from './io.mjs'

const isFalsey = Boolean >> not
export const ifFalsey = isFalsey | ifPredicate

export const promiseDelay = (ms) => new Promise (
  (resolve, _) => ms | setTimeoutOn (() => resolve ()),
)

export const ifNotOk = notOk | ifPredicate
export const N = null | always
export const V = void 8 | always

export const usageF = (msg) => () => {
  const [_, scriptName, ... _args] = process.argv
  const str = join (' ', compactOk ([scriptName, msg]))
  return str | sprintf1 ('Usage: %s')
}

export const eqAny = xs => x => x | againstAny (xs | map (eq))

export const setInterval_ = ms => f => setInterval (f, ms)
export const setTimeout_ = ms => f => setTimeout (f, ms)

export const length = prop ('length')

// --- the second x is for convenience to give handle to the whole object again.
export const deconstruct = f => x => f (x, x)

export const hasProp = p => prop (p) >> ok
export const hasAnyProp = map (hasProp) >> againstAny

export const empty = length >> eq (0)
export const notEmpty = empty >> not
export const base64decode = (x) => Buffer.from (x, 'base64').toString ('ascii')
export const base64encode = (x) => Buffer.from (x).toString ('base64')
export const base64decodeAsU8Array = (x) => new Uint8Array (Buffer.from (x, 'base64'))
export const base64encodeFromU8Array = (x) => Buffer.from (x).toString ('base64')

export const whenEq = recurry (3) (
  eq >> whenPredicate,
)

export const isNotTrue = eq (true) >> not
export const ifNotTrue = isNotTrue | ifPredicate

// --- @todo alleycat-js
export const toMaybe = ifOk (Just, Nothing | always)

export const inspect = x => util.inspect (x, { depth: null, colors: process.stdout.isTTY, })

export const whenLeft = isLeft | whenPredicate

// --- @todo remove whenPredicate?
export const foldWhenLeft = p => whenPredicate (isLeft) (fold (p, noop))

export const foldWhenRight = p => fold (noop, p)

export const isNotEmptyList = isEmptyList >> not
export const ifNotEmptyList = isNotEmptyList | ifPredicate

export const orElse = recurry (2) (
  (f) => (x) => x || f (),
)

/* Like Data.Bifunctor.first / Data.Bifunctor.second in Haskell.
 */

// :: (a -> c) -> Either a b -> Either c b
export const bifirst = (f) => fold (
  l => l | f | Left,
  id >> Right,
)

// :: (b -> c) -> Either a b -> Either a c
export const bisecond = (f) => fold (
  id >> Left,
  r => r | f | Right,
)

// @todo sticks, generic
// :: (a -> c) -> (b -> d) -> Either a b -> Either c d
export const bimapEither = recurry (3) (
  (lf) => (rf) => (x) => x | bifirst (lf) | bisecond (rf)
)

// @todo This version is probably more useful than the one in stick.
export const ifHas = recurry (4) (
  (k) => (yes) => (no) => (o) => has (k, o) ? yes (o [k], o, k) : no (o, k),
)
export const whenHas = recurry (3) (
  (k) => (yes) => (o) => has (k, o) ? yes (o [k], o, k) : void 8,
)
export const ifExistsIn = recurry (4) (
  (o) => (yes) => (no) => (k) => ifHas (k, yes, no, o),
)
export const whenExistsIn = recurry (3) (
  (o) => (yes) => (k) => whenHas (k, yes, o),
)

export const ifHasN = recurry (4) (
  (keys) => (yes) => (no) => (o) => {
    const values = []
    for (const k of keys) {
      if (!has (k, o)) return no (o, keys)
      values.push (o [k])
    }
    return yes (values, o, keys)
  }
)

export const partition = recurry (2) (
  (p) => (xs) => {
    const yes = []
    const no = []
    for (const x of xs)
      (p (x) ? yes : no).push (x)
    return [yes, no]
  }
)

export const nonZero = eq (0) >> not
export const whenNonZero = nonZero | whenPredicate

// --- DS1961S device class.
export const isDS1961S = prop ('id') >> xMatch (/^33/) >> ok

export const propTrue = (p) => prop (p) >> eq (true)
export const ifPropTrue = (p) => propTrue (p) | ifPredicate

export const ifPathEq = recurry (5) (
  (val) => (thePath) => (yes) => (no) => (o) => {
    const valAtPath = path (thePath, o)
    if (valAtPath === val) return yes (valAtPath, o)
    return no (valAtPath, o)
  }
)

// :: Maybe a -> a, @throws
export const fromJust = (mb) => mb | fold (id, null) | orElse (
  () => die ('Not a Just:', mb | inspect)
)

export const fromJustDecorate = (decorate) => (x) => tryCatch (
  id, decorateRejection (decorate) >> raise,
  () => x | fromJust,
)

/* This is just a fold over Maybe, where both cases are functions. It can disappear after we
 * implement our own Just/Nothing. We need it for now because bilby's fold doesn't take a function
 * for the Nothing case.
 */
export const foldMaybe = recurry (3) (
  (yes) => (no) => (mb) => {
    if (mb.isSome) return mb | fold (yes, void 8)
    return no ()
  }
)

// :: (a -> b) -> Maybe a -> b | undefined
export const foldWhenJust = recurry (2) (
  (f) => (mb) => mb | fold (f, void 8),
)

/* Returns undefined if the Maybe is a Nothing, like the `whenPredicate` functions.
*/

// :: a -> Map a b -> b | undefined
export const mapHas = recurry (2) (
  (k) => (m) => m.has (k) ? m.get (k) : void 8
)

// :: Map a b -> a -> b | undefined
export const mapKeyExistsOn = flip (mapHas)

// export const mapKeyExistsOn = recurry (2) (
  // (m) => (k) => m.has (k) ? m.get (k) : void 8
// )

// @todo separate versions for when the original value is and isn't passed. so no need for arg0, arg1 etc.

// :: a -> ((b, Map a b) -> c) -> Map a b -> c | undefined
export const whenMapHas = recurry (3) (
  mapHas >> whenPredicateResults,
)

// :: a -> ((b, Map a b) -> c) -> Map a b -> c | undefined
export const whenMapKeyExistsOn = recurry (3) (
  mapKeyExistsOn >> whenPredicateResults,
)

export const defined = void 8 | ne

/* Note: the `yes` function takes first the found value, then the collection.
 */
export const ifFind = recurry (4) (
  (p) => (yes) => (no) => (xs) => {
    const x = xs | find (p)
    if (x === void 8) return no ()
    return yes (x, xs)
  },
)

// --- @todo generic monad
// --- this feeds the unwrapped value to the next function in the chain --
// note that it does not accumulate the values like the `lets`-style
// functions.
export const doEither = (...eithers) => lets (
  () => eithers | map (flatMap) | spreadTo (composeManyRight),
  (chain) => Right (null) | chain,
)

/* Capped at 1.
 * @todo unify this with bilby flatMap.
 */
export const listFlatMap = recurry (2) (
  (f) => (o) => o.flatMap ((x) => f (x)),
)

/* Transforms
 * Left l -> Left l
 * Right r | r is nil -> Left String
 * Right r | otherwise -> Right r
 */

// :: Either a b -> Either (String | a) b

export const nilToLeft = recurry (2) (
  (msg) => flatMap (ifOk (
    Right,
    always (Left (msg))),
  ),
)

// --- useful in `cond`. @todo better name?
export const guardA = (f) => always >> guard (f)

export const okIcon = ifTrueV (green ('✔'), brightRed ('✘'))

export const compactDefined = (o) => {
  const ret = {}
  for (const k in o) if (o [k] !== void 8) ret [k] = o [k]
  return ret
}

export const ifZero = eq (0) | ifPredicate

export const oneOk = list >> anyAgainst (ok)

export const lookupOn = recurry (2) (
  o => k => o [k],
)
export const lookup = recurry (2) (
  k => o => lookupOn (o, k),
)

export const lookupEitherOn = recurry (2) (
  o => k => o [k] | ifOk (
    Right, () => Left ("Can't find key " + String (k)),
  ),
)

// --- @todo would be nicer to put f at the end.
// --- @todo versions which take yes/no functions.

export const lookupOnOr = recurry (3) (
  (f) => (o) => (k) => lookupOn (o, k) | ifUndefined (f, id),
)
export const lookupOr = recurry (3) (
  (f) => (k) => (o) => lookupOnOr (f, o, k),
)
export const lookupOnOrV = recurry (3) (
  (x) => lookupOnOr (x | always),
)
export const lookupOrV = recurry (3) (
  (x) => lookupOr (x | always),
)
export const lookupOrDie = recurry (3) (
  (msg) => (k) => (o) => lookupOnOr (
    () => die (msg),
    o, k,
  )
)
export const lookupOnOrDie = recurry (3) (
  (msg) => (o) => (k) => lookupOrDie (msg, k, o),
)

// --- usage: `__dirname (import.meta.url)`
export const __dirname = fileURLToPath >> dirname

export const eachX = each | addIndex

export const zip = recurry (2) (
  (xs) => (ys) => {
    const acc = []
    const l = Math.min (xs.length, ys.length)
    for (let i = 0; i < l; i++) {
      acc.push ([xs [i], ys [i]])
    }
    return acc
  }
)

/*
 * const BENCHMARK = true
 * const [setMark, measurePerformance] = getMeasureMarks (BENCHMARK)
 * setMark (...)
 * setMark (...)
 * setMark (...)
 * console.log (measurePerformance ())
 */

// const obs = new PerformanceObserver ((_items) => {
// })
// obs.observe ({ type: 'mark', })

export const getMeasureMarks = (enabled) => enabled | ifFalsey (
  () => [noop, noop],
  () => [
    performance.mark,
    () => {
      const res = performance.getEntries () | reduce (
        ([lastStartTime, lastName, total, res], { startTime, name, }) => [
          startTime, name,
          lastStartTime | ifOk (
            () => total + startTime - lastStartTime,
            () => total,
          ),
          res | concatM (
            lastStartTime | ifOk (
              () => [[lastName, name, startTime - lastStartTime, total + startTime - lastStartTime] | sprintfN (
                '%s // %s -> %.2f, total = %.2f'
              )],
              () => [],
            ),
          ),
        ],
        [null, null, 0, []],
      )
      performance.clearMarks ()
      return res [3]
    },
  ],
)

export const getBenchmarkP = (enabled) => enabled | ifFalsey (
  () => (_desc, p) => p (),
  () => (desc, p) => lets (
    () => new Date,
    (d) => p () | then (() => lets (
      () => new Date - d,
      (d1) => info ('[benchmark] benchmarkP:', desc, 'took', d1, 'ms'),
    )),
  ),
)

export const getBenchmarkAsync = (enabled) => enabled | ifFalsey (
  () => (_desc, f) => f (noop),
  () => (desc, f) => lets (
    () => new Date,
    (d) => () => lets (
      () => new Date - d,
      (d1) => info (
        [desc, d1] | sprintfN ('[benchmark] benchmarkAsync: %s took %.2fms'),
      ),
    ),
    (_, done) => f (done),
  ),
)

export const okOrDie = recurry (2) (
  (dieWith) => ifNil (
    () => die (dieWith),
    id,
  )
)

// export const isUndefined = (x) => x === void 8
// export const isDefined = not << isUndefined
// export const whenUndefined = isUndefined | whenPredicate
// export const whenDefined = isDefined | whenPredicate
// export const ifUndefined = isUndefined | ifPredicate
// export const ifDefined = isDefined | ifPredicate

export const mapGetOrSet = recurry (3) (
  (k) => (f) => (m) => m.get (k) | ifDefined (
    id,
    () => lets (
      () => f (),
      (n) => (m.set (k, n), n),
    ),
  ),
)

// --- @todo alleycat-js/async
export const delayP = (ms) => new Promise (
  (res, _) => ms | setTimeoutOn (res),
)

export const toListSingleton = ifArray (id, (x) => [x])

// ------ @todo stick

export const mapUpdateM = recurry (3) (
  (k) => (f) => (m) => m.set (k, f (m.get (k))),
)
export const mapPath = recurry (2) (
  (xs) => (m) => {
    let j = m
    for (const x of xs) {
      if (!ok (j)) return j
      else j = j.get (x)
    }
  return j
  },
)

export const mapSetM = recurry (3) (
  (k) => (v) => (m) => m.set (k, v),
)
export const updateWithDefaultM = recurry (4) (
  (key) => (defaultF) => (f) => updateM (key) (defaultTo (defaultF) >> f),
)
export const updateWithDefaultVM = recurry (4) (
  (key) => (defaultV) => updateWithDefaultM (key, () => defaultV),
)
export const updateMapWithDefaultM = recurry (4) (
  (key) => (defaultF) => (f) => mapUpdateM (key) (defaultTo (defaultF) >> f),
)
export const remapMapTuples = (f) => (m) => {
  const ret = []
  m.forEach ((v, k) => {
    ret.push (f (k, v))
  })
  return ret
}

export const maybeToEitherWithMsg = recurry (2) (
  (msg) => fold (Right, Left (msg)),
)
