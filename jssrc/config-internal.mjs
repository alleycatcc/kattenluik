/* Fixed configuration values which the user can not / does not need to
 * change, for example the layout of the various versions of the eendraad
 * board.
 */

import {
  pipe, compose, composeRight,
  deconstruct, condS,
  eq, guard, ifOk, ifTrue,
  die, otherwise, invoke, sprintf1,
  lets, map, prop, each,
} from 'stick-js/es'

import { then, } from 'alleycat-js/es/async'
import { Just, } from 'alleycat-js/es/bilby'
import { blue, green, info, red, yellow, } from './io.mjs'
import {
  EventHandler,
  EventAccessGranted, EventAccessGrantedClear,
  EventAccessDenied, EventAccessDeniedClear,
  EventPresence, EventPresenceClear,
  Led,
  LockGeneric, LockGpio, LockI2C,
  I2CSpec,
  LockAutoTriggerLow, LockAutoTriggerHigh,
  LockExternalTriggerLow, LockExternalTriggerHigh,
  SiteSpec,
  eventEventData,
  ledOperateTurnOn, ledOperateTurnOff,
  LedOperateI2C,
} from './types.mjs'
import { lookupOnOrDie, promiseDelay, whenEq, } from './util.mjs'

import { mkAdminReadyLed, mkAdminSite, mkLocations, } from './config-common.mjs'

export const i2cSpecV1_3 = I2CSpec (23, 24, 4e5, 0x0a)

const mkKeepOpen = (targetSiteName) => LockGeneric (invoke (() => {
  // --- @todo this variable won't survive if the Pi is rebooted
  let open = false
  // --- ({ action, data, ) => Promise
  return ({ action: _, data: { siteLookupByName, }, }) => {
    return siteLookupByName.get (targetSiteName) | ifOk (
      (r) => open | ifTrue (
        () => {
          info ('[cancel-keep-open] for site', targetSiteName)
          open = false
          return r.doorCancelHoldOpen ()
          // --- basically long enough to pull the key away so it
          // doesn't start a new authentication right away
          | then (() => promiseDelay (1000))
        },
        () => {
          info ('[keep-open] for door on site', targetSiteName)
          open = true
          // --- the first promise will resolve or reject quickly -- it's just
          // opening a door via a software lock (or failing because it's
          // blocked or because something went wrong)
          return r.doorHoldOpen ()
          | then (() => promiseDelay (5000))
        },
      ),
      () => die ("keep-open: can't get reader for site", targetSiteName),
    )
  }
}))

const mkDoorI2C = (name, locations, i2cSpec, subtargetId, lockSpec) => {
  const lock = lockSpec | deconstruct (([lockType, ... params]) => lockType | condS ([
    eq ('auto-trigger-low') | guard (() => LockI2C (i2cSpec, subtargetId, LockAutoTriggerLow (... params))),
    eq ('auto-trigger-high') | guard (() => LockI2C (i2cSpec, subtargetId, LockAutoTriggerHigh (... params))),
    eq ('external-trigger-low') | guard (() => LockI2C (i2cSpec, subtargetId, LockExternalTriggerLow (... params))),
    eq ('external-trigger-high') | guard (() => LockI2C (i2cSpec, subtargetId, LockExternalTriggerHigh (... params))),
    eq ('keep-open') | guard (() => mkKeepOpen (params [0])),
    otherwise | guard ((x) => die (JSON.stringify (x) | sprintf1 ('Invalid: %s'))),
  ]))
  return SiteSpec (name, locations, Just (lock))
}

const mkDoorGpioEendraadV1_3 = (name, locations, pin, lockSpec) => {
  const lock = lockSpec | deconstruct (([lockType, ... params]) => lockType | condS ([
    eq ('auto-trigger-low') | guard (() => LockGpio (pin, LockAutoTriggerLow (... params))),
    eq ('auto-trigger-high') | guard (() => LockGpio (pin, LockAutoTriggerHigh (... params))),
    eq ('external-trigger-low') | guard (() => LockGpio (pin, LockExternalTriggerLow (... params))),
    eq ('external-trigger-high') | guard (() => LockGpio (pin, LockExternalTriggerHigh (... params))),
    eq ('keep-open') | guard (() => mkKeepOpen (params [0])),
    otherwise | guard ((x) => die (JSON.stringify (x) | sprintf1 ('Invalid: %s'))),
  ]))
  return SiteSpec (name, locations, Just (lock))
}

export const mkDoorEendraadV1_3 = mkDoorI2C

const mkLedProbeEvents = {
  grant: [EventAccessGranted, EventAccessGrantedClear],
  deny: [EventAccessDenied, EventAccessDeniedClear],
  presence: [EventPresence, EventPresenceClear],
}

const mkLedProbeV1_3 = (type) => (ledApi) => (theSiteName, suffix, i2cSpec, subtargetId, mbColorFunc) => lets (
  () => theSiteName + suffix,
  () => type | lookupOnOrDie ('invalid type ' + type, mkLedProbeEvents),
  (ledName, [setEvent, clearEvent]) => Led (
    ledName,
    [LedOperateI2C (i2cSpec, subtargetId, type)],
    [
      EventHandler (setEvent, (event, led) => lets (
        () => event | eventEventData,
        ({ siteName: eventSiteName, ..._ }) => eventSiteName | whenEq (theSiteName,
          () => ledOperateTurnOn (ledApi, led),
        ),
      )),
      EventHandler (clearEvent, (event, led) => lets (
        () => event | eventEventData,
        ({ siteName: eventSiteName, }) => eventSiteName | whenEq (theSiteName,
          () => ledOperateTurnOff (ledApi, led),
        ),
      )),
    ],
    mbColorFunc,
  ),
)

export const mkDoorLedsEendraadV1_3 = (ledApi) => (i2cSpec, theSiteName, subtargetId) => [
  mkLedProbeV1_3 ('grant') (ledApi) (theSiteName, '-grant', i2cSpec, subtargetId, Just (green)),
  mkLedProbeV1_3 ('deny') (ledApi) (theSiteName, '-deny', i2cSpec, subtargetId, Just (red)),
  mkLedProbeV1_3 ('presence') (ledApi) (theSiteName, '-presence', i2cSpec, subtargetId, Just (yellow)),
]

const checkUnique = (tag) => (seenSet) => (xs) => {
  const check = (x) => seenSet.has (x) | ifTrue (
    () => die (tag + ' not unique: ' + x),
    () => seenSet.add (x),
  )
  return xs | each (check)
}

export const mkInstallationV1_3 = ({ doors, doorsGpio=[], admins, leds, }) => {
  const all = [... doors, ... admins]
  all | map (prop (0)) | checkUnique ('Site name') (new Set ())
  all | map (prop (2)) | checkUnique ('Subtarget id') (new Set ())
  const sites = [
    ... doors | map ((spec) => mkSiteEendraadV1_3 (... spec)),
    ... doorsGpio | map ((spec) => mkSiteGpioEendraadV1_3 (... spec)),
    ... admins | map ((spec) => mkAdminEendraadV1_3 (... spec)),
  ]
  return ({
    sites,
    leds: (ledApi) => leds (
      ledApi,
      [mkAdminReadyLed (ledApi) ([27], blue)],
    ),
  })
}

const mkSiteGpioEendraadV1_3 = (siteName, locations, pin, lockSpec) => {
  return lets (
    () => mkLocations (locations),
    (siteLocations) => ({
      siteSpec: mkDoorGpioEendraadV1_3 (siteName, siteLocations, pin, lockSpec),
      // --- note that the GPIO leds do not get initialised (@future,
      // implement if necessary)
      leds: (ledApi) => [],
    }),
  )
}

export const mkSiteEendraadV1_3 = (siteName, locations, subtargetId, lockSpec) => {
  return lets (
    () => mkLocations (locations),
    () => i2cSpecV1_3,
    (siteLocations, i2cSpec) => ({
      siteSpec: mkDoorEendraadV1_3 (siteName, siteLocations, i2cSpec, subtargetId, lockSpec),
      leds: (ledApi) => mkDoorLedsEendraadV1_3 (ledApi) (i2cSpec, siteName, subtargetId),
    }),
  )
}

export const mkAdminEendraadV1_3 = (siteName, locations, subtargetId) => lets (
  () => i2cSpecV1_3,
  (i2cSpec) => ({
    siteSpec: mkAdminSite (siteName, locations),
    leds: (ledApi) => mkDoorLedsEendraadV1_3 (ledApi) (i2cSpec, siteName, subtargetId),
  }),
)

const w1DruppelControllersV1_3 = [
  ({ tag, address, }) => ({
    tag,
    config: ['ds2482-100', '/dev/i2c-1', address],
  }),
  ({ tag, address, }) => ({
    tag,
    config: ['ds2482-800', '/dev/i2c-1', address],
  }),
  ({ tag, address, }) => ({
    tag,
    config: ['ds2477', '/dev/i2c-1', address],
  }),
]

const w1DruppelAuthenticatorsV1_3 = [
  'ds1961', 'ds1964',
]

export const mkW1DruppelV1_3 = (f) => lets (
  () => w1DruppelControllersV1_3,
  () => w1DruppelAuthenticatorsV1_3,
  ([ds2482_100, ds2482_800, ds2477], [ds1961, ds1964]) => f (
    { ds2482_100, ds2482_800, ds2477, }, { ds1961, ds1964, }
  ),
)
