import {
  pipe, compose, composeRight,
  recurry, map, sprintf1,
  ifTrue, invoke, reduce, lets, nil,
  whenOk, sprintfN, each, die, T,
} from 'stick-js/es'

import { cata, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { setTimeoutOn, } from 'alleycat-js/es/general'

import { config, } from './config.mjs'
import { init as dbInit, } from './db.mjs'
import { fireEventOnTarget, } from './event.mjs'
import { info, green, error, underline, warn, } from './io.mjs'
import { mkKeyboardReader, } from './poll.mjs'
import { defined, } from 'alleycat-js/es/predicate'

import serverMod from './server.mjs'
import { init as initSitesMod, makeSite, } from './sites.mjs'
import uptimeMod from './uptime.mjs'

import {
  Server, Event, EventHandler,
  EventAdminReadSuccess, EventAdminReadFailure,
  EventAccessDenied, EventAccessGranted,
  ledOperateTurnOff,
  siteSpecLocations, siteSpecLocationCtrlNum, siteSpecLocationBusNum,
} from './types.mjs'

import { init as initStdin, } from './stdin.mjs'
import { ifHas, mapGetOrSet, okOrDie, } from './util.mjs'

// const DEBUG_ONLY_LOCATION = ({ busNum, }) => busNum | gt (5)
// const DEBUG_ONLY_LOCATION = ({ busNum, }) => busNum === 4 || busNum === 7
// const DEBUG_ONLY_LOCATION = ({ busNum, }) => busNum === 5
const DEBUG_ONLY_LOCATION = T

const configTop = config | configure.init
const { adminReadTimeout, getMockSpec, getOwConfig, getSiteSpecs, } = configTop.gets (
  'adminReadTimeout', 'getMockSpec', 'getOwConfig', 'getSiteSpecs',
)

const getSpeak = (mock) => ({ ctrlNum, ctrlTag, busNum, authNum, }) => {
  const m = mock ? ' [mock]' : ''
  const s = `[ctrl ${ctrlNum} “${ctrlTag}” / bus ${busNum} / auth ${authNum}]`
  return {
    info: (...i) => info (s + m, ...i),
    error: (...e) => warn (s + m, ...e),
  }
}

const mkSiteLookupByLocation = () => ({
  _data: new Map (),
  add (ctrlNum, busNum, site) {
    const m = mapGetOrSet (ctrlNum, () => new Map (), this._data)
    m.set (busNum, site)
    return site
  },
  get (ctrlNum, busNum) {
    return this._data.get (ctrlNum) | whenOk (
      (m) => m.get (busNum)
    ) | okOrDie ([ctrlNum, busNum] | sprintfN ('Unable to find site for %d/%d'))
  }
})

const mkSiteLookupByName = () => ({
  _data: {},
  add (siteName, site) {
    if (defined (this._data [siteName]))
      die ('mkSiteLookupByName (): collision on ' + siteName)
    return this._data [siteName] = site
  },
  get (siteName) {
    return this._data [siteName]
      | okOrDie ([siteName] | sprintfN ('Unable to find site for %s'))
  }
})

const kbReader = mkKeyboardReader ()

/* @throws on unable to init or lock.
 *
 * Starts a poller using `setInterval`.
 */

// :: {} -> IO ()
export const initSites = ({
  drup, mockOnewire, fireEvent,
  dbApi, lockApi,
  pollInterval, registerAdminSiteWithServer,
}) => {
  initSitesMod ()
  const siteLookupByLocation = mkSiteLookupByLocation ()
  const siteLookupByName = mkSiteLookupByName ()
  const mkSite = makeSite ({ fireEvent, dbApi, lockApi, registerAdminSiteWithServer, siteLookupByName, })
  const addToSiteLookupByName = (site) => lets (
    () => site.siteName,
    (name) => siteLookupByName.add (name, site),
  )
  const addToSiteHandleLookupByLocation = (site, location) => lets (
    () => location | siteSpecLocationCtrlNum,
    () => location | siteSpecLocationBusNum,
    (c, b) => siteLookupByLocation.add (c, b, site)
  )
  const updateSiteLookups = (siteHandle) => (location) => {
    addToSiteLookupByName (siteHandle.site)
    addToSiteHandleLookupByLocation (siteHandle, location)
  }

  // --- with side effect (adds sites to various lookups)
  const adminApi = getSiteSpecs () | reduce (
    (accAdminApi, siteSpec) => lets (
      () => mkSite (siteSpec),
      (siteHandle) => siteSpec | siteSpecLocations | each (updateSiteLookups (siteHandle)),
      ({ adminApi, }) => adminApi.valid ? adminApi : accAdminApi,
    ),
    null,
  )

  if (nil (adminApi)) error ("Couldn't determine admin site")

  // --- @throws
  const getSite = ({ ctrlNum, busNum, }) => siteLookupByLocation.get (ctrlNum, busNum)
  const theGetSpeak = getSpeak (mockOnewire)

  const handlers = {
    onError: (e, owInfo) => {
      if (!DEBUG_ONLY_LOCATION (owInfo)) return
      theGetSpeak (owInfo).error (e)
      const { handlers, } = getSite (owInfo)
      handlers.onError (owInfo)
    },
    onShort: (owInfo) => {
      if (!DEBUG_ONLY_LOCATION (owInfo)) return
      theGetSpeak (owInfo).info ('short')
      const { handlers, } = getSite (owInfo)
      handlers.onShort (owInfo)
    },
    onPresence: (owInfo) => {
      if (!DEBUG_ONLY_LOCATION (owInfo)) return false
      theGetSpeak (owInfo).info ('presence')
      const { handlers, } = getSite (owInfo)
      return handlers.onPresence (owInfo)
    },
    onResult: (deviceId, owInfo) => {
      theGetSpeak (owInfo).info ('saw device', deviceId)
      const { handlers, } = getSite (owInfo)
      return handlers.onResult (deviceId, owInfo)
    },
    shouldGenerateSecret: (deviceId, owInfo) => {
      const { handlers, } = getSite (owInfo)
      return handlers.shouldGenerateSecret (deviceId, owInfo)
    },
    onGenerateSecret: (deviceId, secret, owInfo) => {
      theGetSpeak (owInfo).info ('on-generate-secret')
      const { handlers, } = getSite (owInfo)
      handlers.onGenerateSecret (deviceId, secret, owInfo)
    },
    shouldAuthenticate: (deviceId, owInfo) => {
      const { handlers, } = getSite (owInfo)
      return handlers.shouldAuthenticate (deviceId, owInfo)
    },
    onAuthenticate: (deviceId, granted, owInfo) => {
      theGetSpeak (owInfo).info ('authenticated, access ok =', granted)
      const { handlers, } = getSite (owInfo)
      return handlers.onAuthenticate (deviceId, granted, owInfo)
    },
  }

  const drupHandle = drup.forever (getOwConfig (), pollInterval, handlers, getMockSpec (mockOnewire))
  return [drupHandle, adminApi]
}

/* @throws on no TTY
 */

// :: {} -> IO ()
export const initKb = invoke (() => {
  const addMockKbRead = (spec) => spec | cata ({
    SimulatePresenceSpec: (description, keyboardKey, busNumber) => {
      info (description | sprintf1 ('[mock] presence “%s”'))
      kbReader.addKbRead (busNumber, keyboardKey)
    },
    SimulateReadSpec: (description, keyboardKey, _deviceId, secret64, busNumber) => {
      info (description | sprintf1 ('[mock] read “%s”'))
      kbReader.addKbRead (busNumber, keyboardKey)
    },
  })

  return ({
    handleKeyPress, mockKeySpecs,
    adminPrepareRead, adminPrepareCheck, adminReset,
  }) => {
    initStdin (handleKeyPress ({
      handle_a: () => {
        info (underline ('read') | sprintf1 ('unlocking admin in %s mode'))
        adminPrepareRead ()
        adminReadTimeout | setTimeoutOn (() => adminReset ())
      },
      handle_A: () => {
        info (underline ('authenticate') | sprintf1 ('unlocking admin in %s mode'))
        adminPrepareCheck ()
        adminReadTimeout | setTimeoutOn (() => adminReset ())
      },
      // --- mock presence.
      handle_p: () => mockKeySpecs | ifHas ('p', addMockKbRead, () => info ('[noop] no kb key to mock for p')),
      handle_P: () => mockKeySpecs | ifHas ('P', addMockKbRead, () => info ('[noop] no kb key to mock for p')),
      // ------ mock keys.
      // one convenient way to use this is to wire lowercase to a door and uppercase to admin for a given key.
      handle_z: () => mockKeySpecs | ifHas ('z', addMockKbRead, () => info ('[noop] no kb key to mock for z')),
      handle_Z: () => mockKeySpecs | ifHas ('Z', addMockKbRead, () => info ('[noop] no kb key to mock for Z')),
      handle_x: () => mockKeySpecs | ifHas ('x', addMockKbRead, () => info ('[noop] no kb key to mock for x')),
      handle_X: () => mockKeySpecs | ifHas ('X', addMockKbRead, () => info ('[noop] no kb key to mock for X')),
      handle_c: () => mockKeySpecs | ifHas ('c', addMockKbRead, () => info ('[noop] no kb key to mock for c')),
      handle_C: () => mockKeySpecs | ifHas ('C', addMockKbRead, () => info ('[noop] no kb key to mock for C')),
      handle_v: () => mockKeySpecs | ifHas ('v', addMockKbRead, () => info ('[noop] no kb key to mock for v')),
      handle_V: () => mockKeySpecs | ifHas ('V', addMockKbRead, () => info ('[noop] no kb key to mock for V')),
      handle_b: () => mockKeySpecs | ifHas ('b', addMockKbRead, () => info ('[noop] no kb key to mock for b')),
      handle_B: () => mockKeySpecs | ifHas ('B', addMockKbRead, () => info ('[noop] no kb key to mock for B')),
      handle_n: () => mockKeySpecs | ifHas ('n', addMockKbRead, () => info ('[noop] no kb key to mock for n')),
      handle_N: () => mockKeySpecs | ifHas ('N', addMockKbRead, () => info ('[noop] no kb key to mock for N')),
  }))
  }
})

/* @throws on unable to open db.
 */

export const initDb = (dbOpts) => {
  dbInit (dbOpts)
}

// :: Bool |mockLeds| -> ledApi -> [Led] -> IO (), @throws
export const initLeds = recurry (3) (
  (mock) => mock | ifTrue (
    () => (_) => (_) => info ('[mock] init leds'),
    () => (ledApi) => (leds) => leds | each (ledOperateTurnOff (ledApi)),
  ),
)

export const initServer = ({ dbApi, port, }) => {
  const serverM = serverMod.create ({ dbApi, }).init ({
    port,
    // :: Number |port| -> IO ()
    onReady: String >> green >> sprintf1 ('listening on port %s') >> info,
  })
  const handleEvent = (event, ..._) => serverM.handleEvent (event)
  return Server (serverM, [
    EventHandler (EventAdminReadSuccess, handleEvent),
    EventHandler (EventAdminReadFailure, handleEvent),

    EventHandler (EventAccessDenied, handleEvent),
    EventHandler (EventAccessGranted, handleEvent),
  ])
}

export const initUptime = ({ dbApi, }) => uptimeMod.create ().init (dbApi).start ()

export const initEvents = (targets) => {
  const fireAll = (event) => targets | map (
    fireEventOnTarget (event),
  )
  return Event >> fireAll
}
