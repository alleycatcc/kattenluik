import {
  pipe, compose, composeRight,
  assocM,
} from 'stick-js/es'

/* Keystrokes are handled with an event, while getting reader reads is done through polling.
 * We use this object to make them look transparent to the site poll.
 *
 * The goal is an object like this for each poll:
 *
 *     {
 *       [busNumber]: deviceId,
 *       ...
 *     }
 *
 * where an entry means 'presence' for that key.
 */

export const mkKeyboardReader = () => ({
  // --- { [busNumber]: deviceId, ... }
  _kbRead: {},
  addKbRead (busNumber, keyboardKey) {
    return this._kbRead
    | assocM (busNumber, keyboardKey)
  },
  get (busNumber) {
    const x = this._kbRead [busNumber]
    this._kbRead [busNumber] = void 8
    return x
  },
})
