/* This module is for dealing with all sites as a whole. It also exports the
 * function `makeSite` for initialising a 'site' instance (the object
 * created by site.mjs) per site. We try to keep things like event handlers
 * here and mostly use site.mjs for things that really depend on the current
 * (mutable) state of the site.
 */

import {
  pipe, compose, composeRight,
  ifOk, whenOk, sprintf1, die, id, sprintfN,
  ifTrue, updateM, appendM, each, invoke, mergeM,
} from 'stick-js/es'

import { then, recover, startP, } from 'alleycat-js/es/async'
import { cata, fold, isNothing, Just, Nothing, Right, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { decorateRejection, setTimeoutOn, } from 'alleycat-js/es/general'
import { blue, brightRed, green, info, infoSecret, logWith, warn, warnX, } from './io.mjs'

import { config, } from './config.mjs'
import siteMod from './site.mjs'
import {
  EventLockOpen, EventLockClose,
  EventAdminUnblocked, EventAdminBlocked,
  EventAdminReadSuccess, EventAdminReadFailure,
  EventAccessDenied, EventAccessDeniedClear,
  EventAccessGranted, EventAccessGrantedClear,
  EventPresence, EventPresenceClear,
} from './types.mjs'

import {
  foldMaybe, foldWhenJust, promiseDelay, foldWhenLeft,
} from './util.mjs'

const configTop = config | configure.init
const { ledAccessTimeoutGranted, ledAccessTimeoutDenied, ledPresenceTimeout, } = configTop.gets (
  'ledAccessTimeoutGranted', 'ledAccessTimeoutDenied', 'ledPresenceTimeout',
)

// ------ shared mutable state

let mut = {
  foundAdmin: void 8,
  timeouts: void 8,
}

export const init = () => {
  mut | mergeM ({
    foundAdmin: false,
    timeouts: {
      /* :: String -> [Number]
       *
       * Groups of lists of timeout ids which should be cleared if a new
       * event is fired.
       */
      data: {},
      add (group, timeout) {
        this.data | updateM (group, ifOk (
          appendM (timeout),
          () => [timeout],
        ))
      },
      clear (group) {
        this.data [group] | whenOk (
          each (clearTimeout),
        )
      }
    },
  })
}

// --- /boot/config.txt puts the locks in the right state at startup, but
// it's also necessary to init them here in case our process is restarted.

// :: LockApi -> Lock -> IO ()
const initLock = ({ lockIOInit, ... _ }) => lockIOInit

/* This is data which locks specified as LockGeneric need.
 * Currently they need to be able to look up a site (instance of site.mjs)
 * given a siteName so that they can perform the 'keep-open' function.
 */
const lockGenericData = {}

// :: (String, Number) -> [() -> Promise]
const getOnFailure = (fireEvent) => (siteName) => ({
  authenticate: () => new Promise ((resolve, _) => {
    const grp = siteName + ':led'
    mut.timeouts.clear (grp)
    fireEvent (EventAccessDenied (siteName))
    fireEvent (EventAccessGrantedClear (siteName))
    mut.timeouts.add (grp, ledAccessTimeoutDenied | setTimeoutOn (
      () => fireEvent (EventAccessDeniedClear (siteName)),
    ))
    ledAccessTimeoutDenied | setTimeoutOn (() => resolve ())
  }),
  // --- meaning an IO failure for either 'read' or 'check' modes
  adminRead: () => new Promise ((resolve, _) => {
    fireEvent (EventAdminReadFailure (siteName))
    resolve ()
  }),
})

// :: (String, Number, Maybe Lock) -> (...) -> Promise
const getOnSuccess = (fireEvent, lockIOUnlockRelock) => (siteName, mbLock) => ({
  /* For either door or admin. We're in admin mode when we
   * use 'check access for key', and we fold over `mbLock` to figure out
   * which mode it is.
   */
  authenticate: (deviceId) => {
    const grp = siteName + ':led'
    mut.timeouts.clear (grp)
    fireEvent (EventLockOpen)
    fireEvent (EventAccessDeniedClear (siteName))
    fireEvent (EventAccessGranted (siteName, Just (deviceId)))
    mut.timeouts.add (grp, ledAccessTimeoutGranted | setTimeoutOn (
      () => fireEvent (EventAccessGrantedClear (siteName)),
    ))

    return mbLock | foldMaybe (
      // ------ door (we have a lock).
      lockIOUnlockRelock (lockGenericData, siteName)
      // --- onSuccess failed, meaning the authentication was
      // successful, but either the lock failed, or in the case of a
      // keep-open event, the door is already in use. Τurn the red LED on in
      // this case as well to avoid confusing the user.
      >> recover (decorateRejection ('onSuccess(): ') >> ((err) => {
        warnX (err)
        mut.timeouts.clear (grp)
        fireEvent (EventAccessGrantedClear (siteName))
        fireEvent (EventAccessDenied (siteName))
        mut.timeouts.add (grp, ledAccessTimeoutDenied | setTimeoutOn (
          () => fireEvent (EventAccessDeniedClear (siteName)),
        ))
      }))
      // --- @todo no longer correct (adds a short delay before
      // unblocking)
      >> then (() => promiseDelay (ledAccessTimeoutDenied)),

      // ------ admin (we don't have a lock).
      // (ok, do nothing, because EventAccessGranted has already been fired.)
      () => {},
    )
  },

  adminRead: (deviceId, isNewKey, isNewSecret) => {
    fireEvent (EventAdminReadSuccess (siteName, deviceId, isNewKey, isNewSecret))
    return Promise.resolve ()
  },
})

/* `direction`: true = hold open, false = cancel
 */
const doorHoldOpen = (fireEvent, lockIOUnlock, lockIOLock) => (siteName, mbLock) => (direction) => {
  // --- @todo is this necessary for something?
  const _deviceId = 'TODO'
  const inner = (theLock) => {
    const promise = direction | ifTrue (
      // --- hold open
      () => {
        fireEvent (EventLockOpen)
        // --- in other words, light the green led until the manual unlock
        // is removed.
        fireEvent (EventAccessDeniedClear (siteName))
        fireEvent (EventAccessGranted (siteName, Nothing))
        return lockIOUnlock (lockGenericData, siteName, theLock)
      },
      // --- cancel
      () => {
        fireEvent (EventLockClose)
        fireEvent (EventAccessGrantedClear (siteName))
        return lockIOLock (lockGenericData, siteName, theLock)
      },
    )
    return promise | recover (
      decorateRejection ('manual lock/unlock: ') >> die,
    )
  }
  return mbLock | foldMaybe (
    (theLock) => inner (theLock),
    () => warn ('doorHoldOpen: this site has no lock, doing nothing'),
  )
}

const getAdminApi = (onAdminBlock, onAdminUnblock) => (adminSite) => ({
  adminPrepareRead: (generateSecretRequested) => {
    adminSite.adminPrepareRead (generateSecretRequested)
    onAdminUnblock ()
    return Right (null)
  },
  adminPrepareCheck: () => {
    adminSite.adminPrepareCheck ()
    onAdminUnblock ()
    return Right (null)
  },
  adminPrepareGenerateSecret: () => {
    adminSite.adminPrepareGenerateSecret ()
    onAdminUnblock ()
    return Right (null)
  },
  adminReset: () => {
    adminSite.adminReset ()
    onAdminBlock ()
  },
})

// --- `adminApi` will be invalid if this is not an admin site: we must be
// sure we are an admin site before using it.
const getHandlers = (dbApi, adminApi, fireEvent) => (site, isAdmin) => ({
  onSuccessAdminReadP, onFailureAdminReadP,
  onSuccessAuthenticateP, onFailureAuthenticateP,
}) => invoke (() => {
  const {
    dbKeyCreateIfNotExists, dbKeyDeleteByDeviceId, dbKeyExistsFail,
    dbSecretGet, dbSecretUpdate, dbSecretGetFail,
    dbKeyIsActiveFail, dbKeyIsAuthorizedFail,
    dbUpdateActivityTimestamp,
  } = dbApi

  const theSiteName = site.siteName
  const authenticateX = (event, errPrefix, msg) => startP ()
    | then (() => site.doorBlockAuthenticate ())
    | then (() => event ())
    // --- @todo this message shows up a bit late in the console
    | then (() => infoSecret (msg))
    | then (() => site.doorUnblock ())
    | recover (decorateRejection (errPrefix + ': ') >> warnX)
  // --- authentication failed, either because of an error or access denied
  const failAuthenticate = (deviceId) => authenticateX (
    onFailureAuthenticateP,
    'failAuthenticate ()',
    [brightRed ('⊘'), deviceId, theSiteName] | sprintfN ('%s access denied for key %s on site %s'),
  )
  // --- authentication succeeded
  const grantAuthenticate = (deviceId) => authenticateX (
    () => onSuccessAuthenticateP (deviceId),
    'grantAuthenticate ()',
    [green ('✔'), deviceId, theSiteName] | sprintfN ('%s access granted for key %s on site %s'),
  )

  const isNewKeyTbl = {}

  /* @future Several of the error branches below don't result in any
   * feedback to the user (i.e. the red LED doesn't get lit). If this turns
   * out to be desirable we could add another type of event next to
   * success/fail authenticate/read to designate some general error and
   * light the red LED.
   */

  return {
    onError: (_owInfo) => {
      warn ('onError (): one-wire error')
      if (isAdmin) onFailureAdminReadP ()
    },
    onShort: (owInfo) => {
      if (!site.onShort (owInfo)) return
      const grp = theSiteName + ':led'
      mut.timeouts.clear (grp)
      fireEvent (EventPresence (theSiteName))
      mut.timeouts.add (grp, ledPresenceTimeout | setTimeoutOn (
        () => fireEvent (EventPresenceClear (theSiteName)),
      ))
    },
    onPresence: (owInfo) => site.onPresence (owInfo),
    onResult: (deviceId, owInfo) => {
      infoSecret ('saw device ' + deviceId)
      return site.onResult (deviceId, owInfo, { dbKeyCreateIfNotExists, })
      | fold (
        (err) => {
          warnX (err | decorateRejection (`onResult (): `))
          if (isAdmin) onFailureAdminReadP ()
          return false
        },
        (isNewKey) => {
          if (isNewKey) isNewKeyTbl [deviceId] = true
          return true
        },
      )
    },
    shouldGenerateSecret: (deviceId, owInfo) => {
      return site.shouldGenerateSecret (deviceId, owInfo, { dbSecretGet, })
      | fold (
        (err) => {
          warnX (err | decorateRejection (`shouldGenerateSecret (): `))
          if (isAdmin) onFailureAdminReadP ()
          return false
        },
        ([secretEmpty, isAdminRead]) => {
          if (secretEmpty) info (deviceId | sprintf1 ('no secret set for key %s, generating one'))
          else if (isAdminRead) {
            /* This means we are an admin site, the 'read key' button on the admin
             * page was pressed, and the secret exists.
             * We generate a success event and stop the flow by returning
             * `secretEmpty`, i.e. `false`.
             * `isNewKey` indicates whether it was just created during
             * `onResult` (this is used in generating the message on the
             * admin page).
             * In the case that the 'read key' button was pressed and the
             * secret does exist, the flow will continue through to
             * `onGenerateSecret`, and the `onSuccessAdminReadP` event (or
             * its failure counterpart) will fire there.
             */
            const isNewKey = isNewKeyTbl [deviceId] ?? false
            delete isNewKeyTbl [deviceId]
            startP ()
            | then (() => onSuccessAdminReadP (deviceId, isNewKey, false))
            | then (() => adminApi.call ('adminReset', []))
          }
          return secretEmpty
        },
      )
    },
    onGenerateSecret: (deviceId, secret, owInfo) => {
      if (!isAdmin)
        // --- dying (i.e. throwing an exception) is safe in these handlers
        die (`onGenerateSecret (): shouldn't happen because we're not an admin site`)
      const isNewKey = isNewKeyTbl [deviceId] ?? false
      delete isNewKeyTbl [deviceId]
      site.onGenerateSecret (
        deviceId, secret, isNewKey, owInfo,
        { dbSecretUpdate, dbKeyDeleteByDeviceId, }
      ) | fold (
        (err) => {
          warn (err | decorateRejection ('onGenerateSecret (): '))
          // --- an error occurred on our end (DB error)
          onFailureAdminReadP ()
        },
        (_) => {
          // @todo remove return
          return startP ()
          | then (() => onSuccessAdminReadP (deviceId, isNewKey, true))
          | then (() => adminApi.call ('adminReset', []))
        }
      )
    },
    shouldAuthenticate: (deviceId, owInfo) => {
      return site.shouldAuthenticate (
        deviceId, owInfo,
        { dbKeyExistsFail, dbKeyIsActiveFail, dbKeyIsAuthorizedFail, dbSecretGetFail },
      )
      | fold (
        // --- error, or key is not valid / authorized
        (err) => {
          warn (err | decorateRejection ('shouldAuthenticate (): '))
          failAuthenticate (deviceId)
          return false
        },
        id,
      )
    },
    onAuthenticate: (deviceId, granted, owInfo) => {
      site.onAuthenticate (deviceId, granted, owInfo, { dbUpdateActivityTimestamp, }) | foldWhenLeft (
        decorateRejection ('onAuthenticate (): ') >> warn,
      )
      granted | ifTrue (
        () => grantAuthenticate (deviceId),
        () => failAuthenticate (deviceId),
      )
    },
  }
})

export const makeSite = ({
  fireEvent, dbApi, lockApi, registerAdminSiteWithServer,
  siteLookupByName,
}) => invoke (() => {
  const { lockIOLock, lockIOUnlock, lockIOUnlockRelock, } = lockApi

  const onAdminBlock = () => {
    fireEvent (EventAdminBlocked)
    info ('[site] admin site blocked')
  }
  const onAdminUnblock = () => {
    fireEvent (EventAdminUnblocked)
    info ('[site] admin site unblocked')
  }

  lockGenericData.siteLookupByName = siteLookupByName

  /* Creates and initialises a `site` object.
   */
  const initSite = (siteSpec) => (siteName, _locations, mbLock) => {
    mbLock | foldWhenJust (initLock (lockApi))
    const isAdmin = mbLock | isNothing

    const {
      authenticate: onSuccessAuthenticateP,
      adminRead: onSuccessAdminReadP,
    } = getOnSuccess (fireEvent, lockIOUnlockRelock) (siteName, mbLock)
    const {
      authenticate: onFailureAuthenticateP,
      adminRead: onFailureAdminReadP,
    } = getOnFailure (fireEvent) (siteName)
    const onDoorHoldX = doorHoldOpen (fireEvent, lockIOUnlock, lockIOLock) (siteName, mbLock)
    const site = siteMod.create ({
      isAdmin, siteName,
      onDoorHoldOpen: () => onDoorHoldX (true),
      onDoorCancelHoldOpen: () => onDoorHoldX (false),
    }).init ()
    const adminApi = {
      valid: false,
      api: null,
      call (key, args) {
        if (!this.valid) die ('not an admin site')
        return this.api [key] (... args)
      },
    }
    // --- @future: we currently allow (and require) a single admin site,
    // though it may be useful to allow several at some point.
    if (isAdmin && !mut.foundAdmin) {
      mut.foundAdmin = true
      info (siteName | blue | sprintf1 ('using site %s as admin site'))
      const api = getAdminApi (onAdminBlock, onAdminUnblock) (site)
      adminApi.valid = true
      adminApi.api = api
      registerAdminSiteWithServer (
        siteSpec,
        () => api.adminPrepareRead (),
        () => api.adminPrepareCheck (),
        () => api.adminPrepareGenerateSecret (),
        () => api.adminReset (),
      )
    }
    // --- 'siteHandle' -- contains the site instance and useful data
    return {
      adminApi,
      site,
      handlers: getHandlers (dbApi, adminApi, fireEvent) (site, isAdmin) ({
        onSuccessAdminReadP, onFailureAdminReadP,
        onSuccessAuthenticateP, onFailureAuthenticateP,
      }),
    }
  }

  return (siteSpec) => siteSpec | cata ({
    SiteSpec: (siteName, locations, mbLock) => initSite (siteSpec) (siteName, locations, mbLock),
  })
})
