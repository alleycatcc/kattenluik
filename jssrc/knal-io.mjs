import {
  pipe, compose, composeRight,
  recurry, each, sprintfN,
  invoke, ifOk, id, map,
  bitwiseOr, bitwiseLeftBy,
  condS, eq, otherwise, guard,
} from 'stick-js/es'

import { init as initKnal, } from 'knal'

import { info, warn, } from './io.mjs'
import { lookupOnOrDie, } from './util.mjs'

const DEBUG_CMD_BYTES = false
const DEBUG_I2C = false

// ------ for cleanup
// --- main knal objects
const knals = []
// --- knalGpio / knalI2C objects
const knalPeripherals = []

const start = () => {
  const mock = ({ isPiWithGpio, }) => !isPiWithGpio ()
  const knal = initKnal ({ mock, mockNoisy: true, })
  knals.push (knal)
  if (knal.isMock ()) info (
    `this doesn't look like a Raspberry with GPIO pins -- mocking all calls to knal`,
  )
  const knalInitGpio = () => knal.initGpio ()
  const knalInitI2C = (sda, scl, baud) => knal.initI2C (sda, scl, baud)
  return { knalInitGpio, knalInitI2C, }
}

const { knalInitGpio, knalInitI2C, } = start ()

export const I2CInitTarget = (knalI2C, addr) => ({
  message: () => {
    const { go, wr, rd, stop, } = knalI2C.messageSyncRetry (addr, 10, { verbose: DEBUG_I2C, })
    const onFail = (... s) => warn ("I2C message (sync) failed after 10 retries, giving up: ", ... s)
    // --- go @throws
    return { go, wr, rd, stop, onFail, }
  },
})

// --- @throws
export const gpioInit = () => {
  const knal = knalInitGpio ()
  knalPeripherals.push (knal)
  return knal
}

// --- @throws
export const I2CInit = invoke (() => {
  // --- @todo warn on multiple init
  const inittedMap = new Map
  return (sda, scl, baud) => inittedMap.get (sda) | ifOk (
    id,
    () => {
      const knal = knalInitI2C (sda, scl, baud)
      knalPeripherals.push (knal)
      inittedMap.set (sda, knal)
      return knal
    },
  )
})

/**
 * b[7:6] = 0 -> led-grant
 *          1 -> led-deny
 *          2 -> relay, type given by b[5:4]
 * b[5:4] = 0 -> relay auto, direction given by b[0]
 *          1 -> relay external, controlled by b[0]
 * b[3:1] = n -> subtarget id (i.e. bus number (0-7))
 * b[0]   = 0 -> off (for leds); direction low for relay auto; close lock for relay external
 *          1 -> on (for leds); direction high for relay auto; open lock for relay external
 */
const getI2CCmdBytes = invoke (() => {
  const whiches = {
    'led-grant': [0],
    'led-deny': [1],
    'led-presence': [0, 1],
    relay: [2],
  }
  const relayTypes = {
    auto: 0,
    external: 1,
  }
  return (which, relayType=null, subtargetId, state) => {
    const theWhich = which | lookupOnOrDie ('invalid: ' + which, whiches)
    const theRelayType = relayType | ifOk (
      lookupOnOrDie ('invalid: ' + relayType, relayTypes),
      // --- ignored anyway
      () => 0,
    )
    return theWhich | map ((w) =>
      0 | bitwiseOr (
        w | bitwiseLeftBy (6),
      ) | bitwiseOr (
        theRelayType | bitwiseLeftBy (4),
      ) | bitwiseOr (
        subtargetId | bitwiseLeftBy (1),
      ) | bitwiseOr (
        Number (state),
      )
    )
  }
})

// --- @throws
export const knalDoI2C = (knalI2CTarget, subtargetId, which, ... params) => {
  const [relayType, state] = which | condS ([
    eq ('relay') | guard (() => params),
    otherwise | guard (() => [null, params [0]]),
  ])
  const cmdBytes = getI2CCmdBytes (which, relayType, subtargetId, state)
  if (DEBUG_CMD_BYTES) {
    info ('I2C cmd bytes:')
    cmdBytes | each ((b) => console.log ([b, b] | sprintfN ('%b (0x%02x)')))
  }
  const { go, wr, rd: _rd, stop, onFail, } = knalI2CTarget.message ()
  wr (cmdBytes)
  stop ()
  const [err, _] = go ()
  if (err) onFail ('write failed')
}

// --- @todo check throws
const setupOutputPin = (knalGpio, pinNum, state) => knalGpio.setModeOutput (pinNum, state)

// --- @todo check throws
const gpioSet = recurry (3) (
  (knalGpio) => (state) => (pinNum) => knalGpio.write (pinNum, state),
)

/* `initOutput` can be called multiple times. Useful for providing several different lists of pins.
*/

// @todo initInput

// :: KnalGpio -> (Bool, String) -> IO ()
export const gpioInitOutput = recurry (3) (
  (knalGpio) => (state) => (pinNum) => setupOutputPin (knalGpio, pinNum, state),
)

export const gpioInitOutputs = (knalGpio, state, pinNums) => pinNums | each (gpioInitOutput (knalGpio, state))

// gpioOn, gpioOff :: KnalGpio -> Num -> IO (), @throws
export const gpioOn = recurry (2) (
  (knalGpio) => (pin) => gpioSet (knalGpio, true, pin),
)
export const gpioOff = recurry (2) (
  (knalGpio) => (pin) => gpioSet (knalGpio, false, pin),
)

export const cleanup = () => {
  knalPeripherals | each ((knalPer) => knalPer.cleanup ())
  knalPeripherals.length = 0
  knals | each ((knal) => knal.cleanup ())
  knals.length = 0
}

export default {
  gpioInit,
  gpioInitOutput,
  gpioInitOutputs,
  gpioOn,
  gpioOff,
  I2CInit,
  knalDoI2C,
}
