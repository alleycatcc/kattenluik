/* The final assembled config which the modules import.
 */

import {
  pipe, compose, composeRight,
  map, prop, lets,
} from 'stick-js'

import { realpathSync as realpath, } from 'fs'
import { join as pathJoin, } from 'path'

import configure from 'alleycat-js/es/configure'

import { __dirname, listFlatMap, } from './util.mjs'

import { config as configLocal, } from './config-local.mjs'
import { config as configEnv, } from './config-environment.mjs'

const { allowReset=false, getDruppelOpts, getInstallation, getMockSpec, getOwConfig, i2cSpec, pollInterval, w1druppel, } = lets (
  () => configure.init (configLocal),
  (top) => top.gets (
    'allowReset', 'getDruppelOpts', 'getInstallation', 'getMockSpec', 'getOwConfig', 'i2cSpec', 'pollInterval', 'w1druppel',
  ),
)

const { checkScreenStatusCmd, } = lets (
  () => configure.init (configEnv),
  (top) => top.gets ('checkScreenStatusCmd'),
)

const rootDir = realpath (pathJoin (__dirname (import.meta.url), '..'))
const dbDir = pathJoin (rootDir, 'db')
const dbPath = pathJoin (dbDir, 'main.db')
const dbPathTmp = pathJoin (dbDir, 'tmp-import-main.db')

const getSiteSpecs = () => getInstallation ().sites | map (prop ('siteSpec'))

const getLeds = (ledApi) => lets (
  () => getInstallation (),
  (installation) => installation.sites | listFlatMap ((s) => s.leds (ledApi)),
  (installation) => installation.leds (ledApi),
  (_, siteLeds, otherLeds) => [... siteLeds, ... otherLeds],
)

export const config = {
  // --- local
  allowReset,
  getDruppelOpts,
  getMockSpec,
  getOwConfig,
  i2cSpec,
  pollInterval,
  w1druppel,

  // --- environment
  checkScreenStatusCmd,

  getSiteSpecs,
  getLeds,
  pollIntervalMock: 200,

  dbPath,
  dbPathTmp,

  // ------ @todo move most of these to internal/local/environment
  serverPort: 4444,
  i2cBlockTimeout: 5000,
  ledAccessTimeoutGranted: 6000,
  ledAccessTimeoutDenied: 800,
  // --- @todo can we remove this?
  keyColors: ['blue', 'red', 'yellow', 'black', 'green'],
  adminReadTimeout: 10000,
  // --- timeout slightly larger than pollInterval is nice.
  ledPresenceTimeout: 60,
}
