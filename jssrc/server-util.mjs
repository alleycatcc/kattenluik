import {
  pipe, compose, composeRight,
  prop,
} from 'stick-js/es'

import { lookupOn, } from './util.mjs'

/*
 * imsg: can always be empty: optional message for front-end developer.
 * 400 + imsg: non-user error (e.g. front-end sends wrong data)
 * 422 + imsg + umsg: user error (e.g. something fails validation)
 * 5xx + imsg: non-user error.
 */

export const umsg = (umsg) => ({ umsg, })
export const imsg = (imsg) => ({ imsg, })
export const iumsg = (msg) => ({ imsg: msg, umsg: msg, })
export const data = (data) => ({ data, })

export const getReqParams = prop ('params') >> lookupOn

