import {
  pipe, compose, composeRight,
  id,
  factory, factoryProps,
} from 'stick-js/es'

import { fold, } from 'alleycat-js/es/bilby'
import { decorateRejection, setIntervalOn, } from 'alleycat-js/es/general'
import { errorX, warnX, } from './io.mjs'
import { foldWhenLeft, } from './util.mjs'

// --- persist to DB after this many ticks (i.e. seconds)
const PERSIST = 10

let persistCounter = 0

const proto = {
  init (dbApi) {
    this._dbApi = dbApi
    this._cur = this._restore ()
    return this
  },
  // --- dies on db error (assumed to be called during init)
  _restore () {
    const { _dbApi, } = this
    return _dbApi.dbGetUptime () | fold (
      decorateRejection ('_restore (): failed to get uptime: ') >> errorX,
      id,
    )
  },
  _persist () {
    const { _dbApi, _cur, } = this
    _dbApi.dbSetUptime (_cur) | foldWhenLeft (
      decorateRejection ('_persist (): failed to set uptime: ') >> warnX,
    )
  },
  start () {
    this._tick ()
    this._job = 1000 | setIntervalOn (() => this._wrapTick ())
    return this
  },
  stop () {
    clearInterval (this._job)
    return this
  },
  _wrapTick () {
    const { _pause, } = this
    if (_pause) return
    this._tick ()
    persistCounter += 1
    if (persistCounter === PERSIST) {
      this._persist ()
      persistCounter = 0
    }
  },
  _tick () {
    this._cur += 1
  },
  now () {
    return this._cur
  },
  pause () {
    this._pause = true
  },
  unpause () {
    this._pause = false
  },
}

const props = {
  _dbApi: void 8,
  _cur: void 8,
  _job: void 8,
  _pause: false,
}

export default proto | factory | factoryProps (props)
