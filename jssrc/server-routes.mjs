import {
  pipe, compose, composeRight,
  deconstruct, sprintf1, concatTo, sprintfN, noop,
  ifOk, invoke, join, ok, ifHas,
  tryCatch, isString, defaultTo, lets, id,
  recurry, ifTrue, map, die,
} from 'stick-js/es'

import fs from 'fs'
import { mkdtemp, rm, writeFile, } from 'fs/promises'
import os from 'os'

import multer from 'multer'

import { then, recover, startP, } from 'alleycat-js/es/async'
import { Left, Right, fold, } from 'alleycat-js/es/bilby'
import configure from 'alleycat-js/es/configure'
import { del, get, patch, post3, put, sendDownload, sendStatus, sendStatusEmpty, } from 'alleycat-js/es/express'
import { composeManyRight, decorateRejection, iwarn, } from 'alleycat-js/es/general'
import { isEmptyObject, } from 'alleycat-js/es/predicate'

import { config, } from './config.mjs'
import { validateDb, } from './db-util.mjs'
import { brightRed, info, warn, warnX, } from './io.mjs'
import { restart, } from './main.mjs'
import { imsg, } from './server-util.mjs'
import { bifirst, bimapEither, compactDefined, delayP, doEither, ifZero, inspect, lookupOnOr, toListSingleton, } from './util.mjs'

const configTop = config | configure.init
const allowReset = configTop.get ('allowReset')
const dbPath = configTop.get ('dbPath')
const dbPathTmp = configTop.get ('dbPathTmp')

// --- @todo dest
const upload = multer ({ dest: '/tmp/upload/', })

// --- expects a string like '1703501609726' and returns a Date
const validateTs = invoke (() => {
  const inner = (tss) => tryCatch (
    (d) => d,
    () => null,
    () => {
      if (!isString (tss)) return null
      const ts = Number (tss)
      if (isNaN (ts) || ts === Infinity) return null
      const d = new Date (ts)
      if (isNaN (d.valueOf ())) return null
      return d
    }
  )
  return (tss) => inner (tss) | defaultTo (
    () => warn ('invalid ts in query:', String (tss))
  )
})

const tsString = (d) => lets (
  () => [d.getFullYear (), 1 + d.getMonth (), d.getDate (), d.getHours (), d.getMinutes (), d.getSeconds (), d.getMilliseconds ()],
  (fmt) => fmt | sprintfN ('%04d-%02d-%02d-%02d-%02d-%02d.%d'),
)

/*
 * patch: edit one or more fields of an existing item.
 */

const foldResults = (dbFuncName) => fold (
  ([code, err]) => {
    warn ([
      code | String | brightRed,
      err | inspect | concatTo ('Error with ' + dbFuncName + ': '),
    ] | sprintfN (`[%s] %s`))
    return sendStatus (code, 'database error (see logs)' | imsg)
  },
  ([code, body]) => body | ifOk (
    () => sendStatus (code, body),
    () => sendStatusEmpty (code),
  )
)

// --- for calls which return an object with a `changes` parameter; don't use this for DELETE as the
// return is `null` on success.
const doCallCheckChanges = (dbFuncName, dbFunc, vals, error400Msg) => foldResults (dbFuncName) (
  doEither (
    () => dbFunc (...vals) | bifirst (
      (msg) => [500, msg],
    ),
    ({ changes, }) => changes | ifZero (
      // --- client non-user error: the front-end shouldn't allow this.
      () => Left ([400, imsg (error400Msg)]),
      () => Right ([200, {}]),
    ),
  )
)

// --- use this for DELETE, which doesn't return results or changes.
const doCall = (dbFuncName, dbFunc, vals) => foldResults (dbFuncName) (
  dbFunc (...vals) | bimapEither (
    (msg) => [500, msg],
    ( _ ) => [200, {}],
  ),
)

const doCallResults = (dbFuncName, dbFunc, vals) => foldResults (dbFuncName) (
  dbFunc (...vals) | bimapEither (
    (msg) => [500, msg],
    (results) => [200, { data: results, }],
  ),
)

const routeWhen = recurry (2) (
  (p) => (route) => p () ? [route] : []
)
const getAdminReadOrCheck = (
  isAdminSite, onWaitResponse,
  adminSiteReset,
  adminSitePrepareRead, adminSitePrepareCheck,
  adminSitePrepareGenerateSecret,
) => (which) => (_req, res) => {
  if (!isAdminSite ()) return res | sendStatus (
    500, 'no admin site' | imsg,
  )

  const mkResponseAuthenticateFail = () => ({
    data: {
      canUnlockScreen: false, readOk: true, authenticated: false,
    },
  })
  const mkResponseAuthenticateSuccess = (canUnlockScreen) => () => ({
    data: {
      canUnlockScreen, readOk: true, authenticated: true,
    },
  })
  const mkResponseReadFail = () => ({ data: { readOk: false, }})
  const mkResponseReadSuccess = ({ deviceId, isNewKey, isNewSecret, }) => ({
    data: {
      deviceId, isNewKey, isNewSecret,
      readOk: true,
    },
  })
  const cleanup = () => adminSiteReset ()

  const prepareResults = invoke (which | lookupOnOr (
    () => {
      iwarn ('Invalid:', which)
      return noop
    },
    {
      read: adminSitePrepareRead,
      check: adminSitePrepareCheck,
      'regenerate-secret': adminSitePrepareGenerateSecret,
    },
  ))

  return prepareResults | fold (
    (msg) => {
      warnX (decorateRejection ('Error on prepare: ' + which + ': ', msg))
      return res | sendStatus (500, 'db or other error' | imsg)
    },
    () => res | onWaitResponse (
      cleanup,
      mkResponseAuthenticateFail, mkResponseAuthenticateSuccess,
      mkResponseReadFail, mkResponseReadSuccess,
    ),
  )
}

// --- `arg` can be an array or a single value
const _toRoute = (g) => ([method, arg, f]) => lets (
  () => arg | toListSingleton,
  (args) => method (... args, g (f)),
)
const toRoute = _toRoute (id)

export const routes = ({
  dbApi, isAdminSite,
  adminSiteReset,
  adminSitePrepareRead, adminSitePrepareCheck, adminSitePrepareGenerateSecret,
  isAPILocked,
  onWaitResponse,
}) => dbApi | deconstruct (({
  dbAssigneeAdd,
  dbAssigneeDelete,
  dbAssigneeUpdate,
  dbAssigneesGetWithKeyInfo,
  dbBackup,
  dbColorsGet,
  dbDoorsGet,
  dbGetStaleKeys,
  dbGetUptimeApproximateByMonth,
  dbKeyAuthorizationsGet,
  dbKeyAuthorizationAdd,
  dbKeyAuthorizationDelete,
  dbKeyDeleteByDeviceId,
  dbKeyExists: _,
  dbKeyPrivilegesGet,
  dbKeyPrivilegeAdd,
  dbKeyPrivilegeDelete,
  dbKeyUpdate,
  dbKeysGet,
  dbPrivilegesGet,
  dbReset,
  dbSecretDelete,
  dbStatusesGet,
}) => {
  const adminReadOrCheck = getAdminReadOrCheck (
    isAdminSite, onWaitResponse,
    adminSiteReset,
    adminSitePrepareRead, adminSitePrepareCheck, adminSitePrepareGenerateSecret,
  )
  const disableWhenLocked = (f) => (req, res) => isAPILocked () | ifTrue (
    () => res | sendStatusEmpty (404),
    () => f (req, res),
  )
  const emptyListWhenLocked = (f) => (req, res) => isAPILocked () | ifTrue (
    () => res | sendStatus (200) ({ data: [], }),
    () => f (req, res),
  )
  const toRouteDisableWhenLocked = _toRoute (disableWhenLocked)
  const toRouteEmptyListWhenLocked = _toRoute (emptyListWhenLocked)
  return composeManyRight (
    // --- most routes should go here: these return 404 if the interface is locked.
    // --- @todo this means that if the touch screen interface is locked, a user with a laptop also
    // can't get in, would be good to separate the two.
    ... map (toRouteDisableWhenLocked) ([
      [get, '/admin/read', (req, res) => {
        return adminReadOrCheck ('read') (req, res)
      }],
      [get, '/admin/regenerate-secret', (req, res) => {
        return adminReadOrCheck ('regenerate-secret') (req, res)
      }],
      ... routeWhen (() => allowReset, [del, '/all', (_req, res) => {
        // --- @experimental: if the spinner appears only really quickly it's a bit confusing, so
        // force it to appear for at least a second.
        return delayP (1000)
          | then (() => res | doCall ('dbReset', dbReset, []))
      }]),
      [put, '/assignee', (req, res) => {
        const { body, } = req
        const { data, } = body
        const { name, custom, } = data
        return res | doCall ('dbAssigneeAdd', dbAssigneeAdd, [name, custom])
      }],
      [patch, '/assignee', (req, res) => {
        const {body, } = req
        const {data, } = body
        const {id, name, custom} = data
        const vals = compactDefined ({ name, custom, })
        if (vals | isEmptyObject) {
          return res | sendStatus (400) (
            'no field(s) given to update' | imsg,
          )
        }
        return res | doCallCheckChanges ('dbAssigneeUpdate', dbAssigneeUpdate,
          [id, vals],
          name | sprintf1 ('No row matched (bad assignee %s)'),
        )
      }],
      [del, '/assignee/', (req, res) => {
        const { body, } = req
        const { data, } = body
        // --- allowAssignedKeys: false means don't allow removing an assignee
        // who still has assigned keys; true means unassign the keys first and
        // then delete.
        const { id, allowAssignedKeys, } = data
        return res | doCallCheckChanges (
          'dbAssigneeDelete', dbAssigneeDelete, [id, allowAssignedKeys],
          id | sprintf1 ('No row matched (bad assignee id %s)'),
        )
      }],
      [del, '/key-authorizations', (req, res) => {
        const { body, } = req
        const { data, } = body
        const { ids, } = data
        return res | doCallCheckChanges (
          'dbKeyAuthorizationDelete', dbKeyAuthorizationDelete, [ids],
          ids | join (', ') | sprintf1 ('One or more rows not matched (tried to remove keyAuthorization ids %s)'),
        )
      }],
      [put, '/key-authorizations', (req, res) => {
        const { body, } = req
        const { data, } = body
        return res | doCall ('dbKeyAuthorizationAdd', dbKeyAuthorizationAdd, [data.data])
      }],
      [del, '/key-privileges', (req, res) => {
        const { body, } = req
        const { data, } = body
        const { ids, } = data
        return res | doCallCheckChanges (
          'dbKeyPrivilegeDelete', dbKeyPrivilegeDelete, [ids],
          ids | join (', ') | sprintf1 ('One or more rows not matched (tried to remove keyPrivilege ids %s)'),
        )
      }],
      [put, '/key-privileges', (req, res) => {
        const { body, } = req
        const { data, } = body
        return res | doCall ('dbKeyPrivilegeAdd', dbKeyPrivilegeAdd, [data.data])
      }],
      [del, '/key/:deviceId', (req, res) => {
        const { params, } = req
        const { deviceId, } = params
        return res | doCall (
          'dbKeyDeleteByDeviceId', dbKeyDeleteByDeviceId, [deviceId],
          deviceId | sprintf1 ('No row matched (bad key %s)'),
        )
      }],
      // --- we don't allow a secret update here.
      [patch, '/key/:deviceId', (req, res) => {
        const { params, body, } = req
        const { deviceId, } = params
        const { data: { name, colorId, assigneeId, status, custom, updateActivityTimestamp: updateActivityTimestampParam=false, }} = body
        const updateActivityTimestamp = ok (status) && updateActivityTimestampParam
        const vals = compactDefined ({ name, colorId, assigneeId, status, custom, })
        if (vals | isEmptyObject) {
          return res | sendStatus (400) (
            'no field(s) given to update' | imsg,
          )
        }
        return res | doCallCheckChanges (
          'dbKeyUpdate', dbKeyUpdate, [deviceId, vals, updateActivityTimestamp],
          deviceId | sprintf1 ('No row matched (bad key %s)'),
        )
      }],
      [del, '/key/secret/:deviceId', (req, res) => {
        const { params, } = req
        const { deviceId, } = params
        return res | doCall (
          'dbSecretDelete', dbSecretDelete, [deviceId],
          deviceId | sprintf1 ('No row matched (bad key %s)'),
        )
      }],
      // --- argument to `upload.single` is the `name` attribute of the file input
      [post3, ['/db-import', upload.single ('db-import-file')], (req, res) => {
        const { file, } = req
        const { path, } = file
        startP ()
        | then (() => writeFile (dbPathTmp, fs.createReadStream (path)))
        | then (() => validateDb (dbPathTmp))
        | then (() => writeFile (dbPath, fs.createReadStream (dbPathTmp)))
        | recover (die << decorateRejection ('Validate or I/O error: ' + dbPath + ': '))
        | then (() => {
          info ('db imported successfully, restarting')
          return restart ()
        })
        | recover (die << decorateRejection ('Error restarting: '))
        | then (() => res | sendStatus (200, null))
        | recover ((err) => {
          warnX (err | decorateRejection ('/db-import: '))
          // --- @todo restore backup
          res | sendStatus (499, {
            umsg: 'The imported database was invalid',
          })},
        )
      }],
      [get, '/db-export', (req, res) => {
        const timestamp = [req.query, 'ts'] | ifHas (
          (ts) => validateTs (ts),
          () => warn ('no ts param in query'),
        ) | ifOk (
          (d) => '-' + tsString (d),
          () => '',
        )
        const as = 'kattenluik-main' + timestamp + '.db'
        let tmpdir, backupDbPath
        const onErr = (err) => {
          warnX (err)
          res.end ()
        }
        return mkdtemp (os.tmpdir () + '/')
        | then ((dir) => {
          tmpdir = dir
          backupDbPath = dir + '/' + 'backup.db'
          return dbBackup (backupDbPath)
        })
        | then (() => {
          res | sendDownload (backupDbPath, as, (err) => {
            err | ifOk (
              () => onErr ([backupDbPath, as] | sprintfN ('Error sending attachment %s as %s:', inspect (err))),
              () => {
                info ([backupDbPath, as] | sprintfN ('file %s downloaded successfully as %s'))
                // --- failure to remove the file is bad because it could (eventually) fill up the
                // disk, but we still send a successful response.
                return rm (tmpdir, { recursive: true, })
                | recover ((err) => {
                  warnX (err | decorateRejection ('Unable to remove dir ' + tmpdir))
                })
              },
            )
          })
        })
        | recover ((err) => {
          onErr (err)
          res.end ()
        })
      }],
    ]),
    // --- this is for routes which should return an empty list if the interface is locked, as an
    // extra security step.
    ... map (toRouteEmptyListWhenLocked) ([
      [get, '/assignees', (_req, res) => {
        return res | doCallResults ('dbAssigneesGetWithKeyInfo', dbAssigneesGetWithKeyInfo, [])
      }],
      [get, '/colors', (_req, res) => {
        return res | doCallResults ('dbColorsGet', dbColorsGet, [])
      }],
      [get, '/doors', (_req, res) => {
        return res | doCallResults ('dbDoorsGet', dbDoorsGet, [])
      }],
      [get, '/key-authorizations', (_req, res) => {
        return res | doCallResults ('dbKeyAuthorizationsGet', dbKeyAuthorizationsGet, [])
      }],
      [get, '/key-privileges', (req, res) => {
        const { query, } = req
        const { strategy='full', } = query
        return res | doCallResults ('dbKeyPrivilegesGet', dbKeyPrivilegesGet, [strategy])
      }],
      [get, '/keys', (_req, res) => {
        return res | doCallResults ('dbKeysGet', dbKeysGet, [])
      }],
      [get, '/privileges', (_req, res) => {
        return res | doCallResults ('dbPrivilegesGet', dbPrivilegesGet, [])
      }],
      [get, '/stale-keys', (_req, res) => {
        return res | doCallResults ('dbGetStaleKeys', dbGetStaleKeys, [])
      }],
      [get, '/statuses', (_req, res) => {
        return res | doCallResults ('dbStatusesGet', dbStatusesGet, [])
      }],
      [get, '/uptime-approximate-by-month', (_req, res) => {
        return res | doCallResults ('dbGetUptimeApproximateByMonth', dbGetUptimeApproximateByMonth, [])
      }],
    ]),
    // --- normal routes (also available while locked)
    ... map (toRoute) ([
      // --- @todo poorly named; also used for determining whether we can unlock
      [get, '/should-lock-interface', (_req, res) => {
        // --- returns `true`, `false`, or `null`, where `null` means do not lock because there is
        // no key which can unlock.
        isAPILocked ()
        | then ((shouldLock) => res | sendStatus (
          200, { data: { shouldLock, }},
        ))
        | recover ((e) => {
          warn ('/should-lock-interface:', e)
          return res | sendStatusEmpty (500)
        })
      }],
      [get, '/admin/check', (req, res) => {
        return adminReadOrCheck ('check') (req, res)
      }],
    ]),
  )
})
