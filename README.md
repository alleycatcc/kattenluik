# kattenluik

NodeJS event-driven interface for a One-Wire access control system.

Requires the C bindings at https://gitlab.com/alleycatcc/w1druppel.
