#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  id, map, each,
  head, tail, tap,
  ifPredicate,
} from 'stick-js'

die ('Script needs updating')

// @todo update
import { eendraadConfig, } from '../../../jssrc/types.mjs'
import { inits as gpioInits, on as gpioOn, off as gpioOff } from '../../../jslib/gpio-io.mjs'

import onoff from 'onoff'
const { Gpio, } = onoff

import fishLib from 'fish-lib'

import { flatMap, } from 'alleycat-js/es/bilby'
import { length, logWith, setTimeoutOn, } from 'alleycat-js/es/general'

const { info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue, bulletSet, } = fishLib

bulletSet ({ type: 'star', })

const flatten = flatMap (id)

const c = [
  // --- (real) bus 0
  [26, 25, 24],
  // --- (real) bus 1
  [23, 22, 21],
  // --- ...
  [20, 19, 18],
  [17, 16, 13],
  [12, 11, 0],
  [9, 8, 7],
  [5, 4, 1],
]

// expects a non-negative integer and returns a random int in the range [0:n - 1]
// (Math.random gives a random float between 0 and 1)
const randInt = (n) => Math.floor (Math.random () * n)

// const relays = flatten (c | map (head))
const relays = c | map (head)
const leds = flatten (c | map (tail))

// ifFlip is basically a random 'if'
const flip = (_x) => randInt (2) | Boolean
const ifFlip = flip | ifPredicate

const init = () => {
  gpioInits (0, relays)
  gpioInits (0, leds)
}

const allRelaysOn = () => relays | each (gpioOn)
const allRelaysOff = () => relays | each (gpioOff)

const tickRelays = () => {
  info ('tickRelays')
  allRelaysOn ()
  300 | setTimeoutOn (allRelaysOff)
  6000 | setTimeoutOn (tickRelays)
}

const tickLeds = () => {
  info ('tickLeds')
  leds | each (ifFlip (
    gpioOn,
    gpioOff,
  ))
  500 | setTimeoutOn (tickLeds)
}

init ()
tickRelays ()
tickLeds ()
