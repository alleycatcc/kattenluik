#!/usr/bin/env node
import { pipe, compose, composeRight, id, map, each, head, tail, tap, ifPredicate, sprintf1, sprintfN } from 'stick-js';
import { eendraadConfig } from '../../../jssrc/types.mjs';
import { inits as gpioInits, on as gpioOn, off as gpioOff } from '../../../jslib/gpio-io.mjs';
import onoff from 'onoff';
const {
  Gpio
} = onoff;
import fishLib from 'fish-lib';
import { flatMap } from 'alleycat-js/es/bilby';
import { length, logWith, setTimeoutOn } from 'alleycat-js/es/general';
const {
  info,
  warn,
  error,
  green,
  yellow,
  magenta,
  brightRed,
  cyan,
  brightBlue,
  bulletSet
} = fishLib;
bulletSet({
  type: 'star'
});
const flatten = flatMap(id);
const c = [// --- (real) bus 0
[26, 25, 24], // --- (real) bus 1
[23, 22, 21], // --- ...
[20, 19, 18], [17, 16, 13], [12, 11, 0], [9, 8, 7], [5, 4, 1]]; // a random int between 0 and n - 1 (Math.random gives a random float
// between 0 and 1)

const randInt = n => Math.floor(Math.random() * n); // const relays = flatten (c | map (head))


const relays = pipe(c, map(head));
const leds = flatten(pipe(c, map(tail)));

const flip = _x => pipe(randInt(2), Boolean);

const ifFlip = pipe(flip, ifPredicate);

const init = () => {
  gpioInits(0, relays);
  gpioInits(0, leds);
};

const allRelaysOn = () => pipe(relays, each(gpioOn));

const allRelaysOff = () => pipe(relays, each(gpioOff));

const tickRelays = () => {
  info('tickRelays');
  allRelaysOn();
  pipe(300, setTimeoutOn(allRelaysOff));
  pipe(6000, setTimeoutOn(tickRelays));
};

const tickLeds = () => {
  info('tickLeds');
  pipe(leds, each(ifFlip(gpioOn, gpioOff)));
  pipe(500, setTimeoutOn(tickLeds));
};

const x = Math.floor(Math.random() * 20); // info (x | sprintf1 ("int: %s"))

init(); // tickRelays ()

tickLeds();