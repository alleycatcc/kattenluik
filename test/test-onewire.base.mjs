import { createRequire, } from 'module'
const require = createRequire (import.meta.url)
const w1direct = require ('w1direct')
const w1 = new w1direct.Manager ()

export default (config) => {
  w1.registerDS2482Master ({
    name: 'some-name',
    devFile: '/dev/i2c-1',
    ... config,
  })

  const sync = () => w1.syncAllDevices ()
  const ping = () => console.log (sync ())

  setInterval (ping, 1000)
}
