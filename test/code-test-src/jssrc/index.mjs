#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  id, map, each, join,
  head, tail, tap,
  ifPredicate, lets, repeatF,
  defaultToV, bindProp, invoke, list,
} from 'stick-js'

import { init, on, off, } from '../../../jslib/gpio-io.mjs'

import fishLib from 'fish-lib'

import { allP, then, recover, } from 'alleycat-js/es/async'
import { setTimeoutOn, } from 'alleycat-js/es/general'


const PIN = 20
const TICK = 10
const WINDOW = 80

const { log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue, bulletSet, } = fishLib

bulletSet ({ type: 'star', })

// creates a list [0,1,0,...] of length l
const mkCode = (l) => {
  let x = false
  const code = [0]
  l | repeatF (() => (
    x = !x,
    code.push (Number (x))))
  return code
}

// lets ensures that write is only computed once.
const say = lets (
  () => process.stdout | bindProp ('write'),
  (write) => (...args) => args | map (String) | join (' ') | write
)

const codeLength = process.argv [2] | defaultToV (6)
const sets = [() => off (PIN), () => on (PIN)]
const set = (x) => sets [x] ()
const code = mkCode (codeLength)

// invoke ensures that iter is only computed once
const tick = invoke (() => {
  // destructuring of a list: [c, ...code] (or [x, ...xs])
  const iter = ([c, ...code], done) => {
    say (c, '')
    set (c)
    if (code.length === 0) return done ()
    TICK | setTimeoutOn (() => iter (code, done))
  }
  // to keep track of the time the code of iter takes to run
  return code => {
    const d = new Date
    return new Promise ((res, _) => iter (code, res))
    | then (() => new Date - d)
  }
})

const doWindow = () => new Promise ((res, _) => WINDOW | setTimeoutOn (() => res (say ('|'))))

// we log a 1 with say because init sets the PIN to 1.
init (1, PIN)
say (1, '')

allP ([
  doWindow (),
  tick (code),
])
| then (([_, t]) => {
  log ('')
  info ('Code time:', t, 'ms, lower bound was:', String ((code.length - 1) * TICK))
})
| recover ((e) => error (e))
