/* Configuration which is supposed to encapsulate details about the OS and maybe the hardware.
*/

export const config = {
  // --- note: [cmd, [args]], not a single array (this is how `child_process` wants it)
  // --- expected to print '1' or '0', with optional newline, or exit with error.
  checkScreenStatusCmd: ['bash', ['bin/check-screen-status']],
}
