import { realpathSync as realpath, } from 'fs'
import { join as pathJoin, } from 'path'

import { blue, } from './io.mjs'
import {
  Master, MasterSubType800, AddressRaw, AddressBits,
  mkAdmin, mkConfigEendraad, mkAdminReadyLed,
} from './types.mjs'

import { __dirname, } from './util.mjs'

import { Just, } from 'alleycat-js/es/bilby'

const rootDir = realpath (pathJoin (__dirname (import.meta.url), '..'))
const dbDir = pathJoin (rootDir, 'db')
const dbPath = pathJoin (dbDir, 'main.db')

// --- the name is only used for info messages.
const master = Master ('master-main', AddressBits ('000'), MasterSubType800)

const hw = [3, 10, 5000, 80]
const doors = {
  // --- real bus 0 (printed bus 2)
  door1: mkConfigEendraad ('door1', master, 0, ['external-trigger-high']),
  // --- real bus 1 (printed bus 1)
  door2: mkConfigEendraad ('door2', master, 1, ['keep-open', 0]),
  // --- real bus 2 (printed bus 0)
  door3: mkConfigEendraad ('door3', master, 2, ['auto-trigger-low', ... hw]),
  // --- real bus 3 (printed bus 4)
  door4: mkConfigEendraad ('door4', master, 3, ['auto-trigger-low', ... hw]),
  // --- real bus 4 (printed bus 3)
  door5: mkConfigEendraad ('door5', master, 4, ['auto-trigger-low', ... hw]),
  // --- real bus 5 (printed bus 5)
  door6: mkConfigEendraad ('door6', master, 5, ['auto-trigger-low', ... hw]),
  // --- real bus 6 (printed bus 6)
  door7: mkConfigEendraad ('door7', master, 6, ['auto-trigger-low', ... hw]),
}

// --- order must match bus order.
const sites = [
  doors.door1.site,
  doors.door2.site,
  doors.door3.site,
  doors.door4.site,
  doors.door5.site,
  doors.door6.site,
  doors.door7.site,
  mkAdmin ('admin0', master, 7),
]

// --- order doesn't matter.
const configLeds = (ledApi) => [
  ... doors.door1.leds (ledApi),
  ... doors.door2.leds (ledApi),
  ... doors.door3.leds (ledApi),
  ... doors.door4.leds (ledApi),
  ... doors.door5.leds (ledApi),
  ... doors.door6.leds (ledApi),
  ... doors.door7.leds (ledApi),

  mkAdminReadyLed (ledApi) ([[15, Just (blue)]]),
]

export const config = {
  dbPath,
  masters: [master],
  sites,
  i2cDev: '/dev/i2c-1',
  leds: configLeds,
  serverPort: 4444,
  pollInterval: 50,
  // --- helps improve info messages.
  // @todo
  usingHardwareLockTimer: true,
  onewireOpts: {
    search: {
      overdrive: false,
      activePullup: false,
    },
    searchAndAuthenticate: {
      overdrive: false,
      activePullup: false,
    },
    generateSecret: {
      overdrive: false,
      activePullup: false,
    },
  },
  ledAccessTimeoutGranted: 6000,
  ledAccessTimeoutDenied: 800,
  keyColors: ['blue', 'red', 'yellow', 'black', 'green'],
  adminReadTimeout: 10000,
  // --- timeout slightly larger than pollInterval is nice.
  ledPresenceTimeout: 60,
  // --- use these to mock read errors with a given probability; keep on
  // `null` unless testing.
  mockIOErrorSearchOnly: null,
  mockIOErrorSearchAndAuthenticate: null,
}
