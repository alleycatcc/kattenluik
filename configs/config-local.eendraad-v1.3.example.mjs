/* Configuration which is specific to an installation.
 * Contains user-specific information and should not be checked into VCS.
 */

import { bitAddress_DS2482, } from './config-common.mjs'
import { i2cSpecV1_3, mkW1DruppelV1_3, mkInstallationV1_3, } from './config-internal.mjs'

/* lock-spec is
 *   ['auto-trigger-low' | 'auto-trigger-high', ...hw-params] |
 *   ['external-trigger-low' | 'external-trigger-high'] |
 *   ['keep-open', target-bus-number]
 *
 *   where hw-params is
 *     [wave-length, pulse-duration, relock-timeout, window-duration]
 *   (see explanation in types.mjs above `data LockType`)
 */
const hwLockSpec = [3, 10, 5000, 80]

const w1druppel = mkW1DruppelV1_3 (
  ({ ds2482_800, }, { ds1961, ds1964, }) => ({
    verbose: true,
    authenticators: [ds1961, ds1964],
    controllers: [
      ds2482_800 ({
        tag: 'main',
        address: bitAddress_DS2482 (0b000),
      }),
    ],
  }),
)

const getInstallation = () => mkInstallationV1_3 ({
  // --- order of doors does not matter
  doors: [
    // --- relock timeout (here 5000) refers to when the door is opened
    // specifically, i.e. not as a result of keep-open, in which case it
    // just stays open.
    ['door0', [[0, 0]], 0, ['external-trigger-high', 5000]],
    ['door1', [[0, 1]], 1, ['keep-open', 'door0']],
    ['door2', [[0, 2]], 2, ['auto-trigger-low', ... hwLockSpec]],
    ['door3', [[0, 3]], 3, ['auto-trigger-low', ... hwLockSpec]],
    ['door4', [[0, 4]], 4, ['auto-trigger-low', ... hwLockSpec]],
    ['door5', [[0, 5]], 5, ['auto-trigger-low', ... hwLockSpec]],
    ['door6', [[0, 6]], 6, ['auto-trigger-low', ... hwLockSpec]],
  ],
  // --- optional: for testing / unusual situations
  doorsGpio: [
    ['Bewoners achter GPIO', [[0, 6]], 10, ['auto-trigger-low', ... hwLockSpec]],
  ],
  // --- we currently allow (and require) exactly 1 admin site
  admins: [
    ['admin', [[0, 7]], 7],
  ],
  leds: (_ledApi, standardLeds) => [
    ... standardLeds,
  ],
})

const i2cSpec = i2cSpecV1_3

const getOwConfig = () => ({
  overdrive: 'auto',
  activePullup: 'auto-old',
  networkType: 'single-drop',
})

// --- examples: 'test-probe-authenticate-succeeds', 'random-fastest', ...
// (see w1druppel)
const MOCK_SPEC = 'random-normal'

export const config = {
  allowReset: true,
  getInstallation,
  getOwConfig,
  i2cSpec,
  pollInterval: 50,
  w1druppel,

  getMockSpec: (mock=false) => mock && MOCK_SPEC,
  getDruppelOpts: (mock=false) => ({ mock: mock ? MOCK_SPEC : false, }), }
