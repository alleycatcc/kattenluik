import babelParser from '@babel/eslint-parser'
import importPlugin from 'eslint-plugin-import'
import js from '@eslint/js'

const globalsNode = {
  __dirname: false,
  __filename: false,
  Buffer: false,
  clearImmediate: false,
  clearInterval: false,
  clearTimeout: false,
  console: false,
  exports: true,
  global: false,
  Intl: false,
  module: false,
  process: false,
  queueMicrotask: false,
  require: false,
  setImmediate: false,
  setInterval: false,
  setTimeout: false,
  TextDecoder: false,
  TextEncoder: false,
  URL: false,
  URLSearchParams: false
}

export default [
  js.configs.recommended,
  importPlugin.flatConfigs.recommended,
  {
  languageOptions: {
    globals: {
      ...globalsNode,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
    parser: babelParser,
    parserOptions: {
      'ecma-version': 2017,
      'ecma-features': {
        'implied-strict': true,
        // 'experimental-object-rest-spread': true,
        jsx: true,
        modules: true,
      },
      'source-type': 'module',
      'root-mode': 'upward',
    },
  },
  settings: {
    react: {
      version: '16',
    },
    'import/extensions': ['.mjs', '.js'],
    'import/resolver': {
      node: {
        extensions: ['.mjs', '.js'],
      },
    },
  },
  rules: {
    'no-unused-vars': ['warn', {
      varsIgnorePattern: 'pipe|compose|composeRight|logWith|tap|map|id|noop|ok|nil|ifOk|ifNil|_.*',
      argsIgnorePattern: '_.*',
    }],
    'no-unexpected-multiline': 'off',
    'no-empty-pattern': 'off',
    'no-constant-condition': 'off',
    'linebreak-style': ['warn', 'unix'],
    'no-console': 'off',
    'no-empty': 'off',
    'no-extra-semi': 'off',
    'require-yield': 'off',
    'import/no-named-as-default-member': 'off',
  },
}, {
  files: ['**/*.mjs', '**/*.js'],
}]
