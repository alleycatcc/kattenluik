#!/usr/bin/env node

import {
  pipe, compose, composeRight,
  eq, ifPredicate, invoke, sprintf1, each,
} from 'stick-js'

import { init as initKnal, } from 'knal'

import { decorateRejection, setTimeoutOn, setIntervalOn, } from 'alleycat-js/es/general'
// --- @todo make this script part of kattenluik
import { info, log, warn, spawnSync, yellow, } from '../../../jslib/io.mjs'

const CMDS_SHUTDOWN = [
  // --- not really necessary
  ['sudo', ['sync']],
  ['sudo', ['/sbin/shutdown', 'now']],
]

const config = {
  dryRun: true,
  pinBtn: 18,
  pinLed: 4,
  buttonHoldMs: 2000,
  blinkBeforeShutdownMs: 2000,
  debounceMs: 10,
}

const cmds_shutdown = config.dryRun ? [['echo', ['[mock] shutting down']]] : CMDS_SHUTDOWN

let eventsDisabled = false

const [buttonDisabled, disableButton] = invoke (() => {
  let disabled = false
  return [() => disabled, () => disabled = true]
})

const debounce = (() => {
  let last = 0
  return (pin, ms, cb) => {
    const now = Number (new Date)
    if (now > last + ms)
      cb (pin)
    last = now
  }
}) ()

const start = () => {
  const mock = ({ isPiWithGpio, }) => !isPiWithGpio ()
  const knal = initKnal ({ mock, mockNoisy: true, })
  if (knal.isMock ()) info (
    `this doesn't look like a Raspberry with GPIO pins -- mocking all calls to knal`,
  )
  const knalGpio = knal.initGpio ()
  const knalCleanup = () => {
    knalGpio.cleanup ()
    knal.cleanup ()
  }
  return { knalCleanup, knalGpio, }
}

const countdown = (shouldAbort, total, timeout, done) => {
  let cur = 0
  let job
  return job = timeout | setIntervalOn (() => {
    if (shouldAbort ()) {
      clearInterval (job)
      process.stdout.write ('\r     \r')
      return
    }
    cur += timeout
    if (cur >= total) {
      clearTimeout (job)
      return done ()
    }
    process.stdout.write (sprintf1 ('\r%.1f ', 1/1000 * (total - cur)))
  })
}

const blink = (pin, ms) => ms | setIntervalOn (() => {
  knalGpio.toggle (pin)
})

const shouldAbort = () => {
  try {
    return knalGpio.read (config.pinBtn)
  } catch (e) {
    warn (decorateRejection ("Error reading button state, aborting countdown: ", e))
    return true
  }
}

const doDown = invoke (() => {
  let job
  return () => {
    // -- just in case it didn't get cleared
    clearTimeout (job)
    job = countdown (shouldAbort, config.buttonHoldMs, 50, shutdown)
  }
})

const shutdown = () => {
  log ('')
  info ((config.blinkBeforeShutdownMs / 1000) | sprintf1 (
    `shutting down in %.1f seconds`,
  ))

  disableButton ()
  blink (config.pinLed, 200)

  config.blinkBeforeShutdownMs | setTimeoutOn (() => {
    cmds_shutdown | each (
      ([cmd, args]) => spawnSync (cmd, args, true),
    )
  })
}

const exit = () => {
  info ('exiting')
  knalCleanup ()
  process.exit (0)
}

const { knalCleanup, knalGpio, } = start ()

knalGpio.setModeInput (config.pinBtn, 'up')
knalGpio.setModeOutput (config.pinLed, false)

knalGpio.onFalling (config.pinBtn, () => {
  if (eventsDisabled) return
  if (buttonDisabled ()) return
  // --- not strictly necessary to debounce since we're only interested in
  // the very first event, but can't hurt.
  debounce (config.pinBtn, config.debounceMs, (() => doDown ()))
})

const dry = config.dryRun ? '[dry-run] ' : ''
info (dry + 'ready, listening on pin', yellow (String (config.pinBtn)))

process.on ('SIGINT', () => exit ())
process.on ('SIGTERM', () => exit ())
